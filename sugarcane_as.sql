/* Sugarcane smut AS project */
/* Sugarcane alternative splicing project at Dr Mandadi Lab */
/* check sugarcane_as_2.sh file for getting AS from RNA-seq data */
/* how to run 
   module load PostgreSQL/9.6.1-GCCcore-6.3.0-Python-2.7.12-bare
   pg_ctl -o "-p 9999" -D /scratch/user/ren_net/Test/postresql/test.db -l /scratch/user/ren_net/Test/postresql/test.log start
   psql -p 9999 -f sugarcane_as.sql sugarcane_as
   psql -p 9999 sugarcane_as
*/   


/* ^^^^^^^^^^^^^^^^^^^^ 
-- import sorghum gff file code 00
\! echo "Importing Sbicolor GFF annotation"
drop table if exists sbicolor_gff, sbicolor_anot;
create table sbicolor_gff(chr varchar(30), des  varchar(30), feature  varchar(20), st int, ende int,
	score  varchar(5), strand  varchar(5), frame  varchar(5), attr  varchar(150));
\copy sbicolor_gff from '/scratch/user/ren_net/project/sugarcane_as/data_obt/genome/Sbicolor/v3.1/annotation/Sbicolor_313_v3.1.gene.gff3';
alter table sbicolor_gff add trnid varchar(30), add geneid varchar(30), add isf_ct int;
update sbicolor_gff set trnid=
	(regexp_split_to_array((regexp_split_to_array(attr, 'Name='))[2], ';'))[1] where feature='mRNA';
update sbicolor_gff set geneid=
	(regexp_split_to_array((regexp_split_to_array(attr, 'Name='))[2], ';'))[1] where feature='gene';
-- add isf count
alter table sbicolor_gff add temp varchar(30);
update sbicolor_gff set temp=substring(trnid from 1 for 16);
drop table if exists temp;
create table temp as select temp, count(trnid) from sbicolor_gff group by temp;
update sbicolor_gff as a set isf_ct=b.count from temp as b where substring(a.trnid from 1 for 16)=
	b.temp;
-- import sbicolor annotation
create table sbicolor_anot(pac_id varchar(10), locus varchar(20), trn varchar(20), pep varchar(30),
	pfam varchar(80), panther varchar(40), kog varchar(40), kegg_ec varchar(60), ko varchar(20), 
	go_id varchar(200), at_hit varchar(20), at_symb varchar(80), at_anot varchar(300), os_hit 
	varchar(30), os_symb varchar(20), os_anot varchar(200)); 
copy sbicolor_anot from program 'tail -n +2 /scratch/user/ren_net/project/sugarcane_as/data_obt/genome/Sbicolor/v3.1/annotation/Sbicolor_313_v3.1.annotation_info.txt';
-- finish code 00
 ^^^^^^^^^^^^^^^^^^^^ */
 
/* ^^^^^^^^^^^^^^^^^^^^  
-- classifying AS events from gtf file  code 01
create or replace function public.classify_as(as_table varchar(30), as_table_path text)
    RETURNS void as
    $body$
	DECLARE
		res int;
    BEGIN
        RAISE NOTICE 'Performing AS analysis';
        EXECUTE format('drop table if exists %I', as_table);
        EXECUTE format('create table %I (chr varchar(10), src varchar(20), feature varchar(10), 
						st int, ende int, scr varchar(5), str varchar(5), frm varchar(5), 
						attr varchar(8100))', as_table);
        EXECUTE format('COPY %I from $$' || as_table_path || '$$', as_table);
        EXECUTE format('alter table %I add as_type varchar(2250), add as_name varchar(30), add 
			sb_trn varchar(20)', as_table);
        EXECUTE format('update %I set as_type=(regexp_split_to_array(attr, ''"''))[8]', as_table);
		EXECUTE format('update %I set as_name=''ES'' where as_type=''1-2^,0'' or as_type=''0,1-2^'' ',
			as_table);
		EXECUTE format('update %I set as_name=''IR'' where as_type=''0,1^2-'' or as_type=''1^2-,0'' ',
			as_table);	
		EXECUTE format('update %I set as_name=''AD'' where as_type=''1^,2^'' ',	as_table);	
		EXECUTE format('update %I set as_name=''AA'' where as_type=''1-,2-'' ',	as_table);	
		EXECUTE format('update %I as a set sb_trn=b.trnid from sbicolor_gff as b where a.chr=b.chr 
			and a.st>=b.st and a.ende<=b.ende and b.feature=''mRNA'' ', as_table);
		EXECUTE format('select count(*) from %I where  as_name=''ES''; ',	as_table) into res;	
		RAISE NOTICE 'ES for %, %', as_table, res;
		EXECUTE format('select count(*) from %I where  as_name=''IR''; ',	as_table) into res;
		RAISE NOTICE 'IR for %, %', as_table, res;
		EXECUTE format('select count(*) from %I where  as_name=''AA''; ',	as_table) into res;
		RAISE NOTICE 'AA for %, %', as_table, res;
		EXECUTE format('select count(*) from %I where  as_name=''AD''; ',	as_table) into res;
		RAISE NOTICE 'AD for %, %', as_table, res;
		EXECUTE format('select count(*) from %I',	as_table) into res;
		RAISE NOTICE 'Total AS Event %, %', as_table, res;
    END
    $body$ language "plpgsql";

\set pathdir $$'/scratch/user/ren_net/project/sugarcane_as/analysis/astalavista/'$$
select classify_as('d200_c_as',
                        :pathdir || '200dai_control_merged_sorted.gtf_astalavista.gtf');
select classify_as('d200_s_as',
                        :pathdir || '200dai_stress_merged_sorted.gtf_astalavista.gtf');
select classify_as('d5_c_as',
                        :pathdir || '5dai_control_merged_sorted.gtf_astalavista.gtf');
select classify_as('d5_s_as',
                        :pathdir || '5dai_stress_merged_sorted.gtf_astalavista.gtf');
-- finish code 01
 ^^^^^^^^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^^^^^^^^ 
--  venn diragram for AS Events in S bicolor mapped sequences code 02
create or replace function public.venn_as(venn_table varchar(30), as_table_c varchar(10),  
	as_table_s varchar(10), as_event varchar(5))
	RETURNS void as
    $body$
    BEGIN
		EXECUTE format('drop table if exists %I', venn_table);
		EXECUTE format('create table %I as (select chr, st, ende, as_type, as_name from %I 
			where as_name=''%I'' union select chr, st, ende, as_type, as_name from %I where 
			as_name=''%I'') ', venn_table, as_table_c, as_event, as_table_s, as_event);
		EXECUTE format('alter table %I add sb_trn varchar(20), add cont int, add stres int, add 
			comn int', venn_table);	
		EXECUTE format('update %I as a set sb_trn=b.sb_trn from %I as b where a.chr=b.chr and 
			a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name', 
			venn_table, as_table_c);	
		EXECUTE format('update %I as a set sb_trn=b.sb_trn from %I as b where a.chr=b.chr and 
			a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name and 
			a.sb_trn is null', venn_table, as_table_s);
		EXECUTE format('update %I as a set cont=1 from %I as b where a.chr=b.chr and a.st=b.st and 
			a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name', venn_table, as_table_c);		
		EXECUTE format('update %I as a set cont=0 where cont is null', venn_table);
		EXECUTE format('update %I as a set stres=1 from %I as b where a.chr=b.chr and a.st=b.st and 
			a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name', venn_table, as_table_s);
		EXECUTE format('update %I as a set stres=0 where stres is null', venn_table);
		EXECUTE format('update %I set comn= case when cont=1 and stres=1 then 1 else 0 end', 
			venn_table);
		
	END
    $body$ language "plpgsql";
\! echo "venn diragram for AS Events in S bicolor mapped sequences"	
select venn_as('venn_es_d5_as', 'd5_c_as', 'd5_s_as', 'ES');
select venn_as('venn_ir_d5_as', 'd5_c_as', 'd5_s_as', 'IR');
select venn_as('venn_aa_d5_as', 'd5_c_as', 'd5_s_as', 'AA');
select venn_as('venn_ad_d5_as', 'd5_c_as', 'd5_s_as', 'AD');
select venn_as('venn_es_d200_as', 'd200_c_as', 'd200_s_as', 'ES');
select venn_as('venn_ir_d200_as', 'd200_c_as', 'd200_s_as', 'IR');
select venn_as('venn_aa_d200_as', 'd200_c_as', 'd200_s_as', 'AA');
select venn_as('venn_ad_d200_as', 'd200_c_as', 'd200_s_as', 'AD');
-- finish code 02
 ^^^^^^^^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^^^^^^^^ */
-- Gene expression analysis in psql using output data from cuffdiff pipeline
-- Sorghum bicolor and sugaracane smut project for alternative splicing analysis
-- Expression analysis with S. bicolor 
-- code 03
create or replace function public.deg_ana(deg_table varchar(30), deg_table_path text, degtype 
	varchar(10), isf_fpkm_track varchar(30), isf_fpkm_track_table_path text, gene_up varchar(30), 
	gene_down varchar(30), timep int, as_table varchar(10))
    RETURNS void as
    $body$
	DECLARE
		res int;
    BEGIN
		RAISE NOTICE 'Performing DEG analysis';
        EXECUTE format('drop table if exists %I', deg_table);
        EXECUTE format('create table %I (test_id varchar(20), gene_id varchar(12), gene varchar(100), 
						locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7),
						value_1 double precision, value_2 double precision, log2fc varchar(18), 
						test_stat varchar(20), p_value double precision, q_value double precision,
						signi varchar(5) )', deg_table);
		EXECUTE format('COPY %I from $$' || deg_table_path || '$$ with (FORMAT csv, header true) ', 
			deg_table);			
		-- change the inf to Infinity; This is becuase float values accepts Infinity in psql
		EXECUTE format('update %I set log2fc=''Infinity'' where log2fc=''inf'' ', deg_table);	
		EXECUTE format('update %I set log2fc=''-Infinity'' where log2fc=''-inf'' ', deg_table);
		-- convert log2fc from varchar to double
		EXECUTE format('alter table %I ALTER COLUMN log2fc type double precision using 
			(log2fc::double precision)', deg_table);
		-- Add annotation column to genes
		EXECUTE format('alter table %I ADD anot varchar(500), add stres_uniq varchar(5), add as_eve 
			varchar(10), add chr varchar(10), add st varchar(10), add ende varchar(10)', deg_table);	
		EXECUTE format('UPDATE %I as a set anot=b.os_anot from sbicolor_anot as b where 
			a.gene=b.locus', deg_table);	
		EXECUTE format('UPDATE %I set chr=(regexp_split_to_array(locus, '':''))[1]', deg_table);	
		EXECUTE format('UPDATE %I set 
			st=(regexp_split_to_array((regexp_split_to_array(locus, '':''))[2], ''-''))[1]', 
			deg_table);	
		EXECUTE format('UPDATE %I set 
			ende=(regexp_split_to_array((regexp_split_to_array(locus, '':''))[2], ''-''))[2]', 
			deg_table);
		EXECUTE format('alter table %I ALTER COLUMN st type int using (st::int)', deg_table);
		EXECUTE format('alter table %I ALTER COLUMN ende type int using (ende::int)', deg_table);
		EXECUTE format('alter table %I add sb_trn varchar(20), add trin_trn varchar(30)', deg_table);
		-- +1 is necessary in st pos becuase cuffdiff locus starts with +1 pos of that gff pos
		EXECUTE format('update %I as a set sb_trn=b.trnid from sbicolor_gff as b where a.chr=b.chr and 
			a.st+1>=b.st and a.ende<=b.ende and b.feature=''mRNA'' ', deg_table);
		EXECUTE format('update %I as a set trin_trn=b.tran_id  from trinity_tran_anot as b where 
			a.sb_trn=b.sid and b.sid~''Sobic'' ', deg_table);
		-- 	 check for stress unique
		if timep=5 then
			EXECUTE format('UPDATE %I as a set stres_uniq=''yes'' from venn_es_d5_as as b 
				where a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table); 
			EXECUTE format('UPDATE %I as a set stres_uniq=''yes'' from venn_ir_d5_as as b 
				where a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table); 	
			EXECUTE format('UPDATE %I as a set stres_uniq=''yes'' from venn_aa_d5_as as b 
				where a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);	
			EXECUTE format('UPDATE %I as a set stres_uniq=''yes'' from venn_ad_d5_as as b 
				where a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);
		elsif timep=200 then 
			EXECUTE format('UPDATE %I as a set stres_uniq=''yes'' from venn_es_d200_as as b 
				where a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table); 
			EXECUTE format('UPDATE %I as a set stres_uniq=''yes'' from venn_ir_d200_as as b 
				where a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table); 	
			EXECUTE format('UPDATE %I as a set stres_uniq=''yes'' from venn_aa_d200_as as b 
				where a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);	
			EXECUTE format('UPDATE %I as a set stres_uniq=''yes'' from venn_ad_d200_as as b 
				where a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);
		end if;
		--  add as events
		EXECUTE format('UPDATE %I as a set as_eve=b.as_name from %I as b where a.sb_trn=b.sb_trn', 
			deg_table, as_table);
		-- add information from isoform tracking fpkm file such as class code
		if degtype='isf' then
			EXECUTE format('drop table if exists %I', isf_fpkm_track);	
			EXECUTE format('create table %I (tracking_id varchar(20), class_code varchar(5), 
				nearest_ref_id varchar(30), gene_id varchar(24), gene_short_name varchar(30), tss_id 
				varchar(20), locus varchar(30), leng int, coverage varchar(10), q1_FPKM double precision,
				q1_conf_lo double precision, q1_conf_hi double precision, q1_status varchar(5), q2_FPKM 
				double precision, q2_conf_lo double precision, q2_conf_hi double precision, q2_status 
				varchar(5) )',  isf_fpkm_track);
			EXECUTE format('COPY %I from $$' || isf_fpkm_track_table_path || '$$ with (FORMAT csv, 
				header true) ', isf_fpkm_track);				
			-- add isoform class code to isoform expression table
			EXECUTE format('alter table %I add class_code varchar(5), add isf_ct int', deg_table);
			EXECUTE format('update %I as a set class_code=b.class_code from %I as b 
					where a.test_id=b.tracking_id', deg_table, isf_fpkm_track);
			EXECUTE format('update %I as a set isf_ct=b.isf_ct from sbicolor_gff as b where 
				a.gene=substring(b.trnid from 1 for 16);', deg_table);
		end if;
		-- create a table for up and down-regulated genes; criteria log2fc>=2 and q_value <0.05 
		-- (or variable)
		EXECUTE format('drop table if exists %I, %I', gene_up, gene_down);	
		EXECUTE format('create table %I as select * from %I where log2fc>=1 and p_value<=0.05 and 
			value_1>=2 and value_2>=2', gene_up, deg_table);
		EXECUTE format('create table %I as select * from %I where log2fc<=-1 and p_value<=0.05 and 
			value_1>=2 and value_2>=2 ', gene_down, deg_table);		
		-- find as events in up-regulated genes in 200 sample
		if degtype='isf' then
			EXECUTE format('select count(gene) from %I where  as_eve=''ES''; ',gene_up) into res;
			RAISE NOTICE 'ES for %, %', gene_up, res;
			EXECUTE format('select count(gene) from %I where  as_eve=''IR''; ',gene_up) into res;
			RAISE NOTICE 'IR for %, %', gene_up, res;
			EXECUTE format('select count(gene) from %I where  as_eve=''AA''; ',gene_up) into res;
			RAISE NOTICE 'AA for %, %', gene_up, res;
			EXECUTE format('select count(gene) from %I where  as_eve=''AD''; ',gene_up) into res;
			RAISE NOTICE 'AD for %, %', gene_up, res;
		end if;				
	END
    $body$ language "plpgsql";
\set pathdir5 $$'/scratch/user/ren_net/project/sugarcane_as/analysis/5dai_gene_exp_sbicolor/gene_diff_out/cuffdiff_out/'$$
\set pathdir200 $$'/scratch/user/ren_net/project/sugarcane_as/analysis/200dai_gene_exp_sbicolor/gene_diff_out/cuffdiff_out/'$$

select deg_ana('d5_gene_deg', :pathdir5 || 'gene_exp.csv', 'gene', 'NULL',  'NULL', 'd5_gene_up', 
	'd5_gene_down', 5, 'd5_s_as' );
select deg_ana('d5_isf_deg', :pathdir5 || 'isoform_exp.csv', 'isf', 'd5_isf_fpkm_track', 
	:pathdir5 || 'isoforms.fpkm_tracking.csv', 'd5_isf_up', 'd5_isf_down', 5, 'd5_s_as');	
select deg_ana('d200_gene_deg', :pathdir200 || 'gene_exp.csv', 'gene', 'NULL', 'NULL', 
	'd200_gene_up', 'd200_gene_down', 200, 'd200_s_as');
select deg_ana('d200_isf_deg', :pathdir200 || 'isoform_exp.csv', 'isf', 'd200_isf_fpkm_track', 
	:pathdir200 || 'isoforms.fpkm_tracking.csv', 'd200_isf_up', 'd200_isf_down', 200, 'd200_s_as');	
	
	
-- add Atha, monoploid, and S spont gene IDs
create or replace function public.add_ids(deg_table varchar(30), atha_path text, mploid_path text, 
	sspon_path text)
    RETURNS void as
    $body$
	BEGIN
		EXECUTE format('alter table %I add atha varchar(20), add mploid varchar(100), add sspon 
			varchar(100)', deg_table);
		EXECUTE format('drop table if exists temp, temp1, temp2');	
		EXECUTE format('create table temp(qid varchar(30), qlen int, sid varchar(100), slen int, 
			qstart int, qend int, sstart int, send int, nident int, pident double precision, alen 
			int, mm int, gp int, qcov double precision, eval double precision, bs double precision)');
		EXECUTE format('create table temp1 (like temp)');
		EXECUTE format('create table temp2 (like temp)');
		EXECUTE format('COPY temp from $$' || atha_path || '$$ with (FORMAT csv, header true)');
		EXECUTE format('COPY temp1 from $$' || mploid_path || '$$ with (FORMAT csv, header true)');	
		EXECUTE format('COPY temp2 from $$' || sspon_path || '$$ with (FORMAT csv, header true)');
		EXECUTE format('update %I as a set atha=b.sid from temp as b where 
			a.gene=substring(b.qid from 1 for 16)', deg_table);	
		EXECUTE format('update %I as a set mploid=b.sid from temp1 as b where 
			a.gene=substring(b.qid from 1 for 16)', deg_table);	
		EXECUTE format('update %I as a set sspon=b.sid from temp2 as b where 
			a.gene=substring(b.qid from 1 for 16)', deg_table);		
	END
    $body$ language "plpgsql";

\set atha $$'/scratch/user/ren_net/project/sugarcane_as/data_obt/genome/Sbicolor/v3.1/annotation/'$$	
select add_ids('d200_isf_up', :atha || 'blastx_sbio_atha.csv', :atha || 'blastx_sbio_monploid.csv',
	:atha || 'blastx_sbio_sspont.csv');	
select add_ids('d200_isf_down', :atha || 'blastx_sbio_atha.csv', :atha || 'blastx_sbio_monploid.csv',
	:atha || 'blastx_sbio_sspont.csv');	
select add_ids('d5_isf_up', :atha || 'blastx_sbio_atha.csv', :atha || 'blastx_sbio_monploid.csv',
	:atha || 'blastx_sbio_sspont.csv');	
select add_ids('d5_isf_down', :atha || 'blastx_sbio_atha.csv', :atha || 'blastx_sbio_monploid.csv',
	:atha || 'blastx_sbio_sspont.csv');							
	
\copy d200_isf_up to '/scratch/user/ren_net/project/sugarcane_as/analysis/200dai_gene_exp_sbicolor/gene_diff_out/cuffdiff_out/d200_isf_up.csv' DELIMITER ',' CSV HEADER;
\copy d5_isf_up to '/scratch/user/ren_net/project/sugarcane_as/analysis/5dai_gene_exp_sbicolor/gene_diff_out/cuffdiff_out/d5_isf_up.csv' DELIMITER ',' CSV HEADER;	
\copy d200_isf_down to '/scratch/user/ren_net/project/sugarcane_as/analysis/200dai_gene_exp_sbicolor/gene_diff_out/cuffdiff_out/d200_isf_down.csv' DELIMITER ',' CSV HEADER;
\copy d5_isf_down to '/scratch/user/ren_net/project/sugarcane_as/analysis/5dai_gene_exp_sbicolor/gene_diff_out/cuffdiff_out/d5_isf_down.csv' DELIMITER ',' CSV HEADER;	
-- finish code 03
/*	^^^^^^^^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^^^^^^^^ 
-- Trinity Transcript Annotation Analysis
-- sugarcane smut trinity blast analysis with S. bicolor 
-- code 04
drop table if exists trinity_tran_anot;
create table trinity_tran_anot(tran_id varchar(30));
\copy trinity_tran_anot from '/scratch/user/ren_net/project/sugarcane_as/analysis/comp_assembly/trinity_id.txt';
-- count the isoform for each gene
alter table trinity_tran_anot add gene varchar(30);
update trinity_tran_anot set gene=(regexp_split_to_array(tran_id, '_i'))[1];
create table temp as select gene, count(gene) from trinity_tran_anot group by gene;
alter table trinity_tran_anot add isf_ct int;
update trinity_tran_anot as a set isf_ct=b.count from temp as b where a.gene=b.gene;
drop table temp;
drop table if exists blastn_trin_sbicolor;
create table blastn_trin_sbicolor(qid varchar(30), qlen int, sid varchar(20), slen int, qstart int,
	qend int, sstart int, send int, nident int, pident double precision, alen int, mm int, gp int,
	qcov double precision, eval double precision, bs double precision);
\copy blastn_trin_sbicolor from '/scratch/user/ren_net/project/sugarcane_as/analysis/trinity_transcript_blast/blastn_sctrinitrans_sbtrans.txt';
-- delete duplicate rows based on one column
drop table if exists temp;
create table temp as select distinct qid from blastn_trin_sbicolor;
alter table temp add sid varchar(20);
update temp as a set sid=b.sid from blastn_trin_sbicolor as b where a.qid=b.qid;
drop table blastn_trin_sbicolor;
alter table temp rename to blastn_trin_sbicolor;
alter table blastn_trin_sbicolor add anot varchar(300);
update blastn_trin_sbicolor as a set anot=b.os_anot from sbicolor_anot as b where substring(a.sid from 
	1 for 16)=b.locus;
update blastn_trin_sbicolor as a set anot=b.os_anot from sbicolor_anot as b where substring(a.sid from 
	1 for 13)=b.locus and a.anot is null;
alter table trinity_tran_anot add sid varchar(40), add anot varchar(300);
update trinity_tran_anot as a set sid=b.sid,anot=b.anot from blastn_trin_sbicolor as b where 
	a.tran_id=b.qid and b.anot!='';
-- annotation with ncbi nr/nt for unmapped ids with S. bicolor
drop table if exists temp;
create table temp (qid varchar(30), sid varchar(40), anot varchar(300), sp varchar(50));	
\copy temp from '/scratch/user/ren_net/project/sugarcane_as/analysis/trinity_transcript_blast/blastx_sctrintran_sbtran_unanot_ncbi_nr_plant_anot.txt';
update trinity_tran_anot as a set sid=b.sid,anot=b.anot from temp as b where a.tran_id=b.qid and 
	a.anot is null;
drop table temp;
-- \copy (select tran_id from trinity_tran_anot where anot is null) to 'R:\project\sugarcane_as\hpc_analysis\trinity_transcript_blast\blastnx_sctrinitrans_sbtrans_ncbi_unanot.txt';
-- finish code 04
 ^^^^^^^^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^^^^^^^^ 
-- Expression analysis with Trinity Transcripts
-- code 05
create or replace function public.trin_deg_ana(deg_table varchar(30), deg_table_path text, timep 
	int, deg_table_up varchar(10), deg_table_down varchar(10), as_table varchar(10) )
	RETURNS void as
    $body$
	BEGIN
		RAISE NOTICE 'Expression analysis with Trinity Transcripts';
        EXECUTE format('drop table if exists %I', deg_table);
        EXECUTE format('create table %I(trn varchar(30), log2fc double precision, logcpm double 
			precision, p_value double precision, q_value double precision)', deg_table);
		EXECUTE format('COPY %I from $$' || deg_table_path || '$$ with (FORMAT csv, header true) ', 
			deg_table);	
		EXECUTE format('alter table %I add anot varchar(300), add sb_trn varchar(40), add isf_ct 
			int, add stres_uniq varchar(5)', deg_table);
		EXECUTE format('update %I as a set anot=b.anot,sb_trn=b.sid, isf_ct=b.isf_ct from 
			trinity_tran_anot as b where a.trn=b.tran_id', deg_table);
		if timep=5 then
			EXECUTE format('update %I as a set stres_uniq=''yes'' from	venn_es_d5_as as b where 
				a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);	
			EXECUTE format('update %I as a set stres_uniq=''yes'' from	venn_ir_d5_as as b where 
				a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);
			EXECUTE format('update %I as a set stres_uniq=''yes'' from	venn_aa_d5_as as b where 
				a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);	
			EXECUTE format('update %I as a set stres_uniq=''yes'' from	venn_ad_d5_as as b where 
				a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);
		elsif timep=200 then
			EXECUTE format('update %I as a set stres_uniq=''yes'' from	venn_es_d200_as as b where 
				a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);	
			EXECUTE format('update %I as a set stres_uniq=''yes'' from	venn_ir_d200_as as b where 
				a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);
			EXECUTE format('update %I as a set stres_uniq=''yes'' from	venn_aa_d200_as as b where 
				a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);	
			EXECUTE format('update %I as a set stres_uniq=''yes'' from	venn_ad_d200_as as b where 
				a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0', deg_table);
		end if;
		EXECUTE format('drop table if exists %I', deg_table_up, deg_table_down);
		EXECUTE format('create table %I as select * from %I where log2fc>=2 and q_value<0.05', 
			deg_table_up, deg_table);	
		EXECUTE format('create table %I as select * from %I where log2fc<=-2 and q_value<0.05', 
			deg_table_down, deg_table);	
		-- to check if there is sb_trn upregulated or not from expression analysis with S. bicolor
		EXECUTE format('alter table %I add sb_trn_bool varchar(10),  add temp varchar(40), add 
			as_eve_fm_sb varchar(10)', deg_table_up);	
		EXECUTE format('alter table %I add sb_trn_bool varchar(10),  add temp varchar(40), add 
			as_eve_fm_sb varchar(10)', deg_table_down);	
		EXECUTE format('update %I set temp=((regexp_split_to_array(sb_trn,''\.''))[1] || ''.'' || 
			(regexp_split_to_array(sb_trn,''\.''))[2])', deg_table_up);	
		EXECUTE format('update %I set temp=((regexp_split_to_array(sb_trn,''\.''))[1] || ''.'' || 
			(regexp_split_to_array(sb_trn,''\.''))[2])', deg_table_down);	
		/*
		EXECUTE format('update %I as a set sb_trn_bool=''yes'' from dai_5_isf_up as b where 
			a.temp=b.gene', deg_table_up);
		EXECUTE format('update %I as a set sb_trn_bool=''yes'' from dai_5_isf_down as b where 
			a.temp=b.gene', deg_table_down);
		EXECUTE format('update %I as a set sb_trn_bool=''yes'' from dai_5_isf_up as b where 
			a.temp=b.gene', deg_table_up);
		EXECUTE format('update %I as a set sb_trn_bool=''yes'' from dai_5_isf_down as b where 
			a.temp=b.gene', deg_table_down);		
		*/
		EXECUTE format('update %I as a set as_eve_fm_sb=b.as_name from %I as b where 
			a.sb_trn=b.sb_trn', deg_table_up, as_table );	
			
	END
    $body$ language "plpgsql";
\set pathdir $$'/scratch/user/ren_net/project/sugarcane_as/analysis/abundance_est/isoform_matrix/'$$
select trin_deg_ana('d5_gene_deg_trin', 
	:pathdir || '5d_deg/5d.counts.matrix.conditionA_vs_conditionB.edgeR.DE_results.csv', '5', 
	'd5_gene_deg_trin_up', 'd5_gene_deg_trin_down', 'd5_s_as');
select trin_deg_ana('d200_gene_deg_trin', 
	:pathdir || '200d_deg/200d.counts.matrix.conditionA_vs_conditionB.edgeR.DE_results.csv', '200', 
	'd200_gene_deg_trin_up', 'd200_gene_deg_trin_down', 'd200_s_as');	
-- finish code 05
 ^^^^^^^^^^^^^^^^^^^^ */
 
 /* ^^^^^^^^^^^^^^^^^^^^ 
 -- Htseq and Circos analysis start
 -- code 06
 create or replace function public.htseq_ct(htseq_path_1 text, htseq_path_2 text, htseq_path_3 text,  
	table_hseqct varchar(10) )
	RETURNS void as
    $body$
	BEGIN
		RAISE NOTICE 'Htseq and Circos analysis start';
        EXECUTE format('drop table if exists temp1, temp2, temp3');
        EXECUTE format('create table temp1(gene varchar(30), ct int)');
		EXECUTE format('create table temp2 (like temp1)');
		EXECUTE format('create table temp3 (like temp1)');
		EXECUTE format('COPY temp1 from $$' || htseq_path_1 || '$$ ');	
		EXECUTE format('COPY temp2 from $$' || htseq_path_2 || '$$ ');	
		EXECUTE format('COPY temp3 from $$' || htseq_path_3 || '$$ ');		
		EXECUTE format('drop table if exists %I', table_hseqct);	
		EXECUTE format('create table %I (like temp1)', table_hseqct);
		EXECUTE format('insert into %I select gene from temp1 union select gene from temp2 union 
			select gene from temp3', table_hseqct);
		EXECUTE format('alter table %I add chr varchar(10), add id varchar(30),add st int, add ende 
			int, add r1_ct int, add r2_ct int, add r3_ct int, add r_sum int, add log10_r_sum double 
			precision', table_hseqct);
		EXECUTE format('update %I set id=(regexp_split_to_array(gene,''.v3''))[1]', table_hseqct);
		EXECUTE format('update %I as a set chr=b.chr, st=b.st, ende=b.ende from sbicolor_gff as b 
			where a.id=b.geneid and b.feature=''gene''', table_hseqct);
		EXECUTE format('update %I as a set r1_ct=b.ct from temp1 as b where a.gene=b.gene', 
			table_hseqct);
		EXECUTE format('update %I as a set r2_ct=b.ct from temp2 as b where a.gene=b.gene', 
			table_hseqct); 		
		EXECUTE format('update %I as a set r3_ct=b.ct from temp3 as b where a.gene=b.gene', 
			table_hseqct);
		EXECUTE format('update %I set r_sum=r1_ct+r2_ct+r3_ct', table_hseqct);
		EXECUTE format('update %I set log10_r_sum=log(r_sum) where r_sum!=0', table_hseqct);
		EXECUTE format('update %I set log10_r_sum=0 where r_sum=0', table_hseqct); 	
			
	END
    $body$ language "plpgsql";
	
 \set pathdir $$'/scratch/user/ren_net/project/sugarcane_as/analysis/htseq_count/'$$
 select htseq_ct(:pathdir || '5c_rep1_1.count', :pathdir || '5c_rep2_1.count', :pathdir || 
	'5c_rep3_1.count', 'd5_c_hseqct');
select htseq_ct(:pathdir || '5s_rep1_1.count', :pathdir || '5s_rep2_1.count', :pathdir || 
	'5s_rep3_1.count', 'd5_s_hseqct');
select htseq_ct(:pathdir || '200c_rep1_1.count', :pathdir || '200c_rep2_1.count', :pathdir || 
	'200c_rep3_1.count', 'd200_c_hseqct');	
select htseq_ct(:pathdir || '200s_rep1_1.count', :pathdir || '200s_rep2_1.count', :pathdir || 
	'200s_rep3_1.count', 'd200_s_hseqct');	
-- finish code 06	
 ^^^^^^^^^^^^^^^^^^^^ */	

 /*
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Expression analysis with S. bicolor';
END
$$;
Drop table if exists dai_5_gene; 
	CREATE TABLE dai_5_gene 
		(test_id varchar(20), 
		gene_id varchar(12), 
		gene varchar(100), 
		locus varchar(24), 
		sample_1 varchar(9), 
		sample_2 varchar(9), 
		status varchar(7), 
		value_1 double precision, 
		value_2 double precision, 
		log2fc varchar(18), 	
		test_stat varchar(20), 
		p_value double precision, 
		q_value double precision,  
		signi varchar(5) );

Drop table if exists dai_200_gene; 
	CREATE TABLE dai_200_gene 
		(test_id varchar(20), 
		gene_id varchar(12), 
		gene varchar(100), 
		locus varchar(24), 
		sample_1 varchar(9), 
		sample_2 varchar(9), 
		status varchar(7), 
		value_1 double precision, 
		value_2 double precision, 
		log2fc varchar(18), 
		test_stat varchar(20), 
		p_value double precision, 
		q_value double precision,  
		signi varchar(5) );

Drop table if exists dai_5_isf; 
	CREATE TABLE dai_5_isf 
	(test_id varchar(20), 
	gene_id varchar(12), 
	gene varchar(100), 
	locus varchar(24), 
	sample_1 varchar(9), 
	sample_2 varchar(9), 
	status varchar(7), 
	value_1 double precision, 
	value_2 double precision, 
	log2fc varchar(18), 
	test_stat varchar(20), 
	p_value double precision, 
	q_value double precision,  
	signi varchar(5) );
	
Drop table if exists dai_200_isf; 
	CREATE TABLE dai_200_isf 
	(test_id varchar(20), 
	gene_id varchar(12), 
	gene varchar(100), 
	locus varchar(24), 
	sample_1 varchar(9), 
	sample_2 varchar(9), 
	status varchar(7), 
	value_1 double precision, 
	value_2 double precision, 
	log2fc varchar(18), 
	test_stat varchar(20), 
	p_value double precision, 
	q_value double precision,  
	signi varchar(5) );

\copy dai_5_isf from 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\cuffdiff_out_sb\isoform_exp.csv' with (FORMAT csv, header true) ;
\copy dai_5_gene from 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\cuffdiff_out_sb\gene_exp.csv' with (FORMAT csv, header true) ;
\copy dai_200_isf from 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\cuffdiff_out_sb\isoform_exp.csv' with (FORMAT csv, header true) ;
\copy dai_200_gene from 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\cuffdiff_out_sb\gene_exp.csv' with (FORMAT csv, header true) ;

/*change the inf to Infinity*/
UPDATE dai_5_gene
	SET log2fc = 'Infinity' 
	where log2fc='inf';
UPDATE dai_5_isf 
	SET log2fc = 'Infinity' 
	where log2fc='inf';
UPDATE dai_200_gene 
	SET log2fc = 'Infinity' 
	where log2fc='inf';
UPDATE dai_200_isf 
	SET log2fc = 'Infinity' 
	where log2fc='inf';

/*change the -inf to -Infinity*/
/*This is becuase float values accepts Infinity in psql*/
UPDATE dai_5_gene SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE dai_5_isf SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE dai_200_gene SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE dai_200_isf SET log2fc = '-Infinity' where log2fc='-inf';

/*convert log2fc from varchar to double*/
ALTER TABLE dai_5_gene ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE dai_5_isf ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE dai_200_gene ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE dai_200_isf ALTER COLUMN log2fc type double precision using (log2fc::double precision);

/*Add annotation column to genes*/
ALTER TABLE dai_5_gene ADD anot varchar(500), add stres_uniq varchar(5), add as_eve varchar(10);
ALTER TABLE dai_5_isf ADD anot varchar(500), add stres_uniq varchar(5), add as_eve varchar(10);
ALTER TABLE dai_200_gene ADD anot varchar(500), add stres_uniq varchar(5), add as_eve varchar(10);
ALTER TABLE dai_200_isf ADD anot varchar(500), add stres_uniq varchar(5), add as_eve varchar(10);

/*join annotation from sorghum_anot database*/
UPDATE dai_5_gene as a set anot=b.anot from sbicolor_anot as b where a.gene=b.locus;
UPDATE dai_5_isf as a set anot=b.anot from sbicolor_anot as b where a.gene=b.locus;
UPDATE dai_200_gene as a set anot=b.anot from sbicolor_anot as b where a.gene=b.locus;
UPDATE dai_200_isf as a set anot=b.anot from sbicolor_anot as b where a.gene=b.locus;

/* add start and end genomic coordinates to get Sb genes and isoform */
alter table dai_5_gene add chr varchar(10), add st varchar(10), add ende varchar(10);
alter table dai_5_isf add chr varchar(10), add st varchar(10), add ende varchar(10);
alter table dai_200_gene add chr varchar(10), add st varchar(10), add ende varchar(10);
alter table dai_200_isf add chr varchar(10), add st varchar(10), add ende varchar(10);

update dai_5_gene set chr=(regexp_split_to_array(locus, ':'))[1];
update dai_5_gene set st=(regexp_split_to_array((regexp_split_to_array(locus, ':'))[2], '-'))[1];
update dai_5_gene set ende=(regexp_split_to_array((regexp_split_to_array(locus, ':'))[2], '-'))[2];
update dai_5_isf set chr=(regexp_split_to_array(locus, ':'))[1];
update dai_5_isf set st=(regexp_split_to_array((regexp_split_to_array(locus, ':'))[2], '-'))[1];
update dai_5_isf set ende=(regexp_split_to_array((regexp_split_to_array(locus, ':'))[2], '-'))[2];
update dai_200_gene set chr=(regexp_split_to_array(locus, ':'))[1];
update dai_200_gene set st=(regexp_split_to_array((regexp_split_to_array(locus, ':'))[2], '-'))[1];
update dai_200_gene set ende=(regexp_split_to_array((regexp_split_to_array(locus, ':'))[2], '-'))[2];
update dai_200_isf set chr=(regexp_split_to_array(locus, ':'))[1];
update dai_200_isf set st=(regexp_split_to_array((regexp_split_to_array(locus, ':'))[2], '-'))[1];
update dai_200_isf set ende=(regexp_split_to_array((regexp_split_to_array(locus, ':'))[2], '-'))[2];

ALTER TABLE dai_5_gene ALTER COLUMN st type int using (st::int);
ALTER TABLE dai_5_gene ALTER COLUMN ende type int using (ende::int);
ALTER TABLE dai_5_isf ALTER COLUMN st type int using (st::int);
ALTER TABLE dai_5_isf ALTER COLUMN ende type int using (ende::int);
ALTER TABLE dai_200_gene ALTER COLUMN st type int using (st::int);
ALTER TABLE dai_200_gene ALTER COLUMN ende type int using (ende::int);
ALTER TABLE dai_200_isf ALTER COLUMN st type int using (st::int);
ALTER TABLE dai_200_isf ALTER COLUMN ende type int using (ende::int);

/* add Sb transcripts based on coordinates */
alter table dai_5_gene add sb_trn varchar(20), add trin_trn varchar(30);
alter table dai_5_isf add sb_trn varchar(20), add trin_trn varchar(30);
alter table dai_200_gene add sb_trn varchar(20), add trin_trn varchar(30);
alter table dai_200_isf add sb_trn varchar(20), add trin_trn varchar(30);

/* here +1 is necessary in st pos becuase cuffdiff locus starts with +1 pos of that gff pos */
update dai_5_gene as a set sb_trn=b.id from sbicolor_gff as b
 where a.chr=b.chr and a.st+1>=b.st and a.ende<=b.ende and b.feature='mRNA';
update dai_5_isf as a set sb_trn=b.id from sbicolor_gff as b
 where a.chr=b.chr and a.st+1>=b.st and a.ende<=b.ende and b.feature='mRNA';
update dai_200_gene as a set sb_trn=b.id from sbicolor_gff as b
 where a.chr=b.chr and a.st+1>=b.st and a.ende<=b.ende and b.feature='mRNA';
update dai_200_isf as a set sb_trn=b.id from sbicolor_gff as b
 where a.chr=b.chr and a.st+1>=b.st and a.ende<=b.ende and b.feature='mRNA';

update dai_5_isf as a set trin_trn=b.tran_id from trinity_tran_anot as b 
	where a.sb_trn=b.sid and b.sid like '%Sobic%';
update dai_200_isf as a set trin_trn=b.tran_id from trinity_tran_anot as b 
	where a.sb_trn=b.sid and b.sid like '%Sobic%';
 
/* check for stress unique */
update dai_5_gene as a set stres_uniq='yes' from venn_es_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_5_gene as a set stres_uniq='yes' from venn_ir_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_5_gene as a set stres_uniq='yes' from venn_aa_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_5_gene as a set stres_uniq='yes' from venn_ad_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;

update dai_5_isf as a set stres_uniq='yes' from venn_es_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_5_isf as a set stres_uniq='yes' from venn_ir_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_5_isf as a set stres_uniq='yes' from venn_aa_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_5_isf as a set stres_uniq='yes' from venn_ad_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;

update dai_200_gene as a set stres_uniq='yes' from venn_es_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_200_gene as a set stres_uniq='yes' from venn_ir_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_200_gene as a set stres_uniq='yes' from venn_aa_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_200_gene as a set stres_uniq='yes' from venn_ad_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;

update dai_200_isf as a set stres_uniq='yes' from venn_es_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_200_isf as a set stres_uniq='yes' from venn_ir_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_200_isf as a set stres_uniq='yes' from venn_aa_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_200_isf as a set stres_uniq='yes' from venn_ad_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;

/* add as events */
update dai_5_gene as a set as_eve=b.as_name from dai5_s_astalavista as b
	where a.sb_trn=b.sb_trn;
update dai_5_isf as a set as_eve=b.as_name from dai5_s_astalavista as b
	where a.sb_trn=b.sb_trn;
update dai_200_gene as a set as_eve=b.as_name from dai200_s_astalavista as b
	where a.sb_trn=b.sb_trn;
update dai_200_isf as a set as_eve=b.as_name from dai200_s_astalavista as b
	where a.sb_trn=b.sb_trn;

/* add information from isoform tracking fpkm file such as class code */
drop table if exists dai_5_isf_fpkm_track;
CREATE TABLE dai_5_isf_fpkm_track 
		(tracking_id varchar(20), 
		class_code varchar(5), 
		nearest_ref_id varchar(30), 
		gene_id varchar(24), 
		gene_short_name varchar(30), 
		tss_id varchar(20), 
		locus varchar(30), 
		leng int, 
		coverage varchar(10), 
		q1_FPKM double precision, 	
		q1_conf_lo double precision, 
		q1_conf_hi double precision, 
		q1_status varchar(5),  
		q2_FPKM double precision, 	
		q2_conf_lo double precision, 
		q2_conf_hi double precision, 
		q2_status varchar(5));  
		
drop table if exists dai_200_isf_fpkm_track;
CREATE TABLE dai_200_isf_fpkm_track 
		(tracking_id varchar(20), 
		class_code varchar(5), 
		nearest_ref_id varchar(30), 
		gene_id varchar(24), 
		gene_short_name varchar(30), 
		tss_id varchar(20), 
		locus varchar(30), 
		leng int, 
		coverage varchar(10), 
		q1_FPKM double precision, 	
		q1_conf_lo double precision, 
		q1_conf_hi double precision, 
		q1_status varchar(5),  
		q2_FPKM double precision, 	
		q2_conf_lo double precision, 
		q2_conf_hi double precision, 
		q2_status varchar(5));  

\copy dai_5_isf_fpkm_track from 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\cuffdiff_out_sb\isoforms.fpkm_tracking.csv' with (FORMAT csv, header true) ;
\copy dai_200_isf_fpkm_track from 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\cuffdiff_out_sb\isoforms.fpkm_tracking.csv' with (FORMAT csv, header true) ;

/* add isoform class code to isoform expression table */
alter table dai_5_isf add class_code varchar(5), add isf_ct int;
alter table dai_200_isf add class_code varchar(5), add isf_ct int;

update dai_5_isf as a set class_code=b.class_code from dai_5_isf_fpkm_track as b
	where a.test_id=b.tracking_id;
update dai_200_isf as a set class_code=b.class_code from dai_200_isf_fpkm_track as b
	where a.test_id=b.tracking_id;
	
update dai_5_isf as a set isf_ct=b.isf_ct from sbicolor_gff as b
	where a.gene=b.prim_trn;
update dai_200_isf as a set isf_ct=b.isf_ct from sbicolor_gff as b
	where a.gene=b.prim_trn; 
	
/* create a table for up and down-regulated genes */
/* criteria log2fc>=2 and q_value <0.05 (or variable) */
drop table if exists dai_5_gene_up;
create table dai_5_gene_up as
	select * from dai_5_gene
	where log2fc>=1 and p_value<=0.05 and value_1>=2 and value_2>=2;

drop table if exists dai_5_gene_down;
create table dai_5_gene_down as
	select * from dai_5_gene
	where log2fc<=-1 and p_value<=0.05 and value_1>=2 and value_2>=2;
	
drop table if exists dai_5_isf_up;
create table dai_5_isf_up as
	select * from dai_5_isf
	where log2fc>=1 and p_value<=0.05 and value_1>=2 and value_2>=2;

drop table if exists dai_5_isf_down;
create table dai_5_isf_down as
	select * from dai_5_isf
	where log2fc<=-1 and p_value<=0.05 and value_1>=2 and value_2>=2;

drop table if exists dai_200_gene_up;
create table dai_200_gene_up as
	select * from dai_200_gene
	where log2fc>=1 and p_value<=0.05 and value_1>=2 and value_2>=2;

drop table if exists dai_200_gene_down;
create table dai_200_gene_down as
	select * from dai_200_gene
	where log2fc<=-1 and p_value<=0.05 and value_1>=2 and value_2>=2;	

drop table if exists dai_200_isf_up;
create table dai_200_isf_up as
	select * from dai_200_isf
	where log2fc>=1 and p_value<=0.05 and value_1>=2 and value_2>=2;

drop table if exists dai_200_isf_down;
create table dai_200_isf_down as
	select * from dai_200_isf
	where log2fc<=-1 and p_value<=0.05 and value_1>=2 and value_2>=2;	
	
\copy dai_5_gene_up to 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\dai_5_gene_up.txt';
\copy dai_5_gene_down to 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\dai_5_gene_down.txt';
\copy dai_5_isf_up to 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\dai_5_isf_up.txt';
\copy dai_5_isf_down to 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\dai_5_isf_down.txt';
\copy dai_200_gene_up to 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\dai_200_gene_up.txt';
\copy dai_200_gene_down to 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\dai_200_gene_down.txt';
\copy dai_200_isf_up to 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\dai_200_isf_up.txt';
\copy dai_200_isf_down to 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\dai_200_isf_down.txt';

/* find as events in up-regulated genes in 200 sample */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'find as events in up-regulated genes in 200 sample';
END
$$;
select count(gene) from dai_200_isf_up where as_eve='ES';
select count(gene) from dai_200_isf_up where as_eve='IR';
select count(gene) from dai_200_isf_up where as_eve='AA';
select count(gene) from dai_200_isf_up where as_eve='AD';

/* *** Expression analysis with S. bicolor *** END */


/* *** Trinity Transcript Annotation Analysis *** START */
/* sugarcane smut trinity blast analysis with S. bicolor */
/* import all trinity ids */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Trinity Transcript Annotation Analysis';
END
$$;
Drop table if exists trinity_tran_anot;
create table trinity_tran_anot
	(tran_id varchar(30)
	);
\copy trinity_tran_anot from 'R:\project\sugarcane_as\hpc_analysis\comp_assembly\trinity_out_dir\trinity_id.txt';

/* count the isoform for each gene */
alter table trinity_tran_anot add gene varchar(30);
update trinity_tran_anot set gene=(regexp_split_to_array(tran_id, '_i'))[1];
create table temp as select gene, count(gene) from trinity_tran_anot group by gene;
alter table trinity_tran_anot add isf_ct int;
update trinity_tran_anot as a set isf_ct=b.count from temp as b where a.gene=b.gene;
drop table temp;

Drop table if exists blastn_trin_sbicolor;
	create table blastn_trin_sbicolor 
	(qid varchar(30),
	qlen int,
	sid varchar(20),
	slen int,
	qstart int,
	qend int,
	sstart int,
	send int, 
	nident int,
	pident double precision,
	alen int,
	mm int,
	gp int,
	qcov double precision,
	eval double precision,
	bs double precision);

\copy blastn_trin_sbicolor from 'R:\project\sugarcane_as\hpc_analysis\trinity_transcript_blast\blastn_sctrinitrans_sbtrans.txt';

/* delete duplicate rows based on one column */
Drop table if exists temp;
	create table temp as select distinct qid from blastn_trin_sbicolor;
alter table temp add sid varchar(20);
update temp as a set sid=b.sid from blastn_trin_sbicolor as b where a.qid=b.qid;
drop table blastn_trin_sbicolor;
alter table temp rename to blastn_trin_sbicolor;
alter table blastn_trin_sbicolor add anot varchar(300);
update blastn_trin_sbicolor as a set anot=b.anot from sbicolor_anot as b 
	where substring(a.sid from 1 for 16)=b.locus;
update blastn_trin_sbicolor as a set anot=b.anot from sbicolor_anot as b 
	where substring(a.sid from 1 for 13)=b.locus and a.anot is null;
	
alter table trinity_tran_anot add sid varchar(40), add anot varchar(300);

update trinity_tran_anot as a set sid=b.sid,anot=b.anot from blastn_trin_sbicolor as b
	where a.tran_id=b.qid and b.anot!='';

/* annotation with ncbi nr/nt for unmapped ids with S. bicolor */
Drop table if exists temp;
	create table temp 
	(qid varchar(30),
	sid varchar(40),
	anot varchar(300),
	sp varchar(50)
	);
\copy temp from 'R:\project\sugarcane_as\hpc_analysis\trinity_transcript_blast\blastx_sctrintran_sbtran_unanot_ncbi_nr_plant_anot.txt';
update trinity_tran_anot as a set sid=b.sid,anot=b.anot from temp as b
	where a.tran_id=b.qid and a.anot is null;
drop table temp;
\copy (select tran_id from trinity_tran_anot where anot is null) to 'R:\project\sugarcane_as\hpc_analysis\trinity_transcript_blast\blastnx_sctrinitrans_sbtrans_ncbi_unanot.txt';

/* *** Trinity Transcript Annotation Analysis *** END */	


/* *** Expression analysis with Trinity Transcripts *** START *************************************/
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Trinity Transcript Expression Analysis';
END
$$;
Drop table if exists dai_5_trin; 
	CREATE TABLE dai_5_trin 
		(trn varchar(30), 
		log2fc double precision,
		logcpm double precision,
		p_value double precision, 
		q_value double precision );
		
Drop table if exists dai_200_trin; 
	CREATE TABLE dai_200_trin 
		(trn varchar(30), each
		log2fc double precision,
		logcpm double precision,
		p_value double precision, 
		q_value double precision );
		
\copy dai_5_trin from 'R:\project\sugarcane_as\hpc_analysis\abundance_est\isoform_matrix\5d_deg\5d.counts.matrix.conditionA_vs_conditionB.edgeR.DE_results.csv' with (FORMAT csv, header true) ;
\copy dai_200_trin from 'R:\project\sugarcane_as\hpc_analysis\abundance_est\isoform_matrix\200d_deg\200d.counts.matrix.conditionA_vs_conditionB.edgeR.DE_results.csv' with (FORMAT csv, header true) ;

alter table dai_5_trin add anot varchar(300), add sb_trn varchar(40), add isf_ct int, 
	add stres_uniq varchar(5);
alter table dai_200_trin add anot varchar(300), add sb_trn varchar(40), add isf_ct int, 
	add stres_uniq varchar(5);

update dai_5_trin as a set anot=b.anot,sb_trn=b.sid, isf_ct=b.isf_ct from trinity_tran_anot as b
	where a.trn=b.tran_id;
update dai_200_trin as a set anot=b.anot,sb_trn=b.sid, isf_ct=b.isf_ct from trinity_tran_anot as b
	where a.trn=b.tran_id;
	
update dai_5_trin as a set stres_uniq='yes' from venn_es_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_5_trin as a set stres_uniq='yes' from venn_ir_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_5_trin as a set stres_uniq='yes' from venn_aa_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_5_trin as a set stres_uniq='yes' from venn_ad_dai5_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
	
update dai_200_trin as a set stres_uniq='yes' from venn_es_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_200_trin as a set stres_uniq='yes' from venn_ir_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_200_trin as a set stres_uniq='yes' from venn_aa_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
update dai_200_trin as a set stres_uniq='yes' from venn_ad_dai200_astalavista as b where 
	a.sb_trn=b.sb_trn and b.stres=1 and b.cont=0;
	
Drop table if exists dai_5_trin_up; 
CREATE TABLE dai_5_trin_up as
	select * from dai_5_trin where log2fc>=2 and q_value<0.05;
Drop table if exists dai_5_trin_down; 
CREATE TABLE dai_5_trin_down as
	select * from dai_5_trin where log2fc<=-2 and q_value<0.05;
Drop table if exists dai_200_trin_up; 
CREATE TABLE dai_200_trin_up as
	select * from dai_200_trin where log2fc>=2 and q_value<0.05;
Drop table if exists dai_200_trin_down; 
CREATE TABLE dai_200_trin_down as
	select * from dai_200_trin where log2fc<=-2 and q_value<0.05;
	
/* to check if there is sb_trn upregulated or not from expression analysis with S. bicolor*/
alter table dai_5_trin_up add sb_trn_bool varchar(10),  add temp varchar(40), add as_eve_fm_sb varchar(10);
alter table dai_200_trin_up add sb_trn_bool varchar(10),  add temp varchar(40), add as_eve_fm_sb varchar(10);
alter table dai_5_trin_down add sb_trn_bool varchar(10),  add temp varchar(40), add as_eve_fm_sb varchar(10);
alter table dai_200_trin_down add sb_trn_bool varchar(10),  add temp varchar(40), add as_eve_fm_sb varchar(10);

update dai_5_trin_up set temp=((regexp_split_to_array(sb_trn,'\.'))[1] || '.' || (regexp_split_to_array(sb_trn,'\.'))[2]);
update dai_200_trin_up set temp=((regexp_split_to_array(sb_trn,'\.'))[1] || '.' || (regexp_split_to_array(sb_trn,'\.'))[2]);
update dai_5_trin_down set temp=((regexp_split_to_array(sb_trn,'\.'))[1] || '.' || (regexp_split_to_array(sb_trn,'\.'))[2]);
update dai_200_trin_down set temp=((regexp_split_to_array(sb_trn,'\.'))[1] || '.' || (regexp_split_to_array(sb_trn,'\.'))[2]);

update dai_5_trin_up as a set sb_trn_bool='yes' from dai_5_isf_up as b
	where a.temp=b.gene;
update dai_200_trin_up as a set sb_trn_bool='yes' from dai_200_isf_up as b
	where a.temp=b.gene;
update dai_5_trin_down as a set sb_trn_bool='yes' from dai_5_isf_down as b
	where a.temp=b.gene;
update dai_200_trin_down as a set sb_trn_bool='yes' from dai_200_isf_down as b
	where a.temp=b.gene;

update dai_5_trin_up as a set as_eve_fm_sb=b.as_name from dai5_s_astalavista as b
	where a.sb_trn=b.sb_trn;
update dai_200_trin_up as a set as_eve_fm_sb=b.as_name from dai200_s_astalavista as b
	where a.sb_trn=b.sb_trn;
	
\copy dai_5_trin_up to 'R:\project\sugarcane_as\hpc_analysis\5dai_srap_gexp_trinityref\trinity_gexp\dai_5_trin_up.txt';
\copy dai_5_trin_down to 'R:\project\sugarcane_as\hpc_analysis\5dai_srap_gexp_trinityref\trinity_gexp\dai_5_trin_down.txt';
\copy dai_200_trin_up to 'R:\project\sugarcane_as\hpc_analysis\200dai_srap_gexp_trinity\trinity_gexp\dai_200_trin_up.txt';
\copy dai_200_trin_down to 'R:\project\sugarcane_as\hpc_analysis\200dai_srap_gexp_trinity\trinity_gexp\dai_200_trin_down.txt';

/* *** Expression analysis with Trinity Transcripts *** END ***************************************/


/* for duplicate removal from trinity assembled transcriptome */
Drop table if exists blastn_trin_self;
	create table blastn_trin_self
	(qid varchar(30),
	qlen int,
	sid varchar(30),
	slen int,
	qstart int,
	qend int,
	sstart int,
	send int, 
	nident int,
	pident double precision,
	alen int,
	mm int,
	gp int,
	qcov double precision,
	eval double precision,
	bs double precision);
	
\copy blastn_trin_self from 'R:\project\sugarcane_as\hpc_analysis\unique_trin_transcripts\blastn_sctrinitrans_self.txt';

delete from blastn_trin_self where qid=sid;
alter table blastn_trin_self add q_cov_cal double precision;
update blastn_trin_self set q_cov_cal=(nident*100)/qlen;

drop table if exists dup_trin_tran;
create table dup_trin_tran (id varchar(30));
insert into dup_trin_tran select qid from blastn_trin_self 
	where q_cov_cal=100 and qlen < slen;
/* select * from blastn_trin_self where q_cov_cal=100 and qlen=slen; 
Also, check above command to remove duplicates with equal query and subject length */


/* *** AS Events in S bicolor mapped sequences *** START */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'AS Events in S bicolor mapped sequences';
END
$$;
drop table if exists dai5_c_astalavista, dai5_s_astalavista, dai200_c_astalavista, dai200_s_astalavista;

create table dai5_c_astalavista
(
chr varchar(10),
src varchar(20),
feature varchar(10),
st int,
ende int,
scr varchar(5),
str varchar(5),
frm varchar(5),
attr varchar(8100)
);
create table dai5_s_astalavista (like dai5_c_astalavista);
create table dai200_c_astalavista (like dai5_c_astalavista);
create table dai200_s_astalavista (like dai5_c_astalavista);

\copy dai5_c_astalavista from 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\5d_merged_control\merged_sorted.gtf_astalavista.gtf';
\copy dai5_s_astalavista from 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\5d_merged_stress\merged_sorted.gtf_astalavista.gtf';
\copy dai200_c_astalavista from 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\200d_merged_control\merged_sorted.gtf_astalavista.gtf';
\copy dai200_s_astalavista from 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\200d_merged_stress\merged_sorted.gtf_astalavista.gtf';

alter table dai5_c_astalavista add as_type varchar(2250), add as_name varchar(30);
alter table dai5_s_astalavista add as_type varchar(2250), add as_name varchar(30);
alter table dai200_c_astalavista add as_type varchar(2250), add as_name varchar(30);
alter table dai200_s_astalavista add as_type varchar(2250), add as_name varchar(30);

update dai5_c_astalavista set as_type = (regexp_split_to_array(attr, '"'))[8];
update dai5_s_astalavista set as_type = (regexp_split_to_array(attr, '"'))[8];
update dai200_c_astalavista set as_type = (regexp_split_to_array(attr, '"'))[8];
update dai200_s_astalavista set as_type = (regexp_split_to_array(attr, '"'))[8];

/* Exon Skipping or cassette exons */
update dai5_c_astalavista set as_name = 'ES' where as_type='1-2^,0' or as_type='0,1-2^'; 
update dai5_s_astalavista set as_name = 'ES' where as_type='1-2^,0' or as_type='0,1-2^'; 
update dai200_c_astalavista set as_name = 'ES' where as_type='1-2^,0' or as_type='0,1-2^'; 
update dai200_s_astalavista set as_name = 'ES' where as_type='1-2^,0' or as_type='0,1-2^'; 

/* Intron Retension or IR */
update dai5_c_astalavista set as_name = 'IR' where as_type='0,1^2-' or as_type='1^2-,0';
update dai5_s_astalavista set as_name = 'IR' where as_type='0,1^2-' or as_type='1^2-,0';
update dai200_c_astalavista set as_name = 'IR' where as_type='0,1^2-' or as_type='1^2-,0';
update dai200_s_astalavista set as_name = 'IR' where as_type='0,1^2-' or as_type='1^2-,0';

/* two competitive/alternative donor sites */
update dai5_c_astalavista set as_name = 'AD' where as_type='1^,2^' ; 
update dai5_s_astalavista set as_name = 'AD' where as_type='1^,2^' ; 
update dai200_c_astalavista set as_name = 'AD' where as_type='1^,2^' ; 
update dai200_s_astalavista set as_name = 'AD' where as_type='1^,2^' ; 

/* two competitive/alternative acceptor sites */
update dai5_c_astalavista set as_name = 'AA' where as_type='1-,2-' ;
update dai5_s_astalavista set as_name = 'AA' where as_type='1-,2-' ;
update dai200_c_astalavista set as_name = 'AA' where as_type='1-,2-' ;
update dai200_s_astalavista set as_name = 'AA' where as_type='1-,2-' ;

/* IR1 or IR2 */
update dai5_c_astalavista set as_name = 'other' where as_type='1^2-,3^4-' ; 
update dai5_s_astalavista set as_name = 'other' where as_type='1^2-,3^4-' ; 
update dai200_c_astalavista set as_name = 'other' where as_type='1^2-,3^4-' ; 
update dai200_s_astalavista set as_name = 'other' where as_type='1^2-,3^4-' ; 

/* IR1 and IR2 */
update dai5_c_astalavista set as_name = 'other' where as_type='0,1^2-3^4-';
update dai5_s_astalavista set as_name = 'other' where as_type='0,1^2-3^4-';
update dai200_c_astalavista set as_name = 'other' where as_type='0,1^2-3^4-';
update dai200_s_astalavista set as_name = 'other' where as_type='0,1^2-3^4-';

/* alt 5 SS and alt 3SS */
update dai5_c_astalavista set as_name = 'other' where as_type='1^4-,2^3-';
update dai5_s_astalavista set as_name = 'other' where as_type='1^4-,2^3-';
update dai200_c_astalavista set as_name = 'other' where as_type='1^4-,2^3-';
update dai200_s_astalavista set as_name = 'other' where as_type='1^4-,2^3-';

/* alt 5 SS or alt 3SS */
update dai5_c_astalavista set as_name = 'other' where as_type='1^3-,2^4-';
update dai5_s_astalavista set as_name = 'other' where as_type='1^3-,2^4-';
update dai200_c_astalavista set as_name = 'other' where as_type='1^3-,2^4-';
update dai200_s_astalavista set as_name = 'other' where as_type='1^3-,2^4-';

/* alt 5 SS and alt 3SS and ES */
update dai5_c_astalavista set as_name = 'other' where as_type='1^6-,2^3-4^5-';
update dai5_s_astalavista set as_name = 'other' where as_type='1^6-,2^3-4^5-';
update dai200_c_astalavista set as_name = 'other' where as_type='1^6-,2^3-4^5-';
update dai200_s_astalavista set as_name = 'other' where as_type='1^6-,2^3-4^5-';

/* alt 5 SS and alt 3SS and ES1 and ES2 */
update dai5_c_astalavista set as_name = 'other' where as_type='1^8-,2^3-4^5-6^7-'; 
update dai5_s_astalavista set as_name = 'other' where as_type='1^8-,2^3-4^5-6^7-'; 
update dai200_c_astalavista set as_name = 'other' where as_type='1^8-,2^3-4^5-6^7-'; 
update dai200_s_astalavista set as_name = 'other' where as_type='1^8-,2^3-4^5-6^7-'; 

/* alt 5 SS and alt 3SS and ES1 and ES2 and ES3 */
update dai5_c_astalavista set as_name = 'other' where as_type='1^8-,2^3-4^5-6^7-9^10-';
update dai5_s_astalavista set as_name = 'other' where as_type='1^8-,2^3-4^5-6^7-9^10-';
update dai200_c_astalavista set as_name = 'other' where as_type='1^8-,2^3-4^5-6^7-9^10-';
update dai200_s_astalavista set as_name = 'other' where as_type='1^8-,2^3-4^5-6^7-9^10-';

/* set null to other */
update dai5_c_astalavista set as_name = 'other' where as_name is null;
update dai5_s_astalavista set as_name = 'other' where as_name is null;
update dai200_c_astalavista set as_name = 'other' where as_name is null;
update dai200_s_astalavista set as_name = 'other' where as_name is null;

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'ES events dai5_c_astalavista, dai5_s_astalavista, dai200_c_astalavista, dai200_s_astalavista in order';
END
$$;
select count(*) from dai5_c_astalavista where as_name='ES';
select count(*) from dai5_s_astalavista where as_name='ES';
select count(*) from dai200_c_astalavista where as_name='ES';
select count(*) from dai200_s_astalavista where as_name='ES';

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'IR events dai5_c_astalavista, dai5_s_astalavista, dai200_c_astalavista, dai200_s_astalavista in order';
END
$$;
select count(*) from dai5_c_astalavista where as_name='IR';
select count(*) from dai5_s_astalavista where as_name='IR';
select count(*) from dai200_c_astalavista where as_name='IR';
select count(*) from dai200_s_astalavista where as_name='IR';

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'AD events dai5_c_astalavista, dai5_s_astalavista, dai200_c_astalavista, dai200_s_astalavista in order';
END
$$;
select count(*) from dai5_c_astalavista where as_name='AD';
select count(*) from dai5_s_astalavista where as_name='AD';
select count(*) from dai200_c_astalavista where as_name='AD';
select count(*) from dai200_s_astalavista where as_name='AD';

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'AA events dai5_c_astalavista, dai5_s_astalavista, dai200_c_astalavista, dai200_s_astalavista in order';
END
$$;
select count(*) from dai5_c_astalavista where as_name='AA';
select count(*) from dai5_s_astalavista where as_name='AA';
select count(*) from dai200_c_astalavista where as_name='AA';
select count(*) from dai200_s_astalavista where as_name='AA';

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Other events dai5_c_astalavista, dai5_s_astalavista, dai200_c_astalavista, dai200_s_astalavista in order';
END
$$;
select count(*) from dai5_c_astalavista where as_name='other';
select count(*) from dai5_s_astalavista where as_name='other';
select count(*) from dai200_c_astalavista where as_name='other';
select count(*) from dai200_s_astalavista where as_name='other';

/* add sb transcripts to AS events */
alter table dai5_c_astalavista add sb_trn varchar(20);
alter table dai5_s_astalavista add sb_trn varchar(20);
alter table dai200_c_astalavista add sb_trn varchar(20);
alter table dai200_s_astalavista add sb_trn varchar(20);

update dai5_c_astalavista as a set sb_trn=b.id from sbicolor_gff as b
 where a.chr=b.chr and a.st>=b.st and a.ende<=b.ende and b.feature='mRNA';
update dai5_s_astalavista as a set sb_trn=b.id from sbicolor_gff as b
 where a.chr=b.chr and a.st>=b.st and a.ende<=b.ende and b.feature='mRNA';
update dai200_c_astalavista as a set sb_trn=b.id from sbicolor_gff as b
 where a.chr=b.chr and a.st>=b.st and a.ende<=b.ende and b.feature='mRNA';
update dai200_s_astalavista as a set sb_trn=b.id from sbicolor_gff as b
 where a.chr=b.chr and a.st>=b.st and a.ende<=b.ende and b.feature='mRNA';

 /* find tolat genes alternatively spliced */
 DO language plpgsql $$
BEGIN
  RAISE NOTICE 'find tolat genes alternatively spliced';
END
$$;
drop table if exists temp;
create table temp as select distinct sb_trn from dai5_c_astalavista where as_name!='other' union 
	select distinct sb_trn from dai5_s_astalavista where as_name!='other' union select distinct 
	sb_trn from dai200_c_astalavista where as_name!='other' union select distinct 
	sb_trn from dai200_s_astalavista;
select count(*) from temp;
select count(distinct sb_trn) from temp;
drop table temp;
 
/* *** AS Events in S bicolor mapped sequences *** END */


/* *** venn diragram for AS Events in S bicolor mapped sequences *** START */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Venn diagram Analysis';
END
$$;
drop table if exists venn_es_dai5_astalavista;
drop table if exists venn_ir_dai5_astalavista;
drop table if exists venn_aa_dai5_astalavista;
drop table if exists venn_ad_dai5_astalavista;

create table venn_es_dai5_astalavista as 
	(select chr, st, ende, as_type, as_name from dai5_c_astalavista where as_name='ES'
		union select chr, st, ende, as_type, as_name from dai5_s_astalavista where as_name='ES');
create table venn_ir_dai5_astalavista as 
	(select chr, st, ende, as_type, as_name from dai5_c_astalavista where as_name='IR'
		union select chr, st, ende, as_type, as_name from dai5_s_astalavista where as_name='IR');
create table venn_aa_dai5_astalavista as 
	(select chr, st, ende, as_type, as_name from dai5_c_astalavista where as_name='AA'
		union select chr, st, ende, as_type, as_name from dai5_s_astalavista where as_name='AA');
create table venn_ad_dai5_astalavista as 
	(select chr, st, ende, as_type, as_name from dai5_c_astalavista where as_name='AD'
		union select chr, st, ende, as_type, as_name from dai5_s_astalavista where as_name='AD');
		
alter table venn_es_dai5_astalavista add sb_trn varchar(20), add cont int, add stres int, add comn int; 
alter table venn_ir_dai5_astalavista add sb_trn varchar(20), add cont int, add stres int, add comn int;
alter table venn_aa_dai5_astalavista add sb_trn varchar(20), add cont int, add stres int, add comn int;
alter table venn_ad_dai5_astalavista add sb_trn varchar(20), add cont int, add stres int, add comn int;

update venn_es_dai5_astalavista as a set sb_trn=b.sb_trn from dai5_c_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_es_dai5_astalavista as a set sb_trn=b.sb_trn from dai5_s_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name and 
	a.sb_trn is null;
update venn_ir_dai5_astalavista as a set sb_trn=b.sb_trn from dai5_c_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ir_dai5_astalavista as a set sb_trn=b.sb_trn from dai5_s_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name and 
	a.sb_trn is null;	
update venn_aa_dai5_astalavista as a set sb_trn=b.sb_trn from dai5_c_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_aa_dai5_astalavista as a set sb_trn=b.sb_trn from dai5_s_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name and 
	a.sb_trn is null;
update venn_ad_dai5_astalavista as a set sb_trn=b.sb_trn from dai5_c_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ad_dai5_astalavista as a set sb_trn=b.sb_trn from dai5_s_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name and 
	a.sb_trn is null;

update venn_es_dai5_astalavista as a set cont=1 from dai5_c_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ir_dai5_astalavista as a set cont=1 from dai5_c_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_aa_dai5_astalavista as a set cont=1 from dai5_c_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ad_dai5_astalavista as a set cont=1 from dai5_c_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;

update venn_es_dai5_astalavista set cont=0 where cont is null;
update venn_ir_dai5_astalavista set cont=0 where cont is null;
update venn_aa_dai5_astalavista set cont=0 where cont is null;
update venn_ad_dai5_astalavista set cont=0 where cont is null;
	
update venn_es_dai5_astalavista as a set stres=1 from dai5_s_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ir_dai5_astalavista as a set stres=1 from dai5_s_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_aa_dai5_astalavista as a set stres=1 from dai5_s_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ad_dai5_astalavista as a set stres=1 from dai5_s_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
	
update venn_es_dai5_astalavista set stres=0 where stres is null;
update venn_ir_dai5_astalavista set stres=0 where stres is null;
update venn_aa_dai5_astalavista set stres=0 where stres is null;
update venn_ad_dai5_astalavista set stres=0 where stres is null;

update venn_es_dai5_astalavista set comn= case
	when cont=1 and stres=1 then 1
	else 0
	end;
update venn_ir_dai5_astalavista set comn= case
	when cont=1 and stres=1 then 1
	else 0
	end;
update venn_aa_dai5_astalavista set comn= case
	when cont=1 and stres=1 then 1
	else 0
	end;
update venn_ad_dai5_astalavista set comn= case
	when cont=1 and stres=1 then 1
	else 0
	end;

drop table if exists venn_es_dai200_astalavista;
drop table if exists venn_ir_dai200_astalavista;
drop table if exists venn_aa_dai200_astalavista;
drop table if exists venn_ad_dai200_astalavista;

create table venn_es_dai200_astalavista as 
	(select chr, st, ende, as_type, as_name from dai200_c_astalavista where as_name='ES'
		union select chr, st, ende, as_type, as_name from dai200_s_astalavista where as_name='ES');
create table venn_ir_dai200_astalavista as 
	(select chr, st, ende, as_type, as_name from dai200_c_astalavista where as_name='IR'
		union select chr, st, ende, as_type, as_name from dai200_s_astalavista where as_name='IR');
create table venn_aa_dai200_astalavista as 
	(select chr, st, ende, as_type, as_name from dai200_c_astalavista where as_name='AA'
		union select chr, st, ende, as_type, as_name from dai200_s_astalavista where as_name='AA');
create table venn_ad_dai200_astalavista as 
	(select chr, st, ende, as_type, as_name from dai200_c_astalavista where as_name='AD'
		union select chr, st, ende, as_type, as_name from dai200_s_astalavista where as_name='AD');

alter table venn_es_dai200_astalavista add sb_trn varchar(20), add cont int, add stres int, add comn int; 
alter table venn_ir_dai200_astalavista add sb_trn varchar(20), add cont int, add stres int, add comn int;
alter table venn_aa_dai200_astalavista add sb_trn varchar(20), add cont int, add stres int, add comn int;
alter table venn_ad_dai200_astalavista add sb_trn varchar(20), add cont int, add stres int, add comn int;

update venn_es_dai200_astalavista as a set sb_trn=b.sb_trn from dai200_c_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_es_dai200_astalavista as a set sb_trn=b.sb_trn from dai200_s_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name and 
	a.sb_trn is null;
update venn_ir_dai200_astalavista as a set sb_trn=b.sb_trn from dai200_c_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ir_dai200_astalavista as a set sb_trn=b.sb_trn from dai200_s_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name and 
	a.sb_trn is null;	
update venn_aa_dai200_astalavista as a set sb_trn=b.sb_trn from dai200_c_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_aa_dai200_astalavista as a set sb_trn=b.sb_trn from dai200_s_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name and 
	a.sb_trn is null;
update venn_ad_dai200_astalavista as a set sb_trn=b.sb_trn from dai200_c_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ad_dai200_astalavista as a set sb_trn=b.sb_trn from dai200_s_astalavista as b where 
	a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name and 
	a.sb_trn is null;

update venn_es_dai200_astalavista as a set cont=1 from dai200_c_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ir_dai200_astalavista as a set cont=1 from dai200_c_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_aa_dai200_astalavista as a set cont=1 from dai200_c_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ad_dai200_astalavista as a set cont=1 from dai200_c_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;

update venn_es_dai200_astalavista set cont=0 where cont is null;
update venn_ir_dai200_astalavista set cont=0 where cont is null;
update venn_aa_dai200_astalavista set cont=0 where cont is null;
update venn_ad_dai200_astalavista set cont=0 where cont is null;

update venn_es_dai200_astalavista as a set stres=1 from dai200_s_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ir_dai200_astalavista as a set stres=1 from dai200_s_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_aa_dai200_astalavista as a set stres=1 from dai200_s_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;
update venn_ad_dai200_astalavista as a set stres=1 from dai200_s_astalavista as b where a.chr=b.chr and a.st=b.st
	and a.ende=b.ende and a.as_type=b.as_type and a.as_name=b.as_name;

update venn_es_dai200_astalavista set stres=0 where stres is null;
update venn_ir_dai200_astalavista set stres=0 where stres is null;
update venn_aa_dai200_astalavista set stres=0 where stres is null;
update venn_ad_dai200_astalavista set stres=0 where stres is null;

update venn_es_dai200_astalavista set comn= case
	when cont=1 and stres=1 then 1
	else 0
	end;
update venn_ir_dai200_astalavista set comn= case
	when cont=1 and stres=1 then 1
	else 0
	end;
update venn_aa_dai200_astalavista set comn= case
	when cont=1 and stres=1 then 1
	else 0
	end;
update venn_ad_dai200_astalavista set comn= case
	when cont=1 and stres=1 then 1
	else 0
	end;

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'venn_es_dai5_astalavista';
END
$$;
select count(*) from venn_es_dai5_astalavista where comn=1;
select count(*) from venn_es_dai5_astalavista where cont=1 and stres=0;
select count(*) from venn_es_dai5_astalavista where stres=1 and cont=0;

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'venn_ir_dai5_astalavista';
END
$$;
select count(*) from venn_ir_dai5_astalavista where comn=1;
select count(*) from venn_ir_dai5_astalavista where cont=1 and stres=0;
select count(*) from venn_ir_dai5_astalavista where stres=1 and cont=0;

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'venn_aa_dai5_astalavista';
END
$$;
select count(*) from venn_aa_dai5_astalavista where comn=1;
select count(*) from venn_aa_dai5_astalavista where cont=1 and stres=0;
select count(*) from venn_aa_dai5_astalavista where stres=1 and cont=0;

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'venn_ad_dai5_astalavista';
END
$$;
select count(*) from venn_ad_dai5_astalavista where comn=1;
select count(*) from venn_ad_dai5_astalavista where cont=1 and stres=0;
select count(*) from venn_ad_dai5_astalavista where stres=1 and cont=0;

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'venn_es_dai200_astalavista';
END
$$;
select count(*) from venn_es_dai200_astalavista where comn=1;
select count(*) from venn_es_dai200_astalavista where cont=1 and stres=0;
select count(*) from venn_es_dai200_astalavista where stres=1 and cont=0;

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'venn_ir_dai200_astalavista';
END
$$;
select count(*) from venn_ir_dai200_astalavista where comn=1;
select count(*) from venn_ir_dai200_astalavista where cont=1 and stres=0;
select count(*) from venn_ir_dai200_astalavista where stres=1 and cont=0;

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'venn_aa_dai200_astalavista';
END
$$;
select count(*) from venn_aa_dai200_astalavista where comn=1;
select count(*) from venn_aa_dai200_astalavista where cont=1 and stres=0;
select count(*) from venn_aa_dai200_astalavista where stres=1 and cont=0;

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'venn_ad_dai200_astalavista';
END
$$;
select count(*) from venn_ad_dai200_astalavista where comn=1;
select count(*) from venn_ad_dai200_astalavista where cont=1 and stres=0;
select count(*) from venn_ad_dai200_astalavista where stres=1 and cont=0;


/* *** Box analysis *** START */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Box plot analysis';
END
$$;
drop table if exists box_isf_sb;
create table box_isf_sb
	(test_id varchar(30)
	);

insert into box_isf_sb (select test_id from dai_5_isf_up union select test_id from 
	dai_200_isf_up);

alter table box_isf_sb add lfc_5 double precision, add p_value_5 double precision, 
	add lfc_200 double precision, add p_value_200 double precision, add fpkm_5c double precision,
	add fpkm_5s double precision, add fpkm_200c double precision, add fpkm_200s double precision;

update box_isf_sb as a set lfc_5=b.log2fc, p_value_5=b.p_value,fpkm_5c=b.value_1,fpkm_5s=b.value_2 
	from dai_5_isf as b where a.test_id=b.test_id;
update box_isf_sb as a set lfc_200=b.log2fc, p_value_200=b.p_value,fpkm_200c=b.value_1,
	fpkm_200s=b.value_2  from dai_200_isf as b where a.test_id=b.test_id;
	
update box_isf_sb set lfc_5=0 where lfc_5='Infinity' or lfc_5='-Infinity' ;
update box_isf_sb set lfc_200=0 where lfc_200='Infinity' or lfc_200='-Infinity' ;
update box_isf_sb set lfc_5=0.1 where p_value_5>0.05 ;
update box_isf_sb set lfc_200=0.1 where p_value_200>0.05 ;

\copy box_isf_sb to 'R:\project\sugarcane_as\vis\box_plot\box_isf_sb.txt';
/* *** PCA analysis *** END */


/* *** Htseq and Circos analysis start *** START */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Htseq and Circos analysis start';
END
$$;
/* import htseq count tables */
drop table if exists temp1;
create table temp1 
	(gene varchar(30),
	 ct int);
drop table if exists temp2;
create table temp2 (like temp1);
drop table if exists temp3;
create table temp3 (like temp1);
\copy temp1 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\5c_rep1.count';
\copy temp2 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\5c_rep2.count';
\copy temp3 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\5c_rep3.count';
drop table if exists dai5c_hseq_count;
create table dai5c_hseq_count (like temp1);
insert into dai5c_hseq_count select gene from temp1 union select gene from temp2 union select gene 
	from temp3;
alter table dai5c_hseq_count add chr varchar(10), add id varchar(30),add st int, add ende int, add r1_ct int, add r2_ct int, add r3_ct int, add r_sum int, add log10_r_sum double precision;
update dai5c_hseq_count set id=(regexp_split_to_array(gene,'.v3'))[1];
update dai5c_hseq_count as a set chr=b.chr, st=b.st, ende=b.ende from sbicolor_gff as b where a.id=b.id and 
	b.feature='gene';
update dai5c_hseq_count as a set r1_ct=b.ct from temp1 as b where a.gene=b.gene;
update dai5c_hseq_count as a set r2_ct=b.ct from temp2 as b where a.gene=b.gene;
update dai5c_hseq_count as a set r3_ct=b.ct from temp3 as b where a.gene=b.gene;
update dai5c_hseq_count set r_sum=r1_ct+r2_ct+r3_ct;
update dai5c_hseq_count set log10_r_sum=log(r_sum) where r_sum!=0;
update dai5c_hseq_count set log10_r_sum=0 where r_sum=0;

drop table if exists temp1;
create table temp1 
	(gene varchar(30),
	 ct int);
drop table if exists temp2;
create table temp2 (like temp1);
drop table if exists temp3;
create table temp3 (like temp1);
\copy temp1 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\5s_rep1.count';
\copy temp2 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\5s_rep2.count';
\copy temp3 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\5s_rep3.count';
drop table if exists dai5s_hseq_count;
create table dai5s_hseq_count (like temp1);
insert into dai5s_hseq_count select gene from temp1 union select gene from temp2 union select gene 
	from temp3;
alter table dai5s_hseq_count add chr varchar(10), add id varchar(30), add st int, add ende int, add r1_ct int, add r2_ct int, add r3_ct int, add r_sum int, add log10_r_sum double precision;
update dai5s_hseq_count set id=(regexp_split_to_array(gene,'.v3'))[1];
update dai5s_hseq_count as a set chr=b.chr, st=b.st, ende=b.ende from sbicolor_gff as b where a.id=b.id and 
	b.feature='gene';
update dai5s_hseq_count as a set r1_ct=b.ct from temp1 as b where a.gene=b.gene;
update dai5s_hseq_count as a set r2_ct=b.ct from temp2 as b where a.gene=b.gene;
update dai5s_hseq_count as a set r3_ct=b.ct from temp3 as b where a.gene=b.gene;
update dai5s_hseq_count as a set r3_ct=b.ct from temp3 as b where a.gene=b.gene;
update dai5s_hseq_count set r_sum=r1_ct+r2_ct+r3_ct;
update dai5s_hseq_count set log10_r_sum=log(r_sum) where r_sum!=0;
update dai5s_hseq_count set log10_r_sum=0 where r_sum=0;

drop table if exists temp1;
create table temp1 
	(gene varchar(30),
	 ct int);
drop table if exists temp2;
create table temp2 (like temp1);
drop table if exists temp3;
create table temp3 (like temp1);
\copy temp1 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\200c_rep1.count';
\copy temp2 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\200c_rep2.count';
\copy temp3 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\200c_rep3.count';
drop table if exists dai200c_hseq_count;
create table dai200c_hseq_count (like temp1);
insert into dai200c_hseq_count select gene from temp1 union select gene from temp2 union select gene 
	from temp3;
alter table dai200c_hseq_count add chr varchar(10), add id varchar(30), add st int, add ende int, add r1_ct int, add r2_ct int, add r3_ct int, add r_sum int, add log10_r_sum double precision;
update dai200c_hseq_count set id=(regexp_split_to_array(gene,'.v3'))[1];
update dai200c_hseq_count as a set chr=b.chr, st=b.st, ende=b.ende from sbicolor_gff as b where a.id=b.id and 
	b.feature='gene';
update dai200c_hseq_count as a set r1_ct=b.ct from temp1 as b where a.gene=b.gene;
update dai200c_hseq_count as a set r2_ct=b.ct from temp2 as b where a.gene=b.gene;
update dai200c_hseq_count as a set r3_ct=b.ct from temp3 as b where a.gene=b.gene;
update dai200c_hseq_count as a set r3_ct=b.ct from temp3 as b where a.gene=b.gene;
update dai200c_hseq_count set r_sum=r1_ct+r2_ct+r3_ct;
update dai200c_hseq_count set log10_r_sum=log(r_sum) where r_sum!=0;
update dai200c_hseq_count set log10_r_sum=0 where r_sum=0;

drop table if exists temp1;
create table temp1 
	(gene varchar(30),
	 ct int);
drop table if exists temp2;
create table temp2 (like temp1);
drop table if exists temp3;
create table temp3 (like temp1);
\copy temp1 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\200s_rep1.count';
\copy temp2 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\200s_rep2.count';
\copy temp3 from 'R:\project\sugarcane_as\hpc_analysis\htseq_count\200s_rep3.count';
drop table if exists dai200s_hseq_count;
create table dai200s_hseq_count (like temp1);
insert into dai200s_hseq_count select gene from temp1 union select gene from temp2 union select gene 
	from temp3;
alter table dai200s_hseq_count add chr varchar(10), add id varchar(30), add st int, add ende int, add r1_ct int, add r2_ct int, add r3_ct int, add r_sum int, add log10_r_sum double precision;
update dai200s_hseq_count set id=(regexp_split_to_array(gene,'.v3'))[1];
update dai200s_hseq_count as a set chr=b.chr, st=b.st, ende=b.ende from sbicolor_gff as b where a.id=b.id and 
	b.feature='gene';
update dai200s_hseq_count as a set r1_ct=b.ct from temp1 as b where a.gene=b.gene;
update dai200s_hseq_count as a set r2_ct=b.ct from temp2 as b where a.gene=b.gene;
update dai200s_hseq_count as a set r3_ct=b.ct from temp3 as b where a.gene=b.gene;
update dai200s_hseq_count as a set r3_ct=b.ct from temp3 as b where a.gene=b.gene;
update dai200s_hseq_count set r_sum=r1_ct+r2_ct+r3_ct;
update dai200s_hseq_count set log10_r_sum=log(r_sum) where r_sum!=0;
update dai200s_hseq_count set log10_r_sum=0 where r_sum=0;

\copy (select chr, st, ende, log10_r_sum from dai5c_hseq_count where gene like '%Sobic%' and log10_r_sum > 0) to 'R:\project\circos_images\sugarcane_as\gene_density\dai5c_hseq_count.txt';
\copy (select chr, st, ende, log10_r_sum from dai5s_hseq_count where gene like '%Sobic%' and log10_r_sum > 0) to 'R:\project\circos_images\sugarcane_as\gene_density\dai5s_hseq_count.txt';	
\copy (select chr, st, ende, log10_r_sum from dai200c_hseq_count where gene like '%Sobic%' and log10_r_sum > 0) to 'R:\project\circos_images\sugarcane_as\gene_density\dai200c_hseq_count.txt';
\copy (select chr, st, ende, log10_r_sum from dai200s_hseq_count where gene like '%Sobic%' and log10_r_sum > 0) to 'R:\project\circos_images\sugarcane_as\gene_density\dai200s_hseq_count.txt';

/* *** Htseq and Circos analysis start *** END */



/* *** Heatmap *** START */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'heatmap data';
END
$$;
drop table if exists heatmap_isf_sb;
create table heatmap_isf_sb as select test_id from dai_200_isf_up;
alter table heatmap_isf_sb add tp int, add sb_trn varchar(30), add fpkm_5c double precision, 
	add fpkm_5s double precision, add fpkm_200c double precision, add fpkm_200s double precision, 
	add anot varchar(300);
update heatmap_isf_sb set tp=200;
insert into heatmap_isf_sb(test_id) select test_id from dai_5_isf_up;
update heatmap_isf_sb set tp=5 where tp is null;
update heatmap_isf_sb as a set sb_trn=b.nearest_ref_id from dai_200_isf_fpkm_track as b
	where a.test_id=b.tracking_id and tp=200;
update heatmap_isf_sb as a set sb_trn=b.nearest_ref_id from dai_5_isf_fpkm_track as b
	where a.test_id=b.tracking_id and tp=5;
	
update heatmap_isf_sb as a set fpkm_200c=b.q1_FPKM, fpkm_200s=b.q2_FPKM from dai_200_isf_fpkm_track 
	as b where a.test_id=b.tracking_id and tp=200;
update heatmap_isf_sb as a set fpkm_5c=b.q1_FPKM, fpkm_5s=b.q2_FPKM from dai_5_isf_fpkm_track as b
	where a.sb_trn=b.nearest_ref_id and tp=200;
update heatmap_isf_sb as a set fpkm_200c=b.q1_FPKM, fpkm_200s=b.q2_FPKM from dai_200_isf_fpkm_track 
	as b where a.test_id=b.tracking_id and tp=5;
update heatmap_isf_sb as a set fpkm_5c=b.q1_FPKM, fpkm_5s=b.q2_FPKM from dai_5_isf_fpkm_track as b
	where a.sb_trn=b.nearest_ref_id and tp=5;

update heatmap_isf_sb as a set anot=b.anot from dai_200_isf_up as b
	where a.test_id=b.test_id and tp=200;
update heatmap_isf_sb as a set anot=b.anot from dai_5_isf_up as b
	where a.test_id=b.test_id and tp=5;
	
delete from heatmap_isf_sb where sb_trn='-';
update heatmap_isf_sb set fpkm_5c=0.01 where fpkm_5c=0;
update heatmap_isf_sb set fpkm_5s=0.01 where fpkm_5s=0;
update heatmap_isf_sb set fpkm_200c=0.01 where fpkm_200c=0;
update heatmap_isf_sb set fpkm_200s=0.01 where fpkm_200s=0;

alter table heatmap_isf_sb add categ varchar(50), add sn varchar(10);
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='EXPA' where anot like '%expansin%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='COBRA' where anot like '%COBRA%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='GHD' where anot like '%Glycosyl hydrolase%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='LCC' where anot like '%laccase%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='BGL' where anot like '%beta glucosidase%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='FUT' where anot like '%fucosyltransferase%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='AGP' where anot like '%arabinoogalactan%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='BXY' where anot like '%xylosidase%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='XTH' where anot like '%Xyloglucan%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='PDE' where anot like '%phosphodiesterase%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='WAT' where anot like '%Walls Are Thin%';
update heatmap_isf_sb set categ='Cell wall modifying genes', sn='CCR' where anot like '%cinnamoyl coa%';

update heatmap_isf_sb set categ='ROS Scavenger', sn='POX' where anot like '%Peroxidase%';
update heatmap_isf_sb set categ='ROS Scavenger', sn='GST' where anot like '%Glutathione S-transferase%';
update heatmap_isf_sb set categ='ROS Scavenger', sn='RBOH' where anot like '%respiratory burst oxidase%';

update heatmap_isf_sb set categ='Transcription factors', sn='bHLH' where anot like '%bHLH%';
update heatmap_isf_sb set categ='Transcription factors', sn='HB' where anot like '%Homeodomain-like%';
update heatmap_isf_sb set categ='Transcription factors', sn='bZIP' where anot like '%bZIP%';
update heatmap_isf_sb set categ='Transcription factors', sn='bZIP' where anot like '%leucine-zipper%';
update heatmap_isf_sb set categ='Transcription factors', sn='TF' where anot like '%transcription factor%' and anot is null;
update heatmap_isf_sb set categ='Transcription factors', sn='HB' where anot like '%Homeobox%';
update heatmap_isf_sb set categ='Transcription factors', sn='MYB' where anot like '%myb%';
update heatmap_isf_sb set categ='Transcription factors', sn='NAC' where anot like '%NAC%';
update heatmap_isf_sb set categ='Transcription factors', sn='GATA' where anot like '%GATA%';
update heatmap_isf_sb set categ='Transcription factors', sn='GRAS' where anot like '%GRAS%';
update heatmap_isf_sb set categ='Transcription factors', sn='WRKY' where anot like '%WRKY%';
update heatmap_isf_sb set categ='Transcription factors', sn='C3H' where anot like '%Zinc finger, C3HC4%';

update heatmap_isf_sb set categ='Defense signaling', sn='A20/AN1' where anot like '%A20/AN1%';
update heatmap_isf_sb set categ='Defense signaling', sn='GDSL' where anot like '%GDSL%';
update heatmap_isf_sb set categ='Defense signaling', sn='YST' where anot like '%YELLOW STRIPE%';
update heatmap_isf_sb set categ='Defense signaling', sn='PREP' where anot like '%Prolyl oligopeptidase%';
update heatmap_isf_sb set categ='Defense signaling', sn='FQR' where anot like '%flavodoxin-like%';
update heatmap_isf_sb set categ='Defense signaling', sn='LRR' where anot like '%Leucine-rich receptor%';
update heatmap_isf_sb set categ='Defense signaling', sn='PL' where anot like '%phospholipase%';
update heatmap_isf_sb set categ='Defense signaling', sn='ABC' where anot like '%ATP binding cassette%';
update heatmap_isf_sb set categ='Defense signaling', sn='ABC' where anot like '%ABC-2%';
update heatmap_isf_sb set categ='Defense signaling', sn='HERK' where anot like '%hercules receptor%';
update heatmap_isf_sb set categ='Defense signaling', sn='ABA' where anot like '%abscisic%';
update heatmap_isf_sb set categ='Defense signaling', sn='PAOX' where anot like '%polyamine oxidase%';
update heatmap_isf_sb set categ='Defense signaling', sn='F-box' where anot like '%F-box%';
update heatmap_isf_sb set categ='Defense signaling', sn='DR' where anot like '%Disease resistance-responsive%';
update heatmap_isf_sb set categ='Defense signaling', sn='PR' where anot like '%Pathogenesis%';
update heatmap_isf_sb set categ='Defense signaling', sn='Aux' where anot like '%Auxin%';
update heatmap_isf_sb set categ='Defense signaling', sn='CIPK' where anot like '%CBL-interacting';
update heatmap_isf_sb set categ='Defense signaling', sn='SAUR' where anot like '%SAUR-like%';
update heatmap_isf_sb set categ='Defense signaling', sn='PK' where anot like '%Protein kinase%' and anot is null;
update heatmap_isf_sb set categ='Defense signaling', sn='LOX' where anot like '%lipoxygenase 1%';
update heatmap_isf_sb set categ='Defense signaling', sn='WR' where anot like '%Wound%';
update heatmap_isf_sb set categ='Defense signaling', sn='CALM' where anot like '%calmodulin%';
update heatmap_isf_sb set categ='Defense signaling', sn='SAMMT' where anot like '%S-adenosyl%';
update heatmap_isf_sb set categ='Defense signaling', sn='CL' where anot like '%coumarate%';
update heatmap_isf_sb set categ='Defense signaling', sn='ACC' where anot like '%ACC oxidase%';
update heatmap_isf_sb set categ='Defense signaling', sn='AP' where anot like '%aspartyl protease%';
update heatmap_isf_sb set categ='Defense signaling', sn='RING' where anot like '%RING%';
update heatmap_isf_sb set categ='Defense signaling', sn='GAD' where anot like '%glutamate decarboxylase%';
update heatmap_isf_sb set categ='Defense signaling', sn='ADF' where anot like '%actin depolymerizing%';
update heatmap_isf_sb set categ='Defense signaling', sn='PP' where anot like '%protein phosphatases%';

\copy heatmap_isf_sb to 'R:\project\sugarcane_as\vis\heatmap\heatmap_isf_sb.txt';
\copy ( select * from heatmap_isf_sb where categ='Cell wall modifying genes') to 'R:\project\sugarcane_as\vis\heatmap\heatmap_isf_sb_Cell wall modifying genes.txt';	
\copy ( select * from heatmap_isf_sb where categ='ROS Scavenger') to 'R:\project\sugarcane_as\vis\heatmap\heatmap_isf_sb_ROS.txt';
\copy ( select * from heatmap_isf_sb where categ='Transcription factors') to 'R:\project\sugarcane_as\vis\heatmap\heatmap_isf_sb_TF.txt';
\copy ( select * from heatmap_isf_sb where categ='Defense signaling') to 'R:\project\sugarcane_as\vis\heatmap\heatmap_isf_sb_DS.txt';

/* *** Heatmap *** END */

/* *** GO NETWORK *** START */
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'GO network analysis';
END
$$;
create table at_other_map as select *  from dblink('dbname=plant_db user=postgres password=927122', 'select * from at_other_map') as t1(at_id varchar(30), sb_id  varchar(30), sb_gene  varchar(30)) ;
alter table dai_200_isf_up add at_id varchar(30);
update dai_200_isf_up as a set at_id=b.at_id from at_other_map as b where a.gene=b.sb_gene;
\copy (select at_id from dai_200_isf_up where at_id is not null) to 'R:\project\sugarcane_as\vis\go_network\go_network_at_200_isf_up.txt';

/* *** GO NETWORK *** END */
*/

/*=============================================END===============================================================*/
