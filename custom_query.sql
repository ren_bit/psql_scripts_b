/*update wtcr_s2oecr_up_rnet set q_short = 'PHB2' where q_anot like '%mitochondrial prohibitin complex protein 2%' and q_short is null;
update wtcr_s2oecr_up_rnet set t_short = 'PHB2' where t_anot like '%mitochondrial prohibitin complex protein 2%' and t_short is null;
*/

/*
update wtcr_s2oecr_up_rnet set q_short = 'GrpE' where query='LOC_Os09g11250' and q_short is null;
update wtcr_s2oecr_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wts3r_s2oe3r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wts3r_s2oe3r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wts7r_s2oe7r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wts7r_s2oe7r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wtcr_o2oecr_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wtcr_o2oecr_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wts3r_o2oe3r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wts3r_o2oe3r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wts7r_o2oe7r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wts7r_o2oe7r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update o2oecr_s2oecr_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update o2oecr_s2oecr_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update o2oe3r_s2oe3r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update o2oe3r_s2oe3r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update o2oe7r_s2oe7r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update o2oe7r_s2oe7r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;

update wtcr_s2oecr_up_rnet set q_short = 'GrpE' where query='LOC_Os09g11250' and q_short is null;
update wtcr_s2oecr_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wts3r_s2oe3r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wts3r_s2oe3r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wts7r_s2oe7r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wts7r_s2oe7r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wtcr_o2oecr_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wtcr_o2oecr_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wts3r_o2oe3r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wts3r_o2oe3r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update wts7r_o2oe7r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update wts7r_o2oe7r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update o2oecr_s2oecr_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update o2oecr_s2oecr_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update o2oe3r_s2oe3r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update o2oe3r_s2oe3r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
update o2oe7r_s2oe7r_up_rnet set q_short = 'GrpE ' where query='LOC_Os09g11250' and q_short is null;
update o2oe7r_s2oe7r_up_rnet set t_short = 'GrpE ' where tar='LOC_Os09g11250' and t_short is null;
*/

/*Add location of the SSR for spinach ssr */

ALTER TABLE spinach_ssr drop column if exists pos; 
ALTER TABLE spinach_ssr drop column if exists strand; 
ALTER TABLE spinach_ssr drop column if exists trans_id; 

alter table spinach_ssr add pos varchar(20),
			add trans_id varchar(20),
			add strand varchar(5);
/*drop table if exists temp;
create table temp as select chr, feature, st, ende from spinach_gff where feature='CDS';*/
update spinach_ssr as a set pos=b.feature, trans_id=b.id, strand=b.strand
	from spinach_gff  as b
	where a.st>=b.st and a.st<=b.ende
	      and a.ende>=b.st and a.ende<=b.ende and
	     a.id=b.chr and b.feature!='gene' and  b.feature!='mRNA';

/* spinach cds blastx with ncbi nr */
drop table if exists spinach_ncbi_nr_plant_anot;
create table spinach_ncbi_nr_plant_anot (qid varchar(20),
					 sid varchar(40),
					 anot varchar(300),
					 species varchar(100)
					  );
\copy spinach_ncbi_nr_plant_anot from '/Users/renesh/Renesh_Docs/Research/tamu/spinach_ssr/analysis/blast/blastx_spinach_ncbi_nr_plant_anot.txt';

/* spinach cds blastx with uniprot */
drop table if exists spinach_uniprot_plant_anot;
create table spinach_uniprot_plant_anot (qid varchar(20),
					 sid varchar(40),
					 anot varchar(300),
					 species varchar(100)
					  );
\copy spinach_uniprot_plant_anot from '/Users/renesh/Renesh_Docs/Research/tamu/spinach_ssr/analysis/blast/blastx_spinach_uniprot_plant_anot.txt';


ALTER TABLE spinach_ssr drop column if exists ncbi_gi; 
ALTER TABLE spinach_ssr drop column if exists ncbi_anot; 
ALTER TABLE spinach_ssr drop column if exists ncbi_species;
ALTER TABLE spinach_ssr drop column if exists up_id; 
ALTER TABLE spinach_ssr drop column if exists up_anot; 
ALTER TABLE spinach_ssr drop column if exists up_species;


alter table spinach_ssr add ncbi_gi varchar(40),
			add ncbi_anot varchar(300), 
			add ncbi_species varchar(100),
			add up_id varchar(40),
			add up_anot varchar(300), 
			add up_species varchar(100);

/*
ALTER TABLE blastx_spinach_ncbi_nr_plant drop column if exists gi; 
ALTER TABLE blastx_spinach_uniprot_plant drop column if exists up_id; 
alter table blastx_spinach_ncbi_nr_plant add gi varchar(30);
alter table blastx_spinach_uniprot_plant add up_id varchar(30);
*/

/*
ALTER TABLE spinach_ncbi_nr_plant_anot drop column if exists gi_id;
ALTER TABLE spinach_uniprot_plant_anot drop column if exists up_id;

update spinach_ncbi_nr_plant_anot set gi=(regexp_split_to_array(sid, '\|'))[2];
update spinach_uniprot_plant_anot set up_id=(regexp_split_to_array(sid, '\|'))[2];
*/

update spinach_ssr as a  
	set ncbi_gi = b.sid, ncbi_anot=b.anot, ncbi_species=b.species
	from spinach_ncbi_nr_plant_anot as b
	where a.trans_id=b.qid;
	
update spinach_ssr as a  
	set up_id = b.sid, up_anot=b.anot, up_species=b.species
	from spinach_uniprot_plant_anot as b
	where a.trans_id=b.qid;

/*export complete table*/
\copy (select id, trans_id, ssr_type, ssr, st, ende, size, pos, strand, ncbi_gi, ncbi_anot, ncbi_species, up_id, up_anot, up_species, ssr_id, attr from spinach_ssr) to '/Users/renesh/Renesh_Docs/Research/tamu/spinach_ssr/analysis/ssr_table/spinach_ssr_complete.csv' DELIMITER ',' CSV HEADER;
/* export records with predicted position*/
\copy (select id, trans_id, ssr_type, ssr, st, ende, size, pos, strand, ncbi_gi, ncbi_anot, ncbi_species, up_id, up_anot, up_species, ssr_id, attr from spinach_ssr where pos is not null) to '/Users/renesh/Renesh_Docs/Research/tamu/spinach_ssr/analysis/ssr_table/spinach_ssr_with_pos.csv' DELIMITER ',' CSV HEADER;

/*copy table from plant_db db*/
/*
drop table if exists temp;
create table temp as select * from
	dblink('dbname=plant_db', 'SELECT gi, species, anot FROM ncbi_nr_sept_2016' )
	as t(gi varchar(20), species varchar(180), anot varchar(300));
drop table if exists temp1;
create table temp1 as select * from
	dblink('dbname=plant_db', 'SELECT id1, species, anot FROM uniprot_sept_2016' )
	as t(id1 varchar(20), species varchar(180), anot varchar(300));

update spinach_ssr as a  
	set ncbi_anot = b.anot, ncbi_species=b.species
	from temp as b 
	where a.ncbi_gi=b.gi;
update spinach_ssr as a  
	set up_anot = b.anot, up_species = b.species
	from temp1 as b 
	where a.up_gi=b.id1;
*/

/*ende*/
