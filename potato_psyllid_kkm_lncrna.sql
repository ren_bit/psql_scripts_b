-- Potato zebrachip RNA-seq project

/*
module load PostgreSQL/9.6.1-GCCcore-6.3.0-Python-2.7.12-bare
pg_ctl -o "-p 9999" -D /scratch/user/ren_net/Test/postresql/test.db -l /scratch/user/ren_net/Test/postresql/test.log start
psql -p 9999 -f potato_psyllid_kkm_lncrna.sql potato_tomato_zebrachip
psql -p 9999 potato_tomato_zebrachip
*/


/*
-- differential lncrna expression analysis
create or replace function deg_lncrna(_deg_table varchar(20), _input_deg_table varchar(20), 
	_deg_table_up varchar(20), _deg_table_down varchar(20), _norm_count_table varchar(20), 
	_input_norm_count_table varchar(20), _raw_count_table varchar(20), _input_raw_count_table 
	varchar(20))
	returns void as
    $body$
    begin
		execute format('drop table if exists %I', _deg_table);
		execute format('create table %I(id text, basemean double precision, log2fc double precision,
            lfcSE double precision, stat double precision, pval text, padj text)', _deg_table);
		--import deg table 
        execute format('copy %I from $$' || _input_deg_table || '$$' || ' with csv header delimiter 
						as ' || '$$' || ',' || '$$', _deg_table);
		execute format('update %I set pval=NULL where pval=''NA'' ', _deg_table);
        execute format('update %I set padj=NULL where padj=''NA'' ', _deg_table);	
		execute format('alter table %I alter column pval type double precision using 
			(pval::double precision)', _deg_table);
        execute format('alter table %I alter column padj type double precision using 
			(padj::double precision)', _deg_table);
		execute format('copy (select id, log2fc, pval, padj, basemean, lfcse, stat from %I where 
			padj<0.05 and log2fc>=1) to $$' || _deg_table_up || '$$' || ' with (FORMAT csv, header 
			true)' , _deg_table);
		execute format('copy (select id, log2fc, pval, padj, basemean, lfcse, stat from %I where 
			padj<0.05 and log2fc<=-1) to $$' || _deg_table_down || '$$' || ' with (FORMAT csv, 
			header true)' , _deg_table);
		--add normalized count data
		execute format('drop table if exists %I', _norm_count_table);
        execute format('create table %I(id varchar(20), c1_norm double precision, c2_norm double 
			precision, c3_norm double precision, t1_norm double precision, t2_norm double precision,
			t3_norm double precision)', _norm_count_table);
		execute format('copy %I from $$' || _input_norm_count_table || '$$' || ' with csv header 
			delimiter as ' || '$$' || ',' || '$$', _norm_count_table);
		--add raw count data
		execute format('drop table if exists %I', _raw_count_table);
        execute format('create table %I(id varchar(20), c1_raw double precision, c2_raw double 
			precision, c3_raw double precision, t1_raw double precision, t2_raw double precision, 
			t3_raw double precision)', _raw_count_table);
        execute format('copy %I from $$' || _input_raw_count_table || '$$' || ' with csv header 
			delimiter as ' || '$$' || ',' || '$$', _raw_count_table);	
		--add normalized count data to deg tables
        execute format('alter table %I add c1_norm double precision, add c2_norm double precision, 
			add c3_norm double precision, add t1_norm double precision, add t2_norm double 
			precision, add t3_norm double precision;', _deg_table);
        execute format('update %I as a set c1_norm=b.c1_norm, c2_norm=b.c2_norm, c3_norm=b.c3_norm, 
			t1_norm=b.t1_norm, t2_norm=b.t2_norm, t3_norm=b.t3_norm from %I as b where a.id=b.id', 
			_deg_table, _norm_count_table);
     	--add raw count data to deg tables
        execute format('alter table %I add c1_raw double precision, add c2_raw double precision, 
			add c3_raw double precision, add t1_raw double precision, add t2_raw double precision, 
			add t3_raw double precision;', _deg_table);
        execute format('update %I as a set c1_raw=b.c1_raw, c2_raw=b.c2_raw, c3_raw=b.c3_raw, 
			t1_raw=b.t1_raw, t2_raw=b.t2_raw, t3_raw=b.t3_raw from %I as b where a.id=b.id', 
			_deg_table, _raw_count_table);
	end
    $body$ language "plpgsql";
	
\set degdir $$'/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/lncrna/'$$

select deg_lncrna('pot_cont_cold_7_lncrna', :degdir || 'res_pot_cont_cold_7_lncrna_trn.csv', 
	:degdir || 'res_pot_cont_cold_7_lncrna_up.csv', :degdir || 'res_pot_cont_cold_7_lncrna_down.csv',
	'normcount_pot_cont_cold_7_lncrna', :degdir || 'normcount_pot_cont_cold_7_lncrna_trn.csv', 
	'rawcount_pot_cont_cold_7_lncrna', :degdir || 'rawcount_pot_cont_cold_7_lncrna_trn.csv');
select deg_lncrna('pot_cont_hot_7_lncrna', :degdir || 'res_pot_cont_hot_7_lncrna_trn.csv', 
	:degdir || 'res_pot_cont_hot_7_lncrna_up.csv', :degdir || 'res_pot_cont_hot_7_lncrna_down.csv',
	'normcount_pot_cont_hot_7_lncrna', :degdir || 'normcount_pot_cont_hot_7_lncrna_trn.csv', 
	'rawcount_pot_cont_hot_7_lncrna', :degdir || 'rawcount_pot_cont_hot_7_lncrna_trn.csv');	
select deg_lncrna('pot_cold_hot_7_lncrna', :degdir || 'res_pot_cold_hot_7_lncrna_trn.csv', 
	:degdir || 'res_pot_cold_hot_7_lncrna_up.csv', :degdir || 'res_pot_cold_hot_7_lncrna_down.csv',
	'normcount_pot_cold_hot_7_lncrna', :degdir || 'normcount_pot_cold_hot_7_lncrna_trn.csv', 
	'rawcount_pot_cold_hot_7_lncrna', :degdir || 'rawcount_pot_cold_hot_7_lncrna_trn.csv');	

select deg_lncrna('pot_cont_cold_14_lncrna', :degdir || 'res_pot_cont_cold_14_lncrna_trn.csv', 
	:degdir || 'res_pot_cont_cold_14_lncrna_up.csv', :degdir || 'res_pot_cont_cold_14_lncrna_down.csv',
	'normcount_pot_cont_cold_14_lncrna', :degdir || 'normcount_pot_cont_cold_14_lncrna_trn.csv', 
	'rawcount_pot_cont_cold_14_lncrna', :degdir || 'rawcount_pot_cont_cold_14_lncrna_trn.csv');
select deg_lncrna('pot_cont_hot_14_lncrna', :degdir || 'res_pot_cont_hot_14_lncrna_trn.csv', 
	:degdir || 'res_pot_cont_hot_14_lncrna_up.csv', :degdir || 'res_pot_cont_hot_14_lncrna_down.csv',
	'normcount_pot_cont_hot_14_lncrna', :degdir || 'normcount_pot_cont_hot_14_lncrna_trn.csv', 
	'rawcount_pot_cont_hot_14_lncrna', :degdir || 'rawcount_pot_cont_hot_14_lncrna_trn.csv');	
select deg_lncrna('pot_cold_hot_14_lncrna', :degdir || 'res_pot_cold_hot_14_lncrna_trn.csv', 
	:degdir || 'res_pot_cold_hot_14_lncrna_up.csv', :degdir || 'res_pot_cold_hot_14_lncrna_down.csv',
	'normcount_pot_cold_hot_14_lncrna', :degdir || 'normcount_pot_cold_hot_14_lncrna_trn.csv', 
	'rawcount_pot_cold_hot_14_lncrna', :degdir || 'rawcount_pot_cold_hot_14_lncrna_trn.csv');		
	
select deg_lncrna('pot_cont_cold_21_lncrna', :degdir || 'res_pot_cont_cold_21_lncrna_trn.csv', 
	:degdir || 'res_pot_cont_cold_21_lncrna_up.csv', :degdir || 'res_pot_cont_cold_21_lncrna_down.csv',
	'normcount_pot_cont_cold_21_lncrna', :degdir || 'normcount_pot_cont_cold_21_lncrna_trn.csv', 
	'rawcount_pot_cont_cold_21_lncrna', :degdir || 'rawcount_pot_cont_cold_21_lncrna_trn.csv');
select deg_lncrna('pot_cont_hot_21_lncrna', :degdir || 'res_pot_cont_hot_21_lncrna_trn.csv', 
	:degdir || 'res_pot_cont_hot_21_lncrna_up.csv', :degdir || 'res_pot_cont_hot_21_lncrna_down.csv',
	'normcount_pot_cont_hot_21_lncrna', :degdir || 'normcount_pot_cont_hot_21_lncrna_trn.csv', 
	'rawcount_pot_cont_hot_21_lncrna', :degdir || 'rawcount_pot_cont_hot_21_lncrna_trn.csv');	
select deg_lncrna('pot_cold_hot_21_lncrna', :degdir || 'res_pot_cold_hot_21_lncrna_trn.csv', 
	:degdir || 'res_pot_cold_hot_21_lncrna_up.csv', :degdir || 'res_pot_cold_hot_21_lncrna_down.csv',
	'normcount_pot_cold_hot_21_lncrna', :degdir || 'normcount_pot_cold_hot_21_lncrna_trn.csv', 
	'rawcount_pot_cold_hot_21_lncrna', :degdir || 'rawcount_pot_cold_hot_21_lncrna_trn.csv');	
	

select deg_lncrna('tom_cont_cold_7_lncrna', :degdir || 'res_tom_cont_cold_7_lncrna_trn.csv', 
	:degdir || 'res_tom_cont_cold_7_lncrna_up.csv', :degdir || 'res_tom_cont_cold_7_lncrna_down.csv',
	'normcount_tom_cont_cold_7_lncrna', :degdir || 'normcount_tom_cont_cold_7_lncrna_trn.csv', 
	'rawcount_tom_cont_cold_7_lncrna', :degdir || 'rawcount_tom_cont_cold_7_lncrna_trn.csv');
select deg_lncrna('tom_cont_hot_7_lncrna', :degdir || 'res_tom_cont_hot_7_lncrna_trn.csv', 
	:degdir || 'res_tom_cont_hot_7_lncrna_up.csv', :degdir || 'res_tom_cont_hot_7_lncrna_down.csv',
	'normcount_tom_cont_hot_7_lncrna', :degdir || 'normcount_tom_cont_hot_7_lncrna_trn.csv', 
	'rawcount_tom_cont_hot_7_lncrna', :degdir || 'rawcount_tom_cont_hot_7_lncrna_trn.csv');	
select deg_lncrna('tom_cold_hot_7_lncrna', :degdir || 'res_tom_cold_hot_7_lncrna_trn.csv', 
	:degdir || 'res_tom_cold_hot_7_lncrna_up.csv', :degdir || 'res_tom_cold_hot_7_lncrna_down.csv',
	'normcount_tom_cold_hot_7_lncrna', :degdir || 'normcount_tom_cold_hot_7_lncrna_trn.csv', 
	'rawcount_tom_cold_hot_7_lncrna', :degdir || 'rawcount_tom_cold_hot_7_lncrna_trn.csv');	

select deg_lncrna('tom_cont_cold_14_lncrna', :degdir || 'res_tom_cont_cold_14_lncrna_trn.csv', 
	:degdir || 'res_tom_cont_cold_14_lncrna_up.csv', :degdir || 'res_tom_cont_cold_14_lncrna_down.csv',
	'normcount_tom_cont_cold_14_lncrna', :degdir || 'normcount_tom_cont_cold_14_lncrna_trn.csv', 
	'rawcount_tom_cont_cold_14_lncrna', :degdir || 'rawcount_tom_cont_cold_14_lncrna_trn.csv');
select deg_lncrna('tom_cont_hot_14_lncrna', :degdir || 'res_tom_cont_hot_14_lncrna_trn.csv', 
	:degdir || 'res_tom_cont_hot_14_lncrna_up.csv', :degdir || 'res_tom_cont_hot_14_lncrna_down.csv',
	'normcount_tom_cont_hot_14_lncrna', :degdir || 'normcount_tom_cont_hot_14_lncrna_trn.csv', 
	'rawcount_tom_cont_hot_14_lncrna', :degdir || 'rawcount_tom_cont_hot_14_lncrna_trn.csv');	
select deg_lncrna('tom_cold_hot_14_lncrna', :degdir || 'res_tom_cold_hot_14_lncrna_trn.csv', 
	:degdir || 'res_tom_cold_hot_14_lncrna_up.csv', :degdir || 'res_tom_cold_hot_14_lncrna_down.csv',
	'normcount_tom_cold_hot_14_lncrna', :degdir || 'normcount_tom_cold_hot_14_lncrna_trn.csv', 
	'rawcount_tom_cold_hot_14_lncrna', :degdir || 'rawcount_tom_cold_hot_14_lncrna_trn.csv');		
	
select deg_lncrna('tom_cont_cold_21_lncrna', :degdir || 'res_tom_cont_cold_21_lncrna_trn.csv', 
	:degdir || 'res_tom_cont_cold_21_lncrna_up.csv', :degdir || 'res_tom_cont_cold_21_lncrna_down.csv',
	'normcount_tom_cont_cold_21_lncrna', :degdir || 'normcount_tom_cont_cold_21_lncrna_trn.csv', 
	'rawcount_tom_cont_cold_21_lncrna', :degdir || 'rawcount_tom_cont_cold_21_lncrna_trn.csv');
select deg_lncrna('tom_cont_hot_21_lncrna', :degdir || 'res_tom_cont_hot_21_lncrna_trn.csv', 
	:degdir || 'res_tom_cont_hot_21_lncrna_up.csv', :degdir || 'res_tom_cont_hot_21_lncrna_down.csv',
	'normcount_tom_cont_hot_21_lncrna', :degdir || 'normcount_tom_cont_hot_21_lncrna_trn.csv', 
	'rawcount_tom_cont_hot_21_lncrna', :degdir || 'rawcount_tom_cont_hot_21_lncrna_trn.csv');	
select deg_lncrna('tom_cold_hot_21_lncrna', :degdir || 'res_tom_cold_hot_21_lncrna_trn.csv', 
	:degdir || 'res_tom_cold_hot_21_lncrna_up.csv', :degdir || 'res_tom_cold_hot_21_lncrna_down.csv',
	'normcount_tom_cold_hot_21_lncrna', :degdir || 'normcount_tom_cold_hot_21_lncrna_trn.csv', 
	'rawcount_tom_cold_hot_21_lncrna', :degdir || 'rawcount_tom_cold_hot_21_lncrna_trn.csv');	
	
	
	
--create DEG stat table	for lncrna
drop function if exists deg_stat();
create or replace function deg_stat_lncrna(_deg_stat_table varchar(20), _conds varchar(20), 
	_deg_table_pot varchar(20), _deg_table_tom varchar(20))
    returns void as
    $body$
    begin
		execute format('update %I set pot_up=(select count(*) from %I where log2fc>=1 and padj<0.05) 
			where conds=''%I'' ', _deg_stat_table, _deg_table_pot, _conds);
		execute format('update %I set pot_down=(select count(*) from %I where log2fc<=-1 and 
			padj<0.05) where conds=''%I'' ', _deg_stat_table, _deg_table_pot, _conds);
		execute format('update %I set tom_up=(select count(*) from %I where log2fc>=1 and padj<0.05) 
			where conds=''%I'' ', _deg_stat_table, _deg_table_tom, _conds);
		execute format('update %I set tom_down=(select count(*) from %I where log2fc<=-1 and 
			padj<0.05) where conds=''%I'' ', _deg_stat_table, _deg_table_tom, _conds);
	end
    $body$ language "plpgsql";
		


drop table if exists pot_tom_lncrna_deg_stat;
create table pot_tom_lncrna_deg_stat as select conds from pot_tom_trn_deg_stat;
alter table pot_tom_lncrna_deg_stat add pot_up int, add pot_down int, add tom_up int, add tom_down 
	int;
select deg_stat_lncrna('pot_tom_lncrna_deg_stat', 'cnvscd7', 'pot_cont_cold_7_lncrna', 
	'tom_cont_cold_7_lncrna');
select deg_stat_lncrna('pot_tom_lncrna_deg_stat', 'cnvsht7', 'pot_cont_hot_7_lncrna', 
	'tom_cont_hot_7_lncrna' );
select deg_stat_lncrna('pot_tom_lncrna_deg_stat', 'cdvsht7', 'pot_cold_hot_7_lncrna',  
	'tom_cold_hot_7_lncrna');
select deg_stat_lncrna('pot_tom_lncrna_deg_stat', 'cnvscd14', 'pot_cont_cold_14_lncrna', 
	'tom_cont_cold_14_lncrna');
select deg_stat_lncrna('pot_tom_lncrna_deg_stat', 'cnvsht14', 'pot_cont_hot_14_lncrna', 
	'tom_cont_hot_14_lncrna' );
select deg_stat_lncrna('pot_tom_lncrna_deg_stat', 'cdvsht14', 'pot_cold_hot_14_lncrna', 
	'tom_cold_hot_14_lncrna');
select deg_stat_lncrna('pot_tom_lncrna_deg_stat', 'cnvscd21', 'pot_cont_cold_21_lncrna',  
	'tom_cont_cold_21_lncrna');
select deg_stat_lncrna('pot_tom_lncrna_deg_stat', 'cnvsht21', 'pot_cont_hot_21_lncrna', 
	'tom_cont_hot_21_lncrna');
select deg_stat_lncrna('pot_tom_lncrna_deg_stat', 'cdvsht21', 'pot_cold_hot_21_lncrna', 
	'tom_cold_hot_21_lncrna');	
*/


--Import potato lincRNA GTF file
drop table if exists pot_lncrna_gtf;
create table pot_lncrna_gtf (chr varchar(10), src varchar(20), feature varchar(20), st int, ende int, scr varchar(5),
    str varchar(5), frm varchar(5), attr varchar(500) );
\copy table pot_lncrna_gtf '/scratch/user/ren_net/project/ptp_lncrna/data_obt/Kranthi_Potato_Final.gtf';
alter table pot_lncrna_gtf add gene_id varchar(20), add trn varchar(20), add anot varchar(200);