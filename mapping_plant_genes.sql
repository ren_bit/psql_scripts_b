/**
/* Sbicolor and rice mapping */
Drop table if exists blastn_sb_os;
	create table blastn_sb_os
	(qid varchar(30),
	qlen int,
	sid varchar(30),
	slen int,
	qstart int,
	qend int,
	sstart int,
	send int, 
	nident int,
	pident double precision,
	alen int,
	mm int,
	gp int,
	qcov double precision,
	eval double precision,
	bs double precision);
	
\copy blastn_sb_os from 'R:\project\db\sbicolor\blastn_sb_os.txt';

Drop table if exists rice_other_map;
create table rice_other_map
	(rice_id varchar(30),
	 sb_id varchar(30),
	 sb_gene varchar(30));
	 
insert into rice_other_map (rice_id) select id from rice_msu_7;
update rice_other_map as a set sb_id=b.qid from blastn_sb_os as b where a.rice_id=b.sid;
update rice_other_map set sb_gene=substring(sb_id from 1 for 16);
update rice_other_map set sb_gene=substring(sb_id from 1 for 13) where sb_gene is null;

drop table blastn_sb_os;

**/
/* Sbicolor and Atha mapping */
Drop table if exists blastn_sb_at;
	create table blastn_sb_at
	(qid varchar(30),
	qlen int,
	sid varchar(30),
	slen int,
	qstart int,
	qend int,
	sstart int,
	send int, 
	nident int,
	pident double precision,
	alen int,
	mm int,
	gp int,
	qcov double precision,
	eval double precision,
	bs double precision);
	
\copy blastn_sb_at from 'R:\project\db\sbicolor\blastn_sb_at.txt';

Drop table if exists at_other_map;
create table at_other_map
	(at_id varchar(30),
	 sb_id varchar(30),
	 sb_gene varchar(30));
	
drop table if exists temp;
create table temp (id varchar(30));	
\copy temp from 'R:\project\db\arabidopsis\TAIR10_all_gene_models.txt';
insert into at_other_map (at_id) select id from temp;
drop table temp;

update at_other_map as a set sb_id=b.qid from blastn_sb_at as b where a.at_id=b.sid;
update at_other_map set sb_gene=substring(sb_id from 1 for 16) where length(sb_id)=18;
update at_other_map set sb_gene=substring(sb_id from 1 for 13) where length(sb_id)=15;

drop table blastn_sb_at;
