/* Tomato pysllid Avila */
/*
/* load csv table data */
drop table if exists cm_cold_hot_bg_trn;
create table cm_cold_hot_bg_trn
		(geneNames varchar(20),
		geneIDs varchar(30),
		feature varchar(20),
		id int,
		fc double precision,
		pval double precision,
		qval double precision
		);
\copy cm_cold_hot_bg_trn from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_cold_hot_filt_det.csv' with csv header delimiter as ',';

drop table if exists cm_con_cold_bg_trn;
create table cm_con_cold_bg_trn
		(geneNames varchar(20),
		geneIDs varchar(30),
		feature varchar(20),
		id int,
		fc double precision,
		pval double precision,
		qval double precision
		 );
\copy cm_con_cold_bg_trn from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_con_cold_filt_det.csv' with csv header delimiter as ',';

drop table if exists cm_con_hot_bg_trn;
create table cm_con_hot_bg_trn
		(geneNames varchar(20),
		geneIDs varchar(30),
		feature varchar(20),
		id int,
		fc double precision,
		pval double precision,
		qval double precision
		 );
\copy cm_con_hot_bg_trn from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_con_hot_filt_det.csv' with csv header delimiter as ',';

drop table if exists la_cold_hot_bg_trn;
create table la_cold_hot_bg_trn
		(geneNames varchar(20),
		geneIDs varchar(30),
		feature varchar(20),
		id int,
		fc double precision,
		pval double precision,
		qval double precision
		 );
\copy la_cold_hot_bg_trn from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_la_cold_hot_filt_det.csv' with csv header delimiter as ',';

drop table if exists la_con_cold_bg_trn;
create table la_con_cold_bg_trn
		(geneNames varchar(20),
		geneIDs varchar(30),
		feature varchar(20),
		id int,
		fc double precision,
		pval double precision,
		qval double precision
		 );
\copy la_con_cold_bg_trn from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_la_con_cold_filt_det.csv' with csv header delimiter as ',';

drop table if exists la_con_hot_bg_trn;
create table la_con_hot_bg_trn
		(geneNames varchar(20),
		geneIDs varchar(30),
		feature varchar(20),
		id int,
		fc double precision,
		pval double precision,
		qval double precision
		 );
\copy la_con_hot_bg_trn from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_la_con_hot_filt_det.csv' with csv header delimiter as ',';

drop table if exists cmc_lac_bg_trn;
create table cmc_lac_bg_trn
		(geneNames varchar(20),
		geneIDs varchar(30),
		feature varchar(20),
		id int,
		fc double precision,
		pval double precision,
		qval double precision
		 );
\copy cmc_lac_bg_trn from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_cmc_lac_filt_det.csv' with csv header delimiter as ',';

drop table if exists cmcon_lacon_bg_trn;
create table cmcon_lacon_bg_trn
		(geneNames varchar(20),
		geneIDs varchar(30),
		feature varchar(20),
		id int,
		fc double precision,
		pval double precision,
		qval double precision
		 );
\copy cmcon_lacon_bg_trn from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_cmcon_lacon_filt_det.csv' with csv header delimiter as ',';

drop table if exists cmh_lah_bg_trn;
create table cmh_lah_bg_trn
		(geneNames varchar(20),
		geneIDs varchar(30),
		feature varchar(20),
		id int,
		fc double precision,
		pval double precision,
		qval double precision
		 );
\copy cmh_lah_bg_trn from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_cmh_lah_filt_det.csv' with csv header delimiter as ',';



/* add annotation to table data */
alter table cm_cold_hot_bg_trn add log2fc double precision, add anot varchar(300);
alter table cm_con_cold_bg_trn add log2fc double precision, add anot varchar(300);
alter table cm_con_hot_bg_trn add log2fc double precision, add anot varchar(300);
alter table la_cold_hot_bg_trn add log2fc double precision, add anot varchar(300);
alter table la_con_cold_bg_trn add log2fc double precision, add anot varchar(300);
alter table la_con_hot_bg_trn add log2fc double precision, add anot varchar(300);
alter table cmc_lac_bg_trn add log2fc double precision, add anot varchar(300);
alter table cmcon_lacon_bg_trn add log2fc double precision, add anot varchar(300);
alter table cmh_lah_bg_trn add log2fc double precision, add anot varchar(300);

update cm_cold_hot_bg_trn as a set anot=b.anot from tomato_itag3_anot as b where a.geneNames=b.gene;
update cm_con_cold_bg_trn as a set anot=b.anot from tomato_itag3_anot as b where a.geneNames=b.gene;
update cm_con_hot_bg_trn as a set anot=b.anot from tomato_itag3_anot as b where a.geneNames=b.gene;
update la_cold_hot_bg_trn as a set anot=b.anot from tomato_itag3_anot as b where a.geneNames=b.gene;
update la_con_cold_bg_trn as a set anot=b.anot from tomato_itag3_anot as b where a.geneNames=b.gene;
update la_con_hot_bg_trn as a set anot=b.anot from tomato_itag3_anot as b where a.geneNames=b.gene;
update cmc_lac_bg_trn as a set anot=b.anot from tomato_itag3_anot as b where a.geneNames=b.gene;
update cmcon_lacon_bg_trn as a set anot=b.anot from tomato_itag3_anot as b where a.geneNames=b.gene;
update cmh_lah_bg_trn as a set anot=b.anot from tomato_itag3_anot as b where a.geneNames=b.gene;

update cm_cold_hot_bg_trn set log2fc=log(2.0,cast(fc as numeric));
update cm_con_cold_bg_trn set log2fc=log(2.0,cast(fc as numeric));
update cm_con_hot_bg_trn set log2fc=log(2.0,cast(fc as numeric));
update la_cold_hot_bg_trn set log2fc=log(2.0,cast(fc as numeric));
update la_con_cold_bg_trn set log2fc=log(2.0,cast(fc as numeric));
update la_con_hot_bg_trn set log2fc=log(2.0,cast(fc as numeric));
update cmc_lac_bg_trn set log2fc=log(2.0,cast(fc as numeric));
update cmcon_lacon_bg_trn set log2fc=log(2.0,cast(fc as numeric));
update cmh_lah_bg_trn set log2fc=log(2.0,cast(fc as numeric));

/* download tables */
/* order by used to put genenames as . at end */
\copy (select * from cm_cold_hot_bg_trn where pval<0.05 order by geneNames desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\sign_gene_exp\cm_cold_hot_bg_trn_sign.csv' with (FORMAT csv, header true);
\copy (select * from cm_con_cold_bg_trn where pval<0.05 order by geneNames desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\sign_gene_exp\cm_con_cold_bg_trn_sign.csv' with (FORMAT csv, header true);
\copy (select * from cm_con_hot_bg_trn where pval<0.05 order by geneNames desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\sign_gene_exp\cm_con_hot_bg_trn_sign.csv' with (FORMAT csv, header true);
\copy (select * from la_cold_hot_bg_trn where pval<0.05 order by geneNames desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\sign_gene_exp\la_cold_hot_bg_trn_sign.csv' with (FORMAT csv, header true);
\copy (select * from la_con_cold_bg_trn where pval<0.05 order by geneNames desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\sign_gene_exp\la_con_cold_bg_trn_sign.csv' with (FORMAT csv, header true);
\copy (select * from la_con_hot_bg_trn where pval<0.05 order by geneNames desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\sign_gene_exp\la_con_hot_bg_trn_sign.csv' with (FORMAT csv, header true);
\copy (select * from cmc_lac_bg_trn where pval<0.05 order by geneNames desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\sign_gene_exp\cmc_lac_bg_trn_sign.csv' with (FORMAT csv, header true);
\copy (select * from cmcon_lacon_bg_trn where pval<0.05 order by geneNames desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\sign_gene_exp\cmcon_lacon_bg_trn_sign.csv' with (FORMAT csv, header true);
\copy (select * from cmh_lah_bg_trn where pval<0.05 order by geneNames desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\sign_gene_exp\cmh_lah_bg_trn_sign.csv' with (FORMAT csv, header true);


/* add expression count table from bg */
drop table if exists cm_cold_hot_bg_filt_expresval;
create table cm_cold_hot_bg_filt_expresval
		(tid int,
		chr varchar(10),
		std varchar(5),
		start int,
		ennd int,
		tname varchar(30),
		numExons int,
		len int,
		geneIDs varchar(30),
		geneNames varchar(20),
		covcoldrep1 double precision,
		fpkmcoldrep1 double precision,
		covcoldrep2 double precision,
		fpkmcoldrep2 double precision,
		covcoldrep3 double precision,
		fpkmcoldrep3 double precision,
		covhotrep1 double precision,
		fpkmhotrep1 double precision,
		covhotrep2 double precision,
		fpkmhotrep2 double precision,
		covhotrep3 double precision,
		fpkmhotrep3 double precision
		 );
\copy cm_cold_hot_bg_filt_expresval from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_cold_hot_filt_expres_val.csv' with csv header delimiter as ',';

drop table if exists cm_con_cold_bg_filt_expresval;
create table cm_con_cold_bg_filt_expresval
		(tid int,
		chr varchar(10),
		std varchar(5),
		start int,
		ennd int,
		tname varchar(30),
		numExons int,
		len int,
		geneIDs varchar(30),
		geneNames varchar(20),
		covconrep1 double precision,
		fpkmconrep1 double precision,
		covconrep2 double precision,
		fpkmconrep2 double precision,
		covconrep3 double precision,
		fpkmconrep3 double precision,
		covcoldrep1 double precision,
		fpkmcoldrep1 double precision,
		covcoldrep2 double precision,
		fpkmcoldrep2 double precision,
		covcoldrep3 double precision,
		fpkmcoldrep3 double precision
		 );
\copy cm_con_cold_bg_filt_expresval from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_con_cold_filt_expres_val.csv' with csv header delimiter as ',';

drop table if exists cm_con_hot_bg_filt_expresval;
create table cm_con_hot_bg_filt_expresval
		(tid int,
		chr varchar(10),
		std varchar(5),
		start int,
		ennd int,
		tname varchar(30),
		numExons int,
		len int,
		geneIDs varchar(30),
		geneNames varchar(20),
		covconrep1 double precision,
		fpkmconrep1 double precision,
		covconrep2 double precision,
		fpkmconrep2 double precision,
		covconrep3 double precision,
		fpkmconrep3 double precision,
		covhotrep1 double precision,
		fpkmhotrep1 double precision,
		covhotrep2 double precision,
		fpkmhotrep2 double precision,
		covhotrep3 double precision,
		fpkmhotrep3 double precision
		 );
\copy cm_con_hot_bg_filt_expresval from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_con_hot_filt_expres_val.csv' with csv header delimiter as ',';

drop table if exists la_cold_hot_bg_filt_expresval;
create table la_cold_hot_bg_filt_expresval
		(tid int,
		chr varchar(10),
		std varchar(5),
		start int,
		ennd int,
		tname varchar(30),
		numExons int,
		len int,
		geneIDs varchar(30),
		geneNames varchar(20),
		covcoldrep1 double precision,
		fpkmcoldrep1 double precision,
		covcoldrep2 double precision,
		fpkmcoldrep2 double precision,
		covcoldrep3 double precision,
		fpkmcoldrep3 double precision,
		covhotrep1 double precision,
		fpkmhotrep1 double precision,
		covhotrep2 double precision,
		fpkmhotrep2 double precision,
		covhotrep3 double precision,
		fpkmhotrep3 double precision
		 );
\copy la_cold_hot_bg_filt_expresval from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_la_cold_hot_filt_expres_val.csv' with csv header delimiter as ',';

drop table if exists la_con_cold_bg_filt_expresval;
create table la_con_cold_bg_filt_expresval
		(tid int,
		chr varchar(10),
		std varchar(5),
		start int,
		ennd int,
		tname varchar(30),
		numExons int,
		len int,
		geneIDs varchar(30),
		geneNames varchar(20),
		covconrep1 double precision,
		fpkmconrep1 double precision,
		covconrep2 double precision,
		fpkmconrep2 double precision,
		covconrep3 double precision,
		fpkmconrep3 double precision,
		covcoldrep1 double precision,
		fpkmcoldrep1 double precision,
		covcoldrep2 double precision,
		fpkmcoldrep2 double precision,
		covcoldrep3 double precision,
		fpkmcoldrep3 double precision
		 );
\copy la_con_cold_bg_filt_expresval from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_la_con_cold_filt_expres_val.csv' with csv header delimiter as ',';

drop table if exists la_con_hot_bg_filt_expresval;
create table la_con_hot_bg_filt_expresval
		(tid int,
		chr varchar(10),
		std varchar(5),
		start int,
		ennd int,
		tname varchar(30),
		numExons int,
		len int,
		geneIDs varchar(30),
		geneNames varchar(20),
		covconrep1 double precision,
		fpkmconrep1 double precision,
		covconrep2 double precision,
		fpkmconrep2 double precision,
		covconrep3 double precision,
		fpkmconrep3 double precision,
		covhotrep1 double precision,
		fpkmhotrep1 double precision,
		covhotrep2 double precision,
		fpkmhotrep2 double precision,
		covhotrep3 double precision,
		fpkmhotrep3 double precision
		 );
\copy la_con_hot_bg_filt_expresval from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_la_con_hot_filt_expres_val.csv' with csv header delimiter as ',';

drop table if exists cmc_lac_bg_filt_expresval;
create table cmc_lac_bg_filt_expresval
		(tid int,
		chr varchar(10),
		std varchar(5),
		start int,
		ennd int,
		tname varchar(30),
		numExons int,
		len int,
		geneIDs varchar(30),
		geneNames varchar(20),
		covcmcrep1 double precision,
		fpkmcmcrep1 double precision,
		covcmcrep2 double precision,
		fpkmcmcrep2 double precision,
		covcmcrep3 double precision,
		fpkmcmcrep3 double precision,
		covlacrep1 double precision,
		fpkmlacrep1 double precision,
		covlacrep2 double precision,
		fpkmlacrep2 double precision,
		covlacrep3 double precision,
		fpkmlacrep3 double precision
		 );
\copy cmc_lac_bg_filt_expresval from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_cmc_lac_filt_expres_val.csv' with csv header delimiter as ',';

drop table if exists cmcon_lacon_bg_filt_expresval;
create table cmcon_lacon_bg_filt_expresval
		(tid int,
		chr varchar(10),
		std varchar(5),
		start int,
		ennd int,
		tname varchar(30),
		numExons int,
		len int,
		geneIDs varchar(30),
		geneNames varchar(20),
		covcmconrep1 double precision,
		fpkmcmconrep1 double precision,
		covcmconrep2 double precision,
		fpkmcmconrep2 double precision,
		covcmconrep3 double precision,
		fpkmcmconrep3 double precision,
		covlaconrep1 double precision,
		fpkmlaconrep1 double precision,
		covlaconrep2 double precision,
		fpkmlaconrep2 double precision,
		covlaconrep3 double precision,
		fpkmlaconrep3 double precision
		 );
\copy cmcon_lacon_bg_filt_expresval from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_cmcon_lacon_filt_expres_val.csv' with csv header delimiter as ',';

drop table if exists cmh_lah_bg_filt_expresval;
create table cmh_lah_bg_filt_expresval
		(tid int,
		chr varchar(10),
		std varchar(5),
		start int,
		ennd int,
		tname varchar(30),
		numExons int,
		len int,
		geneIDs varchar(30),
		geneNames varchar(20),
		covcmhrep1 double precision,
		fpkmcmhrep1 double precision,
		covcmhrep2 double precision,
		fpkmcmhrep2 double precision,
		covcmhrep3 double precision,
		fpkmcmhrep3 double precision,
		covlahrep1 double precision,
		fpkmlahrep1 double precision,
		covlahrep2 double precision,
		fpkmlahrep2 double precision,
		covlahrep3 double precision,
		fpkmlahrep3 double precision
		 );
\copy cmh_lah_bg_filt_expresval from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\gene_exp\\bg_cmh_lah_filt_expres_val.csv' with csv header delimiter as ',';



/* add FC to express val tables */
alter table cm_cold_hot_bg_filt_expresval add fc double precision;
alter table cm_con_cold_bg_filt_expresval add fc double precision;
alter table cm_con_hot_bg_filt_expresval add fc double precision;
alter table la_cold_hot_bg_filt_expresval add fc double precision;
alter table la_con_cold_bg_filt_expresval add fc double precision;
alter table la_con_hot_bg_filt_expresval add fc double precision;
alter table cmc_lac_bg_filt_expresval add fc double precision;
alter table cmcon_lacon_bg_filt_expresval add fc double precision;
alter table cmh_lah_bg_filt_expresval add fc double precision;

update cm_cold_hot_bg_filt_expresval as a set fc=b.fc from cm_cold_hot_bg_trn as b where a.geneIDs=b.geneIDs;
update cm_con_cold_bg_filt_expresval as a set fc=b.fc from cm_cold_hot_bg_trn as b where a.geneIDs=b.geneIDs;
update cm_con_hot_bg_filt_expresval as a set fc=b.fc from cm_cold_hot_bg_trn as b where a.geneIDs=b.geneIDs;
update la_cold_hot_bg_filt_expresval as a set fc=b.fc from cm_cold_hot_bg_trn as b where a.geneIDs=b.geneIDs;
update la_con_cold_bg_filt_expresval as a set fc=b.fc from cm_cold_hot_bg_trn as b where a.geneIDs=b.geneIDs;
update la_con_hot_bg_filt_expresval as a set fc=b.fc from cm_cold_hot_bg_trn as b where a.geneIDs=b.geneIDs;
update cmc_lac_bg_filt_expresval as a set fc=b.fc from cm_cold_hot_bg_trn as b where a.geneIDs=b.geneIDs;
update cmcon_lacon_bg_filt_expresval as a set fc=b.fc from cm_cold_hot_bg_trn as b where a.geneIDs=b.geneIDs;
update cmh_lah_bg_filt_expresval as a set fc=b.fc from cm_cold_hot_bg_trn as b where a.geneIDs=b.geneIDs;

*/

/*
/* Stringtie - DESeq2 analysis */
/* load csv table data */
drop table if exists cmcon_cmcold_deg_dseq2;
create table cmcon_cmcold_deg_dseq2
		(geneNames text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy cmcon_cmcold_deg_dseq2 from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcon_cmcold_deg_dseq2.csv' with csv header delimiter as ',';

drop table if exists cmcon_cmhot_deg_dseq2;
create table cmcon_cmhot_deg_dseq2
		(geneNames text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy cmcon_cmhot_deg_dseq2 from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcon_cmhot_deg_dseq2.csv' with csv header delimiter as ',';

drop table if exists cmcold_cmhot_deg_dseq2;
create table cmcold_cmhot_deg_dseq2
		(geneNames text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy cmcold_cmhot_deg_dseq2 from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcold_cmhot_deg_dseq2.csv' with csv header delimiter as ',';

drop table if exists lacon_lacold_deg_dseq2;
create table lacon_lacold_deg_dseq2
		(geneNames text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy lacon_lacold_deg_dseq2 from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\lacon_lacold_deg_dseq2.csv' with csv header delimiter as ',';

drop table if exists lacon_lahot_deg_dseq2;
create table lacon_lahot_deg_dseq2
		(geneNames text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy lacon_lahot_deg_dseq2 from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\lacon_lahot_deg_dseq2.csv' with csv header delimiter as ',';

drop table if exists lacold_lahot_deg_dseq2;
create table lacold_lahot_deg_dseq2
		(geneNames text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy lacold_lahot_deg_dseq2 from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\lacold_lahot_deg_dseq2.csv' with csv header delimiter as ',';

drop table if exists cmcon_lacon_deg_dseq2;
create table cmcon_lacon_deg_dseq2
		(geneNames text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy cmcon_lacon_deg_dseq2 from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcon_lacon_deg_dseq2.csv' with csv header delimiter as ',';

drop table if exists cmcold_lacold_deg_dseq2;
create table cmcold_lacold_deg_dseq2
		(geneNames text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy cmcold_lacold_deg_dseq2 from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcold_lacold_deg_dseq2.csv' with csv header delimiter as ',';

drop table if exists cmhot_lahot_deg_dseq2;
create table cmhot_lahot_deg_dseq2
		(geneNames text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy cmhot_lahot_deg_dseq2 from 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmhot_lahot_deg_dseq2.csv' with csv header delimiter as ',';

/* replace pval and padj with null for NA */
update cmcon_cmcold_deg_dseq2 set pval=NULL where pval='NA';
update cmcon_cmcold_deg_dseq2 set padj=NULL where padj='NA';
update cmcon_cmhot_deg_dseq2 set pval=NULL where pval='NA';
update cmcon_cmhot_deg_dseq2 set padj=NULL where padj='NA';
update cmcold_cmhot_deg_dseq2 set pval=NULL where pval='NA';
update cmcold_cmhot_deg_dseq2 set padj=NULL where padj='NA';
update lacon_lacold_deg_dseq2 set pval=NULL where pval='NA';
update lacon_lacold_deg_dseq2 set padj=NULL where padj='NA';
update lacon_lahot_deg_dseq2 set pval=NULL where pval='NA';
update lacon_lahot_deg_dseq2 set padj=NULL where padj='NA';
update lacold_lahot_deg_dseq2 set pval=NULL where pval='NA';
update lacold_lahot_deg_dseq2 set padj=NULL where padj='NA';
update cmcon_lacon_deg_dseq2 set pval=NULL where pval='NA';
update cmcon_lacon_deg_dseq2 set padj=NULL where padj='NA';
update cmcold_lacold_deg_dseq2 set pval=NULL where pval='NA';
update cmcold_lacold_deg_dseq2 set padj=NULL where padj='NA';
update cmhot_lahot_deg_dseq2 set pval=NULL where pval='NA';
update cmhot_lahot_deg_dseq2 set padj=NULL where padj='NA';

/* cast columns to double precision */
alter table cmcon_cmcold_deg_dseq2 alter column pval type double precision using (pval::double precision);
alter table cmcon_cmcold_deg_dseq2 alter column padj type double precision using (padj::double precision);
alter table cmcon_cmhot_deg_dseq2 alter column pval type double precision using (pval::double precision);
alter table cmcon_cmhot_deg_dseq2 alter column padj type double precision using (padj::double precision);
alter table cmcold_cmhot_deg_dseq2 alter column pval type double precision using (pval::double precision);
alter table cmcold_cmhot_deg_dseq2 alter column padj type double precision using (padj::double precision);
alter table lacon_lacold_deg_dseq2 alter column pval type double precision using (pval::double precision);
alter table lacon_lacold_deg_dseq2 alter column padj type double precision using (padj::double precision);
alter table lacon_lahot_deg_dseq2 alter column pval type double precision using (pval::double precision);
alter table lacon_lahot_deg_dseq2 alter column padj type double precision using (padj::double precision);
alter table lacold_lahot_deg_dseq2 alter column pval type double precision using (pval::double precision);
alter table lacold_lahot_deg_dseq2 alter column padj type double precision using (padj::double precision);
alter table cmcon_lacon_deg_dseq2 alter column pval type double precision using (pval::double precision);
alter table cmcon_lacon_deg_dseq2 alter column padj type double precision using (padj::double precision);
alter table cmcold_lacold_deg_dseq2 alter column pval type double precision using (pval::double precision);
alter table cmcold_lacold_deg_dseq2 alter column padj type double precision using (padj::double precision);
alter table cmhot_lahot_deg_dseq2 alter column pval type double precision using (pval::double precision);
alter table cmhot_lahot_deg_dseq2 alter column padj type double precision using (padj::double precision);

/* extract gene names from genenames */
alter table cmcon_cmcold_deg_dseq2 add gene varchar(20);
alter table cmcon_cmhot_deg_dseq2 add gene varchar(20);
alter table cmcold_cmhot_deg_dseq2 add gene varchar(20);
alter table lacon_lacold_deg_dseq2 add gene varchar(20);
alter table lacon_lahot_deg_dseq2 add gene varchar(20);
alter table lacold_lahot_deg_dseq2 add gene varchar(20);
alter table cmcon_lacon_deg_dseq2 add gene varchar(20);
alter table cmcold_lacold_deg_dseq2 add gene varchar(20);
alter table cmhot_lahot_deg_dseq2 add gene varchar(20);

update cmcon_cmcold_deg_dseq2 set gene=substring(geneNames from 6 for 16) where geneNames like '%Soly%';
update cmcon_cmhot_deg_dseq2 set gene=substring(geneNames from 6 for 16) where geneNames like '%Soly%';
update cmcold_cmhot_deg_dseq2 set gene=substring(geneNames from 6 for 16) where geneNames like '%Soly%';
update lacon_lacold_deg_dseq2 set gene=substring(geneNames from 6 for 16) where geneNames like '%Soly%';
update lacon_lahot_deg_dseq2 set gene=substring(geneNames from 6 for 16) where geneNames like '%Soly%';
update lacold_lahot_deg_dseq2 set gene=substring(geneNames from 6 for 16) where geneNames like '%Soly%';
update cmcon_lacon_deg_dseq2 set gene=substring(geneNames from 6 for 16) where geneNames like '%Soly%';
update cmcold_lacold_deg_dseq2 set gene=substring(geneNames from 6 for 16) where geneNames like '%Soly%';
update cmhot_lahot_deg_dseq2 set gene=substring(geneNames from 6 for 16) where geneNames like '%Soly%';

update cmcon_cmcold_deg_dseq2 set gene=geneNames where gene is null;
update cmcon_cmhot_deg_dseq2 set gene=geneNames where gene is null;
update cmcold_cmhot_deg_dseq2 set gene=geneNames where gene is null;
update lacon_lacold_deg_dseq2 set gene=geneNames where gene is null;
update lacon_lahot_deg_dseq2 set gene=geneNames where gene is null;
update lacold_lahot_deg_dseq2 set gene=geneNames where gene is null;
update cmcon_lacon_deg_dseq2 set gene=geneNames where gene is null;
update cmcold_lacold_deg_dseq2 set gene=geneNames where gene is null;
update cmhot_lahot_deg_dseq2 set gene=geneNames where gene is null;

/* Add annotation */
alter table cmcon_cmcold_deg_dseq2 add anot varchar(300);
alter table cmcon_cmhot_deg_dseq2 add anot varchar(300);
alter table cmcold_cmhot_deg_dseq2 add anot varchar(300);
alter table lacon_lacold_deg_dseq2 add anot varchar(300);
alter table lacon_lahot_deg_dseq2 add anot varchar(300);
alter table lacold_lahot_deg_dseq2 add anot varchar(300);
alter table cmcon_lacon_deg_dseq2 add anot varchar(300);
alter table cmcold_lacold_deg_dseq2 add anot varchar(300);
alter table cmhot_lahot_deg_dseq2 add anot varchar(300);

update cmcon_cmcold_deg_dseq2 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update cmcon_cmhot_deg_dseq2 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update cmcold_cmhot_deg_dseq2 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update lacon_lacold_deg_dseq2 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update lacon_lahot_deg_dseq2 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update lacold_lahot_deg_dseq2 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update cmcon_lacon_deg_dseq2 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update cmcold_lacold_deg_dseq2 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update cmhot_lahot_deg_dseq2 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;

/* export table */
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from cmcon_cmcold_deg_dseq2 where pval<0.05 order by gene desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcon_cmcold_deg_dseq2_sign.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from cmcon_cmhot_deg_dseq2 where pval<0.05 order by gene desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcon_cmhot_deg_dseq2_sign.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from cmcold_cmhot_deg_dseq2 where pval<0.05 order by gene desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcold_cmhot_deg_dseq2_sign.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from lacon_lacold_deg_dseq2 where pval<0.05 order by gene desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\lacon_lacold_deg_dseq2_sign.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from lacon_lahot_deg_dseq2 where pval<0.05 order by gene desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\lacon_lahot_deg_dseq2_sign.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from lacold_lahot_deg_dseq2 where pval<0.05 order by gene desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\lacold_lahot_deg_dseq2_sign.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from cmcon_lacon_deg_dseq2 where pval<0.05 order by gene desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcon_lacon_deg_dseq2_sign.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from cmcold_lacold_deg_dseq2 where pval<0.05 order by gene desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmcold_lacold_deg_dseq2_sign.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from cmhot_lahot_deg_dseq2 where pval<0.05 order by gene desc) to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\cmhot_lahot_deg_dseq2_sign.csv' with (FORMAT csv, header true);
*/

/* prepare table for PCA */
drop table if exists all_comp_deg_deseq2_pca;
create table all_comp_deg_deseq2_pca as select gene from cmcon_cmcold_deg_dseq2 where pval<0.05 and gene like '%Soly%' union  select gene from cmcon_cmhot_deg_dseq2 where pval<0.05 and gene like '%Soly%' union select gene from cmcold_cmhot_deg_dseq2 where pval<0.05 and gene like '%Soly%' union select gene from lacon_lacold_deg_dseq2 where pval<0.05 and gene like '%Soly%' union select gene from lacon_lahot_deg_dseq2 where pval<0.05 and gene like '%Soly%' union select gene from lacold_lahot_deg_dseq2 where pval<0.05 and gene like '%Soly%' union select gene from cmcon_lacon_deg_dseq2 where pval<0.05 and gene like '%Soly%' union select gene from cmcold_lacold_deg_dseq2 where pval<0.05 and gene like '%Soly%' union select gene from cmhot_lahot_deg_dseq2 where pval<0.05 and gene like '%Soly%';
alter table all_comp_deg_deseq2_pca add log2fc1 double precision, add log2fc2 double precision, add log2fc3 double precision, add log2fc4 double precision, add log2fc5 double precision, add log2fc6 double precision, add log2fc7 double precision, add log2fc8 double precision, add log2fc9 double precision;
update all_comp_deg_deseq2_pca as a set log2fc1=b.log2fc from cmcon_cmcold_deg_dseq2 as b where a.gene=b.gene;
update all_comp_deg_deseq2_pca as a set log2fc2=b.log2fc from cmcon_cmhot_deg_dseq2 as b where a.gene=b.gene;
update all_comp_deg_deseq2_pca as a set log2fc3=b.log2fc from cmcold_cmhot_deg_dseq2 as b where a.gene=b.gene;
update all_comp_deg_deseq2_pca as a set log2fc4=b.log2fc from lacon_lacold_deg_dseq2 as b where a.gene=b.gene;
update all_comp_deg_deseq2_pca as a set log2fc5=b.log2fc from lacon_lahot_deg_dseq2 as b where a.gene=b.gene;
update all_comp_deg_deseq2_pca as a set log2fc6=b.log2fc from lacold_lahot_deg_dseq2 as b where a.gene=b.gene;
update all_comp_deg_deseq2_pca as a set log2fc7=b.log2fc from cmcon_lacon_deg_dseq2 as b where a.gene=b.gene;
update all_comp_deg_deseq2_pca as a set log2fc8=b.log2fc from cmcold_lacold_deg_dseq2 as b where a.gene=b.gene;
update all_comp_deg_deseq2_pca as a set log2fc9=b.log2fc from cmhot_lahot_deg_dseq2 as b where a.gene=b.gene;

update all_comp_deg_deseq2_pca set log2fc1=0 where log2fc1 is NULL;
update all_comp_deg_deseq2_pca set log2fc2=0 where log2fc2 is NULL;
update all_comp_deg_deseq2_pca set log2fc3=0 where log2fc3 is NULL;
update all_comp_deg_deseq2_pca set log2fc4=0 where log2fc4 is NULL;
update all_comp_deg_deseq2_pca set log2fc5=0 where log2fc5 is NULL;
update all_comp_deg_deseq2_pca set log2fc6=0 where log2fc6 is NULL;
update all_comp_deg_deseq2_pca set log2fc7=0 where log2fc7 is NULL;
update all_comp_deg_deseq2_pca set log2fc8=0 where log2fc8 is NULL;
update all_comp_deg_deseq2_pca set log2fc9=0 where log2fc9 is NULL;

\copy all_comp_deg_deseq2_pca to 'R:\project\potato_zebrachip\analysis\strintie_bg_analysis\deseq2\all_comp_deg_deseq2_pca.csv' with (FORMAT csv, header true);


/* END */
