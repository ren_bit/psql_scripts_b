/* PTP RNAseq ismaelproject
   module load PostgreSQL/9.6.1-GCCcore-6.3.0-Python-2.7.12-bare
   pg_ctl -o "-p 9999" -D /scratch/user/ren_net/Test/postresql/test.db -l /scratch/user/ren_net/Test/postresql/test.log start
   psql -p 9999 -f ptp_rnaseq_ismael.sql potato_tomato_zebrachip
   psql -p 9999 potato_tomato_zebrachip
*/


/* remove extact duplicates transcripts from Trinity.fasta */
/* this one is skipped; previous blast steps are in sh file in pbs_script */
/*
drop table if exists blastn_selft_trinity;
create table blastn_selft_trinity
    (qid varchar(30),
    qlen int,
    sid varchar(30),
    slen int,
    qstart int,
    qend int,
    sstart int,
    send int,
    nident int,
    pident double precision,
    allen int,
    mm int,
    gp int,
    qcov double precision,
    ev double precision,
    bs double precision
    );
\copy blastn_selft_trinity from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/ptp_rnaseq_ismael_trinity/blastn_selft_trinity.txt';
	
delete from blastn_selft_trinity where qid=sid;	
alter table blastn_selft_trinity add qcov_cal double precision, add scov_cal double precision;
update blastn_selft_trinity set qcov_cal=(nident*100)/qlen where qlen<=slen and pident=100;	
update blastn_selft_trinity set scov_cal=(nident*100)/slen where slen<=qlen and pident=100;	
*/

/* ^^^^^^^^^^^^^^^^^^^^^^^^

/*  ##################################################################
					import asian citrus psyllid gff file
	##################################################################
*/
/* import asian citrus psyllid gff file */
/* wget ftp://ftp.citrusgreening.org/genomes/Diaphorina_citri/annotation/OGSv2.0/Dcitr_OGSv2.0.gff3 */

drop table if exists acitrus_psyllid_gff;
create table acitrus_psyllid_gff
    (chr varchar(20),
     src varchar(30),
     feature varchar(30),
     st int,
     ende int,
     scr varchar(5),
     strand varchar(5),
     frm varchar(5),
     attr varchar(500)
    );
\copy acitrus_psyllid_gff from '/scratch/user/ren_net/project/seqdb/psyllid/asian_cistrus_dcitrOGSv2.0.gff3';

alter table acitrus_psyllid_gff add gene varchar(100), add trn varchar(20), add anot varchar(300);
update acitrus_psyllid_gff set trn=(regexp_split_to_array((regexp_split_to_array(attr, ';'))[1], 'ID='))[2] 
	where feature='mRNA';
update acitrus_psyllid_gff set gene=(regexp_split_to_array((regexp_split_to_array(attr, ';'))[2], 'Parent='))[2] 
	where feature='mRNA';	
update acitrus_psyllid_gff set anot=(regexp_split_to_array((regexp_split_to_array(attr, ';'))[7], 'Note='))[2] 
	where feature='mRNA';	

 ^^^^^^^^^^^^^^^^^^^^^^^^ */	
 
 /* ^^^^^^^^^^^^^^^^^^^^^^^^

/*  ##################################################################
					import ncbi and uniprot sprot annotation
	##################################################################
*/
drop table if exists animalprot_ncbinr_6102018, uniprot_sprot_6102018 ;
create table animalprot_ncbinr_6102018 (id varchar(40), anot varchar(500));
create table uniprot_sprot_6102018 (id varchar(40), anot varchar(300));
\copy animalprot_ncbinr_6102018 from '/scratch/user/ren_net/project/seqdb/ncbi/animalprot_ncbinr_6102018.csv' delimiter ',';
\copy uniprot_sprot_6102018 from '/scratch/user/ren_net/project/seqdb/uniprot_sprot/uniprot_sprot.csv' delimiter ',';
	
 ^^^^^^^^^^^^^^^^^^^^^^^^ */		
	
/* ^^^^^^^^^^^^^^^^^^^^^^^^ */
	
/*  ##################################################################
					Add annotation to trinity transcripts
	##################################################################
*/	
/* create table with all IDs of trinity transcripts */
Drop table if exists ptptrinitrans_id;
create table ptptrinitrans_id (id varchar(30));
\copy ptptrinitrans_id from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/anot/ptptrintrans_allid.txt';
alter table ptptrinitrans_id add anot varchar(300);

/* import annotation (blast) from various DB */
Drop table if exists blastx_ptptrin_asianpsyllid;
create table blastx_ptptrin_asianpsyllid
	(qid varchar(30),
	qlen int,
	sid varchar(20),
	slen int,
	qstart int,
	qend int,
	sstart int,
	send int, 
	nident int,
	pident double precision,
	alen int,
	mm int,
	gp int,
	qcov double precision,
	eval double precision,
	bs double precision);
\copy blastx_ptptrin_asianpsyllid from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/anot/blastx_ptptrin_asianpsyllid.txt';

Drop table if exists blastx_unanotdeg_ncbinr;	
create table blastx_unanotdeg_ncbinr
	(qid varchar(30),
	qlen int,
	sid varchar(20),
	slen int,
	qstart int,
	qend int,
	sstart int,
	send int, 
	nident int,
	pident double precision,
	alen int,
	mm int,
	gp int,
	qcov double precision,
	eval double precision,
	bs double precision);
\copy blastx_unanotdeg_ncbinr from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/anot/blastx_unanotdeg_ncbinr.txt';

Drop table if exists blastx_unanotdeg_unisprot;	
create table blastx_unanotdeg_unisprot
	(qid varchar(30),
	qlen int,
	sid varchar(50),
	slen int,
	qstart int,
	qend int,
	sstart int,
	send int, 
	nident int,
	pident double precision,
	alen int,
	mm int,
	gp int,
	qcov double precision,
	eval double precision,
	bs double precision);
\copy blastx_unanotdeg_unisprot from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/anot/blastx_unanotdeg_unisprot.txt';
	
alter table blastx_ptptrin_asianpsyllid add anot varchar(300);
alter table blastx_unanotdeg_ncbinr add anot varchar(500);
alter table blastx_unanotdeg_unisprot add anot varchar(500);
update blastx_ptptrin_asianpsyllid as a set anot=b.anot from acitrus_psyllid_gff as b where 
	substring(a.sid from 1 for 14)=b.gene and b.feature='mRNA';	
update blastx_unanotdeg_ncbinr as a set anot=b.anot from animalprot_ncbinr_6102018 as b where 
	a.sid=b.id;		
update blastx_unanotdeg_unisprot as a set anot=b.anot from uniprot_sprot_6102018 as b where 
	a.sid=b.id;		
	
update ptptrinitrans_id as a set anot=b.anot from blastx_ptptrin_asianpsyllid as b where a.id=b.qid;
update ptptrinitrans_id as a set anot=b.anot from blastx_unanotdeg_ncbinr as b where a.id=b.qid 
	and a.anot is null;	
update ptptrinitrans_id as a set anot=b.anot from blastx_unanotdeg_unisprot as b where a.id=b.qid 
	and a.anot is null;	

/*	^^^^^^^^^^^^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^^^^^^^^^^^^ */
	
/*  ##################################################################
					DEG analysis
	##################################################################
*/	

/* import deg table */
drop table if exists ptpovary_negvspos;
create table ptpovary_negvspos
		(gene text,
		bmneg double precision,
		bmpos double precision,
		bm double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy ptpovary_negvspos from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/gene_exp/deg/matrix.counts.matrix.ovary_neg_vs_ovary_pos.DESeq2.DE_results.csv' with csv header delimiter as ',';
	
drop table if exists ptpsal_negvspos;
create table ptpsal_negvspos
		(gene text,
		bmneg double precision,
		bmpos double precision,
		bm double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy ptpsal_negvspos from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/gene_exp/deg/matrix.counts.matrix.sal_neg_vs_sal_pos.DESeq2.DE_results.csv' with csv header delimiter as ',';

/* import pfam table */
drop table if exists temp;
create table temp (dom varchar(100), domid varchar(10), trn varchar(30), domdesc varchar(200) );
\copy temp from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/anot/ovary_sal_deg_absfc2qv5_pfam_tab.csv' delimiter ',';

/* Add annotation */
alter table ptpovary_negvspos add anot varchar(300), add pfamdom varchar(100), 
	add pfamdomdesc varchar(200);
alter table ptpsal_negvspos add anot varchar(300), add pfamdom varchar(100), 
	add pfamdomdesc varchar(200);
update ptpovary_negvspos as a set anot=b.anot from ptptrinitrans_id as b where a.gene=b.id;
update ptpsal_negvspos as a set anot=b.anot from ptptrinitrans_id as b where a.gene=b.id;
update ptpovary_negvspos as a set pfamdom=b.dom, pfamdomdesc=b.domdesc from temp as b 
	where a.gene=b.trn;
update ptpsal_negvspos as a set pfamdom=b.dom, pfamdomdesc=b.domdesc from temp as b 
	where a.gene=b.trn;
drop table temp;
	
/* cast columns to double precision */
update ptpovary_negvspos set pval=NULL where pval='NA';
update ptpovary_negvspos set padj=NULL where padj='NA';
update ptpsal_negvspos set pval=NULL where pval='NA';
update ptpsal_negvspos set padj=NULL where padj='NA';
alter table ptpovary_negvspos alter column pval type double precision using (pval::double precision);
alter table ptpovary_negvspos alter column padj type double precision using (padj::double precision);
alter table ptpsal_negvspos alter column pval type double precision using (pval::double precision);
alter table ptpsal_negvspos alter column padj type double precision using (padj::double precision);

/* copy DEG */
\copy (select * from ptpovary_negvspos where padj<0.05 and (log2fc>=2 or log2fc<=-2) ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/gene_exp/deg/matrix.counts.matrix.ovary_neg_vs_ovary_pos.DESeq2.absfc2qv5.csv' with (FORMAT csv, header true);
\copy (select * from ptpsal_negvspos where padj<0.05 and (log2fc>=2 or log2fc<=-2) ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/gene_exp/deg/matrix.counts.matrix.sal_neg_vs_sal_pos.DESeq2.absfc2qv5.csv' with (FORMAT csv, header true);

/* copy DEG IDS for annotation only i.e sequences which does not have any annotation*/
\copy (select gene from ptpovary_negvspos where padj<0.05 and (log2fc>=2 or log2fc<=-2) and anot is null union select gene from ptpsal_negvspos where padj<0.05 and (log2fc>=2 or log2fc<=-2) and anot is null ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ptp_rnaseq_ismael/gene_exp/deg/unanotdeg_ids.DESeq2.absfc2qv5.csv' with (FORMAT csv, header true);


/* ^^^^^^^^^^^^^^^^^^^^^^^^ */