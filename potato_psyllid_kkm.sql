/* Potato zebrachip RNA-seq project
   load modules on hpc
   module load PostgreSQL/9.6.1-GCCcore-6.3.0-Python-2.7.12-bare
    pg_ctl -o "-p 9999" -D /scratch/user/ren_net/Test/postresql/test.db -l /scratch/user/ren_net/Test/postresql/test.log start
   run as
   psql -p 9999 -f potato_psyllid_kkm.sql potato_tomato_zebrachip
   on tamu ada hpc
   psql -p 9999 -f potato_psyllid_kkm.sql potato_tomato_zebrachip
  first load required modules; module load PostgreSQL/9.6.1-GCCcore-6.3.0-Python-2.7.12-bare
  start server; pg_ctl -o "-p 9999" -D /scratch/user/ren_net/Test/postresql/test.db -l /scratch/user/ren_net/Test/postresql/test.log start
  reference potato transcript to check analysis
  PGSC0003DMT400011414
                PN7.1 PN7.2 PN7.3 PC7.1 PC7.2 PC7.3
  raw counts    2993  2080  2222  2605  2549  2490

 raw counts can be obtained from transcript_count_matrix_pot.csv, counts(dds_pot_con_cold_7) method,
 coverage*transcript_len/avg_read_len (see prepDE.py)
 For example, transcript_len for PGSC0003DMT400011414 is 1583 from PC7-1_S31_bg.gtf (sum of all exons)
 123.415031*1583/75   75 is avg len in prepDE
*/


/* ^^^^^^^^^^^^^ 
--Import potato PGSC Annotation
--note: Solanum_tuberosum.SolTub_3.0.37.chrnamechange.gtf has been used with stringtie and ballgown and hisat2 as this
--gtf file format corresponds with hisat2 stringtie and prepDE
--PGSC_DM_V403_genes.gff is only for annotation purposes

drop table if exists potato_pgsc34_anot;
create table potato_pgsc34_anot
    (id varchar(20),
     anot varchar(300)
    );
\copy potato_pgsc34_anot from 'R:/project/potato_zebrachip/data_obt/genome/PGSC_DM_v3.4_gene_func.txt';

--Import potato PGSC GTF
--potato_pgsc3037_gtf
--count(distinct gene): 38790
--count(distinct trn): 55726
--potato_pgsc403_gff
--count(distinct gene): 39028
--count(distinct trn): 56215

drop table if exists potato_pgsc3037_gtf;
create table potato_pgsc3037_gtf
    (chr varchar(20),
     src varchar(30),
     feature varchar(30),
     st int,
     ende int,
     scr varchar(5),
     strand varchar(5),
     frm varchar(5),
     attr varchar(500)
    );
\copy potato_pgsc3037_gtf from '/scratch/user/ren_net/project/potato_tomato_psyllid/data_obt/genome/Solanum_tuberosum.SolTub_3.0.37.chrnamechange.gtf' ;
alter table potato_pgsc3037_gtf add gene varchar(30), add trn varchar(30), add anot varchar(300), 
	add trn_ct int;
update potato_pgsc3037_gtf set 
	gene=(regexp_split_to_array((regexp_split_to_array(attr, 'gene_id "'))[2], '"'))[1];
update potato_pgsc3037_gtf set 
	trn=(regexp_split_to_array((regexp_split_to_array(attr, 'transcript_id "'))[2], '"'))[1];
update potato_pgsc3037_gtf as a set anot=b.anot from  potato_pgsc403_gff as b where a.gene=b.gene;
drop table if exists temp;
create table temp as select gene, count(distinct trn) from potato_pgsc3037_gtf group by gene;
update potato_pgsc3037_gtf as a set trn_ct=b.count from temp as b where a.gene=b.gene;

drop table if exists potato_pgsc403_gff;
create table potato_pgsc403_gff
    (chr varchar(20),
     src varchar(30),
     feature varchar(30),
     st int,
     ende int,
     scr varchar(5),
     strand varchar(5),
     frm varchar(5),
     attr varchar(500)
    );
\copy potato_pgsc403_gff from 'R:/project/potato_zebrachip/data_obt/genome/PGSC_DM_V403_genes.gff' ;
alter table potato_pgsc403_gff add gene varchar(30), add trn varchar(30), add anot varchar(300);
update potato_pgsc403_gff set trn=(regexp_split_to_array((regexp_split_to_array(attr, ';'))[1], 'ID='))[2] where feature='mRNA';
update potato_pgsc403_gff set gene=(regexp_split_to_array((regexp_split_to_array(attr, ';'))[2], 'Parent='))[2] where feature='mRNA';
update potato_pgsc403_gff set anot=(regexp_split_to_array((regexp_split_to_array(attr, ';'))[6], 'name='))[2] where feature='mRNA';
update potato_pgsc403_gff set anot=(regexp_split_to_array((regexp_split_to_array(attr, ';'))[5], 'name='))[2] where feature='mRNA' and anot is null;
update potato_pgsc403_gff set anot=regexp_replace(anot, '"', '') where anot is not null;
update potato_pgsc403_gff set anot=regexp_replace(anot, '"', '') where anot is not null;

--change tomato trn ids as per old SL 2.5 format
--This is imported from tomato_psyllid_kkm.sql 
alter table tomato_itag3_anot add if not exists trn_old varchar(20), add if not exists trn2 varchar(20);
update tomato_itag3_anot set trn_old=NULL;
update tomato_itag3_anot set trn_old=regexp_replace(trn, '\.3', '.2');
update tomato_itag3_anot set trn_old=regexp_replace(trn, '\.2', '.1') where trn_old is null;
update tomato_itag3_anot set trn2=substring(trn from 1 for 14);
alter table tomato_itag3_anot add if not exists gene_old varchar(20);
update tomato_itag3_anot set gene_old=regexp_replace(gene, '\.3', '.2');

drop table if exists tomato_25037_gtf;
create table tomato_25037_gtf
    (chr varchar(20),
     src varchar(30),
     feature varchar(30),
     st int,
     ende int,
     scr varchar(5),
     strand varchar(5),
     frm varchar(5),
     attr varchar(500)
    );
\copy tomato_25037_gtf from '/scratch/user/ren_net/project/potato_tomato_psyllid/data_obt/genome/Solanum_lycopersicum.SL2.50.37.chrnamechange.gtf' ;
alter table tomato_25037_gtf add gene varchar(30), add trn varchar(30), add anot varchar(300), 
	add trn_ct int;
update tomato_25037_gtf set 
	gene=(regexp_split_to_array((regexp_split_to_array(attr, 'gene_id "'))[2], '"'))[1];
update tomato_25037_gtf set 
	trn=(regexp_split_to_array((regexp_split_to_array(attr, 'transcript_id "'))[2], '"'))[1];
update tomato_25037_gtf as a set anot=b.anot from  tomato_itag3_anot as b where a.gene=b.gene_old;
drop table if exists temp;
create table temp as select gene, count(distinct trn) from tomato_25037_gtf group by gene;
update tomato_25037_gtf as a set trn_ct=b.count from temp as b where a.gene=b.gene;
 ^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^ 
--drop old tables 

do
$$
declare
    i record;
begin
    raise notice 'Delete old table if exists';
    for i in select * FROM pg_catalog.pg_tables where schemaname like 'public' and tablename ~ 'nc_|pot_|tom_|hm_' and
        tablename !~ '_gene|_trn|venn_|deg_|_ortho'
    loop
        raise notice 'table: %', i.tablename;
        execute format('drop table if exists %I', i.tablename);
    end loop;
end;
$$;
 ^^^^^^^^^^^^^ */ 

 
/* ^^^^^^^^^^^^^ 
--Time series differential expression deseq2
--For example, Control 7h vs Control 14h  
create or replace function time_series_exp( _deg_table varchar(20), _input_deg_table varchar(20), 
	_deg_table_up_all varchar(20),
    _deg_table_up_known varchar(20), _norm_count_table varchar(20), _input_norm_count_table varchar(20),
    _deg_table_down_all varchar(20), _deg_table_down_known varchar(20), _raw_count_table varchar(20),
    _input_raw_count_table varchar(20), _id_type varchar(20) )
    returns void as
    $body$
    begin
        raise notice 'deg table: %', _deg_table;
        execute format('drop table if exists %I', _deg_table);
        execute format('create table %I(id text, basemean double precision, log2fc double precision,
            lfcSE double precision, stat double precision, pval text, padj text)', _deg_table);
        --import deg table 
        execute format('copy %I from $$' || _input_deg_table || '$$' || ' with csv header delimiter 
						as ' || '$$' || ',' || '$$', _deg_table);
        execute format('alter table %I add anot varchar(300)', _deg_table);
        if _id_type = 'gene' and _deg_table ~ 'pot_' then
            raise notice 'Potato ID type: %', _id_type;
            execute format('update %I as  a set anot=b.anot from potato_pgsc403_gff as b where a.id=b.gene', _deg_table);
        elsif _id_type = 'trn' and _deg_table ~ 'pot_' then
            raise notice 'Potato ID type: %', _id_type;
            execute format('update %I as  a set anot=b.anot from potato_pgsc403_gff as b where a.id=b.trn', _deg_table);
        elsif _id_type = 'gene' and _deg_table ~ 'tom_' then
            raise notice 'Tomato ID type: %', _id_type;
            execute format('update %I as  a set anot=b.anot from tomato_25037_gtf as b where a.id=b.gene', _deg_table);
        elsif _id_type = 'trn' and _deg_table ~ 'tom_' then
            raise notice 'Tomato ID type: %', _id_type;
            execute format('update %I as  a set anot=b.anot from tomato_25037_gtf as b where a.id=b.trn', _deg_table);
        end if;
        execute format('update %I set pval=NULL where pval=''NA'' ', _deg_table);
        execute format('update %I set padj=NULL where padj=''NA'' ', _deg_table);
        execute format('alter table %I alter column pval type double precision using 
			(pval::double precision)', _deg_table);
        execute format('alter table %I alter column padj type double precision using 
			(padj::double precision)', _deg_table);
        --export with novel genes/trn MSTRG
        execute format('copy (select id, anot, log2fc, pval, padj, basemean, lfcse, stat from %I where padj<0.05 and
            log2fc>=1) to $$' || _deg_table_up_all || '$$' || ' with (FORMAT csv, header true)' , _deg_table);
        execute format('copy (select id, anot, log2fc, pval, padj, basemean, lfcse, stat from %I
            where padj<0.05 and log2fc<=-1) to $$' || _deg_table_down_all || '$$' || ' with (FORMAT csv, header true)' ,
            _deg_table);
        --export with known transcripts and exclude MSTRG
        execute format('copy (select id, anot, log2fc, pval, padj, basemean, lfcse, stat from %I
            where padj<0.05 and log2fc>=1 and id !~ ''MSTRG'') to $$' || _deg_table_up_known || '$$' || ' with
            (FORMAT csv, header true)' , _deg_table);
        execute format('copy (select id, anot, log2fc, pval, padj, basemean, lfcse, stat from %I
            where padj<0.05 and log2fc<=-1 and id !~ ''MSTRG'') to $$' || _deg_table_down_known || '$$' || ' with
            (FORMAT csv, header true)' , _deg_table);
        --add normalized count data
        raise notice 'deseq2 norm table: %', _norm_count_table;
        execute format('drop table if exists %I', _norm_count_table);
        execute format('create table %I(id varchar(20), c1_norm double precision, c2_norm double precision, c3_norm double precision,
            t1_norm double precision, t2_norm double precision, t3_norm double precision)', _norm_count_table);
        execute format('copy %I from $$' || _input_norm_count_table || '$$' || ' with csv header delimiter as ' || '$$' || ',' || '$$',
            _norm_count_table);
        --add raw count data
        raise notice 'deseq2 raw table: %', _raw_count_table;
        execute format('drop table if exists %I', _raw_count_table);
        execute format('create table %I(id varchar(20), c1_raw double precision, c2_raw double precision, c3_raw double precision,
            t1_raw double precision, t2_raw double precision, t3_raw double precision)', _raw_count_table);
        execute format('copy %I from $$' || _input_raw_count_table || '$$' || ' with csv header delimiter as ' || '$$' || ',' || '$$',
            _raw_count_table);
        --add normalized count data to deg tables
        execute format('alter table %I add c1_norm double precision, add c2_norm double precision, add c3_norm double precision,
            add t1_norm double precision, add t2_norm double precision, add t3_norm double precision;', _deg_table);
        execute format('update %I as a set c1_norm=b.c1_norm, c2_norm=b.c2_norm, c3_norm=b.c3_norm, t1_norm=b.t1_norm,
            t2_norm=b.t2_norm, t3_norm=b.t3_norm from %I as b where a.id=b.id', _deg_table, _norm_count_table);
        --add raw count data to deg tables
        execute format('alter table %I add c1_raw double precision, add c2_raw double precision, add c3_raw double precision,
            add t1_raw double precision, add t2_raw double precision, add t3_raw double precision;', _deg_table);
        execute format('update %I as a set c1_raw=b.c1_raw, c2_raw=b.c2_raw, c3_raw=b.c3_raw, t1_raw=b.t1_raw,
            t2_raw=b.t2_raw, t3_raw=b.t3_raw from %I as b where a.id=b.id', _deg_table, _raw_count_table);
        --add CV data to deg tables
        raise notice 'add cv data to deseq2 table: %', _deg_table;
        execute format('alter table %I add cv double precision', _deg_table);
        execute format('drop table if exists temp');
        execute format('create table temp as select id, stddev_samp(val)/avg(val) as cv from (select id,
                unnest(array[c1_norm, c2_norm, c3_norm, t1_norm, t2_norm, t3_norm]) val from %I where
                (c1_norm+c2_norm+c3_norm+t1_norm+t2_norm+t3_norm)!=0  ) sub group
                by id', _deg_table);
        execute format('update %I as a set cv=b.cv from temp as b where a.id=b.id', _deg_table);
    end
    $body$ language "plpgsql";

\set degdir $$'/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/'$$

--potato gene Time series
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running potato time series deg analysis gene level';
END
$$;
select time_series_exp('pot_con_714_gene', :degdir || 'res_pot_con_714_gene.csv', :degdir || 'res_pot_con_714_gene_up_all.csv',
    :degdir || 'res_pot_con_714_gene_up_known.csv', 'normcount_pot_con_714_gene', :degdir || 'normcount_pot_con_714_gene.csv',
    :degdir || 'res_pot_con_714_gene_down_all.csv', :degdir || 'res_pot_con_714_gene_down_known.csv',
    'rawcount_pot_con_714_gene', :degdir || 'rawcount_pot_con_714_gene.csv', 'gene');
select time_series_exp('pot_con_721_gene', :degdir || 'res_pot_con_721_gene.csv', :degdir || 'res_pot_con_721_gene_up_all.csv',
    :degdir || 'res_pot_con_721_gene_up_known.csv', 'normcount_pot_con_721_gene', :degdir || 'normcount_pot_con_721_gene.csv',
    :degdir || 'res_pot_con_721_gene_down_all.csv', :degdir || 'res_pot_con_721_gene_down_known.csv',
    'rawcount_pot_con_721_gene', :degdir || 'rawcount_pot_con_721_gene.csv', 'gene');
select time_series_exp('pot_con_1421_gene', :degdir || 'res_pot_con_1421_gene.csv', :degdir || 'res_pot_con_1421_gene_up_all.csv',
    :degdir || 'res_pot_con_1421_gene_up_known.csv', 'normcount_pot_con_1421_gene', :degdir || 'normcount_pot_con_1421_gene.csv',
    :degdir || 'res_pot_con_1421_gene_down_all.csv', :degdir || 'res_pot_con_1421_gene_down_known.csv',
    'rawcount_pot_con_1421_gene', :degdir || 'rawcount_pot_con_1421_gene.csv', 'gene');
select time_series_exp('pot_cold_714_gene', :degdir || 'res_pot_cold_714_gene.csv', :degdir || 'res_pot_cold_714_gene_up_all.csv',
    :degdir || 'res_pot_cold_714_gene_up_known.csv', 'normcount_pot_cold_714_gene', :degdir || 'normcount_pot_cold_714_gene.csv',
    :degdir || 'res_pot_cold_714_gene_down_all.csv', :degdir || 'res_pot_cold_714_gene_down_known.csv',
    'rawcount_pot_cold_714_gene', :degdir || 'rawcount_pot_cold_714_gene.csv', 'gene');
select time_series_exp('pot_cold_721_gene', :degdir || 'res_pot_cold_721_gene.csv', :degdir || 'res_pot_cold_721_gene_up_all.csv',
    :degdir || 'res_pot_cold_721_gene_up_known.csv', 'normcount_pot_cold_721_gene', :degdir || 'normcount_pot_cold_721_gene.csv',
    :degdir || 'res_pot_cold_721_gene_down_all.csv', :degdir || 'res_pot_cold_721_gene_down_known.csv',
    'rawcount_pot_cold_721_gene', :degdir || 'rawcount_pot_cold_721_gene.csv', 'gene');
select time_series_exp('pot_cold_1421_gene', :degdir || 'res_pot_cold_1421_gene.csv', :degdir || 'res_pot_cold_1421_gene_up_all.csv',
    :degdir || 'res_pot_cold_1421_gene_up_known.csv', 'normcount_pot_cold_1421_gene', :degdir || 'normcount_pot_cold_1421_gene.csv',
    :degdir || 'res_pot_cold_1421_gene_down_all.csv', :degdir || 'res_pot_cold_1421_gene_down_known.csv',
    'rawcount_pot_cold_1421_gene', :degdir || 'rawcount_pot_cold_1421_gene.csv', 'gene');
select time_series_exp('pot_hot_714_gene', :degdir || 'res_pot_hot_714_gene.csv', :degdir || 'res_pot_hot_714_gene_up_all.csv',
    :degdir || 'res_pot_hot_714_gene_up_known.csv', 'normcount_pot_hot_714_gene', :degdir || 'normcount_pot_hot_714_gene.csv',
    :degdir || 'res_pot_hot_714_gene_down_all.csv', :degdir || 'res_pot_hot_714_gene_down_known.csv',
    'rawcount_pot_hot_714_gene', :degdir || 'rawcount_pot_hot_714_gene.csv', 'gene');
select time_series_exp('pot_hot_721_gene', :degdir || 'res_pot_hot_721_gene.csv', :degdir || 'res_pot_hot_721_gene_up_all.csv',
    :degdir || 'res_pot_hot_721_gene_up_known.csv', 'normcount_pot_hot_721_gene', :degdir || 'normcount_pot_hot_721_gene.csv',
    :degdir || 'res_pot_hot_721_gene_down_all.csv', :degdir || 'res_pot_hot_721_gene_down_known.csv',
    'rawcount_pot_hot_721_gene', :degdir || 'rawcount_pot_hot_721_gene.csv', 'gene');
select time_series_exp('pot_hot_1421_gene', :degdir || 'res_pot_hot_1421_gene.csv', :degdir || 'res_pot_hot_1421_gene_up_all.csv',
    :degdir || 'res_pot_hot_1421_gene_up_known.csv', 'normcount_pot_hot_1421_gene', :degdir || 'normcount_pot_hot_1421_gene.csv',
    :degdir || 'res_pot_hot_1421_gene_down_all.csv', :degdir || 'res_pot_hot_1421_gene_down_known.csv',
    'rawcount_pot_hot_1421_gene', :degdir || 'rawcount_pot_hot_1421_gene.csv', 'gene');

--potato transcript Time series 
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running potato time series deg analysis transcript level';
END
$$;
select time_series_exp('pot_con_714_trn', :degdir || 'res_pot_con_714_trn.csv', :degdir || 'res_pot_con_714_trn_up_all.csv',
    :degdir || 'res_pot_con_714_trn_up_known.csv', 'normcount_pot_con_714_trn', :degdir || 'normcount_pot_con_714_trn.csv',
    :degdir || 'res_pot_con_714_trn_down_all.csv', :degdir || 'res_pot_con_714_trn_down_known.csv',
    'rawcount_pot_con_714_trn', :degdir || 'rawcount_pot_con_714_trn.csv', 'trn');
select time_series_exp('pot_con_721_trn', :degdir || 'res_pot_con_721_trn.csv', :degdir || 'res_pot_con_721_trn_up_all.csv',
    :degdir || 'res_pot_con_721_trn_up_known.csv', 'normcount_pot_con_721_trn', :degdir || 'normcount_pot_con_721_trn.csv',
    :degdir || 'res_pot_con_721_trn_down_all.csv', :degdir || 'res_pot_con_721_trn_down_known.csv',
    'rawcount_pot_con_721_trn', :degdir || 'rawcount_pot_con_721_trn.csv', 'trn');
select time_series_exp('pot_con_1421_trn', :degdir || 'res_pot_con_1421_trn.csv', :degdir || 'res_pot_con_1421_trn_up_all.csv',
    :degdir || 'res_pot_con_1421_trn_up_known.csv', 'normcount_pot_con_1421_trn', :degdir || 'normcount_pot_con_1421_trn.csv',
    :degdir || 'res_pot_con_1421_trn_down_all.csv', :degdir || 'res_pot_con_1421_trn_down_known.csv',
    'rawcount_pot_con_1421_trn', :degdir || 'rawcount_pot_con_1421_trn.csv', 'trn');
select time_series_exp('pot_cold_714_trn', :degdir || 'res_pot_cold_714_trn.csv', :degdir || 'res_pot_cold_714_trn_up_all.csv',
    :degdir || 'res_pot_cold_714_trn_up_known.csv', 'normcount_pot_cold_714_trn', :degdir || 'normcount_pot_cold_714_trn.csv',
    :degdir || 'res_pot_cold_714_trn_down_all.csv', :degdir || 'res_pot_cold_714_trn_down_known.csv',
    'rawcount_pot_cold_714_trn', :degdir || 'rawcount_pot_cold_714_trn.csv', 'trn');
select time_series_exp('pot_cold_721_trn', :degdir || 'res_pot_cold_721_trn.csv', :degdir || 'res_pot_cold_721_trn_up_all.csv',
    :degdir || 'res_pot_cold_721_trn_up_known.csv', 'normcount_pot_cold_721_trn', :degdir || 'normcount_pot_cold_721_trn.csv',
    :degdir || 'res_pot_cold_721_trn_down_all.csv', :degdir || 'res_pot_cold_721_trn_down_known.csv',
    'rawcount_pot_cold_721_trn', :degdir || 'rawcount_pot_cold_721_trn.csv', 'trn');
select time_series_exp('pot_cold_1421_trn', :degdir || 'res_pot_cold_1421_trn.csv', :degdir || 'res_pot_cold_1421_trn_up_all.csv',
    :degdir || 'res_pot_cold_1421_trn_up_known.csv', 'normcount_pot_cold_1421_trn', :degdir || 'normcount_pot_cold_1421_trn.csv',
    :degdir || 'res_pot_cold_1421_trn_down_all.csv', :degdir || 'res_pot_cold_1421_trn_down_known.csv',
    'rawcount_pot_cold_1421_trn', :degdir || 'rawcount_pot_cold_1421_trn.csv', 'trn');
select time_series_exp('pot_hot_714_trn', :degdir || 'res_pot_hot_714_trn.csv', :degdir || 'res_pot_hot_714_trn_up_all.csv',
    :degdir || 'res_pot_hot_714_trn_up_known.csv', 'normcount_pot_hot_714_trn', :degdir || 'normcount_pot_hot_714_trn.csv',
    :degdir || 'res_pot_hot_714_trn_down_all.csv', :degdir || 'res_pot_hot_714_trn_down_known.csv',
    'rawcount_pot_hot_714_trn', :degdir || 'rawcount_pot_hot_714_trn.csv', 'trn');
select time_series_exp('pot_hot_721_trn', :degdir || 'res_pot_hot_721_trn.csv', :degdir || 'res_pot_hot_721_trn_up_all.csv',
    :degdir || 'res_pot_hot_721_trn_up_known.csv', 'normcount_pot_hot_721_trn', :degdir || 'normcount_pot_hot_721_trn.csv',
    :degdir || 'res_pot_hot_721_trn_down_all.csv', :degdir || 'res_pot_hot_721_trn_down_known.csv',
    'rawcount_pot_hot_721_trn', :degdir || 'rawcount_pot_hot_721_trn.csv', 'trn');
select time_series_exp('pot_hot_1421_trn', :degdir || 'res_pot_hot_1421_trn.csv', :degdir || 'res_pot_hot_1421_trn_up_all.csv',
    :degdir || 'res_pot_hot_1421_trn_up_known.csv', 'normcount_pot_hot_1421_trn', :degdir || 'normcount_pot_hot_1421_trn.csv',
    :degdir || 'res_pot_hot_1421_trn_down_all.csv', :degdir || 'res_pot_hot_1421_trn_down_known.csv',
    'rawcount_pot_hot_1421_trn', :degdir || 'rawcount_pot_hot_1421_trn.csv', 'trn');

--potato gene treatment series 
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running potato treatment series deg analysis gene level';
END
$$;
select time_series_exp('pot_con_cold_7_gene', :degdir || 'res_pot_con_cold_7_gene.csv', :degdir || 'res_pot_con_cold_7_gene_up_all.csv',
    :degdir || 'res_pot_con_cold_7_gene_up_known.csv', 'normcount_pot_con_cold_7_gene', :degdir || 'normcount_pot_con_cold_7_gene.csv',
    :degdir || 'res_pot_con_cold_7_gene_down_all.csv', :degdir || 'res_pot_con_cold_7_gene_down_known.csv',
    'rawcount_pot_con_cold_7_gene', :degdir || 'rawcount_pot_con_cold_7_gene.csv', 'gene');
select time_series_exp('pot_con_hot_7_gene', :degdir || 'res_pot_con_hot_7_gene.csv', :degdir || 'res_pot_con_hot_7_gene_up_all.csv',
    :degdir || 'res_pot_con_hot_7_gene_up_known.csv', 'normcount_pot_con_hot_7_gene', :degdir || 'normcount_pot_con_hot_7_gene.csv',
    :degdir || 'res_pot_con_hot_7_gene_down_all.csv', :degdir || 'res_pot_con_hot_7_gene_down_known.csv',
    'rawcount_pot_con_hot_7_gene', :degdir || 'rawcount_pot_con_hot_7_gene.csv', 'gene');
select time_series_exp('pot_cold_hot_7_gene', :degdir || 'res_pot_cold_hot_7_gene.csv', :degdir || 'res_pot_cold_hot_7_gene_up_all.csv',
    :degdir || 'res_pot_cold_hot_7_gene_up_known.csv', 'normcount_pot_cold_hot_7_gene', :degdir || 'normcount_pot_cold_hot_7_gene.csv',
    :degdir || 'res_pot_cold_hot_7_gene_down_all.csv', :degdir || 'res_pot_cold_hot_7_gene_down_known.csv',
    'rawcount_pot_cold_hot_7_gene', :degdir || 'rawcount_pot_cold_hot_7_gene.csv', 'gene');
select time_series_exp('pot_con_cold_14_gene', :degdir || 'res_pot_con_cold_14_gene.csv', :degdir || 'res_pot_con_cold_14_gene_up_all.csv',
    :degdir || 'res_pot_con_cold_14_gene_up_known.csv', 'normcount_pot_con_cold_14_gene', :degdir || 'normcount_pot_con_cold_14_gene.csv',
    :degdir || 'res_pot_con_cold_14_gene_down_all.csv', :degdir || 'res_pot_con_cold_14_gene_down_known.csv',
    'rawcount_pot_con_cold_14_gene', :degdir || 'rawcount_pot_con_cold_14_gene.csv', 'gene');
select time_series_exp('pot_con_hot_14_gene', :degdir || 'res_pot_con_hot_14_gene.csv', :degdir || 'res_pot_con_hot_14_gene_up_all.csv',
    :degdir || 'res_pot_con_hot_14_gene_up_known.csv', 'normcount_pot_con_hot_14_gene', :degdir || 'normcount_pot_con_hot_14_gene.csv',
    :degdir || 'res_pot_con_hot_14_gene_down_all.csv', :degdir || 'res_pot_con_hot_14_gene_down_known.csv',
    'rawcount_pot_con_hot_14_gene', :degdir || 'rawcount_pot_con_hot_14_gene.csv', 'gene');
select time_series_exp('pot_cold_hot_14_gene', :degdir || 'res_pot_cold_hot_14_gene.csv', :degdir || 'res_pot_cold_hot_14_gene_up_all.csv',
    :degdir || 'res_pot_cold_hot_14_gene_up_known.csv', 'normcount_pot_cold_hot_14_gene', :degdir || 'normcount_pot_cold_hot_14_gene.csv',
    :degdir || 'res_pot_cold_hot_14_gene_down_all.csv', :degdir || 'res_pot_cold_hot_14_gene_down_known.csv',
    'rawcount_pot_cold_hot_14_gene', :degdir || 'rawcount_pot_cold_hot_14_gene.csv', 'gene');
select time_series_exp('pot_con_cold_21_gene', :degdir || 'res_pot_con_cold_21_gene.csv', :degdir || 'res_pot_con_cold_21_gene_up_all.csv',
    :degdir || 'res_pot_con_cold_21_gene_up_known.csv', 'normcount_pot_con_cold_21_gene', :degdir || 'normcount_pot_con_cold_21_gene.csv',
    :degdir || 'res_pot_con_cold_21_gene_down_all.csv', :degdir || 'res_pot_con_cold_21_gene_down_known.csv',
    'rawcount_pot_con_cold_21_gene', :degdir || 'rawcount_pot_con_cold_21_gene.csv', 'gene');
select time_series_exp('pot_con_hot_21_gene', :degdir || 'res_pot_con_hot_21_gene.csv', :degdir || 'res_pot_con_hot_21_gene_up_all.csv',
    :degdir || 'res_pot_con_hot_21_gene_up_known.csv', 'normcount_pot_con_hot_21_gene', :degdir || 'normcount_pot_con_hot_21_gene.csv',
    :degdir || 'res_pot_con_hot_21_gene_down_all.csv', :degdir || 'res_pot_con_hot_21_gene_down_known.csv',
    'rawcount_pot_con_hot_21_gene', :degdir || 'rawcount_pot_con_hot_21_gene.csv', 'gene');
select time_series_exp('pot_cold_hot_21_gene', :degdir || 'res_pot_cold_hot_21_gene.csv', :degdir || 'res_pot_cold_hot_21_gene_up_all.csv',
    :degdir || 'res_pot_cold_hot_21_gene_up_known.csv', 'normcount_pot_cold_hot_21_gene', :degdir || 'normcount_pot_cold_hot_21_gene.csv',
    :degdir || 'res_pot_cold_hot_21_gene_down_all.csv', :degdir || 'res_pot_cold_hot_21_gene_down_known.csv',
    'rawcount_pot_cold_hot_21_gene', :degdir || 'rawcount_pot_cold_hot_21_gene.csv', 'gene');

--potato transcript treatment series 
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running potato treatment series deg analysis transcript level';
END
$$;
select time_series_exp('pot_con_cold_7_trn', :degdir || 'res_pot_con_cold_7_trn.csv', :degdir || 'res_pot_con_cold_7_trn_up_all.csv',
    :degdir || 'res_pot_con_cold_7_trn_up_known.csv', 'normcount_pot_con_cold_7_trn', :degdir || 'normcount_pot_con_cold_7_trn.csv',
    :degdir || 'res_pot_con_cold_7_trn_down_all.csv', :degdir || 'res_pot_con_cold_7_trn_down_known.csv',
    'rawcount_pot_con_cold_7_trn', :degdir || 'rawcount_pot_con_cold_7_trn.csv', 'trn');
select time_series_exp('pot_con_hot_7_trn', :degdir || 'res_pot_con_hot_7_trn.csv', :degdir || 'res_pot_con_hot_7_trn_up_all.csv',
    :degdir || 'res_pot_con_hot_7_trn_up_known.csv', 'normcount_pot_con_hot_7_trn', :degdir || 'normcount_pot_con_hot_7_trn.csv',
    :degdir || 'res_pot_con_hot_7_trn_down_all.csv', :degdir || 'res_pot_con_hot_7_trn_down_known.csv',
    'rawcount_pot_con_hot_7_trn', :degdir || 'rawcount_pot_con_hot_7_trn.csv', 'trn');
select time_series_exp('pot_cold_hot_7_trn', :degdir || 'res_pot_cold_hot_7_trn.csv', :degdir || 'res_pot_cold_hot_7_trn_up_all.csv',
    :degdir || 'res_pot_cold_hot_7_trn_up_known.csv', 'normcount_pot_cold_hot_7_trn', :degdir || 'normcount_pot_cold_hot_7_trn.csv',
    :degdir || 'res_pot_cold_hot_7_trn_down_all.csv', :degdir || 'res_pot_cold_hot_7_trn_down_known.csv',
    'rawcount_pot_cold_hot_7_trn', :degdir || 'rawcount_pot_cold_hot_7_trn.csv', 'trn');
select time_series_exp('pot_con_cold_14_trn', :degdir || 'res_pot_con_cold_14_trn.csv', :degdir || 'res_pot_con_cold_14_trn_up_all.csv',
    :degdir || 'res_pot_con_cold_14_trn_up_known.csv', 'normcount_pot_con_cold_14_trn', :degdir || 'normcount_pot_con_cold_14_trn.csv',
    :degdir || 'res_pot_con_cold_14_trn_down_all.csv', :degdir || 'res_pot_con_cold_14_trn_down_known.csv',
    'rawcount_pot_con_cold_14_trn', :degdir || 'rawcount_pot_con_cold_14_trn.csv', 'trn');
select time_series_exp('pot_con_hot_14_trn', :degdir || 'res_pot_con_hot_14_trn.csv', :degdir || 'res_pot_con_hot_14_trn_up_all.csv',
    :degdir || 'res_pot_con_hot_14_trn_up_known.csv', 'normcount_pot_con_hot_14_trn', :degdir || 'normcount_pot_con_hot_14_trn.csv',
    :degdir || 'res_pot_con_hot_14_trn_down_all.csv', :degdir || 'res_pot_con_hot_14_trn_down_known.csv',
    'rawcount_pot_con_hot_14_trn', :degdir || 'rawcount_pot_con_hot_14_trn.csv', 'trn');
select time_series_exp('pot_cold_hot_14_trn', :degdir || 'res_pot_cold_hot_14_trn.csv', :degdir || 'res_pot_cold_hot_14_trn_up_all.csv',
    :degdir || 'res_pot_cold_hot_14_trn_up_known.csv', 'normcount_pot_cold_hot_14_trn', :degdir || 'normcount_pot_cold_hot_14_trn.csv',
    :degdir || 'res_pot_cold_hot_14_trn_down_all.csv', :degdir || 'res_pot_cold_hot_14_trn_down_known.csv',
    'rawcount_pot_cold_hot_14_trn', :degdir || 'rawcount_pot_cold_hot_14_trn.csv', 'trn');
select time_series_exp('pot_con_cold_21_trn', :degdir || 'res_pot_con_cold_21_trn.csv', :degdir || 'res_pot_con_cold_21_trn_up_all.csv',
    :degdir || 'res_pot_con_cold_21_trn_up_known.csv', 'normcount_pot_con_cold_21_trn', :degdir || 'normcount_pot_con_cold_21_trn.csv',
    :degdir || 'res_pot_con_cold_21_trn_down_all.csv', :degdir || 'res_pot_con_cold_21_trn_down_known.csv',
    'rawcount_pot_con_cold_21_trn', :degdir || 'rawcount_pot_con_cold_21_trn.csv', 'trn');
select time_series_exp('pot_con_hot_21_trn', :degdir || 'res_pot_con_hot_21_trn.csv', :degdir || 'res_pot_con_hot_21_trn_up_all.csv',
    :degdir || 'res_pot_con_hot_21_trn_up_known.csv', 'normcount_pot_con_hot_21_trn', :degdir || 'normcount_pot_con_hot_21_trn.csv',
    :degdir || 'res_pot_con_hot_21_trn_down_all.csv', :degdir || 'res_pot_con_hot_21_trn_down_known.csv',
    'rawcount_pot_con_hot_21_trn', :degdir || 'rawcount_pot_con_hot_21_trn.csv', 'trn');
select time_series_exp('pot_cold_hot_21_trn', :degdir || 'res_pot_cold_hot_21_trn.csv', :degdir || 'res_pot_cold_hot_21_trn_up_all.csv',
    :degdir || 'res_pot_cold_hot_21_trn_up_known.csv', 'normcount_pot_cold_hot_21_trn', :degdir || 'normcount_pot_cold_hot_21_trn.csv',
    :degdir || 'res_pot_cold_hot_21_trn_down_all.csv', :degdir || 'res_pot_cold_hot_21_trn_down_known.csv',
    'rawcount_pot_cold_hot_21_trn', :degdir || 'rawcount_pot_cold_hot_21_trn.csv', 'trn');

/*
drop table if exists pot_cold_hot_21_trn;
create table pot_cold_hot_21_trn(id text, basemean double precision, log2fc double precision, lfcSE double precision, stat double precision, pval text, padj text);
\copy pot_cold_hot_21_trn from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/res_pot_cold_hot_21_trn.csv.csv' with csv header delimiter as  ',' ;
alter table pot_cold_hot_21_trn add anot varchar(300);
update pot_cold_hot_21_trn as a set anot=b.anot from potato_pgsc403_gff as b where a.id=b.trn;
update pot_cold_hot_21_trn set pval=NULL where pval='NA';
update pot_cold_hot_21_trn set padj=NULL where padj='NA';
alter table pot_cold_hot_21_trn alter column pval type double precision using (pval::double precision);
alter table pot_cold_hot_21_trn alter column padj type double precision using (padj::double precision);
raise notice 'deseq2 norm table: %', _norm_count_table;
drop table if exists normcount_pot_cold_hot_21_trn;
create table normcount_pot_cold_hot_21_trn(id varchar(20), c1_norm double precision, c2_norm double precision, c3_norm double precision, t1_norm double precision, t2_norm double precision, t3_norm double precision);
\copy normcount_pot_cold_hot_21_trn from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/normcount_pot_cold_hot_21_trn.csv' with csv header delimiter as  ',' ;
raise notice 'add cv data to deseq2 table: %', _deg_table;
drop table if exists temp;
create table temp as select id, stddev_samp(val)/avg(val) as cv from (select id, unnest(array[c1_norm+c2_norm+c3_norm+t1_norm+t2_norm+t3_norm]) val from pot_cold_hot_21_trn where (c1_norm+c2_norm+c3_norm+t1_norm+t2_norm+t3_norm)!=0  ) sub group by id;

*/

--tomato gene Time series   
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running tomato time series deg analysis gene level';
END
$$;
select time_series_exp('tom_con_714_gene', :degdir || 'res_tom_con_714_gene.csv', :degdir || 'res_tom_con_714_gene_up_all.csv',
    :degdir || 'res_tom_con_714_gene_up_known.csv', 'normcount_tom_con_714_gene', :degdir || 'normcount_tom_con_714_gene.csv',
    :degdir || 'res_tom_con_714_gene_down_all.csv', :degdir || 'res_tom_con_714_gene_down_known.csv',
    'rawcount_tom_con_714_gene', :degdir || 'rawcount_tom_con_714_gene.csv', 'gene');
select time_series_exp('tom_con_721_gene', :degdir || 'res_tom_con_721_gene.csv', :degdir || 'res_tom_con_721_gene_up_all.csv',
    :degdir || 'res_tom_con_721_gene_up_known.csv', 'normcount_tom_con_721_gene', :degdir || 'normcount_tom_con_721_gene.csv',
    :degdir || 'res_tom_con_721_gene_down_all.csv', :degdir || 'res_tom_con_721_gene_down_known.csv',
    'rawcount_tom_con_721_gene', :degdir || 'rawcount_tom_con_721_gene.csv', 'gene');
select time_series_exp('tom_con_1421_gene', :degdir || 'res_tom_con_1421_gene.csv', :degdir || 'res_tom_con_1421_gene_up_all.csv',
    :degdir || 'res_tom_con_1421_gene_up_known.csv', 'normcount_tom_con_1421_gene', :degdir || 'normcount_tom_con_1421_gene.csv',
    :degdir || 'res_tom_con_1421_gene_down_all.csv', :degdir || 'res_tom_con_1421_gene_down_known.csv',
    'rawcount_tom_con_1421_gene', :degdir || 'rawcount_tom_con_1421_gene.csv', 'gene');
select time_series_exp('tom_cold_714_gene', :degdir || 'res_tom_cold_714_gene.csv', :degdir || 'res_tom_cold_714_gene_up_all.csv',
    :degdir || 'res_tom_cold_714_gene_up_known.csv', 'normcount_tom_cold_714_gene', :degdir || 'normcount_tom_cold_714_gene.csv',
    :degdir || 'res_tom_cold_714_gene_down_all.csv', :degdir || 'res_tom_cold_714_gene_down_known.csv',
    'rawcount_tom_cold_714_gene', :degdir || 'rawcount_tom_cold_714_gene.csv', 'gene');
select time_series_exp('tom_cold_721_gene', :degdir || 'res_tom_cold_721_gene.csv', :degdir || 'res_tom_cold_721_gene_up_all.csv',
    :degdir || 'res_tom_cold_721_gene_up_known.csv', 'normcount_tom_cold_721_gene', :degdir || 'normcount_tom_cold_721_gene.csv',
    :degdir || 'res_tom_cold_721_gene_down_all.csv', :degdir || 'res_tom_cold_721_gene_down_known.csv',
    'rawcount_tom_cold_721_gene', :degdir || 'rawcount_tom_cold_721_gene.csv', 'gene');
select time_series_exp('tom_cold_1421_gene', :degdir || 'res_tom_cold_1421_gene.csv', :degdir || 'res_tom_cold_1421_gene_up_all.csv',
    :degdir || 'res_tom_cold_1421_gene_up_known.csv', 'normcount_tom_cold_1421_gene', :degdir || 'normcount_tom_cold_1421_gene.csv',
    :degdir || 'res_tom_cold_1421_gene_down_all.csv', :degdir || 'res_tom_cold_1421_gene_down_known.csv',
    'rawcount_tom_cold_1421_gene', :degdir || 'rawcount_tom_cold_1421_gene.csv', 'gene');
select time_series_exp('tom_hot_714_gene', :degdir || 'res_tom_hot_714_gene.csv', :degdir || 'res_tom_hot_714_gene_up_all.csv',
    :degdir || 'res_tom_hot_714_gene_up_known.csv', 'normcount_tom_hot_714_gene', :degdir || 'normcount_tom_hot_714_gene.csv',
    :degdir || 'res_tom_hot_714_gene_down_all.csv', :degdir || 'res_tom_hot_714_gene_down_known.csv',
    'rawcount_tom_hot_714_gene', :degdir || 'rawcount_tom_hot_714_gene.csv', 'gene');
select time_series_exp('tom_hot_721_gene', :degdir || 'res_tom_hot_721_gene.csv', :degdir || 'res_tom_hot_721_gene_up_all.csv',
    :degdir || 'res_tom_hot_721_gene_up_known.csv', 'normcount_tom_hot_721_gene', :degdir || 'normcount_tom_hot_721_gene.csv',
    :degdir || 'res_tom_hot_721_gene_down_all.csv', :degdir || 'res_tom_hot_721_gene_down_known.csv',
    'rawcount_tom_hot_721_gene', :degdir || 'rawcount_tom_hot_721_gene.csv', 'gene');
select time_series_exp('tom_hot_1421_gene', :degdir || 'res_tom_hot_1421_gene.csv', :degdir || 'res_tom_hot_1421_gene_up_all.csv',
    :degdir || 'res_tom_hot_1421_gene_up_known.csv', 'normcount_tom_hot_1421_gene', :degdir || 'normcount_tom_hot_1421_gene.csv',
    :degdir || 'res_tom_hot_1421_gene_down_all.csv', :degdir || 'res_tom_hot_1421_gene_down_known.csv',
    'rawcount_tom_hot_1421_gene', :degdir || 'rawcount_tom_hot_1421_gene.csv', 'gene');

--tomato transcript Time series   
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running tomato time series deg analysis transcript level';
END
$$;
select time_series_exp('tom_con_714_trn', :degdir || 'res_tom_con_714_trn.csv', :degdir || 'res_tom_con_714_trn_up_all.csv',
    :degdir || 'res_tom_con_714_trn_up_known.csv', 'normcount_tom_con_714_trn', :degdir || 'normcount_tom_con_714_trn.csv',
    :degdir || 'res_tom_con_714_trn_down_all.csv', :degdir || 'res_tom_con_714_trn_down_known.csv',
    'rawcount_tom_con_714_trn', :degdir || 'rawcount_tom_con_714_trn.csv', 'trn');
select time_series_exp('tom_con_721_trn', :degdir || 'res_tom_con_721_trn.csv', :degdir || 'res_tom_con_721_trn_up_all.csv',
    :degdir || 'res_tom_con_721_trn_up_known.csv', 'normcount_tom_con_721_trn', :degdir || 'normcount_tom_con_721_trn.csv',
    :degdir || 'res_tom_con_721_trn_down_all.csv', :degdir || 'res_tom_con_721_trn_down_known.csv',
    'rawcount_tom_con_721_trn', :degdir || 'rawcount_tom_con_721_trn.csv', 'trn');
select time_series_exp('tom_con_1421_trn', :degdir || 'res_tom_con_1421_trn.csv', :degdir || 'res_tom_con_1421_trn_up_all.csv',
    :degdir || 'res_tom_con_1421_trn_up_known.csv', 'normcount_tom_con_1421_trn', :degdir || 'normcount_tom_con_1421_trn.csv',
    :degdir || 'res_tom_con_1421_trn_down_all.csv', :degdir || 'res_tom_con_1421_trn_down_known.csv',
    'rawcount_tom_con_1421_trn', :degdir || 'rawcount_tom_con_1421_trn.csv', 'trn');
select time_series_exp('tom_cold_714_trn', :degdir || 'res_tom_cold_714_trn.csv', :degdir || 'res_tom_cold_714_trn_up_all.csv',
    :degdir || 'res_tom_cold_714_trn_up_known.csv', 'normcount_tom_cold_714_trn', :degdir || 'normcount_tom_cold_714_trn.csv',
    :degdir || 'res_tom_cold_714_trn_down_all.csv', :degdir || 'res_tom_cold_714_trn_down_known.csv',
    'rawcount_tom_cold_714_trn', :degdir || 'rawcount_tom_cold_714_trn.csv', 'trn');
select time_series_exp('tom_cold_721_trn', :degdir || 'res_tom_cold_721_trn.csv', :degdir || 'res_tom_cold_721_trn_up_all.csv',
    :degdir || 'res_tom_cold_721_trn_up_known.csv', 'normcount_tom_cold_721_trn', :degdir || 'normcount_tom_cold_721_trn.csv',
    :degdir || 'res_tom_cold_721_trn_down_all.csv', :degdir || 'res_tom_cold_721_trn_down_known.csv',
    'rawcount_tom_cold_721_trn', :degdir || 'rawcount_tom_cold_721_trn.csv', 'trn');
select time_series_exp('tom_cold_1421_trn', :degdir || 'res_tom_cold_1421_trn.csv', :degdir || 'res_tom_cold_1421_trn_up_all.csv',
    :degdir || 'res_tom_cold_1421_trn_up_known.csv', 'normcount_tom_cold_1421_trn', :degdir || 'normcount_tom_cold_1421_trn.csv',
    :degdir || 'res_tom_cold_1421_trn_down_all.csv', :degdir || 'res_tom_cold_1421_trn_down_known.csv',
    'rawcount_tom_cold_1421_trn', :degdir || 'rawcount_tom_cold_1421_trn.csv', 'trn');
select time_series_exp('tom_hot_714_trn', :degdir || 'res_tom_hot_714_trn.csv', :degdir || 'res_tom_hot_714_trn_up_all.csv',
    :degdir || 'res_tom_hot_714_trn_up_known.csv', 'normcount_tom_hot_714_trn', :degdir || 'normcount_tom_hot_714_trn.csv',
    :degdir || 'res_tom_hot_714_trn_down_all.csv', :degdir || 'res_tom_hot_714_trn_down_known.csv',
    'rawcount_tom_hot_714_trn', :degdir || 'rawcount_tom_hot_714_trn.csv', 'trn');
select time_series_exp('tom_hot_721_trn', :degdir || 'res_tom_hot_721_trn.csv', :degdir || 'res_tom_hot_721_trn_up_all.csv',
    :degdir || 'res_tom_hot_721_trn_up_known.csv', 'normcount_tom_hot_721_trn', :degdir || 'normcount_tom_hot_721_trn.csv',
    :degdir || 'res_tom_hot_721_trn_down_all.csv', :degdir || 'res_tom_hot_721_trn_down_known.csv',
    'rawcount_tom_hot_721_trn', :degdir || 'rawcount_tom_hot_721_trn.csv', 'trn');
select time_series_exp('tom_hot_1421_trn', :degdir || 'res_tom_hot_1421_trn.csv', :degdir || 'res_tom_hot_1421_trn_up_all.csv',
    :degdir || 'res_tom_hot_1421_trn_up_known.csv', 'normcount_tom_hot_1421_trn', :degdir || 'normcount_tom_hot_1421_trn.csv',
    :degdir || 'res_tom_hot_1421_trn_down_all.csv', :degdir || 'res_tom_hot_1421_trn_down_known.csv',
    'rawcount_tom_hot_1421_trn', :degdir || 'rawcount_tom_hot_1421_trn.csv', 'trn');

--tomato gene treatment series   
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running tomato treatment series deg analysis gene level';
END
$$;
select time_series_exp('tom_con_cold_7_gene', :degdir || 'res_tom_con_cold_7_gene.csv', :degdir || 'res_tom_con_cold_7_gene_up_all.csv',
    :degdir || 'res_tom_con_cold_7_gene_up_known.csv', 'normcount_tom_con_cold_7_gene', :degdir || 'normcount_tom_con_cold_7_gene.csv',
    :degdir || 'res_tom_con_cold_7_gene_down_all.csv', :degdir || 'res_tom_con_cold_7_gene_down_known.csv',
    'rawcount_tom_con_cold_7_gene', :degdir || 'rawcount_tom_con_cold_7_gene.csv', 'gene');
select time_series_exp('tom_con_hot_7_gene', :degdir || 'res_tom_con_hot_7_gene.csv', :degdir || 'res_tom_con_hot_7_gene_up_all.csv',
    :degdir || 'res_tom_con_hot_7_gene_up_known.csv', 'normcount_tom_con_hot_7_gene', :degdir || 'normcount_tom_con_hot_7_gene.csv',
    :degdir || 'res_tom_con_hot_7_gene_down_all.csv', :degdir || 'res_tom_con_hot_7_gene_down_known.csv',
    'rawcount_tom_con_hot_7_gene', :degdir || 'rawcount_tom_con_hot_7_gene.csv', 'gene');
select time_series_exp('tom_cold_hot_7_gene', :degdir || 'res_tom_cold_hot_7_gene.csv', :degdir || 'res_tom_cold_hot_7_gene_up_all.csv',
    :degdir || 'res_tom_cold_hot_7_gene_up_known.csv', 'normcount_tom_cold_hot_7_gene', :degdir || 'normcount_tom_cold_hot_7_gene.csv',
    :degdir || 'res_tom_cold_hot_7_gene_down_all.csv', :degdir || 'res_tom_cold_hot_7_gene_down_known.csv',
    'rawcount_tom_cold_hot_7_gene', :degdir || 'rawcount_tom_cold_hot_7_gene.csv', 'gene');
select time_series_exp('tom_con_cold_14_gene', :degdir || 'res_tom_con_cold_14_gene.csv', :degdir || 'res_tom_con_cold_14_gene_up_all.csv',
    :degdir || 'res_tom_con_cold_14_gene_up_known.csv', 'normcount_tom_con_cold_14_gene', :degdir || 'normcount_tom_con_cold_14_gene.csv',
    :degdir || 'res_tom_con_cold_14_gene_down_all.csv', :degdir || 'res_tom_con_cold_14_gene_down_known.csv',
    'rawcount_tom_con_cold_14_gene', :degdir || 'rawcount_tom_con_cold_14_gene.csv', 'gene');
select time_series_exp('tom_con_hot_14_gene', :degdir || 'res_tom_con_hot_14_gene.csv', :degdir || 'res_tom_con_hot_14_gene_up_all.csv',
    :degdir || 'res_tom_con_hot_14_gene_up_known.csv', 'normcount_tom_con_hot_14_gene', :degdir || 'normcount_tom_con_hot_14_gene.csv',
    :degdir || 'res_tom_con_hot_14_gene_down_all.csv', :degdir || 'res_tom_con_hot_14_gene_down_known.csv',
    'rawcount_tom_con_hot_14_gene', :degdir || 'rawcount_tom_con_hot_14_gene.csv', 'gene');
select time_series_exp('tom_cold_hot_14_gene', :degdir || 'res_tom_cold_hot_14_gene.csv', :degdir || 'res_tom_cold_hot_14_gene_up_all.csv',
    :degdir || 'res_tom_cold_hot_14_gene_up_known.csv', 'normcount_tom_cold_hot_14_gene', :degdir || 'normcount_tom_cold_hot_14_gene.csv',
    :degdir || 'res_tom_cold_hot_14_gene_down_all.csv', :degdir || 'res_tom_cold_hot_14_gene_down_known.csv',
    'rawcount_tom_cold_hot_14_gene', :degdir || 'rawcount_tom_cold_hot_14_gene.csv', 'gene');
select time_series_exp('tom_con_cold_21_gene', :degdir || 'res_tom_con_cold_21_gene.csv', :degdir || 'res_tom_con_cold_21_gene_up_all.csv',
    :degdir || 'res_tom_con_cold_21_gene_up_known.csv', 'normcount_tom_con_cold_21_gene', :degdir || 'normcount_tom_con_cold_21_gene.csv',
    :degdir || 'res_tom_con_cold_21_gene_down_all.csv', :degdir || 'res_tom_con_cold_21_gene_down_known.csv',
    'rawcount_tom_con_cold_21_gene', :degdir || 'rawcount_tom_con_cold_21_gene.csv', 'gene');
select time_series_exp('tom_con_hot_21_gene', :degdir || 'res_tom_con_hot_21_gene.csv', :degdir || 'res_tom_con_hot_21_gene_up_all.csv',
    :degdir || 'res_tom_con_hot_21_gene_up_known.csv', 'normcount_tom_con_hot_21_gene', :degdir || 'normcount_tom_con_hot_21_gene.csv',
    :degdir || 'res_tom_con_hot_21_gene_down_all.csv', :degdir || 'res_tom_con_hot_21_gene_down_known.csv',
    'rawcount_tom_con_hot_21_gene', :degdir || 'rawcount_tom_con_hot_21_gene.csv', 'gene');
select time_series_exp('tom_cold_hot_21_gene', :degdir || 'res_tom_cold_hot_21_gene.csv', :degdir || 'res_tom_cold_hot_21_gene_up_all.csv',
    :degdir || 'res_tom_cold_hot_21_gene_up_known.csv', 'normcount_tom_cold_hot_21_gene', :degdir || 'normcount_tom_cold_hot_21_gene.csv',
    :degdir || 'res_tom_cold_hot_21_gene_down_all.csv', :degdir || 'res_tom_cold_hot_21_gene_down_known.csv',
    'rawcount_tom_cold_hot_21_gene', :degdir || 'rawcount_tom_cold_hot_21_gene.csv', 'gene');


--tomato transcript treatment series   
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running tomato treatment series deg analysis transcript level';
END
$$;
select time_series_exp('tom_con_cold_7_trn', :degdir || 'res_tom_con_cold_7_trn.csv', :degdir || 'res_tom_con_cold_7_trn_up_all.csv',
    :degdir || 'res_tom_con_cold_7_trn_up_known.csv', 'normcount_tom_con_cold_7_trn', :degdir || 'normcount_tom_con_cold_7_trn.csv',
    :degdir || 'res_tom_con_cold_7_trn_down_all.csv', :degdir || 'res_tom_con_cold_7_trn_down_known.csv',
    'rawcount_tom_con_cold_7_trn', :degdir || 'rawcount_tom_con_cold_7_trn.csv', 'trn');
select time_series_exp('tom_con_hot_7_trn', :degdir || 'res_tom_con_hot_7_trn.csv', :degdir || 'res_tom_con_hot_7_trn_up_all.csv',
    :degdir || 'res_tom_con_hot_7_trn_up_known.csv', 'normcount_tom_con_hot_7_trn', :degdir || 'normcount_tom_con_hot_7_trn.csv',
    :degdir || 'res_tom_con_hot_7_trn_down_all.csv', :degdir || 'res_tom_con_hot_7_trn_down_known.csv',
    'rawcount_tom_con_hot_7_trn', :degdir || 'rawcount_tom_con_hot_7_trn.csv', 'trn');
select time_series_exp('tom_cold_hot_7_trn', :degdir || 'res_tom_cold_hot_7_trn.csv', :degdir || 'res_tom_cold_hot_7_trn_up_all.csv',
    :degdir || 'res_tom_cold_hot_7_trn_up_known.csv', 'normcount_tom_cold_hot_7_trn', :degdir || 'normcount_tom_cold_hot_7_trn.csv',
    :degdir || 'res_tom_cold_hot_7_trn_down_all.csv', :degdir || 'res_tom_cold_hot_7_trn_down_known.csv',
    'rawcount_tom_cold_hot_7_trn', :degdir || 'rawcount_tom_cold_hot_7_trn.csv', 'trn');
select time_series_exp('tom_con_cold_14_trn', :degdir || 'res_tom_con_cold_14_trn.csv', :degdir || 'res_tom_con_cold_14_trn_up_all.csv',
    :degdir || 'res_tom_con_cold_14_trn_up_known.csv', 'normcount_tom_con_cold_14_trn', :degdir || 'normcount_tom_con_cold_14_trn.csv',
    :degdir || 'res_tom_con_cold_14_trn_down_all.csv', :degdir || 'res_tom_con_cold_14_trn_down_known.csv',
    'rawcount_tom_con_cold_14_trn', :degdir || 'rawcount_tom_con_cold_14_trn.csv', 'trn');
select time_series_exp('tom_con_hot_14_trn', :degdir || 'res_tom_con_hot_14_trn.csv', :degdir || 'res_tom_con_hot_14_trn_up_all.csv',
    :degdir || 'res_tom_con_hot_14_trn_up_known.csv', 'normcount_tom_con_hot_14_trn', :degdir || 'normcount_tom_con_hot_14_trn.csv',
    :degdir || 'res_tom_con_hot_14_trn_down_all.csv', :degdir || 'res_tom_con_hot_14_trn_down_known.csv',
    'rawcount_tom_con_hot_14_trn', :degdir || 'rawcount_tom_con_hot_14_trn.csv', 'trn');
select time_series_exp('tom_cold_hot_14_trn', :degdir || 'res_tom_cold_hot_14_trn.csv', :degdir || 'res_tom_cold_hot_14_trn_up_all.csv',
    :degdir || 'res_tom_cold_hot_14_trn_up_known.csv', 'normcount_tom_cold_hot_14_trn', :degdir || 'normcount_tom_cold_hot_14_trn.csv',
    :degdir || 'res_tom_cold_hot_14_trn_down_all.csv', :degdir || 'res_tom_cold_hot_14_trn_down_known.csv',
    'rawcount_tom_cold_hot_14_trn', :degdir || 'rawcount_tom_cold_hot_14_trn.csv', 'trn');
select time_series_exp('tom_con_cold_21_trn', :degdir || 'res_tom_con_cold_21_trn.csv', :degdir || 'res_tom_con_cold_21_trn_up_all.csv',
    :degdir || 'res_tom_con_cold_21_trn_up_known.csv', 'normcount_tom_con_cold_21_trn', :degdir || 'normcount_tom_con_cold_21_trn.csv',
    :degdir || 'res_tom_con_cold_21_trn_down_all.csv', :degdir || 'res_tom_con_cold_21_trn_down_known.csv',
    'rawcount_tom_con_cold_21_trn', :degdir || 'rawcount_tom_con_cold_21_trn.csv', 'trn');
select time_series_exp('tom_con_hot_21_trn', :degdir || 'res_tom_con_hot_21_trn.csv', :degdir || 'res_tom_con_hot_21_trn_up_all.csv',
    :degdir || 'res_tom_con_hot_21_trn_up_known.csv', 'normcount_tom_con_hot_21_trn', :degdir || 'normcount_tom_con_hot_21_trn.csv',
    :degdir || 'res_tom_con_hot_21_trn_down_all.csv', :degdir || 'res_tom_con_hot_21_trn_down_known.csv',
    'rawcount_tom_con_hot_21_trn', :degdir || 'rawcount_tom_con_hot_21_trn.csv', 'trn');
select time_series_exp('tom_cold_hot_21_trn', :degdir || 'res_tom_cold_hot_21_trn.csv', :degdir || 'res_tom_cold_hot_21_trn_up_all.csv',
    :degdir || 'res_tom_cold_hot_21_trn_up_known.csv', 'normcount_tom_cold_hot_21_trn', :degdir || 'normcount_tom_cold_hot_21_trn.csv',
    :degdir || 'res_tom_cold_hot_21_trn_down_all.csv', :degdir || 'res_tom_cold_hot_21_trn_down_known.csv',
    'rawcount_tom_cold_hot_21_trn', :degdir || 'rawcount_tom_cold_hot_21_trn.csv', 'trn');

 ^^^^^^^^^^^^^ */

 /* ^^^^^^^^^^^^^ 
--create DEG stat table
create or replace function deg_stat(_deg_stat_table varchar(20), _conds varchar(20), _deg_table_pot varchar(20),
     _deg_table_tom varchar(20))
    returns void as
    $body$
    begin
       execute format('update %I set pot_upk=(select count(*) from %I where padj<0.05 and log2fc>=1 and id !~ ''MSTRG'')
                        where conds=''%I'' ', _deg_stat_table, _deg_table_pot, _conds);
       execute format('update %I set pot_downk=(select count(*) from %I where padj<0.05 and log2fc<=-1 and id !~ ''MSTRG'')
                        where conds=''%I'' ', _deg_stat_table, _deg_table_pot, _conds);
       execute format('update %I set tom_upk=(select count(*) from %I where padj<0.05 and log2fc>=1 and id !~ ''MSTRG'')
                        where conds=''%I'' ', _deg_stat_table, _deg_table_tom, _conds);
       execute format('update %I set tom_downk=(select count(*) from %I where padj<0.05 and log2fc<=-1 and id !~ ''MSTRG'')
                        where conds=''%I'' ', _deg_stat_table, _deg_table_tom, _conds);

       execute format('update %I set pot_upa=(select count(*) from %I where padj<0.05 and log2fc>=1 )
                        where conds=''%I'' ', _deg_stat_table, _deg_table_pot, _conds);
       execute format('update %I set pot_downa=(select count(*) from %I where padj<0.05 and log2fc<=-1)
                        where conds=''%I'' ', _deg_stat_table, _deg_table_pot, _conds);
       execute format('update %I set tom_upa=(select count(*) from %I where padj<0.05 and log2fc>=1)
                        where conds=''%I'' ', _deg_stat_table, _deg_table_tom, _conds);
       execute format('update %I set tom_downa=(select count(*) from %I where padj<0.05 and log2fc<=-1)
                        where conds=''%I'' ', _deg_stat_table, _deg_table_tom, _conds);
    end
    $body$ language "plpgsql";

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'DEG stat table ';
END
$$;
drop table if exists pot_tom_trn_known_deg_stat, pot_tom_trn_deg_stat;
create table pot_tom_trn_deg_stat(conds varchar(20));
\copy pot_tom_trn_deg_stat from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/conds.csv' with csv header delimiter as ',';
alter table pot_tom_trn_deg_stat add pot_upk int, add pot_downk int, add tom_upk int, add tom_downk int,
                                 add pot_upa int, add pot_downa int, add tom_upa int, add tom_downa int;   ;
select deg_stat('pot_tom_trn_deg_stat', 'cnvscd7', 'pot_con_cold_7_trn', 'tom_con_cold_7_trn');
select deg_stat('pot_tom_trn_deg_stat', 'cnvsht7', 'pot_con_hot_7_trn', 'tom_con_hot_7_trn' );
select deg_stat('pot_tom_trn_deg_stat', 'cdvsht7', 'pot_cold_hot_7_trn',  'tom_cold_hot_7_trn');
select deg_stat('pot_tom_trn_deg_stat', 'cnvscd14', 'pot_con_cold_14_trn', 'tom_con_cold_14_trn');
select deg_stat('pot_tom_trn_deg_stat', 'cnvsht14', 'pot_con_hot_14_trn', 'tom_con_hot_14_trn' );
select deg_stat('pot_tom_trn_deg_stat', 'cdvsht14', 'pot_cold_hot_14_trn', 'tom_cold_hot_14_trn');
select deg_stat('pot_tom_trn_deg_stat', 'cnvscd21', 'pot_con_cold_21_trn',  'tom_con_cold_21_trn');
select deg_stat('pot_tom_trn_deg_stat', 'cnvsht21', 'pot_con_hot_21_trn', 'tom_con_hot_21_trn');
select deg_stat('pot_tom_trn_deg_stat', 'cdvsht21', 'pot_cold_hot_21_trn', 'tom_cold_hot_21_trn');
\copy pot_tom_trn_deg_stat to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/pot_tom_trn_known_deg_stat.csv' with (FORMAT csv, header true);
 ^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^ 
--heatmap potato/tomato
--plot heatmap
--for only known

create or replace function public.update_cov(_hm_table_all varchar(20), _hm_table_known varchar(20), _conds varchar(20),
    _cv varchar(20), _c1 varchar(20), _c2 varchar(20), _c3 varchar(20), _t1 varchar(20), _t2 varchar(20), _t3 varchar(20))
    returns void as
    $body$
    begin
        execute format('drop table if exists temp, temp1');
        execute format('create table temp as select id, stddev_samp(val)/avg(val) as cv from (select id,
                unnest(array[%I, %I, %I, %I, %I, %I]) val from %I where (%I+%I+%I+%I+%I+%I)!=0  ) sub group
                by id', _c1, _c2, _c3, _t1, _t2, _t3, _hm_table_all, _c1, _c2, _c3, _t1, _t2, _t3);
        execute format('create table temp1 as select id, stddev_samp(val)/avg(val) as cv from (select id,
                unnest(array[%I, %I, %I, %I, %I, %I]) val from %I where (%I+%I+%I+%I+%I+%I)!=0 ) sub group
                by id', _c1, _c2, _c3, _t1, _t2, _t3, _hm_table_known, _c1, _c2, _c3, _t1, _t2, _t3);
        execute format('update %I as a set %I=b.cv from temp as b where a.id=b.id', _hm_table_all, _cv);
        execute format('update %I as a set %I=b.cv from temp1 as b where a.id=b.id', _hm_table_known, _cv);
    end
   $body$ language "plpgsql";

create or replace function public.update_hm_pot(_hm_table_all varchar(20), _hm_table_known varchar(20),  _con_cold_7
    varchar(20), _con_hot_7 varchar(20), _cold_hot_7 varchar(20),  _con_cold_14 varchar(20),
    _con_hot_14 varchar(20), _cold_hot_14 varchar(20), _con_cold_21 varchar(20), _con_hot_21 varchar(20),
    _cold_hot_21 varchar(20), _cnvscd7 varchar(20), _cnvsht7 varchar(20), _cdvsht7 varchar(20), _cnvscd14 varchar(20),
    _cnvsht14 varchar(20), _cdvsht14 varchar(20), _cnvscd21 varchar(20), _cnvsht21 varchar(20), _cdvsht21 varchar(20),
    _cnvscd7_cv varchar(20), _cnvsht7_cv varchar(20), _cdvsht7_cv varchar(20), _cnvscd14_cv varchar(20),
    _cnvsht14_cv varchar(20), _cdvsht14_cv varchar(20), _cnvscd21_cv varchar(20), _cnvsht21_cv varchar(20), _cdvsht21_cv 
    varchar(20), _species varchar(20), _id_type varchar(20), _out_hm_table_all varchar(20), _out_hm_table_known 
    varchar(20))
    
    returns void as
    $body$
    begin
        --all is known and novel i.e including MSTRG
        raise notice 'drop table';
        execute format('drop table if exists %I, %I', _hm_table_all, _hm_table_known );
        --for known and novel genes; only deg |log2fc|>=1 and padj<0.05
        raise notice 'create table for all';
        execute format('create table %I as
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) ' ,
            _hm_table_all, _con_cold_7, _con_hot_7, _cold_hot_7, _con_cold_14, _con_hot_14, _cold_hot_14, _con_cold_21,
            _con_hot_21, _cold_hot_21 );
        --for known genes; only deg |log2fc|>=1 and padj<0.05
        raise notice 'create table for known';
        execute format('create table %I as
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) and id !~''MSTRG'' union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) and id !~''MSTRG'' union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) and id !~''MSTRG'' union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) and id !~''MSTRG'' union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) and id !~''MSTRG'' union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) and id !~''MSTRG'' union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) and id !~''MSTRG'' union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) and id !~''MSTRG'' union
            select id from %I where padj<0.05 and (log2fc>=1 or log2fc<=-1) and id !~''MSTRG'' ' ,
            _hm_table_known, _con_cold_7, _con_hot_7, _cold_hot_7, _con_cold_14, _con_hot_14, _cold_hot_14, _con_cold_21,
            _con_hot_21, _cold_hot_21 );
        raise notice 'alter table for all';
        execute format('alter table %I add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision, add anot varchar(300),
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add cn71_norm double precision, add cn72_norm double precision, add cn73_norm double precision,
                        add cd71_norm double precision, add cd72_norm double precision, add cd73_norm double precision,
                        add ht71_norm double precision, add ht72_norm double precision, add ht73_norm double precision,
                        add cn141_norm double precision, add cn142_norm double precision, add cn143_norm double precision,
                        add cd141_norm double precision, add cd142_norm double precision, add cd143_norm double precision,
                        add ht141_norm double precision, add ht142_norm double precision, add ht143_norm double precision,
                        add cn211_norm double precision, add cn212_norm double precision, add cn213_norm double precision,
                        add cd211_norm double precision, add cd212_norm double precision, add cd213_norm double precision,
                        add ht211_norm double precision, add ht212_norm double precision, add ht213_norm double precision,
                        add cn71_raw double precision, add cn72_raw double precision, add cn73_raw double precision,
                        add cd71_raw double precision, add cd72_raw double precision, add cd73_raw double precision,
                        add ht71_raw double precision, add ht72_raw double precision, add ht73_raw double precision,
                        add cn141_raw double precision, add cn142_raw double precision, add cn143_raw double precision,
                        add cd141_raw double precision, add cd142_raw double precision, add cd143_raw double precision,
                        add ht141_raw double precision, add ht142_raw double precision, add ht143_raw double precision,
                        add cn211_raw double precision, add cn212_raw double precision, add cn213_raw double precision,
                        add cd211_raw double precision, add cd212_raw double precision, add cd213_raw double precision,
                        add ht211_raw double precision, add ht212_raw double precision, add ht213_raw double precision' ,
                        _hm_table_all, _cnvscd7, _cnvsht7, _cdvsht7, _cnvscd14, _cnvsht14, _cdvsht14, _cnvscd21,
                        _cnvsht21, _cdvsht21, _cnvscd7_cv, _cnvsht7_cv, _cdvsht7_cv, _cnvscd14_cv, _cnvsht14_cv, 
                        _cdvsht14_cv, _cnvscd21_cv, _cnvsht21_cv, _cdvsht21_cv);                                                 
        raise notice 'alter table for known';
        execute format('alter table %I add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision, add anot varchar(300),
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add cn71_norm double precision, add cn72_norm double precision, add cn73_norm double precision,
                        add cd71_norm double precision, add cd72_norm double precision, add cd73_norm double precision,
                        add ht71_norm double precision, add ht72_norm double precision, add ht73_norm double precision,
                        add cn141_norm double precision, add cn142_norm double precision, add cn143_norm double precision,
                        add cd141_norm double precision, add cd142_norm double precision, add cd143_norm double precision,
                        add ht141_norm double precision, add ht142_norm double precision, add ht143_norm double precision,
                        add cn211_norm double precision, add cn212_norm double precision, add cn213_norm double precision,
                        add cd211_norm double precision, add cd212_norm double precision, add cd213_norm double precision,
                        add ht211_norm double precision, add ht212_norm double precision, add ht213_norm double precision,
                        add cn71_raw double precision, add cn72_raw double precision, add cn73_raw double precision,
                        add cd71_raw double precision, add cd72_raw double precision, add cd73_raw double precision,
                        add ht71_raw double precision, add ht72_raw double precision, add ht73_raw double precision,
                        add cn141_raw double precision, add cn142_raw double precision, add cn143_raw double precision,
                        add cd141_raw double precision, add cd142_raw double precision, add cd143_raw double precision,
                        add ht141_raw double precision, add ht142_raw double precision, add ht143_raw double precision,
                        add cn211_raw double precision, add cn212_raw double precision, add cn213_raw double precision,
                        add cd211_raw double precision, add cd212_raw double precision, add cd213_raw double precision,
                        add ht211_raw double precision, add ht212_raw double precision, add ht213_raw double precision' ,
                        _hm_table_known, _cnvscd7, _cnvsht7, _cdvsht7, _cnvscd14, _cnvsht14, _cdvsht14, _cnvscd21,
                        _cnvsht21, _cdvsht21, _cnvscd7_cv, _cnvsht7_cv, _cdvsht7_cv, _cnvscd14_cv, _cnvsht14_cv, 
                        _cdvsht14_cv, _cnvscd21_cv, _cnvsht21_cv, _cdvsht21_cv);
        --set values to columns where log2fc >= |1|; its for plotting heatmpa and having values instead of 0 
        raise notice 'update table for all where log2fc >= |1| and padj < 0.05';
        execute format('update %I as a set %I = b.log2fc, cn71_norm=b.c1_norm, cn72_norm=b.c2_norm, cn73_norm=b.c3_norm,
                        cd71_norm=b.t1_norm, cd72_norm=b.t2_norm, cd73_norm=b.t3_norm, cn71_raw=b.c1_raw, cn72_raw=b.c2_raw,
                        cn73_raw=b.c3_raw, cd71_raw=b.t1_raw, cd72_raw=b.t2_raw, cd73_raw=b.t3_raw from %I as b where a.id=b.id and
                        b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_all, _cnvscd7, _con_cold_7);
        execute format('update %I as a set %I = b.log2fc, cn71_norm=b.c1_norm, cn72_norm=b.c2_norm, cn73_norm=b.c3_norm,
                        ht71_norm=b.t1_norm, ht72_norm=b.t2_norm, ht73_norm=b.t3_norm, ht71_raw=b.t1_raw, ht72_raw=b.t2_raw,
                        ht73_raw=b.t3_raw from %I as b where a.id=b.id and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)',
                         _hm_table_all, _cnvsht7, _con_hot_7);
        execute format('update %I as a set %I = b.log2fc, cd71_norm=b.c1_norm, cd72_norm=b.c2_norm, cd73_norm=b.c3_norm,
                        ht71_norm=b.t1_norm, ht72_norm=b.t2_norm, ht73_norm=b.t3_norm, cd71_raw=b.c1_raw, cd72_raw=b.c2_raw,
                        cd73_raw=b.c3_raw, ht71_raw=b.t1_raw, ht72_raw=b.t2_raw, ht73_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1) ', _hm_table_all, _cdvsht7, _cold_hot_7);
        execute format('update %I as a set %I = b.log2fc, cn141_norm=b.c1_norm, cn142_norm=b.c2_norm, cn143_norm=b.c3_norm,
                        cd141_norm=b.t1_norm, cd142_norm=b.t2_norm, cd143_norm=b.t3_norm, cn141_raw=b.c1_raw, cn142_raw=b.c2_raw,
                        cn143_raw=b.c3_raw, cd141_raw=b.t1_raw, cd142_raw=b.t2_raw, cd143_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)',_hm_table_all, _cnvscd14, _con_cold_14);
        execute format('update %I as a set %I = b.log2fc, cn141_norm=b.c1_norm, cn142_norm=b.c2_norm, cn143_norm=b.c3_norm,
                        ht141_norm=b.t1_norm, ht142_norm=b.t2_norm, ht143_norm=b.t3_norm, cn141_raw=b.c1_raw, cn142_raw=b.c2_raw,
                        cn143_raw=b.c3_raw, ht141_raw=b.t1_raw, ht142_raw=b.t2_raw, ht143_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_all, _cnvsht14, _con_hot_14);
        execute format('update %I as a set %I = b.log2fc, cd141_norm=b.c1_norm, cd142_norm=b.c2_norm, cd143_norm=b.c3_norm,
                        ht141_norm=b.t1_norm, ht142_norm=b.t2_norm, ht143_norm=b.t3_norm, cd141_raw=b.c1_raw, cd142_raw=b.c2_raw,
                        cd143_raw=b.c3_raw, ht141_raw=b.t1_raw, ht142_raw=b.t2_raw, ht143_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_all, _cdvsht14, _cold_hot_14);
        execute format('update %I as a set %I = b.log2fc, cn211_norm=b.c1_norm, cn212_norm=b.c2_norm, cn213_norm=b.c3_norm,
                        cd211_norm=b.t1_norm, cd212_norm=b.t2_norm, cd213_norm=b.t3_norm, cn211_raw=b.c1_raw, cn212_raw=b.c2_raw,
                        cn213_raw=b.c3_raw, cd211_raw=b.t1_raw, cd212_raw=b.t2_raw, cd213_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_all, _cnvscd21, _con_cold_21);
        execute format('update %I as a set %I = b.log2fc, cn211_norm=b.c1_norm, cn212_norm=b.c2_norm, cn213_norm=b.c3_norm,
                        ht211_norm=b.t1_norm, ht212_norm=b.t2_norm, ht213_norm=b.t3_norm, cn211_raw=b.c1_raw, cn212_raw=b.c2_raw,
                        cn213_raw=b.c3_raw, ht211_raw=b.t1_raw, ht212_raw=b.t2_raw, ht213_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_all, _cnvsht21, _con_hot_21);
        execute format('update %I as a set %I = b.log2fc, cd211_norm=b.c1_norm, cd212_norm=b.c2_norm, cd213_norm=b.c3_norm,
                        ht211_norm=b.t1_norm, ht212_norm=b.t2_norm, ht213_norm=b.t3_norm, cd211_raw=b.c1_raw, cd212_raw=b.c2_raw,
                        cd213_raw=b.c3_raw, ht211_raw=b.t1_raw, ht212_raw=b.t2_raw, ht213_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_all, _cdvsht21, _cold_hot_21);
        raise notice 'update table for known where padj<0.05';
        execute format('update %I as a set %I = b.log2fc, cn71_norm=b.c1_norm, cn72_norm=b.c2_norm, cn73_norm=b.c3_norm,
                        cd71_norm=b.t1_norm, cd72_norm=b.t2_norm, cd73_norm=b.t3_norm, cn71_raw=b.c1_raw, cn72_raw=b.c2_raw,
                        cn73_raw=b.c3_raw, cd71_raw=b.t1_raw, cd72_raw=b.t2_raw, cd73_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_known, _cnvscd7, _con_cold_7);
        execute format('update %I as a set %I = b.log2fc, cn71_norm=b.c1_norm, cn72_norm=b.c2_norm, cn73_norm=b.c3_norm,
                        ht71_norm=b.t1_norm, ht72_norm=b.t2_norm, ht73_norm=b.t3_norm, ht71_raw=b.t1_raw, ht72_raw=b.t2_raw,
                        ht73_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_known, _cnvsht7, _con_hot_7);
        execute format('update %I as a set %I = b.log2fc, cd71_norm=b.c1_norm, cd72_norm=b.c2_norm, cd73_norm=b.c3_norm,
                        ht71_norm=b.t1_norm, ht72_norm=b.t2_norm, ht73_norm=b.t3_norm, cd71_raw=b.c1_raw, cd72_raw=b.c2_raw,
                        cd73_raw=b.c3_raw, ht71_raw=b.t1_raw, ht72_raw=b.t2_raw, ht73_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_known, _cdvsht7, _cold_hot_7);
        execute format('update %I as a set %I = b.log2fc, cn141_norm=b.c1_norm, cn142_norm=b.c2_norm, cn143_norm=b.c3_norm,
                        cd141_norm=b.t1_norm, cd142_norm=b.t2_norm, cd143_norm=b.t3_norm, cn141_raw=b.c1_raw, cn142_raw=b.c2_raw,
                        cn143_raw=b.c3_raw, cd141_raw=b.t1_raw, cd142_raw=b.t2_raw, cd143_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_known, _cnvscd14, _con_cold_14);
        execute format('update %I as a set %I = b.log2fc, cn141_norm=b.c1_norm, cn142_norm=b.c2_norm, cn143_norm=b.c3_norm,
                        ht141_norm=b.t1_norm, ht142_norm=b.t2_norm, ht143_norm=b.t3_norm, cn141_raw=b.c1_raw, cn142_raw=b.c2_raw,
                        cn143_raw=b.c3_raw, ht141_raw=b.t1_raw, ht142_raw=b.t2_raw, ht143_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_known, _cnvsht14, _con_hot_14);
        execute format('update %I as a set %I = b.log2fc, cd141_norm=b.c1_norm, cd142_norm=b.c2_norm, cd143_norm=b.c3_norm,
                        ht141_norm=b.t1_norm, ht142_norm=b.t2_norm, ht143_norm=b.t3_norm, cd141_raw=b.c1_raw, cd142_raw=b.c2_raw,
                        cd143_raw=b.c3_raw, ht141_raw=b.t1_raw, ht142_raw=b.t2_raw, ht143_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_known, _cdvsht14, _cold_hot_14);
        execute format('update %I as a set %I = b.log2fc, cn211_norm=b.c1_norm, cn212_norm=b.c2_norm, cn213_norm=b.c3_norm,
                        cd211_norm=b.t1_norm, cd212_norm=b.t2_norm, cd213_norm=b.t3_norm, cn211_raw=b.c1_raw, cn212_raw=b.c2_raw,
                        cn213_raw=b.c3_raw, cd211_raw=b.t1_raw, cd212_raw=b.t2_raw, cd213_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_known, _cnvscd21, _con_cold_21);
        execute format('update %I as a set %I = b.log2fc, cn211_norm=b.c1_norm, cn212_norm=b.c2_norm, cn213_norm=b.c3_norm,
                        ht211_norm=b.t1_norm, ht212_norm=b.t2_norm, ht213_norm=b.t3_norm, cn211_raw=b.c1_raw, cn212_raw=b.c2_raw,
                        cn213_raw=b.c3_raw, ht211_raw=b.t1_raw, ht212_raw=b.t2_raw, ht213_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_known, _cnvsht21, _con_hot_21);
        execute format('update %I as a set %I = b.log2fc, cd211_norm=b.c1_norm, cd212_norm=b.c2_norm, cd213_norm=b.c3_norm,
                        ht211_norm=b.t1_norm, ht212_norm=b.t2_norm, ht213_norm=b.t3_norm, cd211_raw=b.c1_raw, cd212_raw=b.c2_raw,
                        cd213_raw=b.c3_raw, ht211_raw=b.t1_raw, ht212_raw=b.t2_raw, ht213_raw=b.t3_raw from %I as b where a.id=b.id
                        and b.padj<0.05 and (b.log2fc>=1 or b.log2fc<=-1)', _hm_table_known, _cdvsht21, _cold_hot_21);
        --set values to columns where log2fc < |1|; its for plotting heatmpa and having values instead of 0
        raise notice 'update table for all where  log2fc <=1 or -1';
        execute format('update %I as a set %I = b.log2fc, cn71_norm=b.c1_norm, cn72_norm=b.c2_norm, cn73_norm=b.c3_norm,
                        cd71_norm=b.t1_norm, cd72_norm=b.t2_norm, cd73_norm=b.t3_norm, cn71_raw=b.c1_raw, cn72_raw=b.c2_raw,
                        cn73_raw=b.c3_raw, cd71_raw=b.t1_raw, cd72_raw=b.t2_raw, cd73_raw=b.t3_raw from %I as b where a.id=b.id and
                        (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_all, _cnvscd7, _con_cold_7, _cnvscd7);
        execute format('update %I as a set %I = b.log2fc, cn71_norm=b.c1_norm, cn72_norm=b.c2_norm, cn73_norm=b.c3_norm,
                        ht71_norm=b.t1_norm, ht72_norm=b.t2_norm, ht73_norm=b.t3_norm, ht71_raw=b.t1_raw, ht72_raw=b.t2_raw,
                        ht73_raw=b.t3_raw from %I as b where a.id=b.id and (b.log2fc<1 or b.log2fc>-1) and a.%I is null',
                         _hm_table_all, _cnvsht7, _con_hot_7, _cnvsht7);
        execute format('update %I as a set %I = b.log2fc, cd71_norm=b.c1_norm, cd72_norm=b.c2_norm, cd73_norm=b.c3_norm,
                        ht71_norm=b.t1_norm, ht72_norm=b.t2_norm, ht73_norm=b.t3_norm, cd71_raw=b.c1_raw, cd72_raw=b.c2_raw,
                        cd73_raw=b.c3_raw, ht71_raw=b.t1_raw, ht72_raw=b.t2_raw, ht73_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_all, _cdvsht7, _cold_hot_7, _cdvsht7);
        execute format('update %I as a set %I = b.log2fc, cn141_norm=b.c1_norm, cn142_norm=b.c2_norm, cn143_norm=b.c3_norm,
                        cd141_norm=b.t1_norm, cd142_norm=b.t2_norm, cd143_norm=b.t3_norm, cn141_raw=b.c1_raw, cn142_raw=b.c2_raw,
                        cn143_raw=b.c3_raw, cd141_raw=b.t1_raw, cd142_raw=b.t2_raw, cd143_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null',_hm_table_all, _cnvscd14, _con_cold_14, _cnvscd14);
        execute format('update %I as a set %I = b.log2fc, cn141_norm=b.c1_norm, cn142_norm=b.c2_norm, cn143_norm=b.c3_norm,
                        ht141_norm=b.t1_norm, ht142_norm=b.t2_norm, ht143_norm=b.t3_norm, cn141_raw=b.c1_raw, cn142_raw=b.c2_raw,
                        cn143_raw=b.c3_raw, ht141_raw=b.t1_raw, ht142_raw=b.t2_raw, ht143_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_all, _cnvsht14, _con_hot_14, _cnvsht14);
        execute format('update %I as a set %I = b.log2fc, cd141_norm=b.c1_norm, cd142_norm=b.c2_norm, cd143_norm=b.c3_norm,
                        ht141_norm=b.t1_norm, ht142_norm=b.t2_norm, ht143_norm=b.t3_norm, cd141_raw=b.c1_raw, cd142_raw=b.c2_raw,
                        cd143_raw=b.c3_raw, ht141_raw=b.t1_raw, ht142_raw=b.t2_raw, ht143_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_all, _cdvsht14, _cold_hot_14, _cdvsht14);
        execute format('update %I as a set %I = b.log2fc, cn211_norm=b.c1_norm, cn212_norm=b.c2_norm, cn213_norm=b.c3_norm,
                        cd211_norm=b.t1_norm, cd212_norm=b.t2_norm, cd213_norm=b.t3_norm, cn211_raw=b.c1_raw, cn212_raw=b.c2_raw,
                        cn213_raw=b.c3_raw, cd211_raw=b.t1_raw, cd212_raw=b.t2_raw, cd213_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_all, _cnvscd21, _con_cold_21, _cnvscd21);
        execute format('update %I as a set %I = b.log2fc, cn211_norm=b.c1_norm, cn212_norm=b.c2_norm, cn213_norm=b.c3_norm,
                        ht211_norm=b.t1_norm, ht212_norm=b.t2_norm, ht213_norm=b.t3_norm, cn211_raw=b.c1_raw, cn212_raw=b.c2_raw,
                        cn213_raw=b.c3_raw, ht211_raw=b.t1_raw, ht212_raw=b.t2_raw, ht213_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_all, _cnvsht21, _con_hot_21, _cnvsht21);
        execute format('update %I as a set %I = b.log2fc, cd211_norm=b.c1_norm, cd212_norm=b.c2_norm, cd213_norm=b.c3_norm,
                        ht211_norm=b.t1_norm, ht212_norm=b.t2_norm, ht213_norm=b.t3_norm, cd211_raw=b.c1_raw, cd212_raw=b.c2_raw,
                        cd213_raw=b.c3_raw, ht211_raw=b.t1_raw, ht212_raw=b.t2_raw, ht213_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null ', _hm_table_all, _cdvsht21, _cold_hot_21, _cdvsht21);
        raise notice 'update table for known where padj<0.05';
        execute format('update %I as a set %I = b.log2fc, cn71_norm=b.c1_norm, cn72_norm=b.c2_norm, cn73_norm=b.c3_norm,
                        cd71_norm=b.t1_norm, cd72_norm=b.t2_norm, cd73_norm=b.t3_norm, cn71_raw=b.c1_raw, cn72_raw=b.c2_raw,
                        cn73_raw=b.c3_raw, cd71_raw=b.t1_raw, cd72_raw=b.t2_raw, cd73_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_known, _cnvscd7, _con_cold_7, _cnvscd7);
        execute format('update %I as a set %I = b.log2fc, cn71_norm=b.c1_norm, cn72_norm=b.c2_norm, cn73_norm=b.c3_norm,
                        ht71_norm=b.t1_norm, ht72_norm=b.t2_norm, ht73_norm=b.t3_norm, ht71_raw=b.t1_raw, ht72_raw=b.t2_raw,
                        ht73_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_known, _cnvsht7, _con_hot_7, _cnvsht7);
        execute format('update %I as a set %I = b.log2fc, cd71_norm=b.c1_norm, cd72_norm=b.c2_norm, cd73_norm=b.c3_norm,
                        ht71_norm=b.t1_norm, ht72_norm=b.t2_norm, ht73_norm=b.t3_norm, cd71_raw=b.c1_raw, cd72_raw=b.c2_raw,
                        cd73_raw=b.c3_raw, ht71_raw=b.t1_raw, ht72_raw=b.t2_raw, ht73_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_known, _cdvsht7, _cold_hot_7, _cdvsht7);
        execute format('update %I as a set %I = b.log2fc, cn141_norm=b.c1_norm, cn142_norm=b.c2_norm, cn143_norm=b.c3_norm,
                        cd141_norm=b.t1_norm, cd142_norm=b.t2_norm, cd143_norm=b.t3_norm, cn141_raw=b.c1_raw, cn142_raw=b.c2_raw,
                        cn143_raw=b.c3_raw, cd141_raw=b.t1_raw, cd142_raw=b.t2_raw, cd143_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_known, _cnvscd14, _con_cold_14, _cnvscd14);
        execute format('update %I as a set %I = b.log2fc, cn141_norm=b.c1_norm, cn142_norm=b.c2_norm, cn143_norm=b.c3_norm,
                        ht141_norm=b.t1_norm, ht142_norm=b.t2_norm, ht143_norm=b.t3_norm, cn141_raw=b.c1_raw, cn142_raw=b.c2_raw,
                        cn143_raw=b.c3_raw, ht141_raw=b.t1_raw, ht142_raw=b.t2_raw, ht143_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_known, _cnvsht14, _con_hot_14, _cnvsht14);
        execute format('update %I as a set %I = b.log2fc, cd141_norm=b.c1_norm, cd142_norm=b.c2_norm, cd143_norm=b.c3_norm,
                        ht141_norm=b.t1_norm, ht142_norm=b.t2_norm, ht143_norm=b.t3_norm, cd141_raw=b.c1_raw, cd142_raw=b.c2_raw,
                        cd143_raw=b.c3_raw, ht141_raw=b.t1_raw, ht142_raw=b.t2_raw, ht143_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_known, _cdvsht14, _cold_hot_14, _cdvsht14);
        execute format('update %I as a set %I = b.log2fc, cn211_norm=b.c1_norm, cn212_norm=b.c2_norm, cn213_norm=b.c3_norm,
                        cd211_norm=b.t1_norm, cd212_norm=b.t2_norm, cd213_norm=b.t3_norm, cn211_raw=b.c1_raw, cn212_raw=b.c2_raw,
                        cn213_raw=b.c3_raw, cd211_raw=b.t1_raw, cd212_raw=b.t2_raw, cd213_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_known, _cnvscd21, _con_cold_21, _cnvscd21);
        execute format('update %I as a set %I = b.log2fc, cn211_norm=b.c1_norm, cn212_norm=b.c2_norm, cn213_norm=b.c3_norm,
                        ht211_norm=b.t1_norm, ht212_norm=b.t2_norm, ht213_norm=b.t3_norm, cn211_raw=b.c1_raw, cn212_raw=b.c2_raw,
                        cn213_raw=b.c3_raw, ht211_raw=b.t1_raw, ht212_raw=b.t2_raw, ht213_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_known, _cnvsht21, _con_hot_21, _cnvsht21);
        execute format('update %I as a set %I = b.log2fc, cd211_norm=b.c1_norm, cd212_norm=b.c2_norm, cd213_norm=b.c3_norm,
                        ht211_norm=b.t1_norm, ht212_norm=b.t2_norm, ht213_norm=b.t3_norm, cd211_raw=b.c1_raw, cd212_raw=b.c2_raw,
                        cd213_raw=b.c3_raw, ht211_raw=b.t1_raw, ht212_raw=b.t2_raw, ht213_raw=b.t3_raw from %I as b where a.id=b.id
                        and (b.log2fc<1 or b.log2fc>-1) and a.%I is null', _hm_table_known, _cdvsht21, _cold_hot_21, _cdvsht21);
       --set coefficient of variation (cov) for normalized data to see spread between control and experimental data
       raise notice 'update table for coefficient of variation (cov)' ;
       execute format('select update_cov(''%I'', ''%I'', ''%I'', ''%I'', ''cn71_norm'', ''cn72_norm'', ''cn73_norm'', ''cd71_norm'',
                ''cd72_norm'', ''cd73_norm'' )', _hm_table_all, _hm_table_known, _cnvscd7, _cnvscd7_cv);
       execute format('select update_cov(''%I'', ''%I'', ''%I'', ''%I'', ''cn71_norm'', ''cn72_norm'', ''cn73_norm'', ''ht71_norm'',
                ''ht72_norm'', ''ht73_norm'' )', _hm_table_all, _hm_table_known, _cnvsht7, _cnvsht7_cv);
       execute format('select update_cov(''%I'', ''%I'', ''%I'', ''%I'', ''cd71_norm'', ''cd72_norm'', ''cd73_norm'', ''ht71_norm'',
                ''ht72_norm'', ''ht73_norm'' )', _hm_table_all, _hm_table_known, _cdvsht7, _cdvsht7_cv);
       execute format('select update_cov(''%I'', ''%I'', ''%I'', ''%I'', ''cn141_norm'', ''cn142_norm'', ''cn143_norm'', ''cd141_norm'',
                ''cd142_norm'', ''cd143_norm'' )', _hm_table_all, _hm_table_known, _cnvscd14, _cnvscd14_cv);
       execute format('select update_cov(''%I'', ''%I'', ''%I'', ''%I'', ''cn141_norm'', ''cn142_norm'', ''cn143_norm'', ''ht141_norm'',
                ''ht142_norm'', ''ht143_norm'' )', _hm_table_all, _hm_table_known, _cnvsht14, _cnvsht14_cv);
       execute format('select update_cov(''%I'', ''%I'', ''%I'', ''%I'', ''cd141_norm'', ''cd142_norm'', ''cd143_norm'', ''ht141_norm'',
                ''ht142_norm'', ''ht143_norm'' )', _hm_table_all, _hm_table_known, _cdvsht14, _cdvsht14_cv);
       execute format('select update_cov(''%I'', ''%I'', ''%I'', ''%I'', ''cn211_norm'', ''cn212_norm'', ''cn213_norm'', ''cd211_norm'',
                ''cd212_norm'', ''cd213_norm'' )', _hm_table_all, _hm_table_known, _cnvscd21, _cnvscd21_cv);
       execute format('select update_cov(''%I'', ''%I'', ''%I'', ''%I'', ''cn211_norm'', ''cn212_norm'', ''cn213_norm'', ''ht211_norm'',
                ''ht212_norm'', ''ht212_norm'' )', _hm_table_all, _hm_table_known, _cnvsht21, _cnvsht21_cv);
       execute format('select update_cov(''%I'', ''%I'', ''%I'', ''%I'', ''cd211_norm'', ''cd212_norm'', ''cd213_norm'', ''ht211_norm'',
                ''ht212_norm'', ''ht212_norm'' )', _hm_table_all, _hm_table_known, _cdvsht21, _cdvsht21_cv);

        execute format('update %I set %I = 0 where %I is null', _hm_table_all, _cnvscd7, _cnvscd7);
        execute format('update %I set %I = 0 where %I is null', _hm_table_all, _cnvsht7, _cnvsht7);
        execute format('update %I set %I = 0 where %I is null', _hm_table_all, _cdvsht7, _cdvsht7);
        execute format('update %I set %I = 0 where %I is null', _hm_table_all, _cnvscd14, _cnvscd14);
        execute format('update %I set %I = 0 where %I is null', _hm_table_all, _cnvsht14, _cnvsht14);
        execute format('update %I set %I = 0 where %I is null', _hm_table_all, _cdvsht14, _cdvsht14);
        execute format('update %I set %I = 0 where %I is null', _hm_table_all, _cnvscd21, _cnvscd21);
        execute format('update %I set %I = 0 where %I is null', _hm_table_all, _cnvsht21, _cnvsht21);
        execute format('update %I set %I = 0 where %I is null', _hm_table_all, _cdvsht21, _cdvsht21);
        execute format('update %I set %I = 0 where %I is null', _hm_table_known, _cnvscd7, _cnvscd7);
        execute format('update %I set %I = 0 where %I is null', _hm_table_known, _cnvsht7, _cnvsht7);
        execute format('update %I set %I = 0 where %I is null', _hm_table_known, _cdvsht7, _cdvsht7);
        execute format('update %I set %I = 0 where %I is null', _hm_table_known, _cnvscd14, _cnvscd14);
        execute format('update %I set %I = 0 where %I is null', _hm_table_known, _cnvsht14, _cnvsht14);
        execute format('update %I set %I = 0 where %I is null', _hm_table_known, _cdvsht14, _cdvsht14);
        execute format('update %I set %I = 0 where %I is null', _hm_table_known, _cnvscd21, _cnvscd21);
        execute format('update %I set %I = 0 where %I is null', _hm_table_known, _cnvscd21, _cnvscd21);
        execute format('update %I set %I = 0 where %I is null', _hm_table_known, _cdvsht21, _cdvsht21);

        if _species = 'potato' and _id_type = 'gene'  then
            execute format('update %I as a set anot = b.anot from potato_pgsc403_gff as b where a.id=b.gene',
                       _hm_table_all);
            execute format('update %I as a set anot = b.anot from potato_pgsc403_gff as b where a.id=b.gene',
                       _hm_table_known);
            execute format('copy %I to $$' || _out_hm_table_all || '$$' || ' with (FORMAT csv, header true)' ,
            _hm_table_all);
            execute format('copy %I to $$' || _out_hm_table_known || '$$' || ' with (FORMAT csv, header true)' ,
            _hm_table_known);
        elsif _species = 'potato' and _id_type = 'trn'  then
            execute format('update %I as a set anot = b.anot from potato_pgsc403_gff as b where a.id=b.trn',
                       _hm_table_all);
            execute format('update %I as a set anot = b.anot from potato_pgsc403_gff as b where a.id=b.trn',
                       _hm_table_known);
            execute format('copy %I to $$' || _out_hm_table_all || '$$' || ' with (FORMAT csv, 
							header true)' , _hm_table_all);              
            execute format('copy %I to $$' || _out_hm_table_known || '$$' || ' with (FORMAT csv, header true)' ,
                _hm_table_known);
        elsif _species = 'tomato' and _id_type = 'gene'  then
            execute format('update %I as a set anot = b.anot from tomato_25037_gtf as b where a.id=b.gene',
                       _hm_table_all);
            execute format('update %I as a set anot = b.anot from tomato_25037_gtf as b where a.id=b.gene',
                       _hm_table_known);
            execute format('copy %I to $$' || _out_hm_table_all || '$$' || ' with (FORMAT csv, header true)' ,
                _hm_table_all);
            execute format('copy %I to $$' || _out_hm_table_known || '$$' || ' with (FORMAT csv, header true)' ,
                _hm_table_known);
        elsif _species = 'tomato' and _id_type = 'trn'  then
            execute format('update %I as a set anot = b.anot from tomato_25037_gtf as b where a.id=b.trn',
                       _hm_table_all);
            execute format('update %I as a set anot = b.anot from tomato_25037_gtf as b where a.id=b.trn',
                       _hm_table_known);
            execute format('copy %I to $$' || _out_hm_table_all || '$$' || ' with (FORMAT csv, header true)' ,
                _hm_table_all);
            execute format('copy %I to $$' || _out_hm_table_known || '$$' || ' with (FORMAT csv, header true)' ,
                _hm_table_known);
        end if;

    end
   $body$ language "plpgsql";

\set degdir $$'/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/'$$

DO language plpgsql $$
BEGIN
    RAISE NOTICE 'Running heatmap analysis for potato gene level';
END
$$;
select update_hm_pot('hm_pot_gene_all', 'hm_pot_gene_known', 'pot_con_cold_7_gene', 'pot_con_hot_7_gene',
                     'pot_cold_hot_7_gene', 'pot_con_cold_14_gene', 'pot_con_hot_14_gene', 'pot_cold_hot_14_gene',
                     'pot_con_cold_21_gene', 'pot_con_hot_21_gene', 'pot_cold_hot_21_gene', 'cnvscd7', 'cnvsht7',
                     'cdvsht7', 'cnvscd14', 'cnvsht14', 'cdvsht14', 'cnvscd21', 'cnvsht21', 'cdvsht21', 'cnvscd7_cv',
                     'cnvsht7_cv', 'cdvsht7_cv', 'cnvscd14_cv', 'cnvsht14_cv', 'cdvsht14_cv', 'cnvscd21_cv', 
                     'cnvsht21_cv', 'cdvsht21_cv', 'potato',
                     'gene', :degdir || 'hm_pot_gene_all.csv', :degdir || 'hm_pot_gene_known.csv');

DO language plpgsql $$
BEGIN
    RAISE NOTICE 'Running heatmap analysis for potato transcript level';
END
$$;
select update_hm_pot('hm_pot_trn_all', 'hm_pot_trn_known', 'pot_con_cold_7_trn', 'pot_con_hot_7_trn',
                     'pot_cold_hot_7_trn', 'pot_con_cold_14_trn', 'pot_con_hot_14_trn', 'pot_cold_hot_14_trn',
                     'pot_con_cold_21_trn', 'pot_con_hot_21_trn', 'pot_cold_hot_21_trn', 'cnvscd7', 'cnvsht7',
                     'cdvsht7', 'cnvscd14', 'cnvsht14', 'cdvsht14', 'cnvscd21', 'cnvsht21', 'cdvsht21', 'cnvscd7_cv',
                     'cnvsht7_cv', 'cdvsht7_cv', 'cnvscd14_cv', 'cnvsht14_cv', 'cdvsht14_cv', 'cnvscd21_cv',
                     'cnvsht21_cv', 'cdvsht21_cv', 'potato',
                      'trn', :degdir || 'hm_pot_trn_all.csv', :degdir || 'hm_pot_trn_known.csv');

DO language plpgsql $$
BEGIN
    RAISE NOTICE 'Running heatmap analysis for tomato gene level';
END
$$;
select update_hm_pot('hm_tom_gene_all', 'hm_tom_gene_known', 'tom_con_cold_7_gene', 'tom_con_hot_7_gene',
                     'tom_cold_hot_7_gene', 'tom_con_cold_14_gene', 'tom_con_hot_14_gene', 'tom_cold_hot_14_gene',
                     'tom_con_cold_21_gene', 'tom_con_hot_21_gene', 'tom_cold_hot_21_gene', 'cnvscd7', 'cnvsht7',
                     'cdvsht7', 'cnvscd14', 'cnvsht14', 'cdvsht14', 'cnvscd21', 'cnvsht21', 'cdvsht21', 'cnvscd7_cv',
                     'cnvsht7_cv', 'cdvsht7_cv', 'cnvscd14_cv', 'cnvsht14_cv', 'cdvsht14_cv', 'cnvscd21_cv',
                     'cnvsht21_cv', 'cdvsht21_cv', 'tomato',
                     'gene', :degdir || 'hm_tom_gene_all.csv', :degdir || 'hm_tom_gene_known.csv');

DO language plpgsql $$
BEGIN
    RAISE NOTICE 'Running heatmap analysis for tomato transcript level';
END
$$;
select update_hm_pot('hm_tom_trn_all', 'hm_tom_trn_known', 'tom_con_cold_7_trn', 'tom_con_hot_7_trn',
                     'tom_cold_hot_7_trn', 'tom_con_cold_14_trn', 'tom_con_hot_14_trn', 'tom_cold_hot_14_trn',
                     'tom_con_cold_21_trn', 'tom_con_hot_21_trn', 'tom_cold_hot_21_trn', 'cnvscd7', 'cnvsht7',
                     'cdvsht7', 'cnvscd14', 'cnvsht14', 'cdvsht14', 'cnvscd21', 'cnvsht21', 'cdvsht21', 'cnvscd7_cv',
                     'cnvsht7_cv', 'cdvsht7_cv', 'cnvscd14_cv', 'cnvsht14_cv', 'cdvsht14_cv', 'cnvscd21_cv',
                     'cnvsht21_cv', 'cdvsht21_cv', 'tomato',
                     'trn', :degdir || 'hm_tom_trn_all.csv', :degdir || 'hm_tom_trn_known.csv');

 ^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^ 
--venn potato/tomato
--venn analysis for treatment comparison at each time point eg. cn, cold and hot at 7 days
--cncd, cnht and cdht
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram analysis';
END
$$;

create or replace function venn(_hm_table_all varchar(20), _hm_table_known varchar(20), _venn_table_up_all varchar(20),
    _venn_table_up_known varchar(20), _venn_table_down_all varchar(20), _venn_table_down_known varchar(20),
    _cnvscd varchar(20), _cnvsht varchar(20), _cdvsht varchar(20), _out_venn_table_up_all varchar(20),
    _out_venn_table_up_known varchar(20), _out_venn_table_down_all varchar(20), _out_venn_table_down_known varchar(20),
    _cn1_norm varchar(20), _cn2_norm varchar(20), _cn3_norm varchar(20), _cd1_norm varchar(20), _cd2_norm varchar(20), 
    _cd3_norm varchar(20), _ht1_norm varchar(20), _ht2_norm varchar(20), _ht3_norm varchar(20), _cn1_raw varchar(20), 
    _cn2_raw varchar(20), _cn3_raw varchar(20), _cd1_raw varchar(20), _cd2_raw varchar(20), 
    _cd3_raw varchar(20), _ht1_raw varchar(20), _ht2_raw varchar(20), _ht3_raw varchar(20), _cnvscd_cv varchar(20),
    _cnvsht_cv varchar(20), _cdvsht_cv varchar(20))
    returns void as
    $body$
    begin
        raise notice 'drop table';
        execute format('drop table if exists %I, %I, %I, %I', _venn_table_up_all, _venn_table_up_known, _venn_table_down_all,
                        _venn_table_down_known);
        raise notice 'create table %, %, %, % ', _venn_table_up_all, _venn_table_down_all, _venn_table_up_known,
                                                _venn_table_down_known ;
        execute format('create table %I as select id from %I where %I>=1 union
                                           select id from %I where %I>=1 union
                                           select id from %I where %I>=1',
                                           _venn_table_up_all, _hm_table_all, _cnvscd, _hm_table_all, _cnvsht,
                                           _hm_table_all, _cdvsht);
        execute format('create table %I as select id from %I where %I<=-1 union
                                           select id from %I where %I<=-1 union
                                           select id from %I where %I<=-1',
                                           _venn_table_down_all, _hm_table_all, _cnvscd, _hm_table_all, _cnvsht,
                                           _hm_table_all, _cdvsht);
        execute format('create table %I as select id from %I where %I>=1 union
                                           select id from %I where %I>=1 union
                                           select id from %I where %I>=1',
                                           _venn_table_up_known, _hm_table_known, _cnvscd, _hm_table_known, _cnvsht,
                                           _hm_table_known, _cdvsht);
        execute format('create table %I as select id from %I where %I<=-1 union
                                           select id from %I where %I<=-1 union
                                           select id from %I where %I<=-1',
                                           _venn_table_down_known, _hm_table_known, _cnvscd, _hm_table_known, _cnvsht,
                                           _hm_table_known, _cdvsht);
        --alter table to add columns
        raise notice 'alter table to add columns';
        execute format('alter table %I add %I double precision, add %I double precision, add %I double precision,
                        add anot varchar(300), add categ varchar(10), 
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision',
                         _venn_table_up_all, _cnvscd, _cnvsht, _cdvsht, _cn1_norm, _cn2_norm, _cn3_norm, _cd1_norm, 
                         _cd2_norm, _cd3_norm, _ht1_norm, _ht2_norm, _ht3_norm, _cn1_raw, _cn2_raw, _cn3_raw, _cd1_raw, 
                         _cd2_raw, _cd3_raw, _ht1_raw, _ht2_raw, _ht3_raw, _cnvscd_cv, _cnvsht_cv, _cdvsht_cv);
        execute format('alter table %I add %I double precision, add %I double precision, add %I double precision,
                        add anot varchar(300), add categ varchar(10),
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision',
                         _venn_table_down_all, _cnvscd, _cnvsht, _cdvsht, _cn1_norm, _cn2_norm, _cn3_norm, _cd1_norm,
                         _cd2_norm, _cd3_norm, _ht1_norm, _ht2_norm, _ht3_norm, _cn1_raw, _cn2_raw, _cn3_raw, _cd1_raw,
                         _cd2_raw, _cd3_raw, _ht1_raw, _ht2_raw, _ht3_raw, _cnvscd_cv, _cnvsht_cv, _cdvsht_cv);
        execute format('alter table %I add %I double precision, add %I double precision, add %I double precision,
                        add anot varchar(300), add categ varchar(10),
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision',
                         _venn_table_up_known, _cnvscd, _cnvsht, _cdvsht, _cn1_norm, _cn2_norm, _cn3_norm, _cd1_norm,
                         _cd2_norm, _cd3_norm, _ht1_norm, _ht2_norm, _ht3_norm, _cn1_raw, _cn2_raw, _cn3_raw, _cd1_raw,
                         _cd2_raw, _cd3_raw, _ht1_raw, _ht2_raw, _ht3_raw, _cnvscd_cv, _cnvsht_cv, _cdvsht_cv);
        execute format('alter table %I add %I double precision, add %I double precision, add %I double precision,
                        add anot varchar(300), add categ varchar(10),
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision,
                        add %I double precision, add %I double precision, add %I double precision',
                         _venn_table_down_known, _cnvscd, _cnvsht, _cdvsht, _cn1_norm, _cn2_norm, _cn3_norm, _cd1_norm,
                         _cd2_norm, _cd3_norm, _ht1_norm, _ht2_norm, _ht3_norm, _cn1_raw, _cn2_raw, _cn3_raw, _cd1_raw,
                         _cd2_raw, _cd3_raw, _ht1_raw, _ht2_raw, _ht3_raw, _cnvscd_cv, _cnvsht_cv, _cdvsht_cv);
        --update tables for data
        raise notice 'update table';
        execute format('update %I as a set %I=b.%I, %I=b.%I, %I=b.%I, anot=b.anot, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I
                        from %I as b where a.id=b.id', _venn_table_up_all, _cnvscd, _cnvscd, _cnvsht,
                        _cnvsht, _cdvsht, _cdvsht, _cn1_norm, _cn1_norm, _cn2_norm, _cn2_norm, _cn3_norm, _cn3_norm, 
                        _cd1_norm, _cd1_norm, _cd2_norm, _cd2_norm, _cd3_norm, _cd3_norm, _ht1_norm, _ht1_norm, 
                        _ht2_norm, _ht2_norm, _ht3_norm, _ht3_norm, _cn1_raw, _cn1_raw, _cn2_raw, _cn2_raw, _cn3_raw,
                        _cn3_raw, _cd1_raw, _cd1_raw, _cd2_raw, _cd2_raw, _cd3_raw, _cd3_raw, _ht1_raw, _ht1_raw, _ht2_raw,
                        _ht2_raw, _ht3_raw, _ht3_raw, _cnvscd_cv, _cnvscd_cv, _cnvsht_cv, _cnvsht_cv, _cdvsht_cv, _cdvsht_cv,
                        _hm_table_all );
        execute format('update %I as a set %I=b.%I, %I=b.%I, %I=b.%I, anot=b.anot, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I
                        from %I as b where a.id=b.id', _venn_table_up_known, _cnvscd, _cnvscd, _cnvsht,
                        _cnvsht, _cdvsht, _cdvsht, _cn1_norm, _cn1_norm, _cn2_norm, _cn2_norm, _cn3_norm, _cn3_norm,
                        _cd1_norm, _cd1_norm, _cd2_norm, _cd2_norm, _cd3_norm, _cd3_norm, _ht1_norm, _ht1_norm,
                        _ht2_norm, _ht2_norm, _ht3_norm, _ht3_norm, _cn1_raw, _cn1_raw, _cn2_raw, _cn2_raw, _cn3_raw,
                        _cn3_raw, _cd1_raw, _cd1_raw, _cd2_raw, _cd2_raw, _cd3_raw, _cd3_raw, _ht1_raw, _ht1_raw, _ht2_raw,
                        _ht2_raw, _ht3_raw, _ht3_raw, _cnvscd_cv, _cnvscd_cv, _cnvsht_cv, _cnvsht_cv, _cdvsht_cv,
                        _cdvsht_cv, _hm_table_known );
        execute format('update %I as a set %I=b.%I, %I=b.%I, %I=b.%I, anot=b.anot, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I
                        from %I as b where a.id=b.id', _venn_table_down_all, _cnvscd, _cnvscd, _cnvsht,
                        _cnvsht, _cdvsht, _cdvsht, _cn1_norm, _cn1_norm, _cn2_norm, _cn2_norm, _cn3_norm, _cn3_norm,
                        _cd1_norm, _cd1_norm, _cd2_norm, _cd2_norm, _cd3_norm, _cd3_norm, _ht1_norm, _ht1_norm,
                        _ht2_norm, _ht2_norm, _ht3_norm, _ht3_norm, _cn1_raw, _cn1_raw, _cn2_raw, _cn2_raw, _cn3_raw,
                        _cn3_raw, _cd1_raw, _cd1_raw, _cd2_raw, _cd2_raw, _cd3_raw, _cd3_raw, _ht1_raw, _ht1_raw, _ht2_raw,
                        _ht2_raw, _ht3_raw, _ht3_raw, _cnvscd_cv, _cnvscd_cv, _cnvsht_cv, _cnvsht_cv, _cdvsht_cv,
                        _cdvsht_cv, _hm_table_all );
        execute format('update %I as a set %I=b.%I, %I=b.%I, %I=b.%I, anot=b.anot, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I, %I=b.%I,
                        %I=b.%I, %I=b.%I, %I=b.%I
                        from %I as b where a.id=b.id', _venn_table_down_known, _cnvscd, _cnvscd, _cnvsht,
                        _cnvsht, _cdvsht, _cdvsht, _cn1_norm, _cn1_norm, _cn2_norm, _cn2_norm, _cn3_norm, _cn3_norm,
                        _cd1_norm, _cd1_norm, _cd2_norm, _cd2_norm, _cd3_norm, _cd3_norm, _ht1_norm, _ht1_norm,
                        _ht2_norm, _ht2_norm, _ht3_norm, _ht3_norm, _cn1_raw, _cn1_raw, _cn2_raw, _cn2_raw, _cn3_raw,
                        _cn3_raw, _cd1_raw, _cd1_raw, _cd2_raw, _cd2_raw, _cd3_raw, _cd3_raw, _ht1_raw, _ht1_raw, _ht2_raw,
                        _ht2_raw, _ht3_raw, _ht3_raw, _cnvscd_cv, _cnvscd_cv, _cnvsht_cv, _cnvsht_cv, _cdvsht_cv,
                        _cdvsht_cv, _hm_table_known );
        --venn categ for up all and known
        raise notice 'venn table';
        execute format('update %I set categ=''all'' where %I>=1 and %I>=1 and %I>=1 ', _venn_table_up_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''all'' where %I>=1 and %I>=1 and %I>=1 and categ is null', _venn_table_up_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd-cnht'' where %I>=1 and %I>=1 and %I<1 and categ is null', _venn_table_up_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd-cnht'' where %I>=1 and %I>=1 and %I<1 and categ is null', _venn_table_up_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd-cdht'' where %I>=1 and %I<1 and %I>=1 and categ is null', _venn_table_up_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd-cdht'' where %I>=1 and %I<1 and %I>=1 and categ is null', _venn_table_up_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cnht-cdht'' where %I<1 and %I>=1 and %I>=1 and categ is null', _venn_table_up_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cnht-cdht'' where %I<1 and %I>=1 and %I>=1 and categ is null', _venn_table_up_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd'' where %I>=1 and %I<1 and %I<1 and categ is null', _venn_table_up_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd'' where %I>=1 and %I<1 and %I<1 and categ is null', _venn_table_up_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cnht'' where %I<1 and %I>=1 and %I<1 and categ is null', _venn_table_up_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cnht'' where %I<1 and %I>=1 and %I<1 and categ is null', _venn_table_up_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cdht'' where %I<1 and %I<1 and %I>=1 and categ is null', _venn_table_up_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cdht'' where %I<1 and %I<1 and %I>=1 and categ is null', _venn_table_up_known, _cnvscd,
                        _cnvsht, _cdvsht);

        --venn categ for down all
        execute format('update %I set categ=''all'' where %I<=-1 and %I<=-1 and %I<=-1', _venn_table_down_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''all'' where %I<=-1 and %I<=-1 and %I<=-1 and categ is null', _venn_table_down_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd-cnht'' where %I<=-1 and %I<=-1 and %I>-1 and categ is null', _venn_table_down_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd-cnht'' where %I<=-1 and %I<=-1 and %I>-1 and categ is null', _venn_table_down_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd-cdht'' where %I<=-1 and %I>-1 and %I<=-1 and categ is null', _venn_table_down_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd-cdht'' where %I<=-1 and %I>-1 and %I<=-1 and categ is null', _venn_table_down_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cnht-cdht'' where %I>-1 and %I<=-1 and %I<=-1 and categ is null', _venn_table_down_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cnht-cdht'' where %I>-1 and %I<=-1 and %I<=-1 and categ is null', _venn_table_down_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd'' where %I<=-1 and %I>-1 and %I>-1 and categ is null', _venn_table_down_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cncd'' where %I<=-1 and %I>-1 and %I>-1 and categ is null', _venn_table_down_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cnht'' where %I>-1 and %I<=-1 and %I>-1 and categ is null', _venn_table_down_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cnht'' where %I>-1 and %I<=-1 and %I>-1 and categ is null', _venn_table_down_known, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cdht'' where %I>-1 and %I>-1 and %I<=-1 and categ is null', _venn_table_down_all, _cnvscd,
                        _cnvsht, _cdvsht);
        execute format('update %I set categ=''cdht'' where %I>-1 and %I>-1 and %I<=-1 and categ is null', _venn_table_down_known, _cnvscd,
                        _cnvsht, _cdvsht);
        --save tables
        execute format('copy %I to $$' || _out_venn_table_up_all || '$$' || ' with (FORMAT csv, header true)' ,
                        _venn_table_up_all);
        execute format('copy %I to $$' || _out_venn_table_up_known || '$$' || ' with (FORMAT csv, header true)' ,
                        _venn_table_up_known);
        execute format('copy %I to $$' || _out_venn_table_down_all || '$$' || ' with (FORMAT csv, header true)' ,
                        _venn_table_down_all);
        execute format('copy %I to $$' || _out_venn_table_down_known || '$$' || ' with (FORMAT csv, header true)' ,
                        _venn_table_down_known);
        /*
        execute format('alter table %I add categ varchar(10), add anot varchar(300)', tablename);
        execute format('update %I set categ=''all'' where %I!=0 and %I!=0 and %I!=0', tablename, cname1, cname2, cname3);
        execute format('update %I set categ=''cncd-cnht'' where %I!=0 and %I!=0 and %I=0', tablename, cname1, cname2, cname3);
        execute format('update %I set categ=''cncd-cdht'' where %I!=0 and %I=0 and %I!=0', tablename, cname1, cname2, cname3);
        execute format('update %I set categ=''cnht-cdht'' where %I=0 and %I!=0 and %I!=0', tablename, cname1, cname2, cname3);
        execute format('update %I set categ=''cncd'' where %I!=0 and %I=0 and %I=0', tablename, cname1, cname2, cname3);
        execute format('update %I set categ=''cnht'' where %I=0 and %I!=0 and %I=0', tablename, cname1, cname2, cname3);
        execute format('update %I set categ=''cdht'' where %I=0 and %I=0 and %I!=0', tablename, cname1, cname2, cna;me3)
        execute format('update %I as a set anot=b.anot from %I as b where a.gene=b.gene', tablename, hm_table);
        execute format('update %I as a set anot=b.anot from %I as b where a.gene=b.gene', tablename, hm_table);
        execute format('copy %I to $$' || foutname || '$$' || ' with (FORMAT csv, header true)' , tablename);
        */
    end
    $body$ language "plpgsql";

\set degdir $$'/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/'$$

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for potato gene level 7';
END
$$;
select venn('hm_pot_gene_all', 'hm_pot_gene_known', 'venn_pot_7_gene_up_all', 'venn_pot_7_gene_up_known',
                'venn_pot_7_gene_down_all', 'venn_pot_7_gene_down_known', 'cnvscd7', 'cnvsht7', 'cdvsht7',
                :degdir ||'venn_pot_7_gene_up_all.csv', :degdir ||'venn_pot_7_gene_up_known.csv',
                :degdir ||'venn_pot_7_gene_down_all.csv', :degdir ||'venn_pot_7_gene_down_known.csv', 'cn71_norm', 
                'cn72_norm', 'cn73_norm', 'cd71_norm', 'cd72_norm', 'cd73_norm', 'ht71_norm', 'ht72_norm', 'ht73_norm', 
                'cn71_raw', 'cn72_raw', 'cn73_raw', 'cd71_raw', 'cd72_raw', 'cd73_raw', 'ht71_raw', 'ht72_raw', 'ht73_raw',
                'cnvscd7_cv', 'cnvsht7_cv', 'cdvsht7_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for potato transcript 7';
END
$$;
select venn('hm_pot_trn_all', 'hm_pot_trn_known', 'venn_pot_7_trn_up_all', 'venn_pot_7_trn_up_known',
                'venn_pot_7_trn_down_all', 'venn_pot_7_trn_down_known', 'cnvscd7', 'cnvsht7', 'cdvsht7',
                :degdir ||'venn_pot_7_trn_up_all.csv', :degdir ||'venn_pot_7_trn_up_known.csv',
                :degdir ||'venn_pot_7_trn_down_all.csv', :degdir ||'venn_pot_7_trn_down_known.csv', 'cn71_norm', 
                'cn72_norm', 'cn73_norm', 'cd71_norm', 'cd72_norm', 'cd73_norm', 'ht71_norm', 'ht72_norm', 'ht73_norm', 
                'cn71_raw', 'cn72_raw', 'cn73_raw', 'cd71_raw', 'cd72_raw', 'cd73_raw', 'ht71_raw', 'ht72_raw', 
                'ht73_raw', 'cnvscd7_cv', 'cnvsht7_cv', 'cdvsht7_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for potato gene 14';
END
$$;
select venn('hm_pot_gene_all', 'hm_pot_gene_known', 'venn_pot_14_gene_up_all', 'venn_pot_14_gene_up_known',
                'venn_pot_14_gene_down_all', 'venn_pot_14_gene_down_known', 'cnvscd14', 'cnvsht14', 'cdvsht14',
                :degdir ||'venn_pot_14_gene_up_all.csv', :degdir ||'venn_pot_14_gene_up_known.csv',
                :degdir ||'venn_pot_14_gene_down_all.csv', :degdir ||'venn_pot_14_gene_down_known.csv', 'cn141_norm', 
                'cn142_norm', 'cn143_norm', 'cd141_norm', 'cd142_norm', 'cd143_norm', 'ht141_norm', 'ht142_norm', 'ht143_norm', 
                'cn141_raw', 'cn142_raw', 'cn143_raw', 'cd141_raw', 'cd142_raw', 'cd143_raw', 'ht141_raw', 'ht142_raw', 
                'ht143_raw', 'cnvscd14_cv', 'cnvsht14_cv', 'cdvsht14_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for potato transcript 14';
END
$$;
select venn('hm_pot_trn_all', 'hm_pot_trn_known', 'venn_pot_14_trn_up_all', 'venn_pot_14_trn_up_known',
                'venn_pot_14_trn_down_all', 'venn_pot_14_trn_down_known', 'cnvscd14', 'cnvsht14', 'cdvsht14',
                :degdir ||'venn_pot_14_trn_up_all.csv', :degdir ||'venn_pot_14_trn_up_known.csv',
                :degdir ||'venn_pot_14_trn_down_all.csv', :degdir ||'venn_pot_14_trn_down_known.csv', 'cn141_norm', 
                'cn142_norm', 'cn143_norm', 'cd141_norm', 'cd142_norm', 'cd143_norm', 'ht141_norm', 'ht142_norm', 'ht143_norm', 
                'cn141_raw', 'cn142_raw', 'cn143_raw', 'cd141_raw', 'cd142_raw', 'cd143_raw', 'ht141_raw', 'ht142_raw', 
                'ht143_raw', 'cnvscd14_cv', 'cnvsht14_cv', 'cdvsht14_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for potato gene 21';
END
$$;
select venn('hm_pot_gene_all', 'hm_pot_gene_known', 'venn_pot_21_gene_up_all', 'venn_pot_21_gene_up_known',
                'venn_pot_21_gene_down_all', 'venn_pot_21_gene_down_known', 'cnvscd21', 'cnvsht21', 'cdvsht21',
                :degdir ||'venn_pot_21_gene_up_all.csv', :degdir ||'venn_pot_21_gene_up_known.csv',
                :degdir ||'venn_pot_21_gene_down_all.csv', :degdir ||'venn_pot_21_gene_down_known.csv', 'cn211_norm', 
                'cn212_norm', 'cn213_norm', 'cd211_norm', 'cd212_norm', 'cd213_norm', 'ht211_norm', 'ht212_norm', 'ht213_norm', 
                'cn211_raw', 'cn212_raw', 'cn213_raw', 'cd211_raw', 'cd212_raw', 'cd213_raw', 'ht211_raw', 'ht212_raw', 
                'ht213_raw', 'cnvscd21_cv', 'cnvsht21_cv', 'cdvsht21_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for potato transcript 21';
END
$$;
select venn('hm_pot_trn_all', 'hm_pot_trn_known', 'venn_pot_21_trn_up_all', 'venn_pot_21_trn_up_known',
                'venn_pot_21_trn_down_all', 'venn_pot_21_trn_down_known', 'cnvscd21', 'cnvsht21', 'cdvsht21',
                :degdir ||'venn_pot_21_trn_up_all.csv', :degdir ||'venn_pot_21_trn_up_known.csv',
                :degdir ||'venn_pot_21_trn_down_all.csv', :degdir ||'venn_pot_21_trn_down_known.csv', 'cn211_norm', 
                'cn212_norm', 'cn213_norm', 'cd211_norm', 'cd212_norm', 'cd213_norm', 'ht211_norm', 'ht212_norm', 'ht213_norm', 
                'cn211_raw', 'cn212_raw', 'cn213_raw', 'cd211_raw', 'cd212_raw', 'cd213_raw', 'ht211_raw', 'ht212_raw', 
                'ht213_raw', 'cnvscd21_cv', 'cnvsht21_cv', 'cdvsht21_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for tomato gene 7';
END
$$;
select venn('hm_tom_gene_all', 'hm_tom_gene_known', 'venn_tom_7_gene_up_all', 'venn_tom_7_gene_up_known',
                'venn_tom_7_gene_down_all', 'venn_tom_7_gene_down_known', 'cnvscd7', 'cnvsht7', 'cdvsht7',
                :degdir ||'venn_tom_7_gene_up_all.csv', :degdir ||'venn_tom_7_gene_up_known.csv',
                :degdir ||'venn_tom_7_gene_down_all.csv', :degdir ||'venn_tom_7_gene_down_known.csv', 'cn71_norm', 
                'cn72_norm', 'cn73_norm', 'cd71_norm', 'cd72_norm', 'cd73_norm', 'ht71_norm', 'ht72_norm', 'ht73_norm', 
                'cn71_raw', 'cn72_raw', 'cn73_raw', 'cd71_raw', 'cd72_raw', 'cd73_raw', 'ht71_raw', 'ht72_raw', 
                'ht73_raw', 'cnvscd7_cv', 'cnvsht7_cv', 'cdvsht7_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for tomato transcript 7';
END
$$;
select venn('hm_tom_trn_all', 'hm_tom_trn_known', 'venn_tom_7_trn_up_all', 'venn_tom_7_trn_up_known',
                'venn_tom_7_trn_down_all', 'venn_tom_7_trn_down_known', 'cnvscd7', 'cnvsht7', 'cdvsht7',
                :degdir ||'venn_tom_7_trn_up_all.csv', :degdir ||'venn_tom_7_trn_up_known.csv',
                :degdir ||'venn_tom_7_trn_down_all.csv', :degdir ||'venn_tom_7_trn_down_known.csv', 'cn71_norm', 
                'cn72_norm', 'cn73_norm', 'cd71_norm', 'cd72_norm', 'cd73_norm', 'ht71_norm', 'ht72_norm', 'ht73_norm', 
                'cn71_raw', 'cn72_raw', 'cn73_raw', 'cd71_raw', 'cd72_raw', 'cd73_raw', 'ht71_raw', 'ht72_raw', 
                'ht73_raw', 'cnvscd7_cv', 'cnvsht7_cv', 'cdvsht7_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for tomato gene 14';
END
$$;
select venn('hm_tom_gene_all', 'hm_tom_gene_known', 'venn_tom_14_gene_up_all', 'venn_tom_14_gene_up_known',
                'venn_tom_14_gene_down_all', 'venn_tom_14_gene_down_known', 'cnvscd14', 'cnvsht14', 'cdvsht14',
                :degdir ||'venn_tom_14_gene_up_all.csv', :degdir ||'venn_tom_14_gene_up_known.csv',
                :degdir ||'venn_tom_14_gene_down_all.csv', :degdir ||'venn_tom_14_gene_down_known.csv', 'cn141_norm', 
                'cn142_norm', 'cn143_norm', 'cd141_norm', 'cd142_norm', 'cd143_norm', 'ht141_norm', 'ht142_norm', 'ht143_norm', 
                'cn141_raw', 'cn142_raw', 'cn143_raw', 'cd141_raw', 'cd142_raw', 'cd143_raw', 'ht141_raw', 'ht142_raw', 
                'ht143_raw', 'cnvscd14_cv', 'cnvsht14_cv', 'cdvsht14_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for tomato transcript 14';
END
$$;
select venn('hm_tom_trn_all', 'hm_tom_trn_known', 'venn_tom_14_trn_up_all', 'venn_tom_14_trn_up_known',
                'venn_tom_14_trn_down_all', 'venn_tom_14_trn_down_known', 'cnvscd14', 'cnvsht14', 'cdvsht14',
                :degdir ||'venn_tom_14_trn_up_all.csv', :degdir ||'venn_tom_14_trn_up_known.csv',
                :degdir ||'venn_tom_14_trn_down_all.csv', :degdir ||'venn_tom_14_trn_down_known.csv', 'cn141_norm', 
                'cn142_norm', 'cn143_norm', 'cd141_norm', 'cd142_norm', 'cd143_norm', 'ht141_norm', 'ht142_norm', 'ht143_norm', 
                'cn141_raw', 'cn142_raw', 'cn143_raw', 'cd141_raw', 'cd142_raw', 'cd143_raw', 'ht141_raw', 'ht142_raw', 
                'ht143_raw', 'cnvscd14_cv', 'cnvsht14_cv', 'cdvsht14_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for tomato gene 21';
END
$$;
select venn('hm_tom_gene_all', 'hm_tom_gene_known', 'venn_tom_21_gene_up_all', 'venn_tom_21_gene_up_known',
                'venn_tom_21_gene_down_all', 'venn_tom_21_gene_down_known', 'cnvscd21', 'cnvsht21', 'cdvsht21',
                :degdir ||'venn_tom_21_gene_up_all.csv', :degdir ||'venn_tom_21_gene_up_known.csv',
                :degdir ||'venn_tom_21_gene_down_all.csv', :degdir ||'venn_tom_21_gene_down_known.csv', 'cn211_norm', 
                'cn212_norm', 'cn213_norm', 'cd211_norm', 'cd212_norm', 'cd213_norm', 'ht211_norm', 'ht212_norm', 'ht213_norm', 
                'cn211_raw', 'cn212_raw', 'cn213_raw', 'cd211_raw', 'cd212_raw', 'cd213_raw', 'ht211_raw', 'ht212_raw', 
                'ht213_raw', 'cnvscd21_cv', 'cnvsht21_cv', 'cdvsht21_cv');

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running venn diagram for tomato transcript 21';
END
$$;
select venn('hm_tom_trn_all', 'hm_tom_trn_known', 'venn_tom_21_trn_up_all', 'venn_tom_21_trn_up_known',
                'venn_tom_21_trn_down_all', 'venn_tom_21_trn_down_known', 'cnvscd21', 'cnvsht21', 'cdvsht21',
                :degdir ||'venn_tom_21_trn_up_all.csv', :degdir ||'venn_tom_21_trn_up_known.csv',
                :degdir ||'venn_tom_21_trn_down_all.csv', :degdir ||'venn_tom_21_trn_down_known.csv', 'cn211_norm',
                'cn212_norm', 'cn213_norm', 'cd211_norm', 'cd212_norm', 'cd213_norm', 'ht211_norm', 'ht212_norm', 'ht213_norm',
                'cn211_raw', 'cn212_raw', 'cn213_raw', 'cd211_raw', 'cd212_raw', 'cd213_raw', 'ht211_raw', 'ht212_raw',
                'ht213_raw', 'cnvscd21_cv', 'cnvsht21_cv', 'cdvsht21_cv');

 ^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^ 
--potato/tomato compare degs

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running potato/tomato compare degs ';
END
$$;
drop table if exists deg_pot_tom_gene, deg_pot_tom_trn;

drop function if exists deg_pot_tom_compare_venn();
create or replace function deg_pot_tom_compare_venn(_deg_pot_tom varchar(20), _categ_up varchar(20), 
	_categ_down varchar(20), _cond_a varchar(20), _cond_b varchar(20), _cond_c varchar(20), _cond_d 
	varchar(20), _cond_e varchar(20),  _cond_f varchar(20))
    returns void as
    $body$
    begin
        --up-regulated
        --c("p_cnvscd7 A", "t_cnvscd7 B", "p_cnvsht7 C", "t_cnvsht7 D", "p_cdvsht7 E", "t_cdvsht7 F")
        execute format('update %I as a set %I=''all'' where %I>=1 and %I>=1 and %I>=1 and
                        %I>=1 and %I>=1 and %I>=1 ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d, _cond_e, _cond_f);                       
        execute format('update %I as a set %I=''ABCDE'' where %I>=1 and %I>=1 and %I>=1 and %I>=1 
						and %I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, 
						_cond_b, _cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);          
        execute format('update %I as a set %I=''ABCDF'' where %I>=1 and %I>=1 and %I>=1 and %I>=1 
						and %I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, 
						_cond_b, _cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);               
        execute format('update %I as a set %I=''ABCEF'' where %I>=1 and %I>=1 and %I>=1 and %I<1 
						and %I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, 
						_cond_b, _cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);              
        execute format('update %I as a set %I=''ABDEF'' where %I>=1 and %I>=1 and %I<1 and %I>=1 
						and %I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, 
						_cond_b, _cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);            
        execute format('update %I as a set %I=''ACDEF'' where %I>=1 and %I<1 and %I>=1 and %I>=1 
						and %I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, 
						_cond_b, _cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                 
        execute format('update %I as a set %I=''BCDEF'' where %I<1 and %I>=1 and %I>=1 and %I>=1 
						and %I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, 
						_cond_b, _cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                
        execute format('update %I as a set %I=''ABCD'' where %I>=1 and %I>=1 and %I>=1 and %I>=1 
						and %I<1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, 
						_cond_b, _cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                      
        execute format('update %I as a set %I=''ABCE'' where %I>=1 and %I>=1 and %I>=1 and %I<1 and 
						%I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e,  _cond_f, _categ_up);                      
        execute format('update %I as a set %I=''ABCF'' where %I>=1 and %I>=1 and %I>=1 and %I<1 and 
						%I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''ABDE'' where %I>=1 and %I>=1 and %I<1 and %I>=1 and 
						%I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                     
        execute format('update %I as a set %I=''ABDF'' where %I>=1 and %I>=1 and %I<1 and %I>=1 and 
						%I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e,  _cond_f, _categ_up);                      
        execute format('update %I as a set %I=''ABEF'' where %I>=1 and %I>=1 and %I<1 and %I<1 and 
						%I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);
        execute format('update %I as a set %I=''ACDE'' where %I>=1 and %I<1 and %I>=1 and %I>=1 and 
						%I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                 
        execute format('update %I as a set %I=''ACDF'' where %I>=1 and %I<1 and %I>=1 and %I>=1 and 
						%I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                      
        execute format('update %I as a set %I=''ACEF'' where %I>=1 and %I<1 and %I>=1 and %I<1 and 
						%I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);         
        execute format('update %I as a set %I=''ADEF'' where %I>=1 and %I<1 and %I<1 and %I>=1 and 
						%I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);              
        execute format('update %I as a set %I=''BCDE'' where %I<1 and %I>=1 and %I>=1 and %I>=1 and 
						%I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''BCDF'' where %I<1 and %I>=1 and %I>=1 and %I>=1 and 
						%I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''BCEF'' where %I<1 and %I>=1 and %I>=1 and %I<1 and 
						%I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                
        execute format('update %I as a set %I=''BDEF'' where %I<1 and %I>=1 and %I<1 and %I>=1 and 
						%I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);               
        execute format('update %I as a set %I=''CDEF'' where %I<1 and %I<1 and %I>=1 and %I>=1 and 
						%I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b,
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''ABC'' where %I>=1 and %I>=1 and %I>=1 and %I<1 and 
						%I<1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''ABD'' where %I>=1 and %I>=1 and %I<1 and %I>=1 and 
						%I<1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''ABE'' where %I>=1 and %I>=1 and %I<1 and %I<1 and 
						%I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''ABF'' where %I>=1 and %I>=1 and %I<1 and %I<1 and 
						%I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''ACD'' where %I>=1 and %I<1 and %I>=1 and %I>=1 and 
						%I<1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''ACE'' where %I>=1 and %I<1 and %I>=1 and %I<1 and 
						%I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''ACF'' where %I>=1 and %I<1 and %I>=1 and %I<1 and 
						%I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''ADE'' where %I>=1 and %I<1 and %I<1 and %I>=1 and 
						%I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                      
        execute format('update %I as a set %I=''ADF'' where %I>=1 and %I<1 and %I<1 and %I>=1 and 
						%I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                      
        execute format('update %I as a set %I=''AEF'' where %I>=1 and %I<1 and %I<1 and %I<1 and 
						%I>=1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                
        execute format('update %I as a set %I=''BCD'' where %I<1 and %I>=1 and %I>=1 and %I>=1 and 
						%I<1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''BCE'' where %I<1 and %I>=1 and %I>=1 and %I<1 and 
						%I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);
       execute format('update %I as a set %I=''BCF'' where %I<1 and %I>=1 and %I>=1 and %I<1 and 
					   %I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
					   _cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);
        execute format('update %I as a set %I=''BDE'' where %I<1 and %I>=1 and %I<1 and %I>=1 and 
						%I>=1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                       
        execute format('update %I as a set %I=''BDF'' where %I<1 and %I>=1 and %I<1 and %I>=1 and 
						%I<1 and %I>=1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                                             
        execute format('update %I as a set %I=''BEF'' where %I<1 and %I>=1 and %I<1 and %I<1 and %I>=1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''CDE'' where %I<1 and %I<1 and %I>=1 and %I>=1 and %I>=1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''CDF'' where %I<1 and %I<1 and %I>=1 and %I>=1 and %I<1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''CEF'' where %I<1 and %I<1 and %I>=1 and %I<1 and %I>=1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''DEF'' where %I<1 and %I<1 and %I<1 and %I>=1 and %I>=1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''AB'' where %I>=1 and %I>=1 and %I<1 and %I<1 and 
						%I<1 and %I<1 and %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, 
						_cond_c, _cond_d,  _cond_e, _cond_f, _categ_up);                                              
        execute format('update %I as a set %I=''AC'' where %I>=1 and %I<1 and %I>=1 and %I<1 and %I<1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''AD'' where %I>=1 and %I<1 and %I<1 and %I>=1 and %I<1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''AE'' where %I>=1 and %I<1 and %I<1 and %I<1 and %I>=1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''AF'' where %I>=1 and %I<1 and %I<1 and %I<1 and %I<1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''BC'' where %I<1 and %I>=1 and %I>=1 and %I<1 and %I<1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''BD'' where %I<1 and %I>=1 and %I<1 and %I>=1 and %I<1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''BE'' where %I<1 and %I>=1 and %I<1 and %I<1 and %I>=1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''BF'' where %I<1 and %I>=1 and %I<1 and %I<1 and %I<1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''CD'' where %I<1 and %I<1 and %I>=1 and %I>=1 and %I<1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''CE'' where %I<1 and %I<1 and %I>=1 and %I<1 and %I>=1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''CF'' where %I<1 and %I<1 and %I>=1 and %I<1 and %I<1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''DE'' where %I<1 and %I<1 and %I<1 and %I>=1 and %I>=1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''DF'' where %I<1 and %I<1 and %I<1 and %I>=1 and %I<1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''EF'' where %I<1 and %I<1 and %I<1 and %I<1 and %I>=1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''A'' where %I>=1 and %I<1 and %I<1 and %I<1 and %I<1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''B'' where %I<1 and %I>=1 and %I<1 and %I<1 and %I<1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''C'' where %I<1 and %I<1 and %I>=1 and %I<1 and %I<1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''D'' where %I<1 and %I<1 and %I<1 and %I>=1 and %I<1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''E'' where %I<1 and %I<1 and %I<1 and %I<1 and %I>=1 and %I<1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);
        execute format('update %I as a set %I=''F'' where %I<1 and %I<1 and %I<1 and %I<1 and %I<1 and %I>=1 and
                       %I is null ', _deg_pot_tom, _categ_up, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_up);                                          
        --down-regulated
        execute format('update %I as a set %I=''all'' where %I<=-1 and %I<=-1 and %I<=-1 and
                        %I<=-1 and %I<=-1 and %I<=-1 ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,
                        _cond_e, _cond_f);
        execute format('update %I as a set %I=''ABCDE'' where %I<=-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I>-1
                        and %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABCDF'' where %I<=-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and
                        %I<=-1 and %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABCEF'' where %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and
                        %I<=-1 and %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABDEF'' where %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and
                        %I<=-1 and %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ACDEF'' where %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and
                        %I<=-1 and %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BCDEF'' where %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I<=-1
                        and %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABCD'' where %I<=-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and
                        %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABCE'' where %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and
                        %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABCF'' where %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and
                        %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABDE'' where %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and
                        %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABDF'' where %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and %I>-1
                        and %I<=-1 and %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABEF'' where %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and
                        %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ACDE'' where %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and
                        %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ACDF'' where %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ACEF'' where %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ADEF'' where %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BCDE'' where %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BCDF'' where %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BCEF'' where %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BDEF'' where %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''CDEF'' where %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABC'' where %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABD'' where %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABE'' where %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ABF'' where %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ACD'' where %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ACE'' where %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ACF'' where %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ADE'' where %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''ADF'' where %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''AEF'' where %I<=-1 and %I>-1 and %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BCD'' where %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BCE'' where %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BCF'' where %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BDE'' where %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BDF'' where %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BEF'' where %I>-1 and %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''CDE'' where %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''CDF'' where %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''CEF'' where %I>-1 and %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''DEF'' where %I>-1 and %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''AB'' where %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''AC'' where %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''AD'' where %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''AE'' where %I<=-1 and %I>-1 and %I>-1 and %I>-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''AF'' where %I<=-1 and %I>-1 and %I>-1 and %I>-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BC'' where %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BD'' where %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BE'' where %I>-1 and %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''BF'' where %I>-1 and %I<=-1 and %I>-1 and %I>-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''CD'' where %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''CE'' where %I>-1 and %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''CF'' where %I>-1 and %I>-1 and %I<=-1 and %I>-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''DE'' where %I>-1 and %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''DF'' where %I>-1 and %I>-1 and %I>-1 and %I<=-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''EF'' where %I>-1 and %I>-1 and %I>-1 and %I>-1 and %I<=-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''A'' where %I<=-1 and %I>-1 and %I>-1 and %I>-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''B'' where %I>-1 and %I<=-1 and %I>-1 and %I>-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''C'' where %I>-1 and %I>-1 and %I<=-1 and %I>-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''D'' where %I>-1 and %I>-1 and %I>-1 and %I<=-1 and %I>-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''E'' where %I>-1 and %I>-1 and %I>-1 and %I>-1 and %I<=-1 and %I>-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
        execute format('update %I as a set %I=''F'' where %I>-1 and %I>-1 and %I>-1 and %I>-1 and %I>-1 and %I<=-1 and
                       %I is null ', _deg_pot_tom, _categ_down, _cond_a, _cond_b, _cond_c, _cond_d,  _cond_e,
                        _cond_f, _categ_down);
    end
    $body$ language "plpgsql";

DROP FUNCTION if exists deg_pot_tom_compare();
create or replace function deg_pot_tom_compare(_deg_pot_tom varchar(20), _pot_col varchar(20), 
	_tom_col varchar(20), _pot_deg_table varchar(20), _tom_deg_table varchar(20), _value int, 
	_pot_col_cv varchar(20), _tom_col_cv varchar(20),  _hm_cv_col varchar(20), _pot_norm_ct 
	varchar(20), _tom_norm_ct varchar(20) )  
    returns void as
    $body$
    begin
        if _value = 1 then
            if _deg_pot_tom ~ 'gene' then
                raise notice 'create table name: %', _deg_pot_tom;
                execute format('create table %I as select pot_gene, tom_gene, orthoid from 
								stub_slync_ortho', _deg_pot_tom );
            elsif _deg_pot_tom ~ 'trn' then
                raise notice 'create table name: %', _deg_pot_tom;
                execute format('create table %I as select qid, sid, orthoid from stub_slync_ortho', 
								_deg_pot_tom );
            end if;
            raise notice 'alter table to add columns: %', _deg_pot_tom;
            execute format('alter table %I add p_cnvscd7 double precision, add t_cnvscd7 double 
							precision, add p_cnvsht7 double precision, add t_cnvsht7 double 
							precision, add p_cdvsht7 double precision, add t_cdvsht7 double 
							precision, add p_cnvscd14 double precision, add t_cnvscd14 double 
							precision, add p_cnvsht14 double precision, add t_cnvsht14 double 
							precision, add p_cdvsht14 double precision, add t_cdvsht14 double 
							precision, add p_cnvscd21 double precision, add t_cnvscd21 double 
							precision, add p_cnvsht21 double precision, add t_cnvsht21 double 
							precision, add p_cdvsht21 double precision, add t_cdvsht21 double 
							precision, add anot varchar(300), add categ_7_up varchar(20), add 
							categ_7_down varchar(20), add categ_14_up varchar(20), add categ_14_down 
							varchar(20), add categ_21_up varchar(20), add categ_21_down varchar(20), 
							add pcncdnorm7 varchar(100), add tcncdnorm7 varchar(100),  add 
							pcnhtnorm7 varchar(100), add tcnhtnorm7 varchar(100), add pcdhtnorm7 
							varchar(100), add tcdhtnorm7 varchar(100),														  
							add pcncdnorm14 varchar(100), add tcncdnorm14 varchar(100),  add 
							pcnhtnorm14 varchar(100), add tcnhtnorm14 varchar(100), add pcdhtnorm14 
							varchar(100), add tcdhtnorm14 varchar(100), 
							add pcncdnorm21 varchar(100), add tcncdnorm21 varchar(100),  add 
							pcnhtnorm21 varchar(100), add tcnhtnorm21 varchar(100), add pcdhtnorm21 
							varchar(100), add tcdhtnorm21 varchar(100), 
							add pot_trn_ct int, add tom_trn_ct int,							
							add pcraw varchar(100), add ptraw varchar(100),							 
							add tcraw varchar(100), add ttraw varchar(100), add p_cnvscd7_cv double 
							precision, add t_cnvscd7_cv double precision, add p_cnvsht7_cv double 
							precision, add t_cnvsht7_cv double precision, add p_cdvsht7_cv double 
							precision, add t_cdvsht7_cv double precision, add p_cnvscd14_cv double 
							precision, add t_cnvscd14_cv double precision, add p_cnvsht14_cv double 
							precision, add t_cnvsht14_cv double precision, add p_cdvsht14_cv double 
							precision, add t_cdvsht14_cv double precision, add p_cnvscd21_cv double 
							precision, add t_cnvscd21_cv double precision, add p_cnvsht21_cv double 
							precision, add t_cnvsht21_cv double precision, add p_cdvsht21_cv double 
							precision, add t_cdvsht21_cv double precision   ; ',  _deg_pot_tom);                              
            execute format('COMMENT ON TABLE %I IS ''all are log2fc values with padj < 0.05'' ', 
							_deg_pot_tom);
        end if;
        if _deg_pot_tom ~ 'gene' then
            raise notice 'update table to add column values: %', _deg_pot_tom;
            execute format('update %I as a set %I=round(b.log2fc::numeric, 3) from %I as b where 
							a.pot_gene=b.id and padj<0.05 and (log2fc>=1 or log2fc<=-1) ', 
							_deg_pot_tom, _pot_col,  _pot_deg_table);							                           
            execute format('update %I as a set %I=round(b.log2fc::numeric, 3) from %I as b where 
							a.tom_gene=b.id and padj<0.05 and (log2fc>=1 or log2fc<=-1) ', 
							_deg_pot_tom, _tom_col, _tom_deg_table);    													                       
            execute format('update %I as a set %I=round(b.log2fc::numeric, 3) from %I as b where 
							a.pot_gene=b.id and
                           (log2fc<1 or log2fc>-1) and a.%I is null ', _deg_pot_tom, _pot_col, 
						   _pot_deg_table, _pot_col);
            execute format('update %I as a set %I=round(b.log2fc::numeric, 3) from %I as b where 
							a.tom_gene=b.id and
                           (log2fc<1 or log2fc>-1) and a.%I is null ', _deg_pot_tom, _tom_col, 
						   _tom_deg_table, _tom_col);
            --adjust values to 0.99 where log2fc >=1 and padj>0.05
            execute format('update %I as a set %I=0.99 from %I as b where a.pot_gene=b.id and 
						   (padj>=0.05 or padj is null) and log2fc>=1  ', _deg_pot_tom, _pot_col, 
						   _pot_deg_table, _pot_col);                           
            execute format('update %I as a set %I=0.99 from %I as b where a.tom_gene=b.id and 
						   (padj>=0.05 or padj is null) and log2fc>=1  ', _deg_pot_tom, _tom_col, 
						   _tom_deg_table, _tom_col);                           
            execute format('update %I as a set %I=-0.99 from %I as b where a.pot_gene=b.id and 
						   (padj>=0.05 or padj is null) and log2fc<=-1  ', _deg_pot_tom, _pot_col, 
						   _pot_deg_table, _pot_col);                           
            execute format('update %I as a set %I=-0.99 from %I as b where a.tom_gene=b.id and 
							(padj>=0.05 or padj is null) and log2fc<=-1  ', _deg_pot_tom, _tom_col, 
							_tom_deg_table, _tom_col);                           
            --Add CV values
            execute format('update %I as a set %I=b.cv from %I as b where a.pot_gene=b.id ',
                            _deg_pot_tom, _pot_col_cv, _pot_deg_table);
            execute format('update %I as a set %I=b.cv from %I as b where a.tom_gene=b.id ',
                            _deg_pot_tom, _tom_col_cv, _tom_deg_table);
            --Add count values
            execute format('update %I as a set %I=(round(b.c1_norm::numeric, 2) || '';'' || 
							round(b.c2_norm::numeric, 2) || '';'' || 
							round(b.c3_norm::numeric, 2) || ''--'' || round(b.t1_norm::numeric, 2) ||
							'';'' || round(b.t2_norm::numeric, 2) || '';'' || 
							round(b.t3_norm::numeric, 2) ) 
							from %I as b where a.pot_gene=b.id ',  _deg_pot_tom, _pot_norm_ct,
							_pot_deg_table);
			execute format('update %I as a set %I=(round(b.c1_norm::numeric, 2) || '';'' || 
							round(b.c2_norm::numeric, 2) || '';'' || 
							round(b.c3_norm::numeric, 2) || ''--'' || round(b.t1_norm::numeric, 2) ||
							'';'' || round(b.t2_norm::numeric, 2) || '';'' || 
							round(b.t3_norm::numeric, 2) )
							from %I as b where a.tom_gene=b.id ', _deg_pot_tom, _tom_norm_ct, 
							_tom_deg_table);							 							
            execute format('update %I as a set pcraw=(b.c1_raw || '';'' || b.c2_raw || '';'' || 
							b.c3_raw) from %I as b where a.pot_gene=b.id ',  _deg_pot_tom, 
							_pot_deg_table);                            
            execute format('update %I as a set ptraw=(b.t1_raw || '';'' || b.t2_raw || '';'' || 
							b.t3_raw) from %I as b where a.pot_gene=b.id ',  _deg_pot_tom, 
							_pot_deg_table);                                             							                                  							                      
            execute format('update %I as a set tcraw=(b.c1_raw || '';'' || b.c2_raw || '';'' || 
							b.c3_raw) from %I as b where a.tom_gene=b.id ',  _deg_pot_tom, 
							_tom_deg_table);                            
            execute format('update %I as a set ttraw=(b.t1_raw || '';'' || b.t2_raw || '';'' || 
							b.t3_raw) from %I as b where a.tom_gene=b.id ',  _deg_pot_tom, 
							_tom_deg_table);   
			--Add alt trn count values		
			execute format('update %I as a set	pot_trn_ct=b.trn_ct from potato_pgsc3037_gtf as b
							where a.pot_gene=b.gene', _deg_pot_tom);
			execute format('update %I as a set	tom_trn_ct=b.trn_ct from tomato_25037_gtf  as b
							where a.tom_gene=b.gene', _deg_pot_tom);							
        elsif _deg_pot_tom ~ 'trn' then
            raise notice 'update table to add column log2fc values: %', _deg_pot_tom;
            execute format('update %I as a set %I=round(b.log2fc::numeric, 3) from %I as b where 
							a.qid=b.id and 
							padj<0.05 and (log2fc>=1 or log2fc<=-1) ', _deg_pot_tom, _pot_col, 
							_pot_deg_table);                          
            execute format('update %I as a set %I=round(b.log2fc::numeric, 3) from %I as b where 
							a.sid=b.id and 
							padj<0.05 and (log2fc>=1 or log2fc<=-1) ', _deg_pot_tom, _tom_col, 
							_tom_deg_table);                           
            execute format('update %I as a set %I=round(b.log2fc::numeric, 3) from %I as b where 
							a.qid=b.id and
                           (log2fc<1 or log2fc>-1) and a.%I is null ', _deg_pot_tom, _pot_col, 
						   _pot_deg_table, _pot_col);
            execute format('update %I as a set %I=round(b.log2fc::numeric, 3) from %I as b where 
							a.sid=b.id and
                           (log2fc<1 or log2fc>-1) and a.%I is null ', _deg_pot_tom, _tom_col, 
						   _tom_deg_table, _tom_col);
            --adjust values to 0.99 where log2fc >=1 and padj>0.05
            execute format('update %I as a set %I=0.99 from %I as b where a.qid=b.id and 
							(padj>=0.05 or padj is null) and log2fc>=1  ', _deg_pot_tom, _pot_col, 
							_pot_deg_table, _pot_col);                           
            execute format('update %I as a set %I=0.99 from %I as b where a.sid=b.id and 
							(padj>=0.05 or padj is null) and log2fc>=1  ', _deg_pot_tom, _tom_col, 
							_tom_deg_table, _tom_col);                          
            execute format('update %I as a set %I=-0.99 from %I as b where a.qid=b.id and 
							(padj>=0.05 or padj is null) and log2fc<=-1  ', _deg_pot_tom, _pot_col,
							_pot_deg_table, _pot_col);                           
            execute format('update %I as a set %I=-0.99 from %I as b where a.sid=b.id and 
							(padj>=0.05 or padj is null) and log2fc<=-1 ', _deg_pot_tom, _tom_col, 
							_tom_deg_table, _tom_col);                           
            --Add CV values
            execute format('update %I as a set %I=b.cv from %I as b where a.qid=b.id ',
                            _deg_pot_tom, _pot_col_cv, _pot_deg_table);
            execute format('update %I as a set %I=b.cv from %I as b where a.sid=b.id ',
                            _deg_pot_tom, _tom_col_cv, _tom_deg_table);
            --Add count values
            execute format('update %I as a set %I=(round(b.c1_norm::numeric, 2) || '';'' || 
							round(b.c2_norm::numeric, 2) || '';'' || 
							round(b.c3_norm::numeric, 2) || ''--'' || round(b.t1_norm::numeric, 2) ||
							'';'' || round(b.t2_norm::numeric, 2) || '';'' || 
							round(b.t3_norm::numeric, 2) ) 
							from %I as b where a.qid=b.id ',  _deg_pot_tom, _pot_norm_ct,
							_pot_deg_table);
			execute format('update %I as a set %I=(round(b.c1_norm::numeric, 2) || '';'' || 
							round(b.c2_norm::numeric, 2) || '';'' || 
							round(b.c3_norm::numeric, 2) || ''--'' || round(b.t1_norm::numeric, 2) ||
							'';'' || round(b.t2_norm::numeric, 2) || '';'' || 
							round(b.t3_norm::numeric, 2) )
							from %I as b where a.sid=b.id ', _deg_pot_tom, _tom_norm_ct, 
							_tom_deg_table);			                                                                  
            execute format('update %I as a set pcraw=(b.c1_raw || '';'' || b.c2_raw || '';'' || 
							b.c3_raw) from %I as b where a.qid=b.id ',  _deg_pot_tom, 
							_pot_deg_table);                            
            execute format('update %I as a set ptraw=(b.t1_raw || '';'' || b.t2_raw || '';'' || 
							b.t3_raw) from %I as b where a.qid=b.id ',  _deg_pot_tom, 
							_pot_deg_table);                                                          
            execute format('update %I as a set tcraw=(b.c1_raw || '';'' || b.c2_raw || '';'' || 
							b.c3_raw) from %I as b where a.sid=b.id ',  _deg_pot_tom, 
							_tom_deg_table);                            
            execute format('update %I as a set ttraw=(b.t1_raw || '';'' || b.t2_raw || '';'' || 
							b.t3_raw) from %I as b where a.sid=b.id ',  _deg_pot_tom, 
							_tom_deg_table);
			--Add alt trn count values		
			execute format('update %I as a set	pot_trn_ct=b.trn_ct from potato_pgsc3037_gtf as b
							where a.qid=b.trn', _deg_pot_tom);
			execute format('update %I as a set	tom_trn_ct=b.trn_ct from tomato_25037_gtf  as b
							where a.sid=b.trn', _deg_pot_tom);				
                            
        end if;
        raise notice 'set values 0 where log2fc is null: %', _deg_pot_tom;
        execute format('update %I as a set %I=0 where %I is null', _deg_pot_tom, _pot_col, _pot_col);
        execute format('update %I as a set %I=0 where %I is null', _deg_pot_tom, _tom_col, _tom_col);
        --venn analysis
        raise notice 'running venn analysis %', _deg_pot_tom;
        execute format('select deg_pot_tom_compare_venn (''%I'', ''categ_7_up'', ''categ_7_down'', 
					  ''p_cnvscd7'', ''t_cnvscd7'', ''p_cnvsht7'', ''t_cnvsht7'', ''p_cdvsht7'', 
					  ''t_cdvsht7'')',  _deg_pot_tom );                       
        execute format('select deg_pot_tom_compare_venn (''%I'', ''categ_14_up'', ''categ_14_down'',
					  ''p_cnvscd14'', ''t_cnvscd14'', ''p_cnvsht14'', ''t_cnvsht14'', ''p_cdvsht14'',
					  ''t_cdvsht14'')', _deg_pot_tom );                                            
        execute format('select deg_pot_tom_compare_venn (''%I'', ''categ_21_up'', ''categ_21_down'', 
					  ''p_cnvscd21'', ''t_cnvscd21'', ''p_cnvsht21'', ''t_cnvsht21'', ''p_cdvsht21'',
					  ''t_cdvsht21'')', _deg_pot_tom );
        if _deg_pot_tom ~ 'gene' then
            raise notice 'Add annotation to table: %', _deg_pot_tom;
            execute format('update %I as a set anot=b.anot from potato_pgsc403_gff as b where 
						   a.pot_gene=b.gene', _deg_pot_tom);                       
            execute format('update %I as a set anot=b.anot from tomato_25037_gtf as b where 
							a.tom_gene=b.gene and a.anot is null', _deg_pot_tom);                        
        elsif _deg_pot_tom ~ 'trn' then
            raise notice 'Add annotation to table: %', _deg_pot_tom;
            execute format('update %I as a set anot=b.anot from potato_pgsc403_gff as b where 
							a.qid=b.trn', _deg_pot_tom);                        
            execute format('update %I as a set anot=b.anot from tomato_25037_gtf as b where 
							a.sid=b.trn and a.anot is null', _deg_pot_tom);                        
        end if;
       end
    $body$ language "plpgsql";

--last arguement value represents to create table only one time i.e value = 1
select deg_pot_tom_compare('deg_pot_tom_gene', 'p_cnvscd7', 't_cnvscd7', 'pot_con_cold_7_gene', 
							'tom_con_cold_7_gene', 1, 'p_cnvscd7_cv', 't_cnvscd7_cv', 'cnvscd7_cv',
							'pcncdnorm7', 'tcncdnorm7');  							                        
select deg_pot_tom_compare('deg_pot_tom_trn', 'p_cnvscd7', 't_cnvscd7', 'pot_con_cold_7_trn', 
						   'tom_con_cold_7_trn', 1, 'p_cnvscd7_cv', 't_cnvscd7_cv', 'cnvscd7_cv',
						   'pcncdnorm7', 'tcncdnorm7');                            
select deg_pot_tom_compare('deg_pot_tom_gene', 'p_cnvsht7', 't_cnvsht7', 'pot_con_hot_7_gene', 
						   'tom_con_hot_7_gene', 0, 'p_cnvsht7_cv', 't_cnvsht7_cv', 'cnvsht7_cv',
						   'pcnhtnorm7', 'tcnhtnorm7');                            
select deg_pot_tom_compare('deg_pot_tom_trn', 'p_cnvsht7', 't_cnvsht7', 'pot_con_hot_7_trn', 
						   'tom_con_hot_7_trn', 0, 'p_cnvsht7_cv', 't_cnvsht7_cv', 'cnvsht7_cv',
						   'pcnhtnorm7', 'tcnhtnorm7');                            
select deg_pot_tom_compare('deg_pot_tom_gene', 'p_cdvsht7', 't_cdvsht7', 'pot_cold_hot_7_gene', 
						   'tom_cold_hot_7_gene', 0, 'p_cdvsht7_cv', 't_cdvsht7_cv', 'cdvsht7_cv',
						   'pcdhtnorm7', 'tcdhtnorm7');                            
select deg_pot_tom_compare('deg_pot_tom_trn', 'p_cdvsht7', 't_cdvsht7', 'pot_cold_hot_7_trn', 
						   'tom_cold_hot_7_trn', 0, 'p_cdvsht7_cv', 't_cdvsht7_cv', 'cdvsht7_cv',
						   'pcdhtnorm7', 'tcdhtnorm7');                            

select deg_pot_tom_compare('deg_pot_tom_gene', 'p_cnvscd14', 't_cnvscd14', 'pot_con_cold_14_gene', 
						   'tom_con_cold_14_gene', 0, 'p_cnvscd14_cv', 't_cnvscd14_cv', 
						   'cnvscd14_cv', 'pcncdnorm14', 'tcncdnorm14');                            
select deg_pot_tom_compare('deg_pot_tom_trn', 'p_cnvscd14', 't_cnvscd14', 'pot_con_cold_14_trn', 
						   'tom_con_cold_14_trn', 0, 'p_cnvscd14_cv', 't_cnvscd14_cv', 
						   'cnvscd14_cv', 'pcncdnorm14', 'tcncdnorm14');                            
select deg_pot_tom_compare('deg_pot_tom_gene', 'p_cnvsht14', 't_cnvsht14', 'pot_con_hot_14_gene', 
						   'tom_con_hot_14_gene', 0, 'p_cnvsht14_cv', 't_cnvsht14_cv', 
						   'cnvsht14_cv', 'pcnhtnorm14', 'tcnhtnorm14');                            
select deg_pot_tom_compare('deg_pot_tom_trn', 'p_cnvsht14', 't_cnvsht14', 'pot_con_hot_14_trn', 
						   'tom_con_hot_14_trn', 0, 'p_cnvsht14_cv', 't_cnvsht14_cv', 
						   'cnvsht14_cv', 'pcnhtnorm14', 'tcnhtnorm14');                            
select deg_pot_tom_compare('deg_pot_tom_gene', 'p_cdvsht14', 't_cdvsht14', 'pot_cold_hot_14_gene', 
						   'tom_cold_hot_14_gene', 0, 'p_cdvsht14_cv', 't_cdvsht14_cv', 
						   'cdvsht14_cv', 'pcdhtnorm14', 'tcdhtnorm14');                            
select deg_pot_tom_compare('deg_pot_tom_trn', 'p_cdvsht14', 't_cdvsht14', 'pot_cold_hot_14_trn', 
						   'tom_cold_hot_14_trn', 0, 'p_cdvsht14_cv', 't_cdvsht14_cv', 
						   'cdvsht14_cv', 'pcdhtnorm14', 'tcdhtnorm14');                            

select deg_pot_tom_compare('deg_pot_tom_gene', 'p_cnvscd21', 't_cnvscd21', 'pot_con_cold_21_gene', 
						   'tom_con_cold_21_gene', 0, 'p_cnvscd21_cv', 't_cnvscd21_cv', 
						   'cnvscd21_cv', 'pcncdnorm21', 'tcncdnorm21');                            
select deg_pot_tom_compare('deg_pot_tom_trn', 'p_cnvscd21', 't_cnvscd21', 'pot_con_cold_21_trn', 
						   'tom_con_cold_21_trn', 0, 'p_cnvscd21_cv', 't_cnvscd21_cv', 
						   'cnvscd21_cv', 'pcncdnorm21', 'tcncdnorm21');                            
select deg_pot_tom_compare('deg_pot_tom_gene', 'p_cnvsht21', 't_cnvsht21', 'pot_con_hot_21_gene', 
						   'tom_con_hot_21_gene', 0, 'p_cnvsht21_cv', 't_cnvsht21_cv', 
						   'cnvsht21_cv', 'pcnhtnorm21', 'tcnhtnorm21');                            
select deg_pot_tom_compare('deg_pot_tom_trn', 'p_cnvsht21', 't_cnvsht21', 'pot_con_hot_21_trn', 
						   'tom_con_hot_21_trn', 0, 'p_cnvsht21_cv', 't_cnvsht21_cv', 
						   'cnvsht21_cv', 'pcnhtnorm21', 'tcnhtnorm21');                           
select deg_pot_tom_compare('deg_pot_tom_gene', 'p_cdvsht21', 't_cdvsht21', 'pot_cold_hot_21_gene', 
						   'tom_cold_hot_21_gene', 0, 'p_cdvsht21_cv', 't_cdvsht21_cv', 
						   'cdvsht21_cv', 'pcdhtnorm21', 'tcdhtnorm21');                            
select deg_pot_tom_compare('deg_pot_tom_trn', 'p_cdvsht21', 't_cdvsht21', 'pot_cold_hot_21_trn', 
						   'tom_cold_hot_21_trn', 0, 'p_cdvsht21_cv', 't_cdvsht21_cv', 
						   'cdvsht21_cv', 'pcdhtnorm21', 'tcdhtnorm21');

-- manually added
alter table deg_pot_tom_trn add pot_gene_id varchar(30);   
update deg_pot_tom_trn as a set pot_gene_id=b.gene from potato_pgsc3037_gtf as b where a.qid=b.trn 
	and b.trn is not null;  
-- for four way venn
-- A P CN vs HT; B P CD vs HT; C T CN vs HT; D T CD vs HT
alter table deg_pot_tom_trn add categ_7_up_four varchar(10), add categ_7_down_four varchar(10), 
	add categ_14_up_four varchar(10), add categ_14_down_four varchar(10),  add categ_21_up_four 
	varchar(10), add categ_21_down_four varchar(10);
update deg_pot_tom_trn set categ_7_up_four='all' where p_cnvsht7>=1 and p_cdvsht7>=1 and 
	t_cnvsht7>=1 and t_cdvsht7>=1;	
update deg_pot_tom_trn set categ_7_up_four='ABC' where p_cnvsht7>=1 and p_cdvsht7>=1 and 
	t_cnvsht7>=1 and t_cdvsht7<1;	
update deg_pot_tom_trn set categ_7_up_four='ABD' where p_cnvsht7>=1 and p_cdvsht7>=1 and t_cnvsht7<1
	and t_cdvsht7>=1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='ACD' where p_cnvsht7>=1 and p_cdvsht7<1 and t_cnvsht7>=1
	and t_cdvsht7>=1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='BCD' where p_cnvsht7<1 and p_cdvsht7>=1 and t_cnvsht7>=1
	and t_cdvsht7>=1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='AB' where p_cnvsht7>=1 and p_cdvsht7>=1 and t_cnvsht7<1 
	and t_cdvsht7<1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='AC' where p_cnvsht7>=1 and p_cdvsht7<1 and t_cnvsht7>=1 
	and t_cdvsht7<1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='AD' where p_cnvsht7>=1 and p_cdvsht7<1 and t_cnvsht7<1 
	and t_cdvsht7>=1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='BC' where p_cnvsht7<1 and p_cdvsht7>=1 and t_cnvsht7>=1 
	and t_cdvsht7<1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='BD' where p_cnvsht7<1 and p_cdvsht7>=1 and t_cnvsht7<1 
	and t_cdvsht7>=1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='CD' where p_cnvsht7<1 and p_cdvsht7<1 and t_cnvsht7>=1 
	and t_cdvsht7>=1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='A' where p_cnvsht7>=1 and p_cdvsht7<1 and t_cnvsht7<1 
	and t_cdvsht7<1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='B' where p_cnvsht7<1 and p_cdvsht7>=1 and t_cnvsht7<1 
	and t_cdvsht7<1 and categ_7_up_four is null;	
deg_pot_tom_trn set categ_7_up_four='C' where p_cnvsht7<1 and p_cdvsht7<1 and t_cnvsht7>=1 and 
	t_cdvsht7<1 and categ_7_up_four is null;	
update deg_pot_tom_trn set categ_7_up_four='D' where p_cnvsht7<1 and p_cdvsht7<1 and t_cnvsht7<1 
	and t_cdvsht7>=1 and categ_7_up_four is null;	

update deg_pot_tom_trn set categ_14_up_four='all' where p_cnvsht14>=1 and p_cdvsht14>=1 and 
	t_cnvsht14>=1 and t_cdvsht14>=1;	
update deg_pot_tom_trn set categ_14_up_four='ABC' where p_cnvsht14>=1 and p_cdvsht14>=1 and 
	t_cnvsht14>=1 and t_cdvsht14<1;	
update deg_pot_tom_trn set categ_14_up_four='ABD' where p_cnvsht14>=1 and p_cdvsht14>=1 and t_cnvsht14<1
	and t_cdvsht14>=1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='ACD' where p_cnvsht14>=1 and p_cdvsht14<1 and t_cnvsht14>=1
	and t_cdvsht14>=1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='BCD' where p_cnvsht14<1 and p_cdvsht14>=1 and t_cnvsht14>=1
	and t_cdvsht14>=1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='AB' where p_cnvsht14>=1 and p_cdvsht14>=1 and t_cnvsht14<1 
	and t_cdvsht14<1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='AC' where p_cnvsht14>=1 and p_cdvsht14<1 and t_cnvsht14>=1 
	and t_cdvsht14<1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='AD' where p_cnvsht14>=1 and p_cdvsht14<1 and t_cnvsht14<1 
	and t_cdvsht14>=1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='BC' where p_cnvsht14<1 and p_cdvsht14>=1 and t_cnvsht14>=1 
	and t_cdvsht14<1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='BD' where p_cnvsht14<1 and p_cdvsht14>=1 and t_cnvsht14<1 
	and t_cdvsht14>=1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='CD' where p_cnvsht14<1 and p_cdvsht14<1 and t_cnvsht14>=1 
	and t_cdvsht14>=1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='A' where p_cnvsht14>=1 and p_cdvsht14<1 and t_cnvsht14<1 
	and t_cdvsht14<1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='B' where p_cnvsht14<1 and p_cdvsht14>=1 and t_cnvsht14<1 
	and t_cdvsht14<1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='C' where p_cnvsht14<1 and p_cdvsht14<1 and t_cnvsht14>=1 and 
	t_cdvsht14<1 and categ_14_up_four is null;	
update deg_pot_tom_trn set categ_14_up_four='D' where p_cnvsht14<1 and p_cdvsht14<1 and t_cnvsht14<1 
	and t_cdvsht14>=1 and categ_14_up_four is null;

update deg_pot_tom_trn set categ_21_up_four='all' where p_cnvsht21>=1 and p_cdvsht21>=1 and 
	t_cnvsht21>=1 and t_cdvsht21>=1;	
update deg_pot_tom_trn set categ_21_up_four='ABC' where p_cnvsht21>=1 and p_cdvsht21>=1 and 
	t_cnvsht21>=1 and t_cdvsht21<1;	
update deg_pot_tom_trn set categ_21_up_four='ABD' where p_cnvsht21>=1 and p_cdvsht21>=1 and t_cnvsht21<1
	and t_cdvsht21>=1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='ACD' where p_cnvsht21>=1 and p_cdvsht21<1 and t_cnvsht21>=1
	and t_cdvsht21>=1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='BCD' where p_cnvsht21<1 and p_cdvsht21>=1 and t_cnvsht21>=1
	and t_cdvsht21>=1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='AB' where p_cnvsht21>=1 and p_cdvsht21>=1 and t_cnvsht21<1 
	and t_cdvsht21<1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='AC' where p_cnvsht21>=1 and p_cdvsht21<1 and t_cnvsht21>=1 
	and t_cdvsht21<1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='AD' where p_cnvsht21>=1 and p_cdvsht21<1 and t_cnvsht21<1 
	and t_cdvsht21>=1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='BC' where p_cnvsht21<1 and p_cdvsht21>=1 and t_cnvsht21>=1 
	and t_cdvsht21<1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='BD' where p_cnvsht21<1 and p_cdvsht21>=1 and t_cnvsht21<1 
	and t_cdvsht21>=1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='CD' where p_cnvsht21<1 and p_cdvsht21<1 and t_cnvsht21>=1 
	and t_cdvsht21>=1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='A' where p_cnvsht21>=1 and p_cdvsht21<1 and t_cnvsht21<1 
	and t_cdvsht21<1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='B' where p_cnvsht21<1 and p_cdvsht21>=1 and t_cnvsht21<1 
	and t_cdvsht21<1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='C' where p_cnvsht21<1 and p_cdvsht21<1 and t_cnvsht21>=1 and 
	t_cdvsht21<1 and categ_21_up_four is null;	
update deg_pot_tom_trn set categ_21_up_four='D' where p_cnvsht21<1 and p_cdvsht21<1 and t_cnvsht21<1 
	and t_cdvsht21>=1 and categ_21_up_four is null;	
	
update deg_pot_tom_trn set categ_7_down_four='all' where p_cnvsht7<=-1 and p_cdvsht7<=-1 and 
	t_cnvsht7<=-1 and t_cdvsht7<=-1;	
update deg_pot_tom_trn set categ_7_down_four='ABC' where p_cnvsht7<=-1 and p_cdvsht7<=-1 and 
	t_cnvsht7<=-1 and t_cdvsht7>-1;	
update deg_pot_tom_trn set categ_7_down_four='ABD' where p_cnvsht7<=-1 and p_cdvsht7<=-1 and 
	t_cnvsht7>-1 and t_cdvsht7<=-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='ACD' where p_cnvsht7<=-1 and p_cdvsht7>-1 and 
	t_cnvsht7<=-1 and t_cdvsht7<=-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='BCD' where p_cnvsht7>-1 and p_cdvsht7<=-1 and 
	t_cnvsht7<=-1 and t_cdvsht7<=-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='AB' where p_cnvsht7<=-1 and p_cdvsht7<=-1 and 
	t_cnvsht7>-1 and t_cdvsht7>-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='AC' where p_cnvsht7<=-1 and p_cdvsht7>-1 and 
	t_cnvsht7<=-1 and t_cdvsht7>-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='AD' where p_cnvsht7<=-1 and p_cdvsht7>-1 and 
	t_cnvsht7>-1 and t_cdvsht7<=-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='BC' where p_cnvsht7>-1 and p_cdvsht7<=-1 and 
	t_cnvsht7<=-1 and t_cdvsht7>-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='BD' where p_cnvsht7>-1 and p_cdvsht7<=-1 and 
	t_cnvsht7>-1 and t_cdvsht7<=-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='CD' where p_cnvsht7>-1 and p_cdvsht7>-1 and 
	t_cnvsht7<=-1 and t_cdvsht7<=-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='A' where p_cnvsht7<=-1 and p_cdvsht7>-1 and 
	t_cnvsht7>-1 and t_cdvsht7>-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='B' where p_cnvsht7>-1 and p_cdvsht7<=-1 and 
	t_cnvsht7>-1 and t_cdvsht7>-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='C' where p_cnvsht7>-1 and p_cdvsht7>-1 and 
	t_cnvsht7<=-1 and t_cdvsht7>-1 and categ_7_down_four is null;	
update deg_pot_tom_trn set categ_7_down_four='D' where p_cnvsht7>-1 and p_cdvsht7>-1 and 
	t_cnvsht7>-1 and t_cdvsht7<=-1 and categ_7_down_four is null;
	
update deg_pot_tom_trn set categ_14_down_four='all' where p_cnvsht14<=-1 and p_cdvsht14<=-1 and 
	t_cnvsht14<=-1 and t_cdvsht14<=-1;	
update deg_pot_tom_trn set categ_14_down_four='ABC' where p_cnvsht14<=-1 and p_cdvsht14<=-1 and 
	t_cnvsht14<=-1 and t_cdvsht14>-1;	
update deg_pot_tom_trn set categ_14_down_four='ABD' where p_cnvsht14<=-1 and p_cdvsht14<=-1 and 
	t_cnvsht14>-1 and t_cdvsht14<=-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='ACD' where p_cnvsht14<=-1 and p_cdvsht14>-1 and 
	t_cnvsht14<=-1 and t_cdvsht14<=-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='BCD' where p_cnvsht14>-1 and p_cdvsht14<=-1 and 
	t_cnvsht14<=-1 and t_cdvsht14<=-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='AB' where p_cnvsht14<=-1 and p_cdvsht14<=-1 and 
	t_cnvsht14>-1 and t_cdvsht14>-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='AC' where p_cnvsht14<=-1 and p_cdvsht14>-1 and 
	t_cnvsht14<=-1 and t_cdvsht14>-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='AD' where p_cnvsht14<=-1 and p_cdvsht14>-1 and 
	t_cnvsht14>-1 and t_cdvsht14<=-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='BC' where p_cnvsht14>-1 and p_cdvsht14<=-1 and 
	t_cnvsht14<=-1 and t_cdvsht14>-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='BD' where p_cnvsht14>-1 and p_cdvsht14<=-1 and 
	t_cnvsht14>-1 and t_cdvsht14<=-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='CD' where p_cnvsht14>-1 and p_cdvsht14>-1 and 
	t_cnvsht14<=-1 and t_cdvsht14<=-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='A' where p_cnvsht14<=-1 and p_cdvsht14>-1 and 
	t_cnvsht14>-1 and t_cdvsht14>-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='B' where p_cnvsht14>-1 and p_cdvsht14<=-1 and 
	t_cnvsht14>-1 and t_cdvsht14>-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='C' where p_cnvsht14>-1 and p_cdvsht14>-1 and 
	t_cnvsht14<=-1 and t_cdvsht14>-1 and categ_14_down_four is null;	
update deg_pot_tom_trn set categ_14_down_four='D' where p_cnvsht14>-1 and p_cdvsht14>-1 and 
	t_cnvsht14>-1 and t_cdvsht14<=-1 and categ_14_down_four is null;
	
update deg_pot_tom_trn set categ_21_down_four='all' where p_cnvsht21<=-1 and p_cdvsht21<=-1 and 
	t_cnvsht21<=-1 and t_cdvsht21<=-1;	
update deg_pot_tom_trn set categ_21_down_four='ABC' where p_cnvsht21<=-1 and p_cdvsht21<=-1 and 
	t_cnvsht21<=-1 and t_cdvsht21>-1;	
update deg_pot_tom_trn set categ_21_down_four='ABD' where p_cnvsht21<=-1 and p_cdvsht21<=-1 and 
	t_cnvsht21>-1 and t_cdvsht21<=-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='ACD' where p_cnvsht21<=-1 and p_cdvsht21>-1 and 
	t_cnvsht21<=-1 and t_cdvsht21<=-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='BCD' where p_cnvsht21>-1 and p_cdvsht21<=-1 and 
	t_cnvsht21<=-1 and t_cdvsht21<=-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='AB' where p_cnvsht21<=-1 and p_cdvsht21<=-1 and 
	t_cnvsht21>-1 and t_cdvsht21>-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='AC' where p_cnvsht21<=-1 and p_cdvsht21>-1 and 
	t_cnvsht21<=-1 and t_cdvsht21>-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='AD' where p_cnvsht21<=-1 and p_cdvsht21>-1 and 
	t_cnvsht21>-1 and t_cdvsht21<=-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='BC' where p_cnvsht21>-1 and p_cdvsht21<=-1 and 
	t_cnvsht21<=-1 and t_cdvsht21>-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='BD' where p_cnvsht21>-1 and p_cdvsht21<=-1 and 
	t_cnvsht21>-1 and t_cdvsht21<=-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='CD' where p_cnvsht21>-1 and p_cdvsht21>-1 and 
	t_cnvsht21<=-1 and t_cdvsht21<=-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='A' where p_cnvsht21<=-1 and p_cdvsht21>-1 and 
	t_cnvsht21>-1 and t_cdvsht21>-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='B' where p_cnvsht21>-1 and p_cdvsht21<=-1 and 
	t_cnvsht21>-1 and t_cdvsht21>-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='C' where p_cnvsht21>-1 and p_cdvsht21>-1 and 
	t_cnvsht21<=-1 and t_cdvsht21>-1 and categ_21_down_four is null;	
update deg_pot_tom_trn set categ_21_down_four='D' where p_cnvsht21>-1 and p_cdvsht21>-1 and 
	t_cnvsht21>-1 and t_cdvsht21<=-1 and categ_21_down_four is null;	
	
\copy deg_pot_tom_gene to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_gene.csv' with (FORMAT csv, header true);
\copy deg_pot_tom_trn to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn.csv' with (FORMAT csv, header true);
 ^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^ 
--create DEG stat table 
create or replace function deg_stat_ortho(_deg_stat_table varchar(20), _pconds varchar(20), 
	_tconds varchar(20), _ortho_deg_table varchar(20), _conds varchar(20), _pcondscv varchar(20), 
	_tcondscv varchar(20))
    returns void as
    $body$
    begin
       execute format('update %I set pot_up=(select count(*) from %I where %I>=1) where 
					   conds=''%I'' ',  _deg_stat_table, _ortho_deg_table, _pconds, _conds);                       
       execute format('update %I set pot_down=(select count(*) from %I where %I<=-1) where conds=''%I'' ',  _deg_stat_table,
                       _ortho_deg_table, _pconds, _conds);
       execute format('update %I set tom_up=(select count(*) from %I where %I>=1) where conds=''%I'' ',  _deg_stat_table,
                       _ortho_deg_table, _tconds, _conds);
       execute format('update %I set tom_down=(select count(*) from %I where %I<=-1) where conds=''%I'' ',  _deg_stat_table,
                       _ortho_deg_table, _tconds, _conds);
       --count fot trn where CV <=1
       execute format('update %I set pot_upc=(select count(*) from %I where %I>=1 and %I<=1) where conds=''%I'' ',  _deg_stat_table,
                       _ortho_deg_table, _pconds, _pcondscv, _conds);
       execute format('update %I set pot_downc=(select count(*) from %I where %I<=-1 and %I<=1) where conds=''%I'' ',  _deg_stat_table,
                       _ortho_deg_table, _pconds, _pcondscv, _conds);
       execute format('update %I set tom_upc=(select count(*) from %I where %I>=1 and %I<=1) where conds=''%I'' ',  _deg_stat_table,
                       _ortho_deg_table, _tconds, _tcondscv, _conds);
       execute format('update %I set tom_downc=(select count(*) from %I where %I<=-1 and %I<=1) where conds=''%I'' ',  _deg_stat_table,
                       _ortho_deg_table, _tconds, _tcondscv, _conds);
    end
    $body$ language "plpgsql";

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Ortholog DEG stat table ';
END
$$;
drop table if exists ortho_trn_deg_stat;
create table ortho_trn_deg_stat(conds varchar(20));
\copy ortho_trn_deg_stat from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/conds.csv' with csv header delimiter as ',';
alter table ortho_trn_deg_stat add pot_up int, add pot_down int, add tom_up int, add tom_down int,
                               add pot_upc int, add pot_downc int, add tom_upc int, add tom_downc int ;
select deg_stat_ortho('ortho_trn_deg_stat', 'p_cnvscd7', 't_cnvscd7', 'deg_pot_tom_trn', 'cnvscd7',
					  'p_cnvscd7_cv', 't_cnvscd7_cv');
select deg_stat_ortho('ortho_trn_deg_stat', 'p_cnvsht7', 't_cnvsht7', 'deg_pot_tom_trn', 'cnvsht7',
					  'p_cnvsht7_cv', 't_cnvsht7_cv');
select deg_stat_ortho('ortho_trn_deg_stat', 'p_cdvsht7', 't_cdvsht7', 'deg_pot_tom_trn', 'cdvsht7',
					  'p_cdvsht7_cv', 't_cdvsht7_cv');
select deg_stat_ortho('ortho_trn_deg_stat', 'p_cnvscd14', 't_cnvscd14', 'deg_pot_tom_trn', 
					  'cnvscd14', 'p_cnvscd14_cv', 't_cnvscd14_cv');
select deg_stat_ortho('ortho_trn_deg_stat', 'p_cnvsht14', 't_cnvsht14', 'deg_pot_tom_trn', 
					  'cnvsht14', 'p_cnvsht14_cv', 't_cnvsht14_cv');
select deg_stat_ortho('ortho_trn_deg_stat', 'p_cdvsht14', 't_cdvsht14', 'deg_pot_tom_trn', 
					  'cdvsht14', 'p_cdvsht14_cv', 't_cdvsht14_cv');
select deg_stat_ortho('ortho_trn_deg_stat', 'p_cnvscd21', 't_cnvscd21', 'deg_pot_tom_trn', 
					  'cnvscd21', 'p_cnvscd21_cv', 't_cnvscd21_cv');
select deg_stat_ortho('ortho_trn_deg_stat', 'p_cnvsht21', 't_cnvsht21', 'deg_pot_tom_trn', 
					  'cnvsht21', 'p_cnvsht21_cv', 't_cnvsht21_cv');
select deg_stat_ortho('ortho_trn_deg_stat', 'p_cdvsht21', 't_cdvsht21', 'deg_pot_tom_trn', 
					  'cdvsht21', 'p_cdvsht21_cv', 't_cdvsht21_cv');
\copy ortho_trn_deg_stat to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/ortho_trn_deg_stat.csv' with (FORMAT csv, header true);

 ^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^ 
--prepare WGCNA tables; try to format tables in psql and not in r to avoid mistakes
--id: wgcna_table_export
DO language plpgsql $$
BEGIN
  RAISE NOTICE 'WGCNA table export ';
END
$$;
create or replace function wgcna_table_export(_wgcna_table varchar(20), _tag varchar(20), 
	_wgcna_outfile varchar(20) )
	returns void as
    $body$
	begin
		execute format('drop table if exists %I, temp', _wgcna_table);
		execute format('create table temp as select orthoid, p_cnvscd7, t_cnvscd7, p_cnvsht7, 
						t_cnvsht7, p_cdvsht7, t_cdvsht7, p_cnvscd14, t_cnvscd14, p_cnvsht14, 
						t_cnvsht14, p_cdvsht14, t_cdvsht14, p_cnvscd21, t_cnvscd21, p_cnvsht21, 
						t_cnvsht21, p_cdvsht21, t_cdvsht21 from deg_pot_tom_trn');
		execute format('delete from temp where p_cnvscd7=0 and t_cnvscd7=0 and p_cnvsht7=0 and 
						t_cnvsht7=0 and p_cdvsht7=0 and t_cdvsht7=0 and p_cnvscd14=0 and 
						t_cnvscd14=0 and p_cnvsht14=0 and t_cnvsht14=0 and p_cdvsht14=0 and 
						t_cdvsht14=0 and p_cnvscd21=0 and t_cnvscd21=0 and p_cnvsht21=0 and 
						t_cnvsht21=0 and p_cdvsht21=0 and t_cdvsht21=0');																												
		if _tag = 'all' then
			execute format('create table %I as select * from temp where p_cnvscd7>=1 or 
							p_cnvscd7<=-1 or t_cnvscd7>=1 or t_cnvscd7<=-1 or p_cnvsht7>=1 or 
							p_cnvsht7<=-1 or t_cnvsht7>=1 or t_cnvsht7<=-1 or p_cdvsht7>=1 or 
							p_cdvsht7<=-1 or t_cdvsht7>=1 or t_cdvsht7<=-1 
							or
							p_cnvscd14>=1 or p_cnvscd14<=-1 or t_cnvscd14>=1 or t_cnvscd14<=-1 or 
							p_cnvsht14>=1 or p_cnvsht14<=-1 or t_cnvsht14>=1 or t_cnvsht14<=-1 or 
							p_cdvsht14>=1 or p_cdvsht14<=-1 or t_cdvsht14>=1 or t_cdvsht14<=-1
							or
							p_cnvscd21>=1 or p_cnvscd21<=-1 or t_cnvscd21>=1 or t_cnvscd21<=-1 or 
							p_cnvsht21>=1 or p_cnvsht21<=-1 or t_cnvsht21>=1 or t_cnvsht21<=-1 or 
							p_cdvsht21>=1 or p_cdvsht21<=-1 or t_cdvsht21>=1 or t_cdvsht21<=-1', 
							_wgcna_table);
		elsif _tag = 'cncd' then
			execute format('create table %I as select * from temp where p_cnvscd7>=1 or 
							p_cnvscd7<=-1 or t_cnvscd7>=1 or t_cnvscd7<=-1
							or
							p_cnvscd14>=1 or p_cnvscd14<=-1 or t_cnvscd14>=1 or t_cnvscd14<=-1
							or
							p_cnvscd21>=1 or p_cnvscd21<=-1 or t_cnvscd21>=1 or t_cnvscd21<=-1',
							_wgcna_table);
		elsif _tag = 'cnht' then
			execute format('create table %I as select * from temp where p_cnvsht7>=1 or 
							p_cnvsht7<=-1 or t_cnvsht7>=1 or t_cnvsht7<=-1
							or
							p_cnvsht14>=1 or p_cnvsht14<=-1 or t_cnvsht14>=1 or t_cnvsht14<=-1
							or
							p_cnvsht21>=1 or p_cnvsht21<=-1 or t_cnvsht21>=1 or t_cnvsht21<=-1',
							_wgcna_table);		
		
		elsif _tag = 'cdht' then
			execute format('create table %I as select * from temp where p_cdvsht7>=1 or 
							p_cdvsht7<=-1 or t_cdvsht7>=1 or t_cdvsht7<=-1
							or
							p_cdvsht14>=1 or p_cdvsht14<=-1 or t_cdvsht14>=1 or t_cdvsht14<=-1
							or
							p_cdvsht21>=1 or p_cdvsht21<=-1 or t_cdvsht21>=1 or t_cdvsht21<=-1',
							_wgcna_table);									
		end if;		
		execute format('copy %I to $$' || _wgcna_outfile || '$$' || ' with (FORMAT csv, header 
					   true)', _wgcna_table);											
	end
    $body$ language "plpgsql";

\set wgcnadir $$'/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/'$$

--deg_pot_tom_trn_wgcna_all: atleast one condition from 18 samples >= |1| 
--deg_pot_tom_trn_wgcna_cncd: atleast once cncd condition from 18 samples >= |1| 
--deg_pot_tom_trn_wgcna_cnht: atleast once cnht condition from 18 samples >= |1| 
select wgcna_table_export('deg_pot_tom_trn_wgcna_all', 'all', :wgcnadir || 
						  'deg_pot_tom_trn_wgcna_all.csv');	
select wgcna_table_export('deg_pot_tom_trn_wgcna_cncd', 'cncd', :wgcnadir || 
						  'deg_pot_tom_trn_wgcna_cncd.csv');
select wgcna_table_export('deg_pot_tom_trn_wgcna_cnht', 'cnht', :wgcnadir || 
						  'deg_pot_tom_trn_wgcna_cnht.csv');						  
select wgcna_table_export('deg_pot_tom_trn_wgcna_cdht', 'cdht', :wgcnadir || 
						  'deg_pot_tom_trn_wgcna_cdht.csv');	
	
	
 ^^^^^^^^^^^^^ */	

/* ^^^^^^^^^^^^^  */
--create WGCNA network

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'WGCNA network table ';
END
$$;


create or replace function wgcna_analysis(_wgcna_module_table varchar(20), _wgcna_module_infile 
	varchar(20), _wgcna_module_node_attr_table varchar(20), _wgcna_module_outfile varchar(20), 
	_wgcna_module_node_attr_outfile varchar(20), _wgcna_node_infile varchar(20), _module_type 
	varchar(20), _wgcna_module_outfile_06 varchar(20) )
	returns void as
    $body$
	begin
		execute format('drop table if exists %I, temp, %I ', _wgcna_module_table, 
						_wgcna_module_node_attr_table);
		execute format('create table %I (src varchar(20), tar varchar(20), wt double precision, dir 
						varchar(20), srcalt varchar(4), taralt varchar(4))', _wgcna_module_table);
		execute format('copy %I from $$' || _wgcna_module_infile || '$$' || ' with csv header 
						delimiter as ' || '$$' || ',' || '$$', _wgcna_module_table);	
		execute format('create table temp (src varchar(20), srcalt varchar(5), color varchar(20))'); 
		execute format('copy temp from $$' || _wgcna_node_infile || '$$' || ' with csv header 
						delimiter as ' || '$$' || ',' || '$$');
		--log2fc value will be p and t cnvscd, cnvsht and cdvsht order 7, 14 and 21
		execute format('alter table	%I add color varchar(20), add srcanot varchar(300), add taranot 
						varchar(300), add log2fc text, add normct text, add srccount int, add 
						nodesize int, add p_lfc double precision, add t_lfc double precision, add
						shape varchar(10)', 
						_wgcna_module_table);						
		execute format('update %I as a set color=b.color from temp as b where a.src=b.src', 
						_wgcna_module_table);	
		execute format('update %I as a set color=b.color from temp as b where a.tar=b.src and 
						a.color is null', _wgcna_module_table);		
		execute format('update %I as a set srcanot=b.anot from deg_pot_tom_trn as b where 
						a.src=b.orthoid', _wgcna_module_table);		
		execute format('update %I as a set taranot=b.anot from deg_pot_tom_trn as b where 
						a.tar=b.orthoid', _wgcna_module_table);					
		execute format('update %I as a set log2fc=(round(b.p_cnvscd7::numeric, 2) || '';'' || 
						round(b.t_cnvscd7::numeric, 2) || '';'' || round(b.p_cnvsht7::numeric, 2) ||
						'';'' || round(b.t_cnvsht7::numeric, 2) || '';'' || 
						round(b.p_cdvsht7::numeric, 2) || '';'' || round(b.t_cdvsht7::numeric, 2) ||  
						'';'' || round(b.p_cnvscd14::numeric, 2) || '';'' || 
						round(b.t_cnvscd14::numeric, 2) || '';'' || round(b.p_cnvsht14::numeric, 2) 
						|| '';'' || round(b.t_cnvsht14::numeric, 2) || '';'' || 
						round(b.p_cdvsht14::numeric, 2) || '';'' || round(b.t_cdvsht14::numeric, 2)
						|| '';'' || round(b.p_cnvscd21::numeric, 2) || '';'' || 
						round(b.t_cnvscd21::numeric, 2) || '';'' || round(b.p_cnvsht21::numeric, 2) 
						|| '';'' || round(b.t_cnvsht21::numeric, 2) || '';'' || 
						round(b.p_cdvsht21::numeric, 2) || '';'' ||  round(b.t_cdvsht21::numeric, 2)) 
						from deg_pot_tom_trn as b where a.src=b.orthoid', _wgcna_module_table);					
		execute format('drop table if exists temp');
		execute format('create table temp as select src, count(src) from %I group by src', 
						_wgcna_module_table);
		execute format('update %I as a set srccount=b.count from temp as b where a.src=b.src', 
						_wgcna_module_table);				
		execute format('update %I as a set nodesize=20', _wgcna_module_table);	
		execute format('create table %I as select src from %I union select tar from
						%I', _wgcna_module_node_attr_table, _wgcna_module_table, 
						_wgcna_module_table);
		execute format('alter table	%I add color varchar(20), add nodesize int, add nodeht int, 
						add label varchar(20), add srccount int, add shape varchar(10)', 
						_wgcna_module_node_attr_table);
		execute format('update %I as a set color=b.color, srccount=b.srccount  from %I as b where
						a.src=b.src', _wgcna_module_node_attr_table, _wgcna_module_table);	
		execute format('update %I as a set color=b.color from %I as b where a.src=b.tar and a.color 
						is null', _wgcna_module_node_attr_table, _wgcna_module_table); 	
		execute format('update %I as a set nodesize=20', _wgcna_module_node_attr_table);	
		execute format('update %I as a set nodeht=20', _wgcna_module_node_attr_table);	
		execute format('copy %I to $$' || _wgcna_module_outfile || '$$' || ' with (FORMAT csv, 
						header true)',  _wgcna_module_table);
						
		--set nodesize and height for hub genes
		--hub genes are identified as top 5 genes with highest connections
		if _module_type = 'all' then
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1948 and 
							color=''turquoise'' ', _wgcna_module_node_attr_table);
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1916 and 
							color=''blue'' ', _wgcna_module_node_attr_table);					
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1932 and 
							color=''brown'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1929 and 
							color=''yellow'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1953 and 
							color=''green'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1837 and 
							color=''red'' ', _wgcna_module_node_attr_table);								
		elsif _module_type = 'allturq' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=642 ', 
							_wgcna_module_node_attr_table);
		elsif _module_type = 'allblue' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=416 ', 
							_wgcna_module_node_attr_table);						
		elsif _module_type = 'allbrow' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=399 ', 
							_wgcna_module_node_attr_table);		
		elsif _module_type = 'allyell' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=210 ', 
							_wgcna_module_node_attr_table);	
		elsif _module_type = 'allgree' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=193 ', 
							_wgcna_module_node_attr_table);		
		elsif _module_type = 'allred' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=76 ', 
							_wgcna_module_node_attr_table);							
		elsif _module_type = 'cncdall' then
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=222 and 
							color=''turquoise'' ', _wgcna_module_node_attr_table);
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=117 and 
							color=''blue'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=158 and 
							color=''brown'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=198 and 
							color=''yellow'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=254 and 
							color=''green'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set shape=''Ellipse'' from deg_pot_tom_trn as b where 
							a.src=b.orthoid and b.p_cnvscd21>=1', _wgcna_module_node_attr_table);	
			execute format('update %I as a set shape=''Diamond'' from deg_pot_tom_trn as b where 
							a.src=b.orthoid and b.p_cnvscd21<=-1', _wgcna_module_node_attr_table);
			execute format('copy (select * from %I order by wt desc limit 2000) to $$' || 
							_wgcna_module_outfile_06 || '$$' || ' with (FORMAT csv, header true)', 
							_wgcna_module_table);							
		elsif _module_type = 'cncdturq' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=122 ', 
							_wgcna_module_node_attr_table);	
		elsif _module_type = 'cncdblue' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=60 ', 
							_wgcna_module_node_attr_table);						
		elsif _module_type = 'cncdbrow' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=69 ', 
							_wgcna_module_node_attr_table);				
		elsif _module_type = 'cncdyell' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=66 ', 
							_wgcna_module_node_attr_table);	
		elsif _module_type = 'cncdgree' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=58 ', 
							_wgcna_module_node_attr_table);										
		elsif _module_type = 'cnhtall' then
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1389 and 
							color=''turquoise'' ', _wgcna_module_node_attr_table);
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1462 and 
							color=''blue'' ', _wgcna_module_node_attr_table);				
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1242 and 
							color=''brown'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1359 and 
							color=''yellow'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1385 and 
							color=''green'' ', _wgcna_module_node_attr_table);
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1104 and 
							color=''red'' ', _wgcna_module_node_attr_table);				
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1347 and 
							color=''black'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1142 and 
							color=''pink'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set shape=''Ellipse'' from deg_pot_tom_trn as b where 
							a.src=b.orthoid and b.p_cnvsht21>=1', _wgcna_module_node_attr_table);	
			execute format('update %I as a set shape=''Diamond'' from deg_pot_tom_trn as b where 
							a.src=b.orthoid and b.p_cnvsht21<=-1', _wgcna_module_node_attr_table);	
			execute format('copy (select * from %I order by wt desc limit 2000) to $$' || 
							_wgcna_module_outfile_06 || '$$' || ' with (FORMAT csv, header true)', 
							_wgcna_module_table);						
		elsif _module_type = 'cnhtturq' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=332 ', 
							_wgcna_module_node_attr_table);		
		elsif _module_type = 'cnhtbrow' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=291 ', 
							_wgcna_module_node_attr_table);		
		elsif _module_type = 'cnhtblue' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=217 ', 
							_wgcna_module_node_attr_table);	
		elsif _module_type = 'cnhtyell' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=185 ', 
							_wgcna_module_node_attr_table);	
		elsif _module_type = 'cnhtgree' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=157 ', 
							_wgcna_module_node_attr_table);		
		elsif _module_type = 'cnhtred' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=108 ', 
							_wgcna_module_node_attr_table);	
		elsif _module_type = 'cnhtblac' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=107 ', 
							_wgcna_module_node_attr_table);						
		elsif _module_type = 'cnhtpink' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=61 ', 
							_wgcna_module_node_attr_table);		
		
		elsif _module_type = 'cdhtall' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1085 and 
							color=''turquoise'' ', _wgcna_module_node_attr_table);
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1083 and 
							color=''blue'' ', _wgcna_module_node_attr_table);				
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1058 and 
							color=''brown'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=1038 and 
							color=''yellow'' ', _wgcna_module_node_attr_table);	
			execute format('update %I as a set shape=''Ellipse'' from deg_pot_tom_trn as b where 
							a.src=b.orthoid and b.p_cdvsht21>=1', _wgcna_module_node_attr_table);	
			execute format('update %I as a set shape=''Diamond'' from deg_pot_tom_trn as b where 
							a.src=b.orthoid and b.p_cdvsht21<=-1', _wgcna_module_node_attr_table);
			execute format('copy (select * from %I order by wt desc limit 2000) to $$' || 
							_wgcna_module_outfile_06 || '$$' || ' with (FORMAT csv, header true)',  
							_wgcna_module_table);				
		elsif _module_type = 'cdhtturq' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=442 ', 
							_wgcna_module_node_attr_table);		
		elsif _module_type = 'cdhtbrow' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=245 ', 
							_wgcna_module_node_attr_table);		
		elsif _module_type = 'cdhtblue' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=216 ', 
							_wgcna_module_node_attr_table);	
		elsif _module_type = 'cdhtyell' then		
			execute format('update %I as a set nodesize=60, nodeht=60 where srccount>=176 ', 
							_wgcna_module_node_attr_table);							
		end if;	
		execute format('copy %I to $$' || _wgcna_module_node_attr_outfile || '$$' || ' with (FORMAT 
						csv, header true)',  _wgcna_module_node_attr_table);
	end
    $body$ language "plpgsql";

\set wgcnadir $$'/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/WGCNA/'$$

--all 18 samples with atleast log2fc >= |1| check r code 
select wgcna_analysis('wgcna_modules_allcomp_allmod', 
					  :wgcnadir || 'CytoscapeInput-edges-turquoise-blue-brown-yellow-green-redall.csv',
					  'wgcna_modules_node_attr_allcomp_allmod', 
					  :wgcnadir || 'wgcna_modules_allcomp_allmod_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_allcomp_allmod_all_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-turquoise-blue-brown-yellow-green-redall.csv', 
					  'all', :wgcnadir || 'wgcna_modules_allcomp_allmod_network_2k.csv');
select wgcna_analysis('wgcna_modules_allcomp_turquoise', 
					  :wgcnadir || 'CytoscapeInput-edges-04turquoiseall.csv',
					  'wgcna_modules_node_attr_allcomp_turquoise', 
					  :wgcnadir || 'wgcna_modules_allcomp_turquoise_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_allcomp_turquoise_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-04turquoiseall.csv', 'allturq', 
					  :wgcnadir || 'wgcna_modules_allcomp_turquoise_network_2k.csv');		
select wgcna_analysis('wgcna_modules_allcomp_blue', 
					  :wgcnadir || 'CytoscapeInput-edges-04blueall.csv',
					  'wgcna_modules_node_attr_allcomp_blue', 
					  :wgcnadir || 'wgcna_modules_allcomp_blue_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_allcomp_blue_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-blueall.csv', 'allblue', 
					  :wgcnadir || 'wgcna_modules_allcomp_blue_network_2k.csv');		
select wgcna_analysis('wgcna_modules_allcomp_brown', 
					  :wgcnadir || 'CytoscapeInput-edges-brownall.csv',
					  'wgcna_modules_node_attr_allcomp_brown', 
					  :wgcnadir || 'wgcna_modules_allcomp_brown_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_allcomp_brown_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-brownall.csv', 'allbrow', 
					  :wgcnadir || 'wgcna_modules_allcomp_brown_network_2k.csv');		
select wgcna_analysis('wgcna_modules_allcomp_yellow', 
					  :wgcnadir || 'CytoscapeInput-edges-yellowall.csv',
					  'wgcna_modules_node_attr_allcomp_yellow', 
					  :wgcnadir || 'wgcna_modules_allcomp_yellow_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_allcomp_yellow_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-yellowall.csv', 'allyell', 
					  :wgcnadir || 'wgcna_modules_allcomp_yellow_network_2k.csv');	
select wgcna_analysis('wgcna_modules_allcomp_green', 
					  :wgcnadir || 'CytoscapeInput-edges-greenall.csv',
					  'wgcna_modules_node_attr_allcomp_green', 
					  :wgcnadir || 'wgcna_modules_allcomp_green_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_allcomp_green_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-greenall.csv', 'allgree', 
					  :wgcnadir || 'wgcna_modules_allcomp_green_network_2k.csv');
select wgcna_analysis('wgcna_modules_allcomp_red', 
					  :wgcnadir || 'CytoscapeInput-edges-redall.csv',
					  'wgcna_modules_node_attr_allcomp_red', 
					  :wgcnadir || 'wgcna_modules_allcomp_red_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_allcomp_red_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-redall.csv', 'allred', 
					  :wgcnadir || 'wgcna_modules_allcomp_red_network.csv');					  


--all con vs COLD samples with atleast log2fc >= |1| IN ATLEAST ONe CNCD							  
select wgcna_analysis('wgcna_modules_cncd_allmod', 
					  :wgcnadir || 'CytoscapeInput-edges-turquoise-blue-brown-yellow-greencncd.csv',
					  'wgcna_modules_node_attr_cncd_allmod', 
					  :wgcnadir || 'wgcna_modules_cncd_allmod_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cncd_allmod_all_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-turquoise-blue-brown-yellow-greencncd.csv', 
					  'cncdall', :wgcnadir || 'wgcna_modules_cncd_allmod_network_2k.csv');
select wgcna_analysis('wgcna_modules_cncd_turquoise', 
					  :wgcnadir || 'CytoscapeInput-edges-turquoisecncd.csv',
					  'wgcna_modules_node_attr_cncd_turquoise', 
					  :wgcnadir || 'wgcna_modules_cncd_turquoise_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cncd_turquoise_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-turquoisecncd.csv', 'cncdturq', 
					  :wgcnadir || 'wgcna_modules_cncd_turquoise_network_2k.csv');
select wgcna_analysis('wgcna_modules_cncd_blue', 
					  :wgcnadir || 'CytoscapeInput-edges-bluecncd.csv',
					  'wgcna_modules_node_attr_cncd_blue', 
					  :wgcnadir || 'wgcna_modules_cncd_blue_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cncd_blue_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-bluecncd.csv', 'cncdblue', 
					  :wgcnadir || 'wgcna_modules_cncd_blue_network_2k.csv');	
select wgcna_analysis('wgcna_modules_cncd_brown', 
					  :wgcnadir || 'CytoscapeInput-edges-browncncd.csv',
					  'wgcna_modules_node_attr_cncd_brown', 
					  :wgcnadir || 'wgcna_modules_cncd_brown_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cncd_brown_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-browncncd.csv', 'cncdbrow', 
					  :wgcnadir || 'wgcna_modules_cncd_brown_network_2k.csv');	
select wgcna_analysis('wgcna_modules_cncd_yellow', 
					  :wgcnadir || 'CytoscapeInput-edges-yellowcncd.csv',
					  'wgcna_modules_node_attr_cncd_yellow', 
					  :wgcnadir || 'wgcna_modules_cncd_yellow_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cncd_yellow_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-yellowcncd.csv', 'cncdyell', 
					  :wgcnadir || 'wgcna_modules_cncd_yellow_network_2k.csv');	
select wgcna_analysis('wgcna_modules_cncd_green', 
					  :wgcnadir || 'CytoscapeInput-edges-greencncd.csv',
					  'wgcna_modules_node_attr_cncd_green', 
					  :wgcnadir || 'wgcna_modules_cncd_green_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cncd_green_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-greencncd.csv', 'cncdgree', 
					  :wgcnadir || 'wgcna_modules_cncd_green_network_2k.csv');					  
					  
--all con vs hot samples with atleast log2fc >= |1| IN ATLEAST ONe CNht 					  
select wgcna_analysis('wgcna_modules_cnht_allmod', 
					  :wgcnadir || 'CytoscapeInput-edges-turquoise-blue-brown-yellow-green-red-black-pinkcnht.csv',
					  'wgcna_modules_node_attr_cnht_allmod', 
					  :wgcnadir || 'wgcna_modules_cnht_allmod_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cnht_allmod_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-turquoise-blue-brown-yellow-green-red-black-pinkcnht.csv',
					  'cnhtall', :wgcnadir || 'wgcna_modules_cnht_allmod_network_2k.csv');		
select wgcna_analysis('wgcna_modules_cnht_turquoise', 
					  :wgcnadir || 'CytoscapeInput-edges-turquoisecnht.csv',
					  'wgcna_modules_node_attr_cnht_turquoise', 
					  :wgcnadir || 'wgcna_modules_cnht_turquoise_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cnht_turquoise_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-turquoisecnht.csv', 'cnhtturq', 
					  :wgcnadir || 'wgcna_modules_cnht_turquoise_network_2k.csv');	
select wgcna_analysis('wgcna_modules_cnht_blue', 
					  :wgcnadir || 'CytoscapeInput-edges-bluecnht.csv',
					  'wgcna_modules_node_attr_cnht_blue', 
					  :wgcnadir || 'wgcna_modules_cnht_blue_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cnht_blue_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-bluecnht.csv', 'cnhtblue', 
					  :wgcnadir || 'wgcna_modules_cnht_blue_network_2k.csv');					  
select wgcna_analysis('wgcna_modules_cnht_brown', 
					  :wgcnadir || 'CytoscapeInput-edges-browncnht.csv',
					  'wgcna_modules_node_attr_cnht_brown', 
					  :wgcnadir || 'wgcna_modules_cnht_brown_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cnht_brown_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-browncnht.csv', 'cnhtbrow', 
					  :wgcnadir || 'wgcna_modules_cnht_brown_network_2k.csv');	
select wgcna_analysis('wgcna_modules_cnht_yellow', 
					  :wgcnadir || 'CytoscapeInput-edges-yellowcnht.csv',
					  'wgcna_modules_node_attr_cnht_yellow', 
					  :wgcnadir || 'wgcna_modules_cnht_yellow_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cnht_yellow_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-yellowcnht.csv', 'cnhtyell', 
					  :wgcnadir || 'wgcna_modules_cnht_yellow_network_2k.csv');		
select wgcna_analysis('wgcna_modules_cnht_green', 
					  :wgcnadir || 'CytoscapeInput-edges-greencnht.csv',
					  'wgcna_modules_node_attr_cnht_green', 
					  :wgcnadir || 'wgcna_modules_cnht_green_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cnht_green_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-greencnht.csv', 'cnhtgree', 
					  :wgcnadir || 'wgcna_modules_cnht_green_network_2k.csv');	
select wgcna_analysis('wgcna_modules_cnht_red', 
					  :wgcnadir || 'CytoscapeInput-edges-redcnht.csv',
					  'wgcna_modules_node_attr_cnht_red', 
					  :wgcnadir || 'wgcna_modules_cnht_red_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cnht_red_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-redcnht.csv', 'cnhtred', 
					  :wgcnadir || 'wgcna_modules_cnht_red_network_2k.csv');		
select wgcna_analysis('wgcna_modules_cnht_black', 
					  :wgcnadir || 'CytoscapeInput-edges-blackcnht.csv',
					  'wgcna_modules_node_attr_cnht_black', 
					  :wgcnadir || 'wgcna_modules_cnht_black_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cnht_black_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-blackcnht.csv', 'cnhtblac', 
					  :wgcnadir || 'wgcna_modules_cnht_black_network_2k.csv');		
select wgcna_analysis('wgcna_modules_cnht_pink', 
					  :wgcnadir || 'CytoscapeInput-edges-pinkcnht.csv',
					  'wgcna_modules_node_attr_cnht_pink', 
					  :wgcnadir || 'wgcna_modules_cnht_pink_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cnht_pink_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-pinkcnht.csv', 'cnhtpink', 
					  :wgcnadir || 'wgcna_modules_cnht_pink_network_2k.csv');				

--all cold vs hot samples with atleast log2fc >= |1| IN ATLEAST ONe Cdht				  
select wgcna_analysis('wgcna_modules_cdht_allmod', 
					  :wgcnadir || 'CytoscapeInput-edges-turquoise-blue-brown-yellowcdht.csv',
					  'wgcna_modules_node_attr_cdht_allmod', 
					  :wgcnadir || 'wgcna_modules_cdht_allmod_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cdht_allmod_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-turquoise-blue-brown-yellowcdht.csv', 
					  'cdhtall', :wgcnadir || 'wgcna_modules_cdht_allmod_network_2k.csv');							  
select wgcna_analysis('wgcna_modules_cdht_turquoise', 
					  :wgcnadir || 'CytoscapeInput-edges-turquoisecdht.csv',
					  'wgcna_modules_node_attr_cdht_turquoise', 
					  :wgcnadir || 'wgcna_modules_cdht_turquoise_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cdht_turquoise_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-turquoisecdht.csv', 'cdhtturq', 
					  :wgcnadir || 'wgcna_modules_cdht_turquoise_network_2k.csv');	
select wgcna_analysis('wgcna_modules_cdht_blue', 
					  :wgcnadir || 'CytoscapeInput-edges-bluecdht.csv',
					  'wgcna_modules_node_attr_cdht_blue', 
					  :wgcnadir || 'wgcna_modules_cdht_blue_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cdht_blue_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-bluecdht.csv', 'cdhtblue', 
					  :wgcnadir || 'wgcna_modules_cdht_blue_network_2k.csv');	
select wgcna_analysis('wgcna_modules_cdht_brown', 
					  :wgcnadir || 'CytoscapeInput-edges-browncdht.csv',
					  'wgcna_modules_node_attr_cdht_brown', 
					  :wgcnadir || 'wgcna_modules_cdht_brown_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cdht_brown_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-browncdht.csv', 'cdhtbrow', 
					  :wgcnadir || 'wgcna_modules_cdht_brown_network_2k.csv');	
select wgcna_analysis('wgcna_modules_cdht_yellow', 
					  :wgcnadir || 'CytoscapeInput-edges-yellowcdht.csv',
					  'wgcna_modules_node_attr_cdht_yellow', 
					  :wgcnadir || 'wgcna_modules_cdht_yellow_network.csv',
					  :wgcnadir || 'wgcna_modules_node_attr_cdht_yellow_network.csv',
					  :wgcnadir || 'CytoscapeInput-nodes-yellowcdht.csv', 'cdhtyell', 
					  :wgcnadir || 'wgcna_modules_cdht_yellow_network_2k.csv');					  

/*
drop table if exists wgcna_all_modules, temp, wgcna_all_modules_node_attr;
create table wgcna_all_modules (src varchar(20), tar varchar(20), wt double precision, dir 
	varchar(20), srcalt varchar(4), taralt varchar(4));
    
\copy wgcna_all_modules from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/WGCNA/CytoscapeInput-edges-01turquoise-blue-brown.txt';
create table temp (src varchar(20), srcalt varchar(5), color varchar(10));
\copy temp from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/WGCNA/CytoscapeInput-nodes-01turquoise-blue-brown.txt';
alter table wgcna_all_modules add color varchar(20), add srcanot varchar(300), add taranot 
	varchar(300), add srccount int, add nodesize int;
    
update wgcna_all_modules as a set color=b.color from temp as b where a.src=b.src;
update wgcna_all_modules as a set color=b.color from temp as b where a.tar=b.src and a.color is null;
update wgcna_all_modules as a set srcanot=b.anot from deg_pot_tom_trn as b where a.src=b.orthoid;
update wgcna_all_modules as a set taranot=b.anot from deg_pot_tom_trn as b where a.tar=b.orthoid;

drop table if exists temp;
create table temp as select src, count(src) from wgcna_all_modules group by src;
update wgcna_all_modules as a set srccount=b.count from temp as b where a.src=b.src;
update wgcna_all_modules as a set nodesize=20;
\copy wgcna_all_modules to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/WGCNA/wgcna_all_module_network.csv' with (FORMAT csv, header true);

create table wgcna_all_modules_node_attr as select src from wgcna_all_modules union select tar from 
	wgcna_all_modules;
alter table wgcna_all_modules_node_attr add color varchar(20), add nodesize int, add nodeht int, 
	add label varchar(20), add srccount int;    
update wgcna_all_modules_node_attr as a set color=b.color, srccount=b.srccount  from 
	wgcna_all_modules as b where  a.src=b.src;   
update wgcna_all_modules_node_attr as a set color=b.color from wgcna_all_modules as b where a.src=
	b.tar and a.color is  null;
   
update wgcna_all_modules_node_attr as a set nodesize=20;
update wgcna_all_modules_node_attr as a set nodeht=20;
-- HUB genes
update wgcna_all_modules_node_attr as a set nodesize=60, nodeht=60 where srccount>21 and  color='turquoise';
update wgcna_all_modules_node_attr as a set nodesize=60, nodeht=60 where srccount>30 and color='brown';
update wgcna_all_modules_node_attr as a set nodesize=60, nodeht=60 where srccount>15 and color='blue';
update wgcna_all_modules_node_attr set label='APK1B' where src~'ortho_15356'; --Protein kinase APK1B, chloroplast
update wgcna_all_modules_node_attr set label='TTHX' where src~'ortho_6314'; --Trithorax 
update wgcna_all_modules_node_attr set label='UPA19' where src~'ortho_3114'; --UPA19
update wgcna_all_modules_node_attr set label='GTRX' where src~'ortho_697'; --Glutaredoxin
update wgcna_all_modules_node_attr set label='BG1' where src~'ortho_804'; --Beta-glucosidase 01
update wgcna_all_modules_node_attr set label='HD1' where src~'ortho_8690'; --Bell-like homeodomain protein 1
update wgcna_all_modules_node_attr set label='OSMT' where src~'ortho_15108'; --Osmotin
update wgcna_all_modules_node_attr set label='UNKN1' where src~'ortho_2255'; --Conserved gene of unknown function
update wgcna_all_modules_node_attr set label='CAPE' where src~'ortho_3523'; --Cell attachment protein in somatic embryogenesis
update wgcna_all_modules_node_attr set label='UNKN2' where src~'ortho_1600'; --Conserved gene of unknown function
update wgcna_all_modules_node_attr set label='BAP' where src~'ortho_3877'; --OO_Ba0013J05-OO_Ba0033A15.14 protein
update wgcna_all_modules_node_attr set label='H3.2' where src~'ortho_799'; --Histone H3.2-
update wgcna_all_modules_node_attr set label='DBP' where src~'ortho_15217'; --DNA binding protein-
update wgcna_all_modules_node_attr set label='MHC' where src~'ortho_5957'; --Myosin heavy chain, embryonic smooth muscle isoform
update wgcna_all_modules_node_attr set label='H2A' where src~'ortho_12252'; --Histone H2A
update wgcna_all_modules_node_attr set label='NACK2' where src~'ortho_12874'; --Kinesin NACK2
update wgcna_all_modules_node_attr set label='UNKN3' where src~'ortho_1248'; --Conserved gene of unknown function
update wgcna_all_modules_node_attr set label='UNKN4' where src~'ortho_16165'; --Conserved gene of unknown function
update wgcna_all_modules_node_attr set label='NHYD1' where src~'ortho_5470'; --Nudix hydrolase 1
update wgcna_all_modules_node_attr set label='ZFN' where src~'ortho_4255'; --Zinc finger protein

\copy wgcna_all_modules_node_attr to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/WGCNA/wgcna_all_module_network_node_attr.csv' with (FORMAT csv, header true);
*/
/* ^^^^^^^^^^^^^ */

/* ^^^^^^^^^^^^^  
-- variance calcukation for up and down regulated genes in each modules of WGCNA
create or replace function wgcna_update(_wgcna_node_table varchar(20), _wgcna_module_table 
	varchar(20), _module_type varchar(20), _color varchar(10), p int, t int, _p_out_table text, 
	_t_out_table text, _flag int, _p_out_up_table text, _p_out_down_table text, _t_out_up_table 
	text, _t_out_down_table text)
	returns void as
    $body$
	DECLARE
		res double precision;
	begin
		if _flag=1 then
			execute format('alter table %I drop column if exists log2fc, drop column if exists 
				pot_id, drop column if exists tom_id, drop column if exists p_atha_id, drop column 
				if exists t_atha_id  ', _wgcna_node_table);
			execute format('alter table %I add column log2fc text, add pot_id varchar(20), add tom_id 
				varchar(20), add p_atha_id varchar(20), add t_atha_id varchar(20) ', _wgcna_node_table);		
			execute format('update %I as a set log2fc=b.log2fc from %I as b where a.src=b.src', 
				_wgcna_node_table, _wgcna_module_table);	
			execute format('update %I as a set pot_id=b.qid, tom_id=b.sid, p_atha_id=b.pot_atha_id,  
				t_atha_id=b.tom_atha_id  from deg_pot_tom_trn as b where a.src=b.orthoid', 
				_wgcna_node_table);
		end if;
		execute format('select variance(((regexp_split_to_array(log2fc, '';''))[%s]::double 
				precision)) from %I where color=''%I'' and 
				(((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1 or 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1)', p, 
				_wgcna_node_table, _color, p, p) into res;
		RAISE NOTICE 'Treatment variance pot %, color %, %', _module_type, _color, res;	
		execute format('select sum(((regexp_split_to_array(log2fc, '';''))[%s]::double 
				precision)) from %I where color=''%I'' and 
				(((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1 or 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1)', p, 
				_wgcna_node_table, _color, p, p) into res;	
		RAISE NOTICE 'Treatment sum pot %, color %, %', _module_type, _color, res;
		execute format('select count(*) from %I where color=''%I'' and
			((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1', _wgcna_node_table, 
			_color, p) into res;
		RAISE NOTICE 'Treatment up pot %, color %, %', _module_type, _color, res;	
		execute format('select count(*) from %I where color=''%I'' and
			((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1', _wgcna_node_table, 
			_color, p) into res;
		RAISE NOTICE 'Treatment down pot %, color %, %', _module_type, _color, res;	
		execute format('copy (select pot_id, p_atha_id from %I where color=''%I'' and 
				(((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1 or 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1)) to $$' || 
				_p_out_table || '$$' || 'with (FORMAT csv, header true)', _wgcna_node_table, _color,
				p, p);
		execute format('copy (select pot_id, p_atha_id from %I where color=''%I'' and 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1) to $$' || 
				_p_out_up_table || '$$' || 'with (FORMAT csv, header true)', _wgcna_node_table, 
				_color, p);	
		execute format('copy (select pot_id, p_atha_id from %I where color=''%I'' and 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1) to $$' || 
				_p_out_down_table || '$$' || 'with (FORMAT csv, header true)', _wgcna_node_table, 
				_color, p);			
			
		execute format('select variance(((regexp_split_to_array(log2fc, '';''))[%s]::double 
				precision)) from %I where color=''%I'' and 
				(((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1 or 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1)', t, 
				_wgcna_node_table, _color, t, t) into res;		
		RAISE NOTICE 'Treatment variance tom %, color %, %', _module_type, _color, res;	
		execute format('select sum(((regexp_split_to_array(log2fc, '';''))[%s]::double 
				precision)) from %I where color=''%I'' and 
				(((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1 or 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1)', t, 
				_wgcna_node_table, _color, t, t) into res;		
		RAISE NOTICE 'Treatment sum tom %, color %, %', _module_type, _color, res;	
		execute format('select count(*) from %I where color=''%I'' and
			((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1', _wgcna_node_table, 
			_color, t) into res;
		RAISE NOTICE 'Treatment up tom %, color %, %', _module_type, _color, res;	
		execute format('select count(*) from %I where color=''%I'' and
			((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1', _wgcna_node_table, 
			_color, t) into res;
		RAISE NOTICE 'Treatment down tom %, color %, %', _module_type, _color, res;
		execute format('copy (select tom_id, t_atha_id from %I where color=''%I'' and 
				(((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1 or 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1)) to $$' || 
				_t_out_table || '$$' || 'with (FORMAT csv, header true)', _wgcna_node_table, _color,
				t, t);
		execute format('copy (select tom_id, t_atha_id from %I where color=''%I'' and 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)>=1) to $$' || 
				_t_out_up_table || '$$' || 'with (FORMAT csv, header true)', _wgcna_node_table, 
				_color, t);	
		execute format('copy (select tom_id, t_atha_id from %I where color=''%I'' and 
				((regexp_split_to_array(log2fc, '';''))[%s]::double precision)<=-1) to $$' || 
				_t_out_down_table || '$$' || 'with (FORMAT csv, header true)', _wgcna_node_table, 
				_color, t);				
					
	end
	$body$ language "plpgsql";

\set wgcnadir $$'/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/WGCNA/'$$
	
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd7', 
	'turquoise', 1, 2, :wgcnadir || 'wgcna_pot_deg_cncd7_turq.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd7_turq.csv', 1, :wgcnadir || 'wgcna_pot_deg_cncd7_turq_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd7_turq_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd7_turq_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd7_turq_down.csv');
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd7', 
	'blue', 1, 2, :wgcnadir || 'wgcna_pot_deg_cncd7_blue.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd7_blue.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd7_blue_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd7_blue_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd7_blue_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd7_blue_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd7', 
	'brown', 1, 2, :wgcnadir || 'wgcna_pot_deg_cncd7_brow.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd7_brow.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd7_brow_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd7_brow_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd7_brow_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd7_brow_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd7', 
	'yellow', 1, 2, :wgcnadir || 'wgcna_pot_deg_cncd7_yell.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd7_yell.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd7_yell_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd7_yell_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd7_yell_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd7_yell_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd7', 
	'green', 1, 2, :wgcnadir || 'wgcna_pot_deg_cncd7_gree.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd7_gree.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd7_gree_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd7_gree_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd7_gree_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd7_gree_down.csv');		
	
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd14', 
	'turquoise', 7, 8, :wgcnadir || 'wgcna_pot_deg_cncd14_turq.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd14_turq.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd14_turq_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd14_turq_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd14_turq_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd14_turq_down.csv');
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd14', 
	'blue', 7, 8, :wgcnadir || 'wgcna_pot_deg_cncd14_blue.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd14_blue.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd14_blue_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd14_blue_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd14_blue_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd14_blue_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd14', 
	'brown', 7, 8, :wgcnadir || 'wgcna_pot_deg_cncd14_brow.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd14_brow.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd14_brow_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd14_brow_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd14_brow_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd14_brow_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd14', 
	'yellow', 7, 8, :wgcnadir || 'wgcna_pot_deg_cncd14_yell.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd14_yell.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd14_yell_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd14_yell_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd14_yell_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd14_yell_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd14', 
	'green', 7, 8, :wgcnadir || 'wgcna_pot_deg_cncd14_gree.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd14_gree.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd14_gree_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd14_gree_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd14_gree_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd14_gree_down.csv');			
	
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd21', 
	'turquoise', 13, 14, :wgcnadir || 'wgcna_pot_deg_cncd21_turq.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd21_turq.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd21_turq_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd21_turq_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd21_turq_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd21_turq_down.csv');
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd21', 
	'blue', 13, 14, :wgcnadir || 'wgcna_pot_deg_cncd21_blue.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd21_blue.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd21_blue_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd21_blue_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd21_blue_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd21_blue_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd21', 
	'brown', 13, 14, :wgcnadir || 'wgcna_pot_deg_cncd21_brow.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd21_brow.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd21_brow_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd21_brow_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd21_brow_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd21_brow_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd21', 
	'yellow', 13, 14, :wgcnadir || 'wgcna_pot_deg_cncd21_yell.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd21_yell.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd21_yell_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd21_yell_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd21_yell_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd21_yell_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cncd_allmod', 'wgcna_modules_cncd_allmod', 'cncd21', 
	'green', 13, 14, :wgcnadir || 'wgcna_pot_deg_cncd21_gree.csv', :wgcnadir || 
	'wgcna_tom_deg_cncd21_gree.csv', 0, :wgcnadir || 'wgcna_pot_deg_cncd21_gree_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cncd21_gree_down.csv', :wgcnadir || 'wgcna_tom_deg_cncd21_gree_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cncd21_gree_down.csv');	

select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht7', 
	'turquoise', 3, 4, :wgcnadir || 'wgcna_pot_deg_cnht7_turq.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht7_turq.csv', 1, :wgcnadir || 'wgcna_pot_deg_cnht7_turq_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cnht7_turq_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht7_turq_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cnht7_turq_down.csv');
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht7', 
	'blue', 3, 4, :wgcnadir || 'wgcna_pot_deg_cnht7_blue.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht7_blue.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht7_blue_up.csv', :wgcnadir || 
	'wgcna_pot_deg_blue_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht7_blue_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_blue_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht7', 
	'brown', 3, 4, :wgcnadir || 'wgcna_pot_deg_cnht7_brow.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht7_brow.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht7_brow_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cnht7_brow_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht7_brow_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cnht7_brow_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht7', 
	'yellow', 3, 4, :wgcnadir || 'wgcna_pot_deg_cnht7_yell.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht7_yell.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht7_yell_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cnht7_yell_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht7_yell_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cnht7_yell_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht7', 
	'green', 3, 4, :wgcnadir || 'wgcna_pot_deg_cnht7_gree.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht7_gree.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht7_gree_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cnht7_gree_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht7_gree_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cnht7_gree_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht7', 
	'red', 3, 4, :wgcnadir || 'wgcna_pot_deg_cnht7_red.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht7_red.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht7_red_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cnht7_red_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht7_red_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cnht7_red_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht7', 
	'black', 3, 4, :wgcnadir || 'wgcna_pot_deg_cnht7_blac.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht7_blac.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht7_blac_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cnht7_blac_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht7_blac_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cnht7_blac_down.csv');
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht7', 
	'pink', 3, 4, :wgcnadir || 'wgcna_pot_deg_cnht7_pink.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht7_pink.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht7_pink_up.csv', :wgcnadir || 
	'wgcna_pot_deg_cnht7_pink_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht7_pink_up.csv', :wgcnadir 
	|| 'wgcna_tom_deg_cnht7_pink_down.csv');		
	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht14', 
	'turquoise', 9, 10, :wgcnadir || 'wgcna_pot_deg_cnht14_turq.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht14_turq.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht14_turq_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht14_turq_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht14_turq_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht14_turq_down.csv');
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht14', 
	'blue', 9, 10, :wgcnadir || 'wgcna_pot_deg_cnht14_blue.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht14_blue.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht14_blue_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht14_blue_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht14_blue_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht14_blue_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht14', 
	'brown', 9, 10, :wgcnadir || 'wgcna_pot_deg_cnht14_brow.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht14_brow.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht14_brow_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht14_brow_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht14_brow_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht14_brow_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht14', 
	'yellow', 9, 10, :wgcnadir || 'wgcna_pot_deg_cnht14_yell.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht14_yell.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht14_yell_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht14_yell_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht14_yell_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht14_yell_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht14', 
	'green', 9, 10, :wgcnadir || 'wgcna_pot_deg_cnht14_gree.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht14_gree.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht14_gree_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht14_gree_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht14_gree_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht14_gree_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht14', 
	'red', 9, 10, :wgcnadir || 'wgcna_pot_deg_cnht14_red.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht14_red.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht14_red_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht14_red_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht14_red_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht14_red_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht14', 
	'black', 9, 10, :wgcnadir || 'wgcna_pot_deg_cnht14_blac.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht14_blac.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht14_blac_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht14_blac_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht14_blac_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht14_blac_down.csv');
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht14', 
	'pink', 9, 10, :wgcnadir || 'wgcna_pot_deg_cnht14_pink.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht14_pink.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht14_pink_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht14_pink_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht14_pink_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht14_pink_down.csv');			
	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht21', 
	'turquoise', 15, 16, :wgcnadir || 'wgcna_pot_deg_cnht21_turq.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht21_turq.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht21_turq_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht21_turq_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht21_turq_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht21_turq_down.csv');
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht21', 
	'blue', 15, 16, :wgcnadir || 'wgcna_pot_deg_cnht21_blue.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht21_blue.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht21_blue_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht21_blue_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht21_blue_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht21_blue_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht21', 
	'brown', 15, 16, :wgcnadir || 'wgcna_pot_deg_cnht21_brow.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht21_brow.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht21_brow_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht21_brow_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht21_brow_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht21_brow_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht21', 
	'yellow', 15, 16, :wgcnadir || 'wgcna_pot_deg_cnht21_yell.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht21_yell.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht21_yell_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht21_yell_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht21_yell_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht21_yell_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht21', 
	'green', 15, 16, :wgcnadir || 'wgcna_pot_deg_cnht21_gree.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht21_gree.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht21_gree_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht21_gree_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht21_gree_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht21_gree_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht21', 
	'red', 15, 16, :wgcnadir || 'wgcna_pot_deg_cnht21_red.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht21_red.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht21_red_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht21_red_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht21_red_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht21_red_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht21', 
	'black', 15, 16, :wgcnadir || 'wgcna_pot_deg_cnht21_blac.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht21_blac.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht21_blac_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht21_blac_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht21_blac_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht21_blac_down.csv');
select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht21', 
	'pink', 15, 16, :wgcnadir || 'wgcna_pot_deg_cnht21_pink.csv', :wgcnadir || 
	'wgcna_tom_deg_cnht21_pink.csv', 0, :wgcnadir || 'wgcna_pot_deg_cnht21_pink_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cnht21_pink_down.csv', :wgcnadir || 'wgcna_tom_deg_cnht21_pink_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cnht21_pink_down.csv');	

select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht7', 
	'turquoise', 5, 6, :wgcnadir || 'wgcna_pot_deg_cdht7_turq.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht7_turq.csv', 1, :wgcnadir || 'wgcna_pot_deg_cdht7_turq_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht7_turq_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht7_turq_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht7_turq_down.csv');
select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht7', 
	'blue', 5, 6, :wgcnadir || 'wgcna_pot_deg_cdht7_blue.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht7_blue.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht7_blue_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht7_blue_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht7_blue_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht7_blue_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht7', 
	'brown', 5, 6, :wgcnadir || 'wgcna_pot_deg_cdht7_brow.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht7_brow.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht7_brow_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht7_brow_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht7_brow_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht7_brow_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht7', 
	'yellow', 5, 6, :wgcnadir || 'wgcna_pot_deg_cdht7_yell.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht7_yell.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht7_yell_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht7_yell_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht7_yell_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht7_yell_down.csv');	


select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht14', 
	'turquoise', 11, 12, :wgcnadir || 'wgcna_pot_deg_cdht14_turq.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht14_turq.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht14_turq_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht14_turq_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht14_turq_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht14_turq_down.csv');
select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht14', 
	'blue', 11, 12, :wgcnadir || 'wgcna_pot_deg_cdht14_blue.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht14_blue.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht14_blue_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht14_blue_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht14_blue_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht14_blue_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht14', 
	'brown', 11, 12, :wgcnadir || 'wgcna_pot_deg_cdht14_brow.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht14_brow.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht14_brow_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht14_brow_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht14_brow_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht14_brow_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht14', 
	'yellow', 11, 12, :wgcnadir || 'wgcna_pot_deg_cdht14_yell.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht14_yell.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht14_yell_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht14_yell_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht14_yell_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht14_yell_down.csv');	


select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht21', 
	'turquoise', 17, 18, :wgcnadir || 'wgcna_pot_deg_cdht21_turq.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht21_turq.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht21_turq_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht21_turq_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht21_turq_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht21_turq_down.csv');
select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht21', 
	'blue', 17, 18, :wgcnadir || 'wgcna_pot_deg_cdht21_blue.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht21_blue.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht21_blue_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht21_blue_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht21_blue_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht21_blue_down.csv');	
select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht21', 
	'brown', 17, 18, :wgcnadir || 'wgcna_pot_deg_cdht21_brow.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht21_brow.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht21_brow_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht21_brow_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht21_brow_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht21_brow_down.csv');		
select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht21', 
	'yellow', 17, 18, :wgcnadir || 'wgcna_pot_deg_cdht21_yell.csv', :wgcnadir || 
	'wgcna_tom_deg_cdht21_yell.csv', 0, :wgcnadir || 'wgcna_pot_deg_cdht21_yell_up.csv', :wgcnadir 
	|| 'wgcna_pot_deg_cdht21_yell_down.csv', :wgcnadir || 'wgcna_tom_deg_cdht21_yell_up.csv', 
	:wgcnadir || 'wgcna_tom_deg_cdht21_yell_down.csv');	

 ^^^^^^^^^^^^^ */	
--select wgcna_update('wgcna_modules_node_attr_cnht_allmod', 'wgcna_modules_cnht_allmod', 'cnht');
--select wgcna_update('wgcna_modules_node_attr_cdht_allmod', 'wgcna_modules_cdht_allmod', 'cdht');










/* regular code
create table deg_pot_tom_trn as  select qid, sid, orthoid from stub_slync_ortho;
alter table deg_pot_tom_trn add p_cnvscd7 double precision, add t_cnvscd7 double precision,
                            add p_cnvsht7 double precision, add t_cnvsht7 double precision, add p_cdvsht7 double precision,
                            add t_cdvsht7 double precision, add p_cnvscd14 double precision,
                            add t_cnvscd14 double precision, add p_cnvsht14 double precision, add t_cnvsht14 double
                            precision, add p_cdvsht14 double precision, add t_cdvsht14 double precision, add p_cnvscd21
                            double precision, add t_cnvscd21 double precision, add p_cnvsht21 double precision, add
                            t_cnvsht21 double precision, add p_cdvsht21 double precision, add t_cdvsht21 double
                            precision, add anot varchar(300), add categ_7_up varchar(20),
                            add categ_7_down varchar(20), add categ_14_up varchar(20), add categ_14_down varchar(20),
                            add categ_21_up varchar(20), add categ_21_down varchar(20),
                            add p_cnvscd7_cv double precision, add t_cnvscd7_cv double precision,
                            add p_cnvsht7_cv double precision, add t_cnvsht7_cv double precision, add p_cdvsht7_cv double precision,
                            add t_cdvsht7_cv double precision, add p_cnvscd14_cv double precision,
                            add t_cnvscd14_cv double precision, add p_cnvsht14_cv double precision, add t_cnvsht14_cv double
                            precision, add p_cdvsht14_cv double precision, add t_cdvsht14_cv double precision, add p_cnvscd21_cv
                            double precision, add t_cnvscd21_cv double precision, add p_cnvsht21_cv double precision, add
                            t_cnvsht21_cv double precision, add p_cdvsht21_cv double precision, add t_cdvsht21_cv double
                            precision;
update deg_pot_tom_trn as a set p_cnvsht21=b.log2fc from pot_con_hot_21_trn as b where a.qid=b.id and padj<0.05 and
                           (log2fc>=1 or log2fc<=-1);
update deg_pot_tom_trn as a set p_cnvsht21=b.log2fc from pot_con_hot_21_trn as b where a.qid=b.id and (log2fc<1 or log2fc>-1) and a.p_cnvsht21 is null;
update deg_pot_tom_trn as a set p_cnvsht21=0.99 from pot_con_hot_21_trn as b where a.qid=b.id and padj>=0.05 and  log2fc>=1 and a.p_cnvsht21 is null;

*/





/*
update deg_pot_tom as a set p_cnvscd7=b.log2fc from pot_con_cold_7 as b where a.qid=b.gene and b.padj < 0.05;
same for other columns
*/


/* ^^^^^^^^^^^^^ 
--reciprocal blast

DO language plpgsql $$
BEGIN
  RAISE NOTICE 'Running potato/tomato reciprocal blast';
END
$$;
--reciprocal blast to find orthologues
drop table if exists blastn_stub_slyc;
create table blastn_stub_slyc
    (qid varchar(30),
    qlen int,
    sid varchar(30),
    slen int,
    qstart int,
    qend int,
    sstart int,
    send int,
    nident int,
    pident double precision,
    allen int,
    mm int,
    gp int,
    qcov double precision,
    ev double precision,
    bs double precision
    );
\copy blastn_stub_slyc from '/scratch/user/ren_net/project/potato_tomato_psyllid/data_obt/genome/blastn_stub_slyc.txt';

drop table if exists blastn_slyc_stub;
create table blastn_slyc_stub
    (qid varchar(30),
    qlen int,
    sid varchar(30),
    slen int,
    qstart int,
    qend int,
    sstart int,
    send int,
    nident int,
    pident double precision,
    allen int,
    mm int,
    gp int,
    qcov double precision,
    ev double precision,
    bs double precision
    );
\copy blastn_slyc_stub from '/scratch/user/ren_net/project/potato_tomato_psyllid/data_obt/genome/blastn_slyc_stub.txt';

--get unique record with high scoring matches
--delete duplicate ids based on the score
delete
from blastn_slyc_stub a
where exists (
    select * from blastn_slyc_stub b
    where a.qid=b.qid
    and a.bs > b.bs);

delete
from blastn_stub_slyc a
where exists (
    select * from blastn_stub_slyc b
    where a.qid=b.qid
    and a.bs > b.bs);

--delete duplicate ids based on the pident
delete
from blastn_slyc_stub a
where exists (
    select * from blastn_slyc_stub b
    where a.qid=b.qid
    and a.pident > b.pident);

delete
from blastn_stub_slyc a
where exists (
    select * from blastn_stub_slyc b
    where a.qid=b.qid
    and a.pident > b.pident);

--when all records are same
delete
from blastn_slyc_stub a
where exists (
    select * from blastn_slyc_stub b
    where a.qid=b.qid
    and a.sstart < b.sstart);

delete
from blastn_stub_slyc a
where exists (
    select * from blastn_stub_slyc b
    where a.qid=b.qid
    and a.sstart < b.sstart);

delete
from blastn_slyc_stub a
where exists (
    select * from blastn_slyc_stub b
    where a.qid=b.qid
    and a.qstart < b.qstart);

delete
from blastn_stub_slyc a
where exists (
    select * from blastn_stub_slyc b
    where a.qid=b.qid
    and a.qstart < b.qstart);

drop table if exists temp;
create table temp as select distinct qid, sid from blastn_stub_slyc;
alter table temp add sid2 varchar(30), add sid_count int;
update temp as a set sid2=b.sid from blastn_slyc_stub as b where a.sid=b.qid;

drop table if exists temp2;
create table temp2 as select sid, count(sid) from blastn_stub_slyc group by sid;

update temp as a set sid_count=b.count from temp2 as b where a.sid=b.sid;
drop table temp2;
delete from temp where qid!=sid2 and sid_count>1;
drop table if exists stub_slync_ortho;
alter table temp rename to stub_slync_ortho;
delete from stub_slync_ortho where sid2 is null;
ALTER TABLE stub_slync_ortho ADD COLUMN id SERIAL PRIMARY KEY, add orthoid varchar(20);
update stub_slync_ortho set orthoid=('ortho_'||id);

--add genes to stub_slync_ortho
alter table stub_slync_ortho add pot_gene varchar(30), add tom_gene varchar(30);
update stub_slync_ortho as a set pot_gene=b.gene from potato_pgsc3037_gtf as b where a.qid=b.trn;
update stub_slync_ortho as a set tom_gene=b.gene from tomato_25037_gtf as b where a.sid=b.trn;

--get orthologue counts
select count(*) from stub_slync_ortho;
 ^^^^^^^^^^^^^ */


/* ^^^^^^^^^^^^^ 
-- orthologous analysis with Arabidopsis
create or replace function atha_ortho(blast_table varchar(10), blast_table_path text, col 
	varchar(10), plant varchar(5) )
	returns void as
    $body$
	begin
		execute format('drop table if exists %I', blast_table);
		execute format('create table %I (qid varchar(30), qlen int, sid varchar(30), slen int, 
			qstart int, qend int, sstart int, send int, nident int, pident double precision, allen 
			int, mm int, gp int, qcov double precision, ev double precision, bs double precision)', 
			blast_table);
		execute format('copy %I from $$' || blast_table_path || '$$',  blast_table);
		execute format('alter table deg_pot_tom_trn drop column if exists %I', col);		
		execute format('alter table deg_pot_tom_trn add %I varchar(20)', col);
		if plant='pot' then
			execute format('update deg_pot_tom_trn as a set %I=b.sid from %I as b where a.qid=b.qid', 
				col, blast_table);
		elsif plant='tom' then
			execute format('update deg_pot_tom_trn as a set %I=b.sid from %I as b where a.sid=b.qid', 
				col, blast_table);
		end if;	
	end
    $body$ language "plpgsql";

\! echo "orthologous analysis with Arabidopsis"		
\set blastdir $$'/scratch/user/ren_net/project/potato_tomato_psyllid/data_obt/genome/'$$
select atha_ortho('blastx_pot_atha', :blastdir || 'blastx_pota_atha.txt', 'pot_atha_id', 'pot'); 
select atha_ortho('blastx_tom_atha', :blastdir || 'blastx_toma_atha.txt', 'tom_atha_id', 'tom'); 

alter table deg_pot_tom_trn add atha_anot varchar(300);
update deg_pot_tom_trn as a set atha_anot=b.anot2 from ath_gene_fam_1 as b where 
	substring(a.pot_atha_id from 1 for 9)=b.loc;

\copy deg_pot_tom_trn to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn.csv' with (FORMAT csv, header true);
-- out file deg_pot_tom_trn_sign_pot_atha.csv
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvscd7>=1 or p_cnvscd7<=-1 or t_cnvscd7>=1 or t_cnvscd7<=-1 or p_cnvsht7>=1 or p_cnvsht7<=-1 or t_cnvsht7>=1 or t_cnvsht7<=-1 or p_cdvsht7>=1 or p_cdvsht7<=-1 or t_cdvsht7>=1 or t_cdvsht7<=-1 or p_cnvscd14>=1 or p_cnvscd14<=-1 or t_cnvscd14>=1 or t_cnvscd14<=-1 or p_cnvsht14>=1 or p_cnvsht14<=-1 or t_cnvsht14>=1 or t_cnvsht14<=-1 or p_cdvsht14>=1 or p_cdvsht14<=-1 or t_cdvsht14>=1 or t_cdvsht14<=-1 or p_cnvscd21>=1 or p_cnvscd21<=-1 or t_cnvscd21>=1 or t_cnvscd21<=-1 or p_cnvsht21>=1 or p_cnvsht21<=-1 or t_cnvsht21>=1 or t_cnvsht21<=-1 or p_cdvsht21>=1 or p_cdvsht21<=-1 or t_cdvsht21>=1 or t_cdvsht21<=-1) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha.csv' with (FORMAT csv, header true);
\copy (select tom_atha_id from deg_pot_tom_trn where p_cnvscd7>=1 or p_cnvscd7<=-1 or t_cnvscd7>=1 or t_cnvscd7<=-1 or p_cnvsht7>=1 or p_cnvsht7<=-1 or t_cnvsht7>=1 or t_cnvsht7<=-1 or p_cdvsht7>=1 or p_cdvsht7<=-1 or t_cdvsht7>=1 or t_cdvsht7<=-1 or p_cnvscd14>=1 or p_cnvscd14<=-1 or t_cnvscd14>=1 or t_cnvscd14<=-1 or p_cnvsht14>=1 or p_cnvsht14<=-1 or t_cnvsht14>=1 or t_cnvsht14<=-1 or p_cdvsht14>=1 or p_cdvsht14<=-1 or t_cdvsht14>=1 or t_cdvsht14<=-1 or p_cnvscd21>=1 or p_cnvscd21<=-1 or t_cnvscd21>=1 or t_cnvscd21<=-1 or p_cnvsht21>=1 or p_cnvsht21<=-1 or t_cnvsht21>=1 or t_cnvsht21<=-1 or p_cdvsht21>=1 or p_cdvsht21<=-1 or t_cdvsht21>=1 or t_cdvsht21<=-1) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_tom_atha.csv' with (FORMAT csv, header true);

-- out file deg_pot_tom_trn_sign_pot_atha_onlyup.csv
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvscd7>=1 or t_cnvscd7>=1 or p_cnvsht7>=1 or t_cnvsht7>=1 or p_cdvsht7>=1 or t_cdvsht7>=1 or p_cnvscd14>=1 or t_cnvscd14>=1 or p_cnvsht14>=1 or t_cnvsht14>=1 or p_cdvsht14>=1 or t_cdvsht14>=1 or p_cnvscd21>=1 or t_cnvscd21>=1 or p_cnvsht21>=1 or t_cnvsht21>=1 or p_cdvsht21>=1 or t_cdvsht21>=1 ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_onlyup.csv' with (FORMAT csv, header true);
\copy (select tom_atha_id from deg_pot_tom_trn where p_cnvscd7>=1 or t_cnvscd7>=1 or p_cnvsht7>=1 or t_cnvsht7>=1 or p_cdvsht7>=1 or t_cdvsht7>=1 or p_cnvscd14>=1 or t_cnvscd14>=1 or p_cnvsht14>=1 or t_cnvsht14>=1 or p_cdvsht14>=1 or t_cdvsht14>=1 or p_cnvscd21>=1 or t_cnvscd21>=1 or p_cnvsht21>=1 or t_cnvsht21>=1 or p_cdvsht21>=1 or t_cdvsht21>=1 ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_tom_atha_onlyup.csv' with (FORMAT csv, header true);

-- out file deg_pot_tom_trn_sign_pot_atha_onlydown.csv
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvscd7<=-1 or t_cnvscd7<=-1 or p_cnvsht7<=-1 or t_cnvsht7<=-1 or p_cdvsht7<=-1 or t_cdvsht7<=-1 or p_cnvscd14<=-1 or t_cnvscd14<=-1 or p_cnvsht14<=-1 or t_cnvsht14<=-1 or p_cdvsht14<=-1 or t_cdvsht14<=-1 or p_cnvscd21<=-1 or t_cnvscd21<=-1 or p_cnvsht21<=-1 or t_cnvsht21<=-1 or p_cdvsht21<=-1 or t_cdvsht21<=-1 ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_onlydown.csv' with (FORMAT csv, header true);
\copy (select tom_atha_id from deg_pot_tom_trn where p_cnvscd7<=-1 or t_cnvscd7<=-1 or p_cnvsht7<=-1 or t_cnvsht7<=-1 or p_cdvsht7<=-1 or t_cdvsht7<=-1 or p_cnvscd14<=-1 or t_cnvscd14<=-1 or p_cnvsht14<=-1 or t_cnvsht14<=-1 or p_cdvsht14<=-1 or t_cdvsht14<=-1 or p_cnvscd21<=-1 or t_cnvscd21<=-1 or p_cnvsht21<=-1 or t_cnvsht21<=-1 or p_cdvsht21<=-1 or t_cdvsht21<=-1 ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_tom_atha_onlydown.csv' with (FORMAT csv, header true);

--deg only at 7 days
--out file deg_pot_tom_trn_sign_pot_atha_7d.csv
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvscd7>=1 or p_cnvscd7<=-1 or t_cnvscd7>=1 or t_cnvscd7<=-1 or p_cnvsht7>=1 or p_cnvsht7<=-1 or t_cnvsht7>=1 or t_cnvsht7<=-1 or p_cdvsht7>=1 or p_cdvsht7<=-1 or t_cdvsht7>=1 or t_cdvsht7<=-1 ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_7d.csv' with (FORMAT csv, header true);

--deg only at 14 days
--out file deg_pot_tom_trn_sign_pot_atha_14d.csv
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvscd14>=1 or p_cnvscd14<=-1 or t_cnvscd14>=1 or t_cnvscd14<=-1 or p_cnvsht14>=1 or p_cnvsht14<=-1 or t_cnvsht14>=1 or t_cnvsht14<=-1 or p_cdvsht14>=1 or p_cdvsht14<=-1 or t_cdvsht14>=1 or t_cdvsht14<=-1 ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_14d.csv' with (FORMAT csv, header true);

--deg only at 21 days
--out file deg_pot_tom_trn_sign_pot_atha_21d.csv
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvscd21>=1 or p_cnvscd21<=-1 or t_cnvscd21>=1 or t_cnvscd21<=-1 or p_cnvsht21>=1 or p_cnvsht21<=-1 or t_cnvsht21>=1 or t_cnvsht21<=-1 or p_cdvsht21>=1 or p_cdvsht21<=-1 or t_cdvsht21>=1 or t_cdvsht21<=-1) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_21d.csv' with (FORMAT csv, header true);
-- up regulated
--out file deg_pot_tom_trn_sign_pot_atha_21d_up.csv
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvscd21>=1 or t_cnvscd21>=1 or p_cnvsht21>=1 or t_cnvsht21>=1 or p_cdvsht21>=1 or t_cdvsht21>=1 ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_21d_up.csv' with (FORMAT csv, header true);
--down regulated
--out file deg_pot_tom_trn_sign_pot_atha_21d_down.csv
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvscd21<=-1 or t_cnvscd21<=-1 or p_cnvsht21<=-1 or t_cnvsht21<=-1 or p_cdvsht21<=-1 or t_cdvsht21<=-1) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_21d_down.csv' with (FORMAT csv, header true);
 
--deg only at 21 days
--out file deg_pot_tom_trn_sign_pot_atha_21d_cncd.csv
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvscd21>=1 or p_cnvscd21<=-1 or t_cnvscd21>=1 or t_cnvscd21<=-1 ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_21d_cncd.csv' with (FORMAT csv, header true);
 
--out file deg_pot_tom_trn_sign_pot_atha_21d_cnht.csv 
\copy (select pot_atha_id from deg_pot_tom_trn where p_cnvsht21>=1 or p_cnvsht21<=-1 or t_cnvsht21>=1 or t_cnvsht21<=-1 ) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_21d_cnht.csv' with (FORMAT csv, header true);
 
--out file deg_pot_tom_trn_sign_pot_atha_21d_cdht.csv 
\copy (select pot_atha_id from deg_pot_tom_trn where p_cdvsht21>=1 or p_cdvsht21<=-1 or t_cdvsht21>=1 or t_cdvsht21<=-1) to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deg_pot_tom_trn_sign_pot_atha_21d_cdht.csv' with (FORMAT csv, header true);
 
 

 ^^^^^^^^^^^^^ */










































































/*   ###################################################################################################################
                                            IGNOre : Old analysis without functions
     ###################################################################################################################
*/
/*
drop table if exists pot_con_714;
create table pot_con_714
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_con_714 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_714.csv' with csv header delimiter as ',';

/* Control 7h vs Control 21h  */
drop table if exists pot_con_721;
create table pot_con_721
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_con_721 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_721.csv' with csv header delimiter as ',';

/* Control 14h vs Control 21h  */
drop table if exists pot_con_1421;
create table pot_con_1421
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_con_1421 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_1421.csv' with csv header delimiter as ',';

/* Cold 7h vs Cold 14h  */
drop table if exists pot_cold_714;
create table pot_cold_714
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_cold_714 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_714.csv' with csv header delimiter as ',';

/* Cold 7h vs Cold 21h  */
drop table if exists pot_cold_721;
create table pot_cold_721
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_cold_721 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_721.csv' with csv header delimiter as ',';

/* Cold 14h vs Cold 21h  */
drop table if exists pot_cold_1421;
create table pot_cold_1421
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_cold_1421 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_1421.csv' with csv header delimiter as ',';

/* hot 7h vs hot 14h  */
drop table if exists pot_hot_714;
create table pot_hot_714
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_hot_714 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_hot_714.csv' with csv header delimiter as ',';

/* hot 7h vs hot 21h  */
drop table if exists pot_hot_721;
create table pot_hot_721
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_hot_721 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_hot_721.csv' with csv header delimiter as ',';

/* hot 14h vs hot 21h  */
drop table if exists pot_hot_1421;
create table pot_hot_1421
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_hot_1421 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_hot_1421.csv' with csv header delimiter as ',';

/* by treatment */
/* cont vs cold 7h  */
drop table if exists pot_con_cold_7;
create table pot_con_cold_7
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_con_cold_7 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_cold_7.csv' with csv header delimiter as ',';

/* cont vs hot 7h  */
drop table if exists pot_con_hot_7;
create table pot_con_hot_7
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_con_hot_7 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_hot_7.csv' with csv header delimiter as ',';

/* cold vs hot 7h  */
drop table if exists pot_cold_hot_7;
create table pot_cold_hot_7
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_cold_hot_7 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_hot_7.csv' with csv header delimiter as ',';

/* con vs cold 14h  */
drop table if exists pot_con_cold_14;
create table pot_con_cold_14
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_con_cold_14 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_cold_14.csv' with csv header delimiter as ',';

/* con vs hot 14h  */
drop table if exists pot_con_hot_14;
create table pot_con_hot_14
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_con_hot_14 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_hot_14.csv' with csv header delimiter as ',';

/* cold vs hot 14h  */
drop table if exists pot_cold_hot_14;
create table pot_cold_hot_14
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_cold_hot_14 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_hot_14.csv' with csv header delimiter as ',';

/* con vs cold 21h  */
drop table if exists pot_con_cold_21;
create table pot_con_cold_21
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_con_cold_21 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_cold_21.csv' with csv header delimiter as ',';

/* con vs hot 21h  */
drop table if exists pot_con_hot_21;
create table pot_con_hot_21
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_con_hot_21 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_hot_21.csv' with csv header delimiter as ',';

/* cold vs hot 21h  */
drop table if exists pot_cold_hot_21;
create table pot_cold_hot_21
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy pot_cold_hot_21 from 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_hot_21.csv' with csv header delimiter as ',';

/* Add annotation */
alter table pot_con_714 add anot varchar(300);
alter table pot_con_721 add anot varchar(300);
alter table pot_con_1421 add anot varchar(300);
alter table pot_cold_714 add anot varchar(300);
alter table pot_cold_721 add anot varchar(300);
alter table pot_cold_1421 add anot varchar(300);
alter table pot_hot_714 add anot varchar(300);
alter table pot_hot_721 add anot varchar(300);
alter table pot_hot_1421 add anot varchar(300);
alter table pot_con_cold_7 add anot varchar(300);
alter table pot_con_hot_7 add anot varchar(300);
alter table pot_cold_hot_7 add anot varchar(300);
alter table pot_con_cold_14 add anot varchar(300);
alter table pot_con_hot_14 add anot varchar(300);
alter table pot_cold_hot_14 add anot varchar(300);
alter table pot_con_cold_21 add anot varchar(300);
alter table pot_con_hot_21 add anot varchar(300);
alter table pot_cold_hot_21 add anot varchar(300);

update pot_con_714 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_con_721 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_con_1421 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_cold_714 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_cold_721 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_cold_1421 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_hot_714 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_hot_721 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_hot_1421 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_con_cold_7 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_con_hot_7 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_cold_hot_7 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_con_cold_14 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_con_hot_14 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_cold_hot_14 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_con_cold_21 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_con_hot_21 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;
update pot_cold_hot_21 as a set anot=b.anot from potato_pgsc403_gff as b where a.gene=b.trn;

/* cast columns to double precision */
update pot_con_714 set pval=NULL where pval='NA';
update pot_con_714 set padj=NULL where padj='NA';
update pot_con_721 set pval=NULL where pval='NA';
update pot_con_721 set padj=NULL where padj='NA';
update pot_con_1421 set pval=NULL where pval='NA';
update pot_con_1421 set padj=NULL where padj='NA';
update pot_cold_714 set pval=NULL where pval='NA';
update pot_cold_714 set padj=NULL where padj='NA';
update pot_cold_721 set pval=NULL where pval='NA';
update pot_cold_721 set padj=NULL where padj='NA';
update pot_cold_1421 set pval=NULL where pval='NA';
update pot_cold_1421 set padj=NULL where padj='NA';
update pot_hot_714 set pval=NULL where pval='NA';
update pot_hot_714 set padj=NULL where padj='NA';
update pot_hot_721 set pval=NULL where pval='NA';
update pot_hot_721 set padj=NULL where padj='NA';
update pot_hot_1421 set pval=NULL where pval='NA';
update pot_hot_1421 set padj=NULL where padj='NA';
update pot_con_cold_7 set pval=NULL where pval='NA';
update pot_con_cold_7 set padj=NULL where padj='NA';
update pot_con_hot_7 set pval=NULL where pval='NA';
update pot_con_hot_7 set padj=NULL where padj='NA';
update pot_cold_hot_7 set pval=NULL where pval='NA';
update pot_cold_hot_7 set padj=NULL where padj='NA';
update pot_con_cold_14 set pval=NULL where pval='NA';
update pot_con_cold_14 set padj=NULL where padj='NA';
update pot_con_hot_14 set pval=NULL where pval='NA';
update pot_con_hot_14 set padj=NULL where padj='NA';
update pot_cold_hot_14 set pval=NULL where pval='NA';
update pot_cold_hot_14 set padj=NULL where padj='NA';
update pot_con_cold_21 set pval=NULL where pval='NA';
update pot_con_cold_21 set padj=NULL where padj='NA';
update pot_con_hot_21 set pval=NULL where pval='NA';
update pot_con_hot_21 set padj=NULL where padj='NA';
update pot_cold_hot_21 set pval=NULL where pval='NA';
update pot_cold_hot_21 set padj=NULL where padj='NA';
alter table pot_con_714 alter column pval type double precision using (pval::double precision);
alter table pot_con_714 alter column padj type double precision using (padj::double precision);
alter table pot_con_721 alter column pval type double precision using (pval::double precision);
alter table pot_con_721 alter column padj type double precision using (padj::double precision);
alter table pot_con_1421 alter column pval type double precision using (pval::double precision);
alter table pot_con_1421 alter column padj type double precision using (padj::double precision);
alter table pot_cold_714 alter column pval type double precision using (pval::double precision);
alter table pot_cold_714 alter column padj type double precision using (padj::double precision);
alter table pot_cold_721 alter column pval type double precision using (pval::double precision);
alter table pot_cold_721 alter column padj type double precision using (padj::double precision);
alter table pot_cold_1421 alter column pval type double precision using (pval::double precision);
alter table pot_cold_1421 alter column padj type double precision using (padj::double precision);
alter table pot_hot_714 alter column pval type double precision using (pval::double precision);
alter table pot_hot_714 alter column padj type double precision using (padj::double precision);
alter table pot_hot_721 alter column pval type double precision using (pval::double precision);
alter table pot_hot_721 alter column padj type double precision using (padj::double precision);
alter table pot_hot_1421 alter column pval type double precision using (pval::double precision);
alter table pot_hot_1421 alter column padj type double precision using (padj::double precision);
alter table pot_con_cold_7 alter column pval type double precision using (pval::double precision);
alter table pot_con_cold_7 alter column padj type double precision using (padj::double precision);
alter table pot_con_hot_7 alter column pval type double precision using (pval::double precision);
alter table pot_con_hot_7 alter column padj type double precision using (padj::double precision);
alter table pot_cold_hot_7 alter column pval type double precision using (pval::double precision);
alter table pot_cold_hot_7 alter column padj type double precision using (padj::double precision);
alter table pot_con_cold_14 alter column pval type double precision using (pval::double precision);
alter table pot_con_cold_14 alter column padj type double precision using (padj::double precision);
alter table pot_con_hot_14 alter column pval type double precision using (pval::double precision);
alter table pot_con_hot_14 alter column padj type double precision using (padj::double precision);
alter table pot_cold_hot_14 alter column pval type double precision using (pval::double precision);
alter table pot_cold_hot_14 alter column padj type double precision using (padj::double precision);
alter table pot_con_cold_21 alter column pval type double precision using (pval::double precision);
alter table pot_con_cold_21 alter column padj type double precision using (padj::double precision);
alter table pot_con_hot_21 alter column pval type double precision using (pval::double precision);
alter table pot_con_hot_21 alter column padj type double precision using (padj::double precision);
alter table pot_cold_hot_21 alter column pval type double precision using (pval::double precision);
alter table pot_cold_hot_21 alter column padj type double precision using (padj::double precision);

/* export tables of degs */
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_714 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_714_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_714 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_714_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_721 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_721_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_721 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_721_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_1421 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_1421_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_1421 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_1421_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_714 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_714_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_714 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_714_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_721 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_721_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_721 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_721_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_1421 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_1421_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_1421 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_1421_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_hot_714 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_hot_714_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_hot_714 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_hot_714_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_hot_721 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_hot_721_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_hot_721 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_hot_721_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_hot_1421 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_hot_1421_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_hot_1421 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_hot_1421_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_cold_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_cold_7_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_cold_7 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_cold_7_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_hot_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_hot_7_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_hot_7 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_hot_7_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_hot_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_hot_7_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_hot_7 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_hot_7_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_cold_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_cold_14_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_cold_14 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_cold_14_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_hot_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_hot_14_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_hot_14 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_hot_14_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_hot_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_hot_14_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_hot_14 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_hot_14_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_cold_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_cold_21_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_cold_21 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_cold_21_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_hot_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_hot_21_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_con_hot_21 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_con_hot_21_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_hot_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_hot_21_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from pot_cold_hot_21 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to 'R:\project\potato_zebrachip\analysis\ballgown_kkm\read_counts_potato_deprep\deseq2\res_pot_cold_hot_21_down.csv' with (FORMAT csv, header true);

/* This is on db on ada tamu */
/* get normalized counts */

drop table if exists nc_pot_con_cold_7;
create table nc_pot_con_cold_7
    (gene varchar(20),
     pn71 double precision,
     pn72 double precision,
     pn73 double precision,
     pc71 double precision,
     pc72 double precision,
     pc73 double precision
    );
\copy nc_pot_con_cold_7 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_con_cold_7.csv' with csv header delimiter as ',';

drop table if exists nc_pot_con_hot_7;
create table nc_pot_con_hot_7
    (gene varchar(20),
     pn71 double precision,
     pn72 double precision,
     pn73 double precision,
     ph71 double precision,
     ph72 double precision,
     ph73 double precision
    );
\copy nc_pot_con_hot_7 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_con_hot_7.csv' with csv header delimiter as ',';

drop table if exists nc_pot_cold_hot_7;
create table nc_pot_cold_hot_7
    (gene varchar(20),
     pc71 double precision,
     pc72 double precision,
     pc73 double precision,
     ph71 double precision,
     ph72 double precision,
     ph73 double precision
    );
\copy nc_pot_cold_hot_7 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_cold_hot_7.csv' with csv header delimiter as ',';

drop table if exists nc_pot_con_cold_14;
create table nc_pot_con_cold_14
    (gene varchar(20),
     pn141 double precision,
     pn142 double precision,
     pn143 double precision,
     pc141 double precision,
     pc142 double precision,
     pc143 double precision
    );
\copy nc_pot_con_cold_14 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_con_cold_14.csv' with csv header delimiter as ',';

drop table if exists nc_pot_con_hot_14;
create table nc_pot_con_hot_14
    (gene varchar(20),
     pn141 double precision,
     pn142 double precision,
     pn143 double precision,
     ph141 double precision,
     ph142 double precision,
     ph143 double precision
    );
\copy nc_pot_con_hot_14 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_con_hot_14.csv' with csv header delimiter as ',';

drop table if exists nc_pot_cold_hot_14;
create table nc_pot_cold_hot_14
    (gene varchar(20),
     pc141 double precision,
     pc142 double precision,
     pc143 double precision,
     ph141 double precision,
     ph142 double precision,
     ph143 double precision
    );
\copy nc_pot_cold_hot_14 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_cold_hot_14.csv' with csv header delimiter as ',';

drop table if exists nc_pot_con_cold_21;
create table nc_pot_con_cold_21
    (gene varchar(20),
     pn211 double precision,
     pn212 double precision,
     pn213 double precision,
     pc211 double precision,
     pc212 double precision,
     pc213 double precision
    );
\copy nc_pot_con_cold_21 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_con_cold_21.csv' with csv header delimiter as ',';

drop table if exists nc_pot_con_hot_21;
create table nc_pot_con_hot_21
    (gene varchar(20),
     pn211 double precision,
     pn212 double precision,
     pn213 double precision,
     ph211 double precision,
     ph212 double precision,
     ph213 double precision
    );
\copy nc_pot_con_hot_21 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_con_hot_21.csv' with csv header delimiter as ',';

drop table if exists nc_pot_cold_hot_21;
create table nc_pot_cold_hot_21
    (gene varchar(20),
     pc211 double precision,
     pc212 double precision,
     pc213 double precision,
     ph211 double precision,
     ph212 double precision,
     ph213 double precision
    );
\copy nc_pot_cold_hot_21 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_cold_hot_21.csv' with csv header delimiter as ',';

drop table if exists nc_pot_con_714;
create table nc_pot_con_714
    (gene varchar(20),
     pn71 double precision,
     pn72 double precision,
     pn73 double precision,
     pn141 double precision,
     pn142 double precision,
     pn143 double precision
    );
\copy nc_pot_con_714 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_con_714.csv' with csv header delimiter as ',';

drop table if exists nc_pot_con_721;
create table nc_pot_con_721
    (gene varchar(20),
     pn71 double precision,
     pn72 double precision,
     pn73 double precision,
     pn211 double precision,
     pn212 double precision,
     pn213 double precision
    );
\copy nc_pot_con_721 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_con_721.csv' with csv header delimiter as ',';

drop table if exists nc_pot_con_1421;
create table nc_pot_con_1421
    (gene varchar(20),
     pn141 double precision,
     pn142 double precision,
     pn143 double precision,
     pn211 double precision,
     pn212 double precision,
     pn213 double precision
    );
\copy nc_pot_con_1421 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_con_1421.csv' with csv header delimiter as ',';

drop table if exists nc_pot_cold_714;
create table nc_pot_cold_714
    (gene varchar(20),
     pc71 double precision,
     pc72 double precision,
     pc73 double precision,
     pc141 double precision,
     pc142 double precision,
     pc143 double precision
    );
\copy nc_pot_cold_714 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_cold_714.csv' with csv header delimiter as ',';

drop table if exists nc_pot_cold_721;
create table nc_pot_cold_721
    (gene varchar(20),
     pc71 double precision,
     pc72 double precision,
     pc73 double precision,
     pc211 double precision,
     pc212 double precision,
     pc213 double precision
    );
\copy nc_pot_cold_721 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_cold_721.csv' with csv header delimiter as ',';

drop table if exists nc_pot_cold_1421;
create table nc_pot_cold_1421
    (gene varchar(20),
     pc141 double precision,
     pc142 double precision,
     pc143 double precision,
     pc211 double precision,
     pc212 double precision,
     pc213 double precision
    );
\copy nc_pot_cold_1421 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_cold_1421.csv' with csv header delimiter as ',';

drop table if exists nc_pot_hot_714;
create table nc_pot_hot_714
    (gene varchar(20),
     ph71 double precision,
     ph72 double precision,
     ph73 double precision,
     ph141 double precision,
     ph142 double precision,
     ph143 double precision
    );
\copy nc_pot_hot_714 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_hot_714.csv' with csv header delimiter as ',';

drop table if exists nc_pot_hot_721;
create table nc_pot_hot_721
    (gene varchar(20),
     ph71 double precision,
     ph72 double precision,
     ph73 double precision,
     ph211 double precision,
     ph212 double precision,
     ph213 double precision
    );
\copy nc_pot_hot_721 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_hot_721.csv' with csv header delimiter as ',';

drop table if exists nc_pot_hot_1421;
create table nc_pot_hot_1421
    (gene varchar(20),
     ph141 double precision,
     ph142 double precision,
     ph143 double precision,
     ph211 double precision,
     ph212 double precision,
     ph213 double precision
    );
\copy nc_pot_hot_1421 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_pot_hot_1421.csv' with csv header delimiter as ',';


/* add normalized counts to table */
alter table pot_con_714 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_con_721 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_con_1421 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_cold_714 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_cold_721 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_cold_1421 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_hot_714 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_hot_721 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_hot_1421 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_con_cold_7 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_con_hot_7 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_cold_hot_7 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_con_cold_14 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_con_hot_14 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_cold_hot_14 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_con_cold_21 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_con_hot_21 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table pot_cold_hot_21 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;

update pot_con_714 as a set c1=b.pn71, c2=b.pn72, c3=b.pn73, t1=b.pn141, t2=b.pn142, t3=b.pn143 from nc_pot_con_714 as b where a.gene=b.gene;
update pot_con_721 as a set c1=b.pn71, c2=b.pn72, c3=b.pn73, t1=b.pn211, t2=b.pn212, t3=b.pn213 from nc_pot_con_721 as b where a.gene=b.gene;
update pot_con_1421 as a set c1=b.pn141, c2=b.pn142, c3=b.pn143, t1=b.pn211, t2=b.pn212, t3=b.pn213 from nc_pot_con_1421 as b where a.gene=b.gene;
update pot_cold_714 as a set c1=b.pc71, c2=b.pc72, c3=b.pc73, t1=b.pc141, t2=b.pc142, t3=b.pc143 from nc_pot_cold_714 as b where a.gene=b.gene;
update pot_cold_721 as a set c1=b.pc71, c2=b.pc72, c3=b.pc73, t1=b.pc211, t2=b.pc212, t3=b.pc213 from nc_pot_cold_721 as b where a.gene=b.gene;
update pot_cold_1421 as a set c1=b.pc141, c2=b.pc142, c3=b.pc143, t1=b.pc211, t2=b.pc212, t3=b.pc213 from nc_pot_cold_1421 as b where a.gene=b.gene;
update pot_hot_714 as a set c1=b.ph71, c2=b.ph72, c3=b.ph73, t1=b.ph141, t2=b.ph142, t3=b.ph143 from nc_pot_hot_714 as b where a.gene=b.gene;
update pot_hot_721 as a set c1=b.ph71, c2=b.ph72, c3=b.ph73, t1=b.ph211, t2=b.ph212, t3=b.ph213 from nc_pot_hot_721 as b where a.gene=b.gene;
update pot_hot_1421 as a set c1=b.ph141, c2=b.ph142, c3=b.ph143, t1=b.ph211, t2=b.ph212, t3=b.ph213 from nc_pot_hot_1421 as b where a.gene=b.gene;
update pot_con_cold_7 as a set c1=b.pn71, c2=b.pn72, c3=b.pn73, t1=b.pc71, t2=b.pc72, t3=b.pc73 from nc_pot_con_cold_7 as b where a.gene=b.gene;
update pot_con_cold_14 as a set c1=b.pn141, c2=b.pn142, c3=b.pn143, t1=b.pc141, t2=b.pc142, t3=b.pc143 from nc_pot_con_cold_14 as b where a.gene=b.gene;
update pot_con_cold_21 as a set c1=b.pn211, c2=b.pn212, c3=b.pn213, t1=b.pc211, t2=b.pc212, t3=b.pc213 from nc_pot_con_cold_21 as b where a.gene=b.gene;
update pot_con_hot_7 as a set c1=b.pn71, c2=b.pn72, c3=b.pn73, t1=b.ph71, t2=b.ph72, t3=b.ph73 from nc_pot_con_hot_7 as b where a.gene=b.gene;
update pot_con_hot_14 as a set c1=b.pn141, c2=b.pn142, c3=b.pn143, t1=b.ph141, t2=b.ph142, t3=b.ph143 from nc_pot_con_hot_14 as b where a.gene=b.gene;
update pot_con_hot_21 as a set c1=b.pn211, c2=b.pn212, c3=b.pn213, t1=b.ph211, t2=b.ph212, t3=b.ph213 from nc_pot_con_hot_21 as b where a.gene=b.gene;
*/



/*
drop table if exists hm_pot_known;
create table hm_pot_known as
select gene from pot_con_cold_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from pot_con_hot_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from pot_cold_hot_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from pot_con_cold_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from pot_con_hot_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from pot_cold_hot_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from pot_con_cold_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from pot_con_hot_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from pot_cold_hot_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%';

drop table if exists hm_tom;
create table hm_tom_known as
select gene from tom_con_cold_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from tom_con_hot_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from tom_cold_hot_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from tom_con_cold_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from tom_con_hot_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from tom_cold_hot_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from tom_con_cold_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from tom_con_hot_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%'
union
select gene from tom_cold_hot_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%';
/* for only known end */
*/
/*
alter table hm_pot add cnvscd7 double precision, add cnvsht7 double precision, add cdvsht7 double precision,
    add cnvscd14 double precision, add cnvsht14 double precision, add cdvsht14 double precision,
    add cnvscd21 double precision, add cnvsht21 double precision, add cdvsht21 double precision,
    add anot varchar(300);

alter table hm_tom add cnvscd7 double precision, add cnvsht7 double precision, add cdvsht7 double precision,
    add cnvscd14 double precision, add cnvsht14 double precision, add cdvsht14 double precision,
    add cnvscd21 double precision, add cnvsht21 double precision, add cdvsht21 double precision,
    add anot varchar(300);
*/

/* END */

