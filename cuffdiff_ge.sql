/*Gene expression analysis in psql using output data from cuffdiff pipeline*/

/* create table */
/*CREATE TABLE wtcr_s2oecr (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/
/*CREATE TABLE wts3r_s2oe3r (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/
/*CREATE TABLE if not exists wts7r_s2oe7r (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/
/*CREATE TABLE if not exists wtcr_o2oecr (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/
/*CREATE TABLE if not exists wts3r_o2oe3r (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/
/*CREATE TABLE if not exists wts7r_o2oe7r (test_id varchar(12), gene_id varchar(12), gene varchar(95), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) )*/
/*CREATE TABLE if not exists s2oecr_o2oecr (test_id varchar(12), gene_id varchar(12), gene varchar(95), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/
/*CREATE TABLE if not exists s2oe3r_o2oe3r (test_id varchar(12), gene_id varchar(12), gene varchar(95), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/
/*CREATE TABLE if not exists s2oe7r_o2oe7r (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/
/*CREATE TABLE if not exists wn_kn (test_id varchar(12), gene_id varchar(12), gene varchar(15000), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/
/*CREATE TABLE if not exists wu_ku (test_id varchar(12), gene_id varchar(12), gene varchar(15000), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );*/

/*Lil project-create table*/
/*
Drop table if exists cd_md; CREATE TABLE cd_md (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );
Drop table if exists cud_cd; CREATE TABLE cud_cd (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );
Drop table if exists cud_md; CREATE TABLE cud_md (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );
Drop table if exists cud_mud; CREATE TABLE cud_mud (test_id varchar(12), gene_id varchar(12), gene varchar(150), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );
Drop table if exists mud_md; CREATE TABLE mud_md (test_id varchar(12), gene_id varchar(12), gene varchar(150), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );
Drop table if exists nmd_md; CREATE TABLE nmd_md (test_id varchar(12), gene_id varchar(12), gene varchar(150), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );
Drop table if exists nmud_md; CREATE TABLE nmud_md (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );
Drop table if exists nmud_mud; CREATE TABLE nmud_mud (test_id varchar(12), gene_id varchar(12), gene varchar(75), locus varchar(24), sample_1 varchar(9), sample_2 varchar(9), status varchar(7), value_1 double precision, value_2 double precision, log2fc varchar(18), test_stat double precision, p_value double precision, q_value double precision,  signi varchar(5) );
*/
Drop table if exists nmd_mud; 
	CREATE TABLE nmd_mud 
		(test_id varchar(12), 
		gene_id varchar(12), 
		gene varchar(100), 
		locus varchar(24), 
		sample_1 varchar(9), 
		sample_2 varchar(9), 
		status varchar(7), 
		value_1 double precision, 
		value_2 double precision, 
		log2fc varchar(18), 
		test_stat double precision, 
		p_value double precision, 
		q_value double precision,  
		signi varchar(5) );
Drop table if exists nmud_nmd; 
	CREATE TABLE nmud_nmd 
		(test_id varchar(12), 
		gene_id varchar(12), 
		gene varchar(100), 
		locus varchar(24), 
		sample_1 varchar(9), 
		sample_2 varchar(9), 
		status varchar(7), 
		value_1 double precision, 
		value_2 double precision, 
		log2fc varchar(18), 
		test_stat double precision, 
		p_value double precision, 
		q_value double precision,  
		signi varchar(5) );
Drop table if exists cd_mud; 
	CREATE TABLE cd_mud 
	(test_id varchar(12), 
	gene_id varchar(12), 
	gene varchar(100), 
	locus varchar(24), 
	sample_1 varchar(9), 
	sample_2 varchar(9), 
	status varchar(7), 
	value_1 double precision, 
	value_2 double precision, 
	log2fc varchar(18), 
	test_stat double precision, 
	p_value double precision, 
	q_value double precision,  
	signi varchar(5) );


/* copy table from file*/
/*First  convert the tab file into csv file*/
/*\copy wtcr_s2oecr from '/home/renesh/RBedre/Desktop/LSU_Project/New_RNAseq/Original_data/Project_DefaultProject/pipeline_out_wtcr_s2oecr/gene_diff_out/cuffdiff_out/gene_exp.diff.csv' with (FORMAT csv, header true) ;*/
/*\copy wts3r_s2oe3r from '/home/renesh/RBedre/Desktop/LSU_Project/New_RNAseq/Original_data/Project_DefaultProject/pipeline_out_wts3r_s2oe3r/gene_diff_out/cuffdiff_out/gene_exp.diff.csv' with (FORMAT csv, header true) ;*/
/*\copy wts7r_s2oe7r from '/home/renesh/RBedre/Desktop/LSU_Project/New_RNAseq/Original_data/Project_DefaultProject/pipeline_out_wts7r_s2oe7r/gene_diff_out/cuffdiff_out/gene_exp.diff.csv' with (FORMAT csv, header true) ;*/
/*\copy wtcr_o2oecr from '/home/renesh/RBedre/Desktop/LSU_Project/New_RNAseq/Original_data/Project_DefaultProject/pipeline_out_wtcr_o2oecr/gene_diff_out/cuffdiff_out/gene_exp.diff.csv' with (FORMAT csv, header true) ;*/
/*\copy wts3r_o2oe3r from '/home/renesh/RBedre/Desktop/LSU_Project/New_RNAseq/Original_data/Project_DefaultProject/pipeline_out_wts3r_o2oe3r/gene_diff_out/cuffdiff_out/gene_exp.diff.csv' with (FORMAT csv, header true) ;*/
/*\copy wts7r_o2oe7r from '/home/renesh/RBedre/Desktop/LSU_Project/New_RNAseq/Original_data/Project_DefaultProject/pipeline_out_wts7r_o2oe7r/gene_diff_out/cuffdiff_out/gene_exp.diff.csv' with (FORMAT csv, header true) ;*/
/*\copy s2oecr_o2oecr from '/home/renesh/RBedre/Desktop/LSU_Project/New_RNAseq/Original_data/Project_DefaultProject/pipeline_out_wts7r_o2oe7r/gene_diff_out/cuffdiff_out/gene_exp.diff.csv' with (FORMAT csv, header true) ;*/
/*\copy s2oe3r_o2oe3r from '/home/renesh/RBedre/Desktop/LSU_Project/New_RNAseq/Original_data/Project_DefaultProject/pipeline_out_s2oe3r_o2oe3r/gene_diff_out/cuffdiff_out/gene_exp.diff.csv' with (FORMAT csv, header true) ;*/
/*\copy s2oe7r_o2oe7r from '/home/renesh/RBedre/Desktop/LSU_Project/New_RNAseq/Original_data/Project_DefaultProject/pipeline_out_s2oe7r_o2oe7r/gene_diff_out/cuffdiff_out/gene_exp.diff.csv' with (FORMAT csv, header true) ;*/
/*\copy wn_kn from '/Users/renesh/Renesh_Docs/IMP_Documents/Business/agupta/wn_kn_cuffdiff.csv' with (FORMAT csv, header true) ;*/
/*\copy wu_ku from '/Users/renesh/Renesh_Docs/IMP_Documents/Business/agupta/wu_ku_cuffdiff.csv' with (FORMAT csv, header true) ;*/

/*Lil project-copy table from file*/
/*
\copy cd_md from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/cd_md/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
\copy cud_cd from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/cud_cd/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
\copy cud_md from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/cud_md/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
\copy cud_mud from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/cud_mud/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
\copy mud_md from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/mud_md/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
\copy nmd_md from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/nmd_md/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
\copy nmud_md from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/nmud_md/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
\copy nmud_mud from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/nmud_mud/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
*/
\copy nmd_mud from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/nmd_mud/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
\copy nmud_nmd from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/nmud_nmd/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;
\copy cd_mud from '/Users/renesh/Renesh_Docs/IMP_Documents/bus/lil/gen_exp/cd_mud/cuffdiff_out/gene_exp.csv' with (FORMAT csv, header true) ;


/*change the inf to Infinity*/
/*This is becuase float values accepts Infinity in psql*/
/*UPDATE wtcr_s2oecr SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE wts3r_s2oe3r SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE wts7r_s2oe7r SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE wtcr_o2oecr SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE wts3r_o2oe3r SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE wts7r_o2oe7r SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE s2oecr_o2oecr SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE s2oe3r_o2oe3r SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE s2oe7r_o2oe7r SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE wn_kn SET log2fc = 'Infinity' where log2fc='inf';*/
/*UPDATE wu_ku SET log2fc = 'Infinity' where log2fc='inf';*/

/*Lil project-change the inf to Infinity*/
/*
UPDATE cd_md SET log2fc = 'Infinity' where log2fc='inf';
UPDATE cud_cd SET log2fc = 'Infinity' where log2fc='inf';
UPDATE cud_md SET log2fc = 'Infinity' where log2fc='inf';
UPDATE cud_mud SET log2fc = 'Infinity' where log2fc='inf';
UPDATE mud_md SET log2fc = 'Infinity' where log2fc='inf';
UPDATE nmd_md SET log2fc = 'Infinity' where log2fc='inf';
UPDATE nmud_md SET log2fc = 'Infinity' where log2fc='inf';
UPDATE nmud_mud SET log2fc = 'Infinity' where log2fc='inf';
*/
UPDATE nmd_mud 
	SET log2fc = 'Infinity' 
	where log2fc='inf';
UPDATE nmud_nmd 
	SET log2fc = 'Infinity' 
	where log2fc='inf';
UPDATE cd_mud 
	SET log2fc = 'Infinity' 
	where log2fc='inf';


/*change the -inf to -Infinity*/
/*This is becuase float values accepts Infinity in psql*/
/*UPDATE wtcr_s2oecr SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE wts3r_s2oe3r SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE wts7r_s2oe7r SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE wtcr_o2oecr SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE wts3r_o2oe3r SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE wts7r_o2oe7r SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE s2oecr_o2oecr SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE s2oe3r_o2oe3r SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE s2oe7r_o2oe7r SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE wn_kn SET log2fc = '-Infinity' where log2fc='-inf';*/
/*UPDATE wu_ku SET log2fc = '-Infinity' where log2fc='-inf';*/

/*Lil project-change the -inf to -Infinity*/
/*
UPDATE cd_md SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE cud_cd SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE cud_md SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE cud_mud SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE mud_md SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE nmd_md SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE nmud_md SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE nmud_mud SET log2fc = '-Infinity' where log2fc='-inf';
*/
UPDATE nmd_mud SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE nmud_nmd SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE cd_mud SET log2fc = '-Infinity' where log2fc='-inf';

/*convert log2fc from varchar to double*/
/*ALTER TABLE wtcr_s2oecr ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE wts3r_s2oe3r ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE wts7r_s2oe7r ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE wtcr_o2oecr ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE wts3r_o2oe3r ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE wts7r_o2oe7r ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE s2oecr_o2oecr ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE s2oe3r_o2oe3r ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE s2oe7r_o2oe7r ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE wn_kn ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/
/*ALTER TABLE wu_ku ALTER COLUMN log2fc type double precision using (log2fc::double precision);*/

/*Lil project-convert log2fc from varchar to double*/
/*
ALTER TABLE cd_md ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE cud_cd ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE cud_md ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE cud_mud ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE mud_md ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE nmd_md ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE nmud_md ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE nmud_mud ALTER COLUMN log2fc type double precision using (log2fc::double precision);
*/
ALTER TABLE nmd_mud ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE nmud_nmd ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE cd_mud ALTER COLUMN log2fc type double precision using (log2fc::double precision);

/*Add annotation column to genes*/
/*ALTER TABLE wtcr_s2oecr ADD anot varchar(500);*/
/*ALTER TABLE wts3r_s2oe3r ADD anot varchar(500);*/
/*ALTER TABLE wts7r_s2oe7r ADD anot varchar(500);*/
/*ALTER TABLE wtcr_o2oecr ADD anot varchar(500);*/
/*ALTER TABLE wts3r_o2oe3r ADD anot varchar(500);*/
/*ALTER TABLE wts7r_o2oe7r ADD anot varchar(500);*/
/*ALTER TABLE s2oecr_o2oecr ADD anot varchar(500);*/
/*ALTER TABLE s2oe3r_o2oe3r ADD anot varchar(500);*/
/*ALTER TABLE s2oe7r_o2oe7r ADD anot varchar(500);*/

/*Lil project-Add annotation column to genes*/
/*
ALTER TABLE cd_md ADD anot varchar(500);
ALTER TABLE cud_cd ADD anot varchar(500);
ALTER TABLE cud_md ADD anot varchar(500);
ALTER TABLE cud_mud ADD anot varchar(500);
ALTER TABLE mud_md ADD anot varchar(500);
ALTER TABLE nmd_md ADD anot varchar(500);
ALTER TABLE nmud_md ADD anot varchar(500);
ALTER TABLE nmud_mud ADD anot varchar(500);
*/
ALTER TABLE nmd_mud ADD anot varchar(500);
ALTER TABLE nmud_nmd ADD anot varchar(500);
ALTER TABLE cd_mud ADD anot varchar(500);

/*Lil projec-add ncbi ids*/
/*
ALTER TABLE cd_md ADD ncbi_gene_id int;
ALTER TABLE cud_cd ADD ncbi_gene_id int;
ALTER TABLE cud_md ADD ncbi_gene_id int;
ALTER TABLE cud_mud ADD ncbi_gene_id int;
ALTER TABLE mud_md ADD ncbi_gene_id int;
ALTER TABLE nmd_md ADD ncbi_gene_id int;
ALTER TABLE nmud_md ADD ncbi_gene_id int;
ALTER TABLE nmud_mud ADD ncbi_gene_id int;
*/
ALTER TABLE nmd_mud ADD ncbi_gene_id int;
ALTER TABLE nmud_nmd ADD ncbi_gene_id int;
ALTER TABLE cd_mud ADD ncbi_gene_id int;

/*join annotation from rice_msu_7 database*/
/*first copy rice_msu_7 table to respective db from plat_db db*/
/*UPDATE wtcr_s2oecr as a set anot=b.anot from rice_msu_7 as b where a.gene=b.locus;*/
/*UPDATE wts3r_s2oe3r as a set anot=b.anot from rice_msu_7 as b where a.gene=b.locus;*/
/*UPDATE wts7r_s2oe7r as a set anot=b.anot from rice_msu_7 as b where a.gene=b.locus;*/
/*UPDATE wtcr_o2oecr as a set anot=b.anot from rice_msu_7 as b where a.gene=b.locus;*/
/*UPDATE wts3r_o2oe3r as a set anot=b.anot from rice_msu_7 as b where a.gene=b.locus;*/
/*UPDATE wts7r_o2oe7r as a set anot=b.anot from rice_msu_7 as b where a.gene=b.locus;*/
/*UPDATE s2oecr_o2oecr as a set anot=b.anot from rice_msu_7 as b where a.gene=b.locus;*/
/*UPDATE s2oe3r_o2oe3r as a set anot=b.anot from rice_msu_7 as b where a.gene=b.locus;*/
/*UPDATE s2oe7r_o2oe7r as a set anot=b.anot from rice_msu_7 as b where a.gene=b.locus;*/

/*Lil project-join annotation from rice_msu_7 database*/
/*
UPDATE cd_md as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
UPDATE cud_cd as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
UPDATE cud_md as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
UPDATE cud_mud as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
UPDATE mud_md as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
UPDATE nmd_md as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
UPDATE nmud_md as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
UPDATE nmud_mud as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
*/
UPDATE nmd_mud as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
UPDATE nmud_nmd as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;
UPDATE cd_mud as a set anot=b.anot, ncbi_gene_id=b.ncbi_gene_id from rice_msu_7 as b where a.gene=b.locus;

/*=============================================END===============================================================*/
