/*import tables for monocot and format as per genfam */

drop table if exists rice_phyt12;
create table rice_phyt12 (phyt_id varchar(40), loc varchar(50), trn varchar(40), prot_name varchar(40), pfam varchar(180), panther varchar(50), kog varchar(160), kegg_ec varchar(80), ko varchar(60), go_id varchar(400), at_hit varchar(30), at_symb varchar(80), at_des varchar(300));