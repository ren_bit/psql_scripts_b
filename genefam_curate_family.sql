/*curate gene family for redundancy */

/* remove redundant loc tag for each family */
drop table if exists ath_gene_fam_1;
create table ath_gene_fam_1 as select distinct gene_fam, loc_tag from ath_gene_fam;
update ath_gene_fam_1 set gene_fam='Heat Shock Transcription Factor Family HSF' where gene_fam='Heat Shock Transcription Factor Family';
update ath_gene_fam_1 set gene_fam='` bZIP transcription factor' where gene_fam='`';
update ath_gene_fam_1 set gene_fam='zinc finger-homeobox gene family ZF-HD' where gene_fam='zinc finger-homeobox gene family';
update ath_gene_fam_1 set gene_fam='Calcium Dependent Protein Kinase CDPK' where gene_fam='Calcium Dependent Protein Kinase';
update ath_gene_fam_1 set loc_tag=upper(loc_tag);


/* remove redundancy of gene family */
update ath_gene_fam_1 set gene_fam='MYB transcription factor superfamily' where gene_fam like '%MYB%';
update ath_gene_fam_1 set gene_fam='WRKY transcription factor superfamily' where gene_fam like '%WRKY%';
update ath_gene_fam_1 set gene_fam='bHLH transcription factor superfamily' where gene_fam like '%bHLH%';
update ath_gene_fam_1 set gene_fam='ABC transporters superfamily' where gene_fam like '%ABC%';
update ath_gene_fam_1 set gene_fam='14-3-3 proteins superfamily' where gene_fam like '%14-3-3%';
update ath_gene_fam_1 set gene_fam='Antiporters superfamily' where gene_fam like '%Antiporter%';
update ath_gene_fam_1 set gene_fam='Dof superfamily' where gene_fam like '%Dof%';
update ath_gene_fam_1 set gene_fam='GRAS superfamily' where gene_fam like '%GRAS%';
update ath_gene_fam_1 set gene_fam='MADS transcription factor superfamily' where gene_fam like '%MADS%';
update ath_gene_fam_1 set gene_fam='Phospholipase D' where gene_fam like '%Phospholipase%';
update ath_gene_fam_1 set gene_fam='ATPases gene family' where gene_fam like '%ATPases%';
update ath_gene_fam_1 set gene_fam='bZIP transcription factor superfamily' where gene_fam like '%bZIP%';
update ath_gene_fam_1 set gene_fam='HSF transcription factor superfamily' where gene_fam like '%HSF%';
update ath_gene_fam_1 set gene_fam='ZF-HD transcription factor superfamily' where gene_fam like '%ZF-HD%';
update ath_gene_fam_1 set gene_fam='Calcium Dependent Protein Kinase' where gene_fam like '%CDPK%';


drop table if exists ath_gene_fam_group_1;
create table ath_gene_fam_group_1 as select gene_fam, array_agg(loc_tag) from ath_gene_fam_1 group by gene_fam;
alter table ath_gene_fam_group_1 add count int;

/*
/* concat array_agg */
CREATE AGGREGATE array_cat_agg(anyarray) (
  SFUNC=array_cat,
  STYPE=anyarray
);
*/

/* Uniqe element from array */
create or replace function public.array_unique(arr anyarray)
returns anyarray as $body$
    select array( select distinct unnest($1) )
$body$ language 'sql';

/*
drop table if exists temp;
create table temp as select gene_fam, array_cat_agg(array_agg) from ath_gene_fam_group_1 group by gene_fam;
*/

update ath_gene_fam_group_1 set array_agg=array_unique(array_agg);
update ath_gene_fam_group_1 set count=array_length(array_agg, 1);

drop table if exists rice_gene_fam;
create table rice_gene_fam (phyt_id varchar(40), loc varchar(50), trn varchar(40), prot_name varchar(40), pfam varchar(180), panther varchar(50), kog varchar(160), kegg_ec varchar(80), ko varchar(60), go_id varchar(400), at_hit varchar(30), at_symb varchar(80), at_des varchar(300));
\copy rice_gene_fam from '/Users/renesh/Renesh_Docs/Research/tamu/db/Osativa/v7.0/annotation/Osativa_204_v7.0.annotation_info.txt';
alter table rice_gene_fam add at_loc varchar(30), add gene_fam varchar(100);
update rice_gene_fam set at_loc = (regexp_split_to_array(at_hit, '\.'))[1];
update rice_gene_fam as a set gene_fam = b.gene_fam from ath_gene_fam_1 as b where upper(a.at_loc) = upper(b.loc_tag);
drop table if exists rice_gene_fam_1;
create table rice_gene_fam_1 as select distinct gene_fam, loc from rice_gene_fam;
update rice_gene_fam_1 set loc=upper(loc);

drop table if exists rice_gene_fam_group_1;
create table rice_gene_fam_group_1 as select gene_fam, array_agg(loc) from rice_gene_fam_1 group by gene_fam;
alter table rice_gene_fam_group_1 add count int;

update rice_gene_fam_group_1 set array_agg=array_unique(array_agg);
update rice_gene_fam_group_1 set count=array_length(array_agg, 1);