/*
Author: Renesh Bedre
Lab: Dr Mandadi Lab, Texas A&M AgriLife Research
project_id: sugarcane_as
desc: create a table for common (shared) events among four conditions for venn diagarm
*/

/* create a table based on union to get unqiue records */
drop table if exists as_events_all_merge;
create table as_events_all_merge as select chr, st, ende, as_type from dai200_c_astalavista
union
select chr, st, ende, as_type from dai200_i_astalavista
union
select chr, st, ende, as_type from dai5_c_astalavista
union
select chr, st, ende, as_type from dai5_i_astalavista;

alter table as_events_all_merge 
add dai5c int, 
add dai5i int, 
add dai200c int,
add dai200i int,
add as_name varchar(40);

update as_events_all_merge as a set
dai5c = '1' 
from dai5_c_astalavista as b
where a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type;
update as_events_all_merge as a set
dai5i = '1' 
from dai5_i_astalavista as b
where a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type;
update as_events_all_merge as a set
dai200c = '1' 
from dai200_c_astalavista as b
where a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type;
update as_events_all_merge as a set
dai200i = '1' 
from dai200_i_astalavista as b
where a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type;

update as_events_all_merge as a set
as_name = b.as_name 
from dai5_c_astalavista as b
where a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type;
update as_events_all_merge as a set
as_name = b.as_name 
from dai5_i_astalavista as b
where a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name is null;
update as_events_all_merge as a set
as_name = b.as_name 
from dai200_c_astalavista as b
where a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name is null;
update as_events_all_merge as a set
as_name = b.as_name 
from dai200_i_astalavista as b
where a.chr=b.chr and a.st=b.st and a.ende=b.ende and a.as_type=b.as_type and a.as_name is null;

/* for venn */
alter table as_events_all_merge 
add dai200_g varchar(10), 
add dai5_g varchar(10),
add both_g varchar(10);

update as_events_all_merge set both_g='fc' where 
dai5c='1' and dai5i is null and dai200c is null and dai200i is null; /* A */
update as_events_all_merge set both_g='fi' where 
dai5c is null and dai5i='1' and dai200c is null and dai200i is null; /* B */
update as_events_all_merge set both_g='tc' where 
dai5c is null and dai5i is null and dai200c='1' and dai200i is null; /* C */
update as_events_all_merge set both_g='ti' where 
dai5c is null and dai5i is null and dai200c is null and dai200i='1'; /* D */
update as_events_all_merge set both_g='fc-fi' where 
dai5c='1' and dai5i='1' and dai200c is null and dai200i is null; /* AB */
update as_events_all_merge set both_g='fc-tc' where 
dai5c='1' and dai5i is null and dai200c='1' and dai200i is null; /* AC */
update as_events_all_merge set both_g='fc-ti' where 
dai5c='1' and dai5i is null and dai200c is null and dai200i='1'; /* AD */
update as_events_all_merge set both_g='fi-tc' where 
dai5c is null and dai5i='1' and dai200c='1' and dai200i is null; /* BC */
update as_events_all_merge set both_g='fi-ti' where 
dai5c is null and dai5i='1' and dai200c is null and dai200i='1'; /* BD */
update as_events_all_merge set both_g='tc-ti' where 
dai5c is null and dai5i is null and dai200c='1' and dai200i='1'; /* CD */
update as_events_all_merge set both_g='fc-fi-tc' where 
dai5c='1' and dai5i='1' and dai200c='1' and dai200i is null; /* ABC */
update as_events_all_merge set both_g='fc-fi-ti' where 
dai5c='1' and dai5i='1' and dai200c is null and dai200i='1'; /* ABD */
update as_events_all_merge set both_g='fc-tc-ti' where 
dai5c='1' and dai5i is null and dai200c='1' and dai200i='1'; /* ACD */
update as_events_all_merge set both_g='fi-tc-ti' where 
dai5c is null and dai5i='1' and dai200c='1' and dai200i='1'; /* BCD */
update as_events_all_merge set both_g='all' where 
dai5c='1' and dai5i='1' and dai200c='1' and dai200i='1'; /* ABCD */

update as_events_all_merge set dai5_g='all' where 
dai5c='1' and dai5i='1'; /* AB */
update as_events_all_merge set dai5_g='fc' where 
dai5c='1' and dai5i is null; /* A */
update as_events_all_merge set dai5_g='fi' where 
dai5c is null and dai5i='1'; /* B */

update as_events_all_merge set dai200_g='all' where 
dai200c='1' and dai200i='1'; /* AB */
update as_events_all_merge set dai200_g='tc' where 
dai200c='1' and dai200i is null; /* A */
update as_events_all_merge set dai200_g='ti' where 
dai200c is null and dai200i='1'; /* B */

/* END */