/* Tomato zebrachip RNA-seq project */
/* run as */
/* psql -f potato_psyllid_kkm.sql potato_tomato_zebrachip */

/* change trn ids as per old SL 2.5 format */
alter table tomato_itag3_anot add if not exists trn_old varchar(20), add if not exists trn2 varchar(20);
update tomato_itag3_anot set trn_old=NULL;
update tomato_itag3_anot set trn_old=regexp_replace(trn, '\.3', '.2');
update tomato_itag3_anot set trn_old=regexp_replace(trn, '\.2', '.1') where trn_old is null;
update tomato_itag3_anot set trn2=substring(trn from 1 for 14);

/* Time series differential expression deseq2 */
/* Control 7h vs Control 14h  */
drop table if exists tom_con_714;
create table tom_con_714
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_con_714 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_714.csv' with csv header delimiter as ',';

/* Control 7h vs Control 21h  */
drop table if exists tom_con_721;
create table tom_con_721
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_con_721 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_721.csv' with csv header delimiter as ',';

/* Control 14h vs Control 21h  */
drop table if exists tom_con_1421;
create table tom_con_1421
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_con_1421 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_1421.csv' with csv header delimiter as ',';

/* Cold 7h vs Cold 14h  */
drop table if exists tom_cold_714;
create table tom_cold_714
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_cold_714 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_714.csv' with csv header delimiter as ',';

/* Cold 7h vs Cold 21h  */
drop table if exists tom_cold_721;
create table tom_cold_721
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_cold_721 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_721.csv' with csv header delimiter as ',';

/* Cold 14h vs Cold 21h  */
drop table if exists tom_cold_1421;
create table tom_cold_1421
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_cold_1421 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_1421.csv' with csv header delimiter as ',';

/* hot 7h vs hot 14h  */
drop table if exists tom_hot_714;
create table tom_hot_714
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_hot_714 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_hot_714.csv' with csv header delimiter as ',';

/* hot 7h vs hot 21h  */
drop table if exists tom_hot_721;
create table tom_hot_721
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_hot_721 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_hot_721.csv' with csv header delimiter as ',';

/* hot 14h vs hot 21h  */
drop table if exists tom_hot_1421;
create table tom_hot_1421
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_hot_1421 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_hot_1421.csv' with csv header delimiter as ',';

/* cont vs cold 7h  */
drop table if exists tom_con_cold_7;
create table tom_con_cold_7
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_con_cold_7 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_cold_7.csv' with csv header delimiter as ',';

/* cont vs hot 7h  */
drop table if exists tom_con_hot_7;
create table tom_con_hot_7
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_con_hot_7 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_hot_7.csv' with csv header delimiter as ',';

/* cold vs hot 7h  */
drop table if exists tom_cold_hot_7;
create table tom_cold_hot_7
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_cold_hot_7 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_hot_7.csv' with csv header delimiter as ',';

/* con vs cold 14h  */
drop table if exists tom_con_cold_14;
create table tom_con_cold_14
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_con_cold_14 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_cold_14.csv' with csv header delimiter as ',';

/* con vs hot 14h  */
drop table if exists tom_con_hot_14;
create table tom_con_hot_14
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_con_hot_14 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_hot_14.csv' with csv header delimiter as ',';

/* cold vs hot 14h  */
drop table if exists tom_cold_hot_14;
create table tom_cold_hot_14
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_cold_hot_14 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_hot_14.csv' with csv header delimiter as ',';

/* con vs cold 21h  */
drop table if exists tom_con_cold_21;
create table tom_con_cold_21
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_con_cold_21 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_cold_21.csv' with csv header delimiter as ',';

/* con vs hot 21h  */
drop table if exists tom_con_hot_21;
create table tom_con_hot_21
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_con_hot_21 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_hot_21.csv' with csv header delimiter as ',';

/* cold vs hot 21h  */
drop table if exists tom_cold_hot_21;
create table tom_cold_hot_21
		(gene text,
		basemean double precision,
		log2fc double precision,
		lfcSE double precision,
        stat double precision,
		pval text,
		padj text
		);
\copy tom_cold_hot_21 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_hot_21.csv' with csv header delimiter as ',';

/* Add annotation */
alter table tom_con_714 add anot varchar(300);
alter table tom_con_721 add anot varchar(300);
alter table tom_con_1421 add anot varchar(300);
alter table tom_cold_714 add anot varchar(300);
alter table tom_cold_721 add anot varchar(300);
alter table tom_cold_1421 add anot varchar(300);
alter table tom_hot_714 add anot varchar(300);
alter table tom_hot_721 add anot varchar(300);
alter table tom_hot_1421 add anot varchar(300);
alter table tom_con_cold_7 add anot varchar(300);
alter table tom_con_hot_7 add anot varchar(300);
alter table tom_cold_hot_7 add anot varchar(300);
alter table tom_con_cold_14 add anot varchar(300);
alter table tom_con_hot_14 add anot varchar(300);
alter table tom_cold_hot_14 add anot varchar(300);
alter table tom_con_cold_21 add anot varchar(300);
alter table tom_con_hot_21 add anot varchar(300);
alter table tom_cold_hot_21 add anot varchar(300);

update tom_con_714 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_con_721 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_con_1421 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_cold_714 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_cold_721 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_cold_1421 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_hot_714 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_hot_721 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_hot_1421 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_con_cold_7 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_con_hot_7 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_cold_hot_7 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_con_cold_14 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_con_hot_14 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_cold_hot_14 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_con_cold_21 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_con_hot_21 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;
update tom_cold_hot_21 as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.trn_old;

update tom_con_714 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_con_721 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_con_1421 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_cold_714 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_cold_721 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_cold_1421 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_hot_714 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_hot_721 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_hot_1421 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_con_cold_7 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_con_hot_7 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_cold_hot_7 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_con_cold_14 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_con_hot_14 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_cold_hot_14 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_con_cold_21 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_con_hot_21 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;
update tom_cold_hot_21 as a set anot=b.anot from tomato_itag3_anot as b where substring(a.gene from 1 for 14)=b.trn2 and  a.anot is null;

/* cast columns to double precision */
update tom_con_714 set pval=NULL where pval='NA';
update tom_con_714 set padj=NULL where padj='NA';
update tom_con_721 set pval=NULL where pval='NA';
update tom_con_721 set padj=NULL where padj='NA';
update tom_con_1421 set pval=NULL where pval='NA';
update tom_con_1421 set padj=NULL where padj='NA';
update tom_cold_714 set pval=NULL where pval='NA';
update tom_cold_714 set padj=NULL where padj='NA';
update tom_cold_721 set pval=NULL where pval='NA';
update tom_cold_721 set padj=NULL where padj='NA';
update tom_cold_1421 set pval=NULL where pval='NA';
update tom_cold_1421 set padj=NULL where padj='NA';
update tom_hot_714 set pval=NULL where pval='NA';
update tom_hot_714 set padj=NULL where padj='NA';
update tom_hot_721 set pval=NULL where pval='NA';
update tom_hot_721 set padj=NULL where padj='NA';
update tom_hot_1421 set pval=NULL where pval='NA';
update tom_hot_1421 set padj=NULL where padj='NA';
update tom_con_cold_7 set pval=NULL where pval='NA';
update tom_con_cold_7 set padj=NULL where padj='NA';
update tom_con_hot_7 set pval=NULL where pval='NA';
update tom_con_hot_7 set padj=NULL where padj='NA';
update tom_cold_hot_7 set pval=NULL where pval='NA';
update tom_cold_hot_7 set padj=NULL where padj='NA';
update tom_con_cold_14 set pval=NULL where pval='NA';
update tom_con_cold_14 set padj=NULL where padj='NA';
update tom_con_hot_14 set pval=NULL where pval='NA';
update tom_con_hot_14 set padj=NULL where padj='NA';
update tom_cold_hot_14 set pval=NULL where pval='NA';
update tom_cold_hot_14 set padj=NULL where padj='NA';
update tom_con_cold_21 set pval=NULL where pval='NA';
update tom_con_cold_21 set padj=NULL where padj='NA';
update tom_con_hot_21 set pval=NULL where pval='NA';
update tom_con_hot_21 set padj=NULL where padj='NA';
update tom_cold_hot_21 set pval=NULL where pval='NA';
update tom_cold_hot_21 set padj=NULL where padj='NA';
alter table tom_con_714 alter column pval type double precision using (pval::double precision);
alter table tom_con_714 alter column padj type double precision using (padj::double precision);
alter table tom_con_721 alter column pval type double precision using (pval::double precision);
alter table tom_con_721 alter column padj type double precision using (padj::double precision);
alter table tom_con_1421 alter column pval type double precision using (pval::double precision);
alter table tom_con_1421 alter column padj type double precision using (padj::double precision);
alter table tom_cold_714 alter column pval type double precision using (pval::double precision);
alter table tom_cold_714 alter column padj type double precision using (padj::double precision);
alter table tom_cold_721 alter column pval type double precision using (pval::double precision);
alter table tom_cold_721 alter column padj type double precision using (padj::double precision);
alter table tom_cold_1421 alter column pval type double precision using (pval::double precision);
alter table tom_cold_1421 alter column padj type double precision using (padj::double precision);
alter table tom_hot_714 alter column pval type double precision using (pval::double precision);
alter table tom_hot_714 alter column padj type double precision using (padj::double precision);
alter table tom_hot_721 alter column pval type double precision using (pval::double precision);
alter table tom_hot_721 alter column padj type double precision using (padj::double precision);
alter table tom_hot_1421 alter column pval type double precision using (pval::double precision);
alter table tom_hot_1421 alter column padj type double precision using (padj::double precision);
alter table tom_con_cold_7 alter column pval type double precision using (pval::double precision);
alter table tom_con_cold_7 alter column padj type double precision using (padj::double precision);
alter table tom_con_hot_7 alter column pval type double precision using (pval::double precision);
alter table tom_con_hot_7 alter column padj type double precision using (padj::double precision);
alter table tom_cold_hot_7 alter column pval type double precision using (pval::double precision);
alter table tom_cold_hot_7 alter column padj type double precision using (padj::double precision);
alter table tom_con_cold_14 alter column pval type double precision using (pval::double precision);
alter table tom_con_cold_14 alter column padj type double precision using (padj::double precision);
alter table tom_con_hot_14 alter column pval type double precision using (pval::double precision);
alter table tom_con_hot_14 alter column padj type double precision using (padj::double precision);
alter table tom_cold_hot_14 alter column pval type double precision using (pval::double precision);
alter table tom_cold_hot_14 alter column padj type double precision using (padj::double precision);
alter table tom_con_cold_21 alter column pval type double precision using (pval::double precision);
alter table tom_con_cold_21 alter column padj type double precision using (padj::double precision);
alter table tom_con_hot_21 alter column pval type double precision using (pval::double precision);
alter table tom_con_hot_21 alter column padj type double precision using (padj::double precision);
alter table tom_cold_hot_21 alter column pval type double precision using (pval::double precision);
alter table tom_cold_hot_21 alter column padj type double precision using (padj::double precision);

/* export tables of degs */
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_714 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_714_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_714 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_714_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_721 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_721_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_721 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_721_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_1421 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_1421_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_1421 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_1421_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_714 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_714_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_714 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_714_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_721 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_721_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_721 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_721_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_1421 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_1421_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_1421 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_1421_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_hot_714 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_hot_714_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_hot_714 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_hot_714_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_hot_721 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_hot_721_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_hot_721 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_hot_721_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_hot_1421 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_hot_1421_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_hot_1421 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_hot_1421_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_cold_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_cold_7_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_cold_7 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_cold_7_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_hot_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_hot_7_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_hot_7 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_hot_7_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_hot_7 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_hot_7_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_hot_7 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_hot_7_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_cold_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_cold_14_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_cold_14 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_cold_14_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_hot_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_hot_14_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_hot_14 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_hot_14_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_hot_14 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_hot_14_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_hot_14 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_hot_14_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_cold_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_cold_21_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_cold_21 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_cold_21_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_hot_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_hot_21_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_con_hot_21 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_con_hot_21_down.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_hot_21 where padj<0.05 and log2fc>=1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_hot_21_up.csv' with (FORMAT csv, header true);
\copy (select gene, anot, log2fc, pval, padj, basemean, lfcse, stat from tom_cold_hot_21 where padj<0.05 and log2fc<=-1 and gene not like '%MSTRG%') to '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/res_tom_cold_hot_21_down.csv' with (FORMAT csv, header true);



/* This is on db on ada tamu */
/* get normalized counts */

drop table if exists nc_tom_con_cold_7;
create table nc_tom_con_cold_7
    (gene varchar(20),
     pn71 double precision,
     pn72 double precision,
     pn73 double precision,
     pc71 double precision,
     pc72 double precision,
     pc73 double precision
    );
\copy nc_tom_con_cold_7 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_con_cold_7.csv' with csv header delimiter as ',';

drop table if exists nc_tom_con_hot_7;
create table nc_tom_con_hot_7
    (gene varchar(20),
     pn71 double precision,
     pn72 double precision,
     pn73 double precision,
     ph71 double precision,
     ph72 double precision,
     ph73 double precision
    );
\copy nc_tom_con_hot_7 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_con_hot_7.csv' with csv header delimiter as ',';

drop table if exists nc_tom_cold_hot_7;
create table nc_tom_cold_hot_7
    (gene varchar(20),
     pc71 double precision,
     pc72 double precision,
     pc73 double precision,
     ph71 double precision,
     ph72 double precision,
     ph73 double precision
    );
\copy nc_tom_cold_hot_7 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_cold_hot_7.csv' with csv header delimiter as ',';

drop table if exists nc_tom_con_cold_14;
create table nc_tom_con_cold_14
    (gene varchar(20),
     pn141 double precision,
     pn142 double precision,
     pn143 double precision,
     pc141 double precision,
     pc142 double precision,
     pc143 double precision
    );
\copy nc_tom_con_cold_14 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_con_cold_14.csv' with csv header delimiter as ',';

drop table if exists nc_tom_con_hot_14;
create table nc_tom_con_hot_14
    (gene varchar(20),
     pn141 double precision,
     pn142 double precision,
     pn143 double precision,
     ph141 double precision,
     ph142 double precision,
     ph143 double precision
    );
\copy nc_tom_con_hot_14 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_con_hot_14.csv' with csv header delimiter as ',';

drop table if exists nc_tom_cold_hot_14;
create table nc_tom_cold_hot_14
    (gene varchar(20),
     pc141 double precision,
     pc142 double precision,
     pc143 double precision,
     ph141 double precision,
     ph142 double precision,
     ph143 double precision
    );
\copy nc_tom_cold_hot_14 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_cold_hot_14.csv' with csv header delimiter as ',';

drop table if exists nc_tom_con_cold_21;
create table nc_tom_con_cold_21
    (gene varchar(20),
     pn211 double precision,
     pn212 double precision,
     pn213 double precision,
     pc211 double precision,
     pc212 double precision,
     pc213 double precision
    );
\copy nc_tom_con_cold_21 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_con_cold_21.csv' with csv header delimiter as ',';

drop table if exists nc_tom_con_hot_21;
create table nc_tom_con_hot_21
    (gene varchar(20),
     pn211 double precision,
     pn212 double precision,
     pn213 double precision,
     ph211 double precision,
     ph212 double precision,
     ph213 double precision
    );
\copy nc_tom_con_hot_21 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_con_hot_21.csv' with csv header delimiter as ',';

drop table if exists nc_tom_cold_hot_21;
create table nc_tom_cold_hot_21
    (gene varchar(20),
     pc211 double precision,
     pc212 double precision,
     pc213 double precision,
     ph211 double precision,
     ph212 double precision,
     ph213 double precision
    );
\copy nc_tom_cold_hot_21 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_cold_hot_21.csv' with csv header delimiter as ',';

drop table if exists nc_tom_con_714;
create table nc_tom_con_714
    (gene varchar(20),
     pn71 double precision,
     pn72 double precision,
     pn73 double precision,
     pn141 double precision,
     pn142 double precision,
     pn143 double precision
    );
\copy nc_tom_con_714 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_con_714.csv' with csv header delimiter as ',';

drop table if exists nc_tom_con_721;
create table nc_tom_con_721
    (gene varchar(20),
     pn71 double precision,
     pn72 double precision,
     pn73 double precision,
     pn211 double precision,
     pn212 double precision,
     pn213 double precision
    );
\copy nc_tom_con_721 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_con_721.csv' with csv header delimiter as ',';

drop table if exists nc_tom_con_1421;
create table nc_tom_con_1421
    (gene varchar(20),
     pn141 double precision,
     pn142 double precision,
     pn143 double precision,
     pn211 double precision,
     pn212 double precision,
     pn213 double precision
    );
\copy nc_tom_con_1421 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_con_1421.csv' with csv header delimiter as ',';

drop table if exists nc_tom_cold_714;
create table nc_tom_cold_714
    (gene varchar(20),
     pc71 double precision,
     pc72 double precision,
     pc73 double precision,
     pc141 double precision,
     pc142 double precision,
     pc143 double precision
    );
\copy nc_tom_cold_714 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_cold_714.csv' with csv header delimiter as ',';

drop table if exists nc_tom_cold_721;
create table nc_tom_cold_721
    (gene varchar(20),
     pc71 double precision,
     pc72 double precision,
     pc73 double precision,
     pc211 double precision,
     pc212 double precision,
     pc213 double precision
    );
\copy nc_tom_cold_721 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_cold_721.csv' with csv header delimiter as ',';

drop table if exists nc_tom_cold_1421;
create table nc_tom_cold_1421
    (gene varchar(20),
     pc141 double precision,
     pc142 double precision,
     pc143 double precision,
     pc211 double precision,
     pc212 double precision,
     pc213 double precision
    );
\copy nc_tom_cold_1421 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_cold_1421.csv' with csv header delimiter as ',';

drop table if exists nc_tom_hot_714;
create table nc_tom_hot_714
    (gene varchar(20),
     ph71 double precision,
     ph72 double precision,
     ph73 double precision,
     ph141 double precision,
     ph142 double precision,
     ph143 double precision
    );
\copy nc_tom_hot_714 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_hot_714.csv' with csv header delimiter as ',';

drop table if exists nc_tom_hot_721;
create table nc_tom_hot_721
    (gene varchar(20),
     ph71 double precision,
     ph72 double precision,
     ph73 double precision,
     ph211 double precision,
     ph212 double precision,
     ph213 double precision
    );
\copy nc_tom_hot_721 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_hot_721.csv' with csv header delimiter as ',';

drop table if exists nc_tom_hot_1421;
create table nc_tom_hot_1421
    (gene varchar(20),
     ph141 double precision,
     ph142 double precision,
     ph143 double precision,
     ph211 double precision,
     ph212 double precision,
     ph213 double precision
    );
\copy nc_tom_hot_1421 from '/scratch/user/ren_net/project/potato_tomato_psyllid/analysis/ballgown_kkm/read_counts_potato_deprep/deseq2/nc_tom_hot_1421.csv' with csv header delimiter as ',';


/* add normalized counts to table */
alter table tom_con_714 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_con_721 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_con_1421 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_cold_714 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_cold_721 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_cold_1421 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_hot_714 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_hot_721 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_hot_1421 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_con_cold_7 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_con_hot_7 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_cold_hot_7 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_con_cold_14 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_con_hot_14 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_cold_hot_14 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_con_cold_21 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_con_hot_21 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;
alter table tom_cold_hot_21 add c1 double precision, add c2 double precision, add c3 double precision, add t1 double precision, add t2 double precision, add t3 double precision;

update tom_con_714 as a set c1=b.pn71, c2=b.pn72, c3=b.pn73, t1=b.pn141, t2=b.pn142, t3=b.pn143 from nc_tom_con_714 as b where a.gene=b.gene;
update tom_con_721 as a set c1=b.pn71, c2=b.pn72, c3=b.pn73, t1=b.pn211, t2=b.pn212, t3=b.pn213 from nc_tom_con_721 as b where a.gene=b.gene;
update tom_con_1421 as a set c1=b.pn141, c2=b.pn142, c3=b.pn143, t1=b.pn211, t2=b.pn212, t3=b.pn213 from nc_tom_con_1421 as b where a.gene=b.gene;
update tom_cold_714 as a set c1=b.pc71, c2=b.pc72, c3=b.pc73, t1=b.pc141, t2=b.pc142, t3=b.pc143 from nc_tom_cold_714 as b where a.gene=b.gene;
update tom_cold_721 as a set c1=b.pc71, c2=b.pc72, c3=b.pc73, t1=b.pc211, t2=b.pc212, t3=b.pc213 from nc_tom_cold_721 as b where a.gene=b.gene;
update tom_cold_1421 as a set c1=b.pc141, c2=b.pc142, c3=b.pc143, t1=b.pc211, t2=b.pc212, t3=b.pc213 from nc_tom_cold_1421 as b where a.gene=b.gene;
update tom_hot_714 as a set c1=b.ph71, c2=b.ph72, c3=b.ph73, t1=b.ph141, t2=b.ph142, t3=b.ph143 from nc_tom_hot_714 as b where a.gene=b.gene;
update tom_hot_721 as a set c1=b.ph71, c2=b.ph72, c3=b.ph73, t1=b.ph211, t2=b.ph212, t3=b.ph213 from nc_tom_hot_721 as b where a.gene=b.gene;
update tom_hot_1421 as a set c1=b.ph141, c2=b.ph142, c3=b.ph143, t1=b.ph211, t2=b.ph212, t3=b.ph213 from nc_tom_hot_1421 as b where a.gene=b.gene;
update tom_con_cold_7 as a set c1=b.pn71, c2=b.pn72, c3=b.pn73, t1=b.pc71, t2=b.pc72, t3=b.pc73 from nc_tom_con_cold_7 as b where a.gene=b.gene;
update tom_con_cold_14 as a set c1=b.pn141, c2=b.pn142, c3=b.pn143, t1=b.pc141, t2=b.pc142, t3=b.pc143 from nc_tom_con_cold_14 as b where a.gene=b.gene;
update tom_con_cold_21 as a set c1=b.pn211, c2=b.pn212, c3=b.pn213, t1=b.pc211, t2=b.pc212, t3=b.pc213 from nc_tom_con_cold_21 as b where a.gene=b.gene;
update tom_con_hot_7 as a set c1=b.pn71, c2=b.pn72, c3=b.pn73, t1=b.ph71, t2=b.ph72, t3=b.ph73 from nc_tom_con_hot_7 as b where a.gene=b.gene;
update tom_con_hot_14 as a set c1=b.pn141, c2=b.pn142, c3=b.pn143, t1=b.ph141, t2=b.ph142, t3=b.ph143 from nc_tom_con_hot_14 as b where a.gene=b.gene;
update tom_con_hot_21 as a set c1=b.pn211, c2=b.pn212, c3=b.pn213, t1=b.ph211, t2=b.ph212, t3=b.ph213 from nc_tom_con_hot_21 as b where a.gene=b.gene;


/* END */
