/* import pfam annotation for family enrichment */

/*
drop table if exists bdist_pfam;
create table bdist_pfam
(
dom varchar(200),
acc varchar(20),
query varchar(20),
acc2 varchar(20),
ev double precision,
scr double precision,
bias double precision,
ev2 double precision,
scr2 double precision,
bias2 double precision,
exp double precision,
reg int,
clu int,
ov int,
env int,
dom2 int,
rep int,
inc int,
des varchar(300)
);

\copy bdist_pfam from '/Users/renesh/Renesh_Docs/Research/tamu/db/pfam_anot/bdist_pfam_tab.csv' DELIMITER ',';
delete from bdist_pfam where ev > 1e-05;
update bdist_pfam set query=regexp_replace(query, '\.p', '');

drop table if exists bdist_pfam_group;
create table bdist_pfam_group as select query, array_agg(dom) from bdist_pfam group by query;
alter table bdist_pfam_group add dom_ct int;
update bdist_pfam_group set dom_ct=array_length(array_agg, 1);

/* pfam_dom_auto are the domains which are joined using update command and not manually did */
/* gene_fam_2 is a duplicate of gene_fam column; created to avoid changes in main gene_fam */
alter table bdist_phyt12  drop if exists pfam_dom_auto, drop if exists gene_fam_2;
alter table bdist_phyt12 add gene_fam_2 varchar(100), add pfam_dom_auto varchar(400);
update bdist_phyt12 set gene_fam_2=gene_fam;
update bdist_phyt12 as a set pfam_dom_auto=b.array_agg from bdist_pfam_group as b where a.trn=b.query;
*/

/* set gene families */
update bdist_phyt12 set gene_fam_2='Glycosyltransferase Gene Families' where pfam_dom_auto like '%Galactosyl_T%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='ACBP60s' where pfam_dom_auto like '%Calmodulin_bind%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='AP2-EREBP Transcription Factor Family' where pfam_dom_auto like '%{AP2}%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='Class III peroxidase' where pfam_dom_auto like '%{peroxidase}%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='AS2/LOB gene family' where pfam_dom_auto like '%{LOB}%' ;
update bdist_phyt12 set gene_fam_2='BBR/BPC Transcription Factor Family' where pfam_dom_auto like '%{GAGA_bind}%' ;
update bdist_phyt12 set gene_fam_2='Magnesium Transporter Gene Family' where pfam_dom_auto like '%{CorA}%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='Plant defensins superfamily' where pfam_dom_auto like '%{Gamma-thionin}%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='SAURs' where pfam_dom_auto like '%{Auxin_inducible}%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='SBP Transcription Factor Family' where pfam_dom_auto like '%{SBP}%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='TCP transcription factor family К' where pfam_dom_auto like '%{TCP}%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='Homeobox Transcription Factor Superfamily' where pfam_dom_auto like '%{Homeobox}%';
update bdist_phyt12 set gene_fam_2='AAAP family' where pfam_dom_auto like '%Aa_trans%';
update bdist_phyt12 set gene_fam_2='C3H Transcription Factor Family' where pfam_dom_auto like '%zf-C3H%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='C2H2 Transcription Factor Family' where pfam_dom_auto like '%zf-C2H2%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='Receptor kinase-like protein family' where pfam_dom_auto like '%LRR%' and at_des like '%receptor-like protein kinase%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='Receptor kinase-like protein family' where pfam_dom_auto like '%LRR%' and at_des like '%repeat protein kinase%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='Receptor kinase-like protein family' where pfam_dom_auto like '%LRR%' and at_des like '%repeat transmembrane protein kinase%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='Receptor kinase-like protein family' where pfam_dom_auto like '%LRR%' and at_des like '%repeat transmembrane protein kinase%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='Leucine-rich repeat extensin' where pfam_dom_auto like '%LRR%' and at_des like '%Leucine-rich repeat family protein%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='Leucine-rich repeat extensin' where pfam_dom_auto like '%LRR%' and at_des like '%Leucine-rich repeat (LRR) family protein%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des like '%F-box family protein%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des like '%F-box and associated interaction domains-containing protein%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des like '%F-box/RNI-like/FBD-like domains-containing protein%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des like '%F-box/RNI-like superfamily protein%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des like '%RNI-like superfamily protein%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto='{F-box}' and at_des='' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto='{F-box,F-box-like}' and at_des='' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto='{F-box-like}' and at_des='' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des like '%F-BOX WITH WD-40 2%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto='{F-box-like,F-box}' and at_des='' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des like '%ARABIDILLO-1%' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%{F-box-like}%' and at_des='phloem protein 2-B2' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%{F-box}%' and at_des='F-box and Leucine Rich Repeat domains containing protein' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%{F-box-like}%' and at_des='F-box associated ubiquitination effector family protein' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%{F-box}%' and at_des='FBD, F-box and Leucine Rich Repeat domains containing protein' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des='phloem protein 2-B1' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des='F-box associated ubiquitination effector family protein' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des='Protein with RNI-like/FBD-like domains' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des='phloem protein 2-B11' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box-like%' and at_des='auxin signaling F-box 2' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des='FBD, F-box, Skp2-like and Leucine Rich Repeat domains containing protein' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box%' and at_des='phloem protein 2-B10' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box-like%' and at_des='EIN3-binding F box protein 1' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box-like%' and at_des='EID1-like 3' and gene_fam_2 is null;
update bdist_phyt12 set gene_fam_2='F-Box Proteins' where pfam_dom_auto like '%F-box-like%' and at_des='F-box protein 7' and gene_fam_2 is null;

update bdist_phyt12 set gene_fam_2='ARF Transcription Factor Family' where pfam_dom_auto like '%Auxin_resp%' and at_des like '%auxin response factor%' and gene_fam_2 is null;


/*
drop table if exists ath_pfam;
create table ath_pfam
(
dom varchar(200),
acc varchar(20),
query varchar(20),
acc2 varchar(20),
ev double precision,
scr double precision,
bias double precision,
ev2 double precision,
scr2 double precision,
bias2 double precision,
exp double precision,
reg int,
clu int,
ov int,
env int,
dom2 int,
rep int,
inc int,
des varchar(300)
);

\copy ath_pfam from '/Users/renesh/Renesh_Docs/Research/tamu/db/pfam_anot/ath_pfam_tab.csv' DELIMITER ',';
delete from ath_pfam where ev > 1e-05;

drop table if exists ath_pfam_group;
create table ath_pfam_group as select query, array_agg(dom) from ath_pfam group by query;
alter table ath_pfam_group add loc varchar(20), add dom_ct int;
update ath_pfam_group set loc=(regexp_split_to_array(query, '\.'))[1] where query like '%.1%';
update ath_pfam_group set dom_ct=array_length(array_agg, 1);

/* pfam_dom_auto are the domains which are joined using update command and not manually did */
alter table ath_gene_fam_1 drop if exists pfam_dom_auto, drop if exists pfam_len, drop if exists dom_ct, drop if exists tot_dom;
alter table ath_gene_fam_1 add  pfam_dom_auto varchar(200), add dom_ct int, add tot_dom varchar(10);
update ath_gene_fam_1 as a set pfam_dom_auto=b.array_agg from ath_pfam_group as b where upper(a.loc_tag)=upper(b.loc);
update ath_gene_fam_1 as a set dom_ct=b.dom_ct from ath_pfam_group as b where upper(a.loc_tag)=upper(b.loc);
drop table if exists temp;
create table temp as select distinct gene_fam from ath_gene_fam_1 where dom_ct > 1;
update ath_gene_fam_1 as a set tot_dom='multiple' from temp as b where a.gene_fam=b.gene_fam;
update ath_gene_fam_1 set tot_dom='single' where tot_dom is null;
*/

/*
/*curated gene families */
alter table ath_gene_fam_1 drop if exists gene_fam_2;
alter table ath_gene_fam_1 add gene_fam_2 varchar(100);
update ath_gene_fam_1 set gene_fam_2=gene_fam;
/* https://www.ncbi.nlm.nih.gov/pubmed/27704746 */
update ath_gene_fam_1 set gene_fam_2='AS2/LOB gene family' where gene_fam_2='Lateral Organ Boundaries Gene Family';
update ath_gene_fam_1 set gene_fam_2='AS2/LOB gene family' where gene_fam_2='AS2 family';
update ath_gene_fam_1 set gene_fam_2='BBR/BPC Transcription Factor Family' where gene_fam_2='BBR/BPC Transcription Factor Family';
update ath_gene_fam_1 set gene_fam_2='BBR/BPC Transcription Factor Family' where gene_fam_2='BBR/BPC-family of GAGA-motif binding transcription factors';
update ath_gene_fam_1 set gene_fam_2='Homeobox Transcription Factor Superfamily' where gene_fam_2='WOX gene family';
update ath_gene_fam_1 set gene_fam_2='Homeobox Transcription Factor Superfamily' where gene_fam_2='Homeobox Transcription Factor Family';
update ath_gene_fam_1 set gene_fam_2='' where loc_tag='At5g16040' and gene_fam_2='14-3-3 proteins superfamily';
update ath_gene_fam_1 set gene_fam_2='AAAP family' where gene_fam_2='Organic Solute Cotransporters' and pfam_dom_auto like '%Aa_trans%';

*/


