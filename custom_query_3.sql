/*
Author: Renesh Bedre
Lab: Dr Mandadi Lab, Texas A&M AgriLife Research
project_id: sugarcane_as
desc: join the sbicolor annotation to alt splice data
*/

drop table if exists sbicolor_gff;
create table sbicolor_gff as select *  from 
dblink('dbname=plant_db user=postgres password=927122', 'select * from sbicolor_gff') as 
t1(chr varchar(30), des  varchar(30), feature  varchar(20), st int, ende int, score  varchar(5), 
strand  varchar(5), frame  varchar(5), attr  varchar(150)) ;
alter table sbicolor_gff add loc varchar(20);
update sbicolor_gff set loc=(regexp_split_to_array((regexp_split_to_array(attr, 'Name='))[2], ';'))[1];

ALTER TABLE as_events_all_merge drop column if exists loc;
alter table  as_events_all_merge add loc varchar(20);
update as_events_all_merge as a set loc=b.loc from sbicolor_gff as b where 
a.st>=b.st and a.st<=b.ende and
a.ende>=b.st and a.ende<=b.ende and 
a.chr=b.chr and b.feature='gene' and a.as_name is not null; 

ALTER TABLE as_events_all_merge drop column if exists anot;
alter table  as_events_all_merge add anot varchar(300);
drop table if exists sbicolor_anot;
create table sbicolor_anot as select *  from 
dblink('dbname=plant_db user=postgres password=927122', 'select locus, at_anot from sbicolor_anot') as 
t1(locus varchar(20), anot varchar(300)) ;
update as_events_all_merge as a set anot=b.anot from sbicolor_anot as b
where a.loc=b.locus and a.as_name is not null;

\copy (select * from as_events_all_merge where as_name is not null) to 'R:\project\sugracane_as\as_events_all_merge.txt';

/* END */