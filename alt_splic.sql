/* Sugarcane alternative splicing project at Dr Mandadi Lab */
/* check sugarcane_as_2.sh file for getting AS from RNA-seq data */
/* how to run 
   module load PostgreSQL/9.6.1-GCCcore-6.3.0-Python-2.7.12-bare
   pg_ctl -o "-p 9999" -D /scratch/user/ren_net/Test/postresql/test.db -l /scratch/user/ren_net/Test/postresql/test.log start
   psql -p 9999 -f alt_splic.sql sugarcane_as
   psql -p 9999 sugarcane_as
*/   
   
create or replace function public.classify_as(as_table varchar(30), as_table_path text)
    RETURNS void as
    $body$
	DECLARE
		res int;
    BEGIN
        RAISE NOTICE 'Performing AS analysis';
        EXECUTE format('drop table if exists %I', as_table);
        EXECUTE format('create table %I (chr varchar(10), src varchar(20), feature varchar(10), 
						st int, ende int, scr varchar(5), str varchar(5), frm varchar(5), 
						attr varchar(8100))', as_table);
        EXECUTE format('COPY %I from $$' || as_table_path || '$$', as_table);
        EXECUTE format('alter table %I add as_type varchar(2250), add as_name varchar(30)', as_table);
        EXECUTE format('update %I set as_type=(regexp_split_to_array(attr, ''"''))[8]', as_table);
		EXECUTE format('update %I set as_name=''ES'' where as_type=''1-2^,0'' or as_type=''0,1-2^'' ',
			as_table);
		EXECUTE format('update %I set as_name=''IR'' where as_type=''0,1^2-'' or as_type=''1^2-,0'' ',
			as_table);	
		EXECUTE format('update %I set as_name=''AD'' where as_type=''1^,2^'' ',	as_table);	
		EXECUTE format('update %I set as_name=''AA'' where as_type=''1-,2-'' ',	as_table);	
		EXECUTE format('select count(*) from %I where  as_name=''ES''; ',	as_table) into res;	
		RAISE NOTICE 'ES for %, %', as_table, res;
		EXECUTE format('select count(*) from %I where  as_name=''IR''; ',	as_table) into res;
		RAISE NOTICE 'IR for %, %', as_table, res;
		EXECUTE format('select count(*) from %I where  as_name=''AA''; ',	as_table) into res;
		RAISE NOTICE 'AA for %, %', as_table, res;
		EXECUTE format('select count(*) from %I where  as_name=''AD''; ',	as_table) into res;
		RAISE NOTICE 'AD for %, %', as_table, res;
		EXECUTE format('select count(*) from %I',	as_table) into res;
		RAISE NOTICE 'Total AS Event %, %', as_table, res;
    END
    $body$ language "plpgsql";

\set pathdir $$'/scratch/user/ren_net/project/sugarcane_as/analysis/astalavista/'$$
select classify_as('d200_c_as',
                        :pathdir || '200d_control_merged_sorted.gtf_astalavista.gtf');
select classify_as('d200_s_as',
                        :pathdir || '200d_stress_merged_sorted.gtf_astalavista.gtf');
select classify_as('d5_c_as',
                        :pathdir || '5d_control_merged_sorted.gtf_astalavista.gtf');
select classify_as('d5_s_as',
                        :pathdir || '5d_stress_merged_sorted.gtf_astalavista.gtf');














/* old analysis without function */
/*

/* create table */
drop table if exists dai200_C_astalavista, dai200_I_astalavista, dai5_C_astalavista, dai5_I_astalavista;
create table dai200_C_astalavista
(
chr varchar(10),
src varchar(20),
feature varchar(10),
st int,
ende int,
scr varchar(5),
str varchar(5),
frm varchar(5),
attr varchar(8100)
);
create table dai200_I_astalavista (like dai200_C_astalavista);
create table dai5_C_astalavista (like dai200_C_astalavista);
create table dai5_I_astalavista (like dai200_C_astalavista);

/* import table */
\copy dai200_C_astalavista from '/Users/renesh/Renesh_Docs/Research/tamu/sugracane_as/cuffmerge/200dai_C_merged_asm/merged_sorted.gtf_astalavista.gtf';
\copy dai200_I_astalavista from '/Users/renesh/Renesh_Docs/Research/tamu/sugracane_as/cuffmerge/200dai_I_merged_asm/merged_sorted.gtf_astalavista.gtf';
\copy dai5_C_astalavista from '/Users/renesh/Renesh_Docs/Research/tamu/sugracane_as/cuffmerge/5dai_C_merged_asm/merged_sorted.gtf_astalavista.gtf';
\copy dai5_I_astalavista from '/Users/renesh/Renesh_Docs/Research/tamu/sugracane_as/cuffmerge/5dai_I_merged_asm/merged_sorted.gtf_astalavista.gtf';

/* operation */
alter table dai200_C_astalavista add as_type varchar(2250);
alter table dai200_I_astalavista add as_type varchar(2250);
alter table dai5_C_astalavista add as_type varchar(2250);
alter table dai5_I_astalavista add as_type varchar(2250);

alter table dai200_C_astalavista add as_name varchar(30);
alter table dai200_I_astalavista add as_name varchar(30);
alter table dai5_C_astalavista add as_name varchar(30);
alter table dai5_I_astalavista add as_name varchar(30);

update dai200_C_astalavista set as_type = (regexp_split_to_array(attr, '"'))[8];
update dai200_I_astalavista set as_type = (regexp_split_to_array(attr, '"'))[8];
update dai5_C_astalavista set as_type = (regexp_split_to_array(attr, '"'))[8];
update dai5_I_astalavista set as_type = (regexp_split_to_array(attr, '"'))[8];

update dai200_C_astalavista set
as_name = 'ES' where as_type='1-2^,0' or as_type='0,1-2^'; /* Exon Skipping */
update dai200_I_astalavista set
as_name = 'ES' where as_type='1-2^,0' or as_type='0,1-2^'; /* Exon Skipping */
update dai5_C_astalavista set
as_name = 'ES' where as_type='1-2^,0' or as_type='0,1-2^'; /* Exon Skipping */
update dai5_I_astalavista set
as_name = 'ES' where as_type='1-2^,0' or as_type='0,1-2^'; /* Exon Skipping */

update dai200_C_astalavista set as_name = 'IR' where as_type='0,1^2-' or as_type='1^2-,0'; /* Intron Retension */
update dai200_I_astalavista set as_name = 'IR' where as_type='0,1^2-' or as_type='1^2-,0'; /* Intron Retension */
update dai5_C_astalavista set as_name = 'IR' where as_type='0,1^2-' or as_type='1^2-,0'; /* Intron Retension */
update dai5_I_astalavista set as_name = 'IR' where as_type='0,1^2-' or as_type='1^2-,0'; /* Intron Retension */

update dai200_C_astalavista set as_name = 'AD' where as_type='1^,2^' ; /* two competitive/alternative donor sites */
update dai200_I_astalavista set as_name = 'AD' where as_type='1^,2^' ; /* two competitive/alternative donor sites */
update dai5_C_astalavista set as_name = 'AD' where as_type='1^,2^' ; /* two competitive/alternative donor sites */
update dai5_I_astalavista set as_name = 'AD' where as_type='1^,2^' ; /* two competitive/alternative donor sites */

update dai200_C_astalavista set as_name = 'AA' where as_type='1-,2-' ; /* two competitive/alternative acceptor sites */
update dai200_I_astalavista set as_name = 'AA' where as_type='1-,2-' ; /* two competitive/alternative acceptor sites */
update dai5_C_astalavista set as_name = 'AA' where as_type='1-,2-' ; /* two competitive/alternative acceptor sites */
update dai5_I_astalavista set as_name = 'AA' where as_type='1-,2-' ; /* two competitive/alternative acceptor sites */

update dai200_C_astalavista set as_name = 'IR1-o-IR2' where as_type='1^2-,3^4-' ; /* IR1 or IR2 */
update dai200_I_astalavista set as_name = 'IR1-o-IR2' where as_type='1^2-,3^4-' ; /* IR1 or IR2 */
update dai5_C_astalavista set as_name = 'IR1-o-IR2' where as_type='1^2-,3^4-' ; /* IR1 or IR2 */
update dai5_I_astalavista set as_name = 'IR1-o-IR2' where as_type='1^2-,3^4-' ; /* IR1 or IR2 */

update dai200_C_astalavista set as_name = 'IR1-a-IR2' where as_type='0,1^2-3^4-' ; /* IR1 and IR2 */
update dai200_I_astalavista set as_name = 'IR1-a-IR2' where as_type='0,1^2-3^4-' ; /* IR1 and IR2 */
update dai5_C_astalavista set as_name = 'IR1-a-IR2' where as_type='0,1^2-3^4-' ; /* IR1 and IR2 */
update dai5_I_astalavista set as_name = 'IR1-a-IR2' where as_type='0,1^2-3^4-' ; /* IR1 and IR2 */

update dai200_C_astalavista set as_name = '5SS-a-3SS' where as_type='1^4-,2^3-' ; /* alt 5 SS and alt 3SS */
update dai200_I_astalavista set as_name = '5SS-a-3SS' where as_type='1^4-,2^3-' ; /* alt 5 SS and alt 3SS */
update dai5_C_astalavista set as_name = '5SS-a-3SS' where as_type='1^4-,2^3-' ; /* alt 5 SS and alt 3SS */
update dai5_I_astalavista set as_name = '5SS-a-3SS' where as_type='1^4-,2^3-' ; /* alt 5 SS and alt 3SS */

update dai200_C_astalavista set as_name = '5SS-o-3SS' where as_type='1^3-,2^4-' ; /* alt 5 SS or alt 3SS */
update dai200_I_astalavista set as_name = '5SS-o-3SS' where as_type='1^3-,2^4-' ; /* alt 5 SS or alt 3SS */
update dai5_C_astalavista set as_name = '5SS-o-3SS' where as_type='1^3-,2^4-' ; /* alt 5 SS or alt 3SS */
update dai5_I_astalavista set as_name = '5SS-o-3SS' where as_type='1^3-,2^4-' ; /* alt 5 SS or alt 3SS */

update dai200_C_astalavista set as_name = '5SS-a-3SS-a-ES' where as_type='1^6-,2^3-4^5-' ; /* alt 5 SS and alt 3SS and ES */
update dai200_I_astalavista set as_name = '5SS-a-3SS-a-ES' where as_type='1^6-,2^3-4^5-' ; /* alt 5 SS and alt 3SS and ES */
update dai5_C_astalavista set as_name = '5SS-a-3SS-a-ES' where as_type='1^6-,2^3-4^5-' ; /* alt 5 SS and alt 3SS and ES */
update dai5_I_astalavista set as_name = '5SS-a-3SS-a-ES' where as_type='1^6-,2^3-4^5-' ; /* alt 5 SS and alt 3SS and ES */

update dai200_C_astalavista set as_name = '5SS-a-3SS-a-ES1-a-ES2' where
as_type='1^8-,2^3-4^5-6^7-' ; /* alt 5 SS and alt 3SS and ES1 and ES2 */
update dai200_I_astalavista set as_name = '5SS-a-3SS-a-ES1-a-ES2' where
as_type='1^8-,2^3-4^5-6^7-' ; /* alt 5 SS and alt 3SS and ES1 and ES2 */
update dai5_C_astalavista set as_name = '5SS-a-3SS-a-ES1-a-ES2' where
as_type='1^8-,2^3-4^5-6^7-' ; /* alt 5 SS and alt 3SS and ES1 and ES2 */
update dai5_I_astalavista set as_name = '5SS-a-3SS-a-ES1-a-ES2' where
as_type='1^8-,2^3-4^5-6^7-' ; /* alt 5 SS and alt 3SS and ES1 and ES2 */

update dai200_C_astalavista set as_name = '5SS-a-3SS-a-ES1-a-ES2-a-ES3' where
as_type='1^8-,2^3-4^5-6^7-9^10-' ; /* alt 5 SS and alt 3SS and ES1 and ES2 and ES3 */
update dai200_I_astalavista set as_name = '5SS-a-3SS-a-ES1-a-ES2-a-ES3' where
as_type='1^8-,2^3-4^5-6^7-9^10-' ; /* alt 5 SS and alt 3SS and ES1 and ES2 and ES3 */
update dai5_C_astalavista set as_name = '5SS-a-3SS-a-ES1-a-ES2-a-ES3' where
as_type='1^8-,2^3-4^5-6^7-9^10-' ; /* alt 5 SS and alt 3SS and ES1 and ES2 and ES3 */
update dai5_I_astalavista set as_name = '5SS-a-3SS-a-ES1-a-ES2-a-ES3' where
as_type='1^8-,2^3-4^5-6^7-9^10-' ; /* alt 5 SS and alt 3SS and ES1 and ES2 and ES3 */
*/

/* END */


