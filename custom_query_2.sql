/*
Author: Renesh Bedre
Lab: Dr MandAAi Lab, Texas A&M AgriLife Research
project_id: sugarcane_as
desc: count for venn diagram
*/

/*
select count(*) from as_events_all_merge where both_g='fc' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fi' and as_name='ES';
select count(*) from as_events_all_merge where both_g='tc' and as_name='ES';
select count(*) from as_events_all_merge where both_g='ti' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fc-fi' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fc-tc' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fc-ti' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fi-tc' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fi-ti' and as_name='ES';
select count(*) from as_events_all_merge where both_g='tc-ti' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fc-fi-tc' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fc-fi-ti' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fc-tc-ti' and as_name='ES';
select count(*) from as_events_all_merge where both_g='fi-tc-ti' and as_name='ES';
select count(*) from as_events_all_merge where both_g='all' and as_name='ES';

select count(*) from as_events_all_merge where both_g='fc' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fi' and as_name='IR';
select count(*) from as_events_all_merge where both_g='tc' and as_name='IR';
select count(*) from as_events_all_merge where both_g='ti' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fc-fi' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fc-tc' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fc-ti' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fi-tc' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fi-ti' and as_name='IR';
select count(*) from as_events_all_merge where both_g='tc-ti' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fc-fi-tc' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fc-fi-ti' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fc-tc-ti' and as_name='IR';
select count(*) from as_events_all_merge where both_g='fi-tc-ti' and as_name='IR';
select count(*) from as_events_all_merge where both_g='all' and as_name='IR';

select count(*) from as_events_all_merge where both_g='fc' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fi' and as_name='AD';
select count(*) from as_events_all_merge where both_g='tc' and as_name='AD';
select count(*) from as_events_all_merge where both_g='ti' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fc-fi' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fc-tc' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fc-ti' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fi-tc' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fi-ti' and as_name='AD';
select count(*) from as_events_all_merge where both_g='tc-ti' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fc-fi-tc' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fc-fi-ti' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fc-tc-ti' and as_name='AD';
select count(*) from as_events_all_merge where both_g='fi-tc-ti' and as_name='AD';
select count(*) from as_events_all_merge where both_g='all' and as_name='AD';

select count(*) from as_events_all_merge where both_g='fc' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fi' and as_name='AA';
select count(*) from as_events_all_merge where both_g='tc' and as_name='AA';
select count(*) from as_events_all_merge where both_g='ti' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fc-fi' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fc-tc' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fc-ti' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fi-tc' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fi-ti' and as_name='AA';
select count(*) from as_events_all_merge where both_g='tc-ti' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fc-fi-tc' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fc-fi-ti' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fc-tc-ti' and as_name='AA';
select count(*) from as_events_all_merge where both_g='fi-tc-ti' and as_name='AA';
select count(*) from as_events_all_merge where both_g='all' and as_name='AA';

select count(*) from as_events_all_merge where both_g='fc' and as_name is null;
select count(*) from as_events_all_merge where both_g='fi' and as_name is null;
select count(*) from as_events_all_merge where both_g='tc' and as_name is null;
select count(*) from as_events_all_merge where both_g='ti' and as_name is null;
select count(*) from as_events_all_merge where both_g='fc-fi' and as_name is null;
select count(*) from as_events_all_merge where both_g='fc-tc' and as_name is null;
select count(*) from as_events_all_merge where both_g='fc-ti' and as_name is null;
select count(*) from as_events_all_merge where both_g='fi-tc' and as_name is null;
select count(*) from as_events_all_merge where both_g='fi-ti' and as_name is null;
select count(*) from as_events_all_merge where both_g='tc-ti' and as_name is null;
select count(*) from as_events_all_merge where both_g='fc-fi-tc' and as_name is null;
select count(*) from as_events_all_merge where both_g='fc-fi-ti' and as_name is null;
select count(*) from as_events_all_merge where both_g='fc-tc-ti' and as_name is null;
select count(*) from as_events_all_merge where both_g='fi-tc-ti' and as_name is null;
select count(*) from as_events_all_merge where both_g='all' and as_name is null;

select count(*) from as_events_all_merge where dai5_g='fc' and as_name='ES';
select count(*) from as_events_all_merge where dai5_g='fi' and as_name='ES';
select count(*) from as_events_all_merge where dai5_g='all' and as_name='ES';

select count(*) from as_events_all_merge where dai5_g='fc' and as_name='IR';
select count(*) from as_events_all_merge where dai5_g='fi' and as_name='IR';
select count(*) from as_events_all_merge where dai5_g='all' and as_name='IR';

select count(*) from as_events_all_merge where dai5_g='fc' and as_name='AD';
select count(*) from as_events_all_merge where dai5_g='fi' and as_name='AD';
select count(*) from as_events_all_merge where dai5_g='all' and as_name='AD';

select count(*) from as_events_all_merge where dai5_g='fc' and as_name='AA';
select count(*) from as_events_all_merge where dai5_g='fi' and as_name='AA';
select count(*) from as_events_all_merge where dai5_g='all' and as_name='AA';

select count(*) from as_events_all_merge where dai5_g='fc' and as_name is null;
select count(*) from as_events_all_merge where dai5_g='fi' and as_name is null;
select count(*) from as_events_all_merge where dai5_g='all' and as_name is null;

select count(*) from as_events_all_merge where dai200_g='tc' and as_name='ES';
select count(*) from as_events_all_merge where dai200_g='ti' and as_name='ES';
select count(*) from as_events_all_merge where dai200_g='all' and as_name='ES';

select count(*) from as_events_all_merge where dai200_g='tc' and as_name='IR';
select count(*) from as_events_all_merge where dai200_g='ti' and as_name='IR';
select count(*) from as_events_all_merge where dai200_g='all' and as_name='IR';

select count(*) from as_events_all_merge where dai200_g='tc' and as_name='AD';
select count(*) from as_events_all_merge where dai200_g='ti' and as_name='AD';
select count(*) from as_events_all_merge where dai200_g='all' and as_name='AD';

select count(*) from as_events_all_merge where dai200_g='tc' and as_name='AA';
select count(*) from as_events_all_merge where dai200_g='ti' and as_name='AA';
select count(*) from as_events_all_merge where dai200_g='all' and as_name='AA';
*/
select count(*) from as_events_all_merge where dai200_g='tc' and as_name is null;
select count(*) from as_events_all_merge where dai200_g='ti' and as_name is null;
select count(*) from as_events_all_merge where dai200_g='all' and as_name is null;

/* END */