/* import custome files */

/* ssr table from spinach */

/*
drop table if exists spinach_ssr;
create table spinach_ssr (id varchar(30),
			ssr_type  varchar(10),
			feature  varchar(10),
			st int,
			ende int,
			score  varchar(5),
			strand  varchar(5),
			frame  varchar(5),
			attr  varchar(1000)
			);

\copy spinach_ssr from '/Users/renesh/Renesh_Docs/Research/tamu/spinach_ssr/data_obt/spinach_genome_seq/genome_assembly/spinach.ssr';
*/

/* import sbicolor annotation */
/*
drop table if exists sbicolor_anot;
create table sbicolor_anot
(
pac_id varchar(10),
locus varchar(20),
trn varchar(20),
pep varchar(30),
pfam varchar(80),
panther varchar(40),
kog varchar(40),
kegg_ec varchar(60),
ko varchar(20),
go_id varchar(200),
at_hit varchar(20),
at_symb varchar(80),
at_anot varchar(300),
os_hit varchar(30),
os_symb varchar(20),
os_anot varchar(200)
);

\copy sbicolor_anot from '/Users/renesh/Renesh_Docs/Research/tamu/sugracane_as/data_obt/genome/Sbicolor/v3.1/annotation/Sbicolor_313_v3.1.annotation_info.txt';
*/

/* import rice goslim */
/*
drop table if exists temp;
create table temp
(
trn varchar(30),
go_id varchar(20),
go_c varchar(5),
go_a varchar(100),
ev varchar(10),
at_id varchar(20)
);

\copy temp from '/Users/renesh/Renesh_Docs/Research/tamu/db/rice/rice_goslim_v7.txt';

alter table temp add gene varchar(20);
update temp set gene = (regexp_split_to_array(trn, '\.'))[1];

drop table if exists rice_goslim;
create table rice_goslim
(
gene varchar(30),
trn varchar(30),
go_id varchar(20),
go_c varchar(5),
go_a varchar(200),
ev varchar(10)
);

insert into rice_goslim (gene, trn, go_id, go_c, go_a, ev)
select gene, trn, go_id, go_c, go_a, ev from temp;
*/

/* import arabidopsis goslim */
/*
drop table if exists temp;
create table temp
(
loc varchar(30),
acc varchar(20),
ob varchar(20),
rel varchar(50),
go_a varchar(200),
go_id varchar(20),
tair_id varchar(20),
go_c varchar(5),
go_a2 varchar(100),
ev varchar(10),
ev_des varchar(100),
ev_with varchar(300),
ref varchar(100),
anot varchar(100),
dat varchar(20)
);

\copy temp from '/Users/renesh/Renesh_Docs/Research/tamu/db/arabidopsis/ATH_GO_GOSLIM.txt';

drop table if exists ath_goslim;
create table ath_goslim
(
gene varchar(30),
trn varchar(20),
go_id varchar(20),
go_c varchar(5),
go_a varchar(200),
ev varchar(10)
);

insert into ath_goslim (gene, trn, go_id, go_c, go_a, ev)
select loc, ob, go_id, go_c, go_a, ev from temp;

drop table if exists temp;
*/

/* ATH Gene Family */
/*
drop table if exists ath_gene_fam;
create table ath_gene_fam
(
gene_fam varchar(80),
sub_fam varchar(160),
gene_name varchar(40),
alt_gene_name varchar(50),
loc varchar(50),
loc_tag varchar(20),
gi varchar(20),
refseq varchar(80),
entrez_id varchar(20),
genebank_id varchar(30),
genebank_prot_id varchar(30),
prot_func varchar(700),
comments varchar(400),
gene_fam_crit varchar(3000),
comn_domain varchar(400),
tair_id varchar(160),
tair_id_lab varchar(30),
tair_ref varchar(200),
pubmed_id varchar(260),
web_url varchar(200),
web_title varchar(200)
);

\copy ath_gene_fam from '/Users/renesh/Renesh_Docs/Research/tamu/db/arabidopsis/ath_gene_fam.csv' with (FORMAT csv, header true, encoding 'windows-1251') ;

drop table if exists ath_gene_fam_group;
create table ath_gene_fam_group as select gene_fam, array_agg(loc_tag) from ath_gene_fam group by gene_fam;

drop table if exists temp;
create table temp as select gene_fam, count(loc_tag) from ath_gene_fam group by gene_fam;

alter table ath_gene_fam_group add count int;
update ath_gene_fam_group as a set count = b.count from temp as b where a.gene_fam=b.gene_fam;

drop table temp;
*/

/* import rice phytozome 11 table */
drop table if exists rice_phyt11;
create table rice_phyt11
(
phyt_id varchar(20),
loc varchar(30),
trn varchar(30),
prot_name varchar(30),
pfam varchar(80),
panther varchar(50),
kog varchar(20),
kegg_ec varchar(40),
ko varchar(20),
go_id varchar(400),
at_hit varchar(20),
at_symb varchar(80),
at_des varchar(200)
);

\copy rice_phyt11 from '/Users/renesh/Renesh_Docs/Research/tamu/db/rice/rice_phyt11.txt';

alter table rice_phyt11 add at_gene_hit varchar(20), add gene_fam varchar(100);
update rice_phyt11 set at_gene_hit = (regexp_split_to_array(at_hit, '\.'))[1];
update rice_phyt11 as a set gene_fam = b.gene_fam from ath_gene_fam as b where upper(a.at_gene_hit) = upper(b.loc_tag);

drop table if exists temp;
create table temp as select distinct loc from rice_phyt11;
alter table temp add gene_fam varchar(100);
update temp as a set gene_fam = b.gene_fam from rice_phyt11 as b where a.loc = b.loc;

drop table if exists rice_phyt11_gene_fam_group;
create table rice_phyt11_gene_fam_group as select gene_fam, array_agg(loc) from temp group by gene_fam;

alter table rice_phyt11_gene_fam_group add loc_len int, add trn_array varchar[], add trn_len int,
add phyt_id_array varchar[], add phyt_id_len int;

drop table if exists temp;
create table temp as select distinct trn from rice_phyt11;
alter table temp add gene_fam varchar(100);
update temp as a set gene_fam = b.gene_fam from rice_phyt11 as b where a.trn = b.trn;

drop table if exists temp2;
create table temp2 as select gene_fam, array_agg(trn) from temp group by gene_fam;

update rice_phyt11_gene_fam_group as a set trn_array = b.array_agg from temp2 as b where a.gene_fam=b.gene_fam;

drop table if exists temp;
create table temp as select distinct phyt_id from rice_phyt11;
alter table temp add gene_fam varchar(100);
update temp as a set gene_fam = b.gene_fam from rice_phyt11 as b where a.phyt_id = b.phyt_id;

drop table if exists temp2;
create table temp2 as select gene_fam, array_agg(phyt_id) from temp group by gene_fam;

update rice_phyt11_gene_fam_group as a set phyt_id_array = b.array_agg from temp2 as b where a.gene_fam=b.gene_fam;

drop table if exists temp2;
drop table if exists temp;

delete from rice_phyt11_gene_fam_group where gene_fam is null;

update rice_phyt11_gene_fam_group set loc_len=array_length(array_agg, 1);
update rice_phyt11_gene_fam_group set trn_len=array_length(trn_array, 1);
update rice_phyt11_gene_fam_group set phyt_id_len=array_length(phyt_id_array, 1);

/*end*/
			
