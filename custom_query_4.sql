/*
Author: Renesh Bedre
Lab: Dr Mandadi Lab, Texas A&M AgriLife Research
project_id: c-1-04
project desc: for chinmay, LSU
desc: comaprative viral genomics and visualization analysis
*/

drop table if exists all_tblastx;
create table all_tblastx 
(
qid varchar(30),
sid varchar(30),
pident double precision,
len int,
mm int,
gp int,
qstart int,
qend int,
sstart int,
send int, 
eval double precision,
bs double precision
);

\copy all_tblastx from 'R:\project\collaboration\chinmay\analysis\all_tblastx.txt';

delete from all_tblastx where qid=sid;

/* END */