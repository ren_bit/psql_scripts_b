/* import gff */

/* beta vulagris (sugar beet gff) */
/*
drop table if exists sbeet_gff;
create table sbeet_gff (chr varchar(30),
			des  varchar(30),
			feature  varchar(10),
			st int,
			ende int,
			score  varchar(5),
			strand  varchar(5),
			frame  varchar(5),
			attr  varchar(50)
			);

\copy sbeet_gff from '/Users/renesh/Renesh_Docs/Research/tamu/spinach_ssr/data_obt/spinach_genome_seq/sugarbeet_chr/validated-genes.SMRT.RefBeet-1.2.csv' with (FORMAT csv, header true) ;
*/

/* Spinacia oleracea(Spinach gff) */
/*
drop table if exists spinach_gff;
create table spinach_gff (chr varchar(30),
			des  varchar(30),
			feature  varchar(20),
			st int,
			ende int,
			score  varchar(5),
			strand  varchar(5),
			frame  varchar(5),
			attr  varchar(100)
			);

\copy spinach_gff from '/Users/renesh/Renesh_Docs/Research/tamu/spinach_ssr/data_obt/spinach_genome_seq/genome_assembly/spinach.csv' with (FORMAT csv, header true) ;
*/

/* Sorghum bicolor (gff) */
drop table if exists sbicolor_gff;
create table sbicolor_gff
(
chr varchar(30),
des  varchar(30),
feature  varchar(20),
st int,
ende int,
score  varchar(5),
strand  varchar(5),
frame  varchar(5),
attr  varchar(150)
);

/*\copy sbicolor_gff from '/Users/renesh/Renesh_Docs/Research/tamu/sugracane_as/data_obt/genome/Sbicolor/v3.1/annotation/Sbicolor_313_v3.1.gene.gff3' with */
\copy sbicolor_gff from 'R:\project\sugracane_as\data_obt\genome\Sbicolor\v3.1\annotation\Sbicolor_313_v3.1.gene.gff3';

/* end */