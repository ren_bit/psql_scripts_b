/*Gene expression analysis in psql using output data from cuffdiff pipeline*/
/* Sorghum bicolor and sugaracane smut project for alternative splicing analysis*/

Drop table if exists dai_5_gene; 
	CREATE TABLE dai_5_gene 
		(test_id varchar(20), 
		gene_id varchar(12), 
		gene varchar(100), 
		locus varchar(24), 
		sample_1 varchar(9), 
		sample_2 varchar(9), 
		status varchar(7), 
		value_1 double precision, 
		value_2 double precision, 
		log2fc varchar(18), 
		test_stat varchar(20), 
		p_value double precision, 
		q_value double precision,  
		signi varchar(5) );

Drop table if exists dai_200_gene; 
	CREATE TABLE dai_200_gene 
		(test_id varchar(20), 
		gene_id varchar(12), 
		gene varchar(100), 
		locus varchar(24), 
		sample_1 varchar(9), 
		sample_2 varchar(9), 
		status varchar(7), 
		value_1 double precision, 
		value_2 double precision, 
		log2fc varchar(18), 
		test_stat varchar(20), 
		p_value double precision, 
		q_value double precision,  
		signi varchar(5) );

Drop table if exists dai_5_isf; 
	CREATE TABLE dai_5_isf 
	(test_id varchar(20), 
	gene_id varchar(12), 
	gene varchar(100), 
	locus varchar(24), 
	sample_1 varchar(9), 
	sample_2 varchar(9), 
	status varchar(7), 
	value_1 double precision, 
	value_2 double precision, 
	log2fc varchar(18), 
	test_stat varchar(20), 
	p_value double precision, 
	q_value double precision,  
	signi varchar(5) );
	
Drop table if exists dai_200_isf; 
	CREATE TABLE dai_200_isf 
	(test_id varchar(20), 
	gene_id varchar(12), 
	gene varchar(100), 
	locus varchar(24), 
	sample_1 varchar(9), 
	sample_2 varchar(9), 
	status varchar(7), 
	value_1 double precision, 
	value_2 double precision, 
	log2fc varchar(18), 
	test_stat varchar(20), 
	p_value double precision, 
	q_value double precision,  
	signi varchar(5) );

\copy dai_5_isf from 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\cuffdiff_out_sb\isoform_exp.csv' with (FORMAT csv, header true) ;
\copy dai_5_gene from 'R:\project\sugarcane_as\hpc_analysis\5d_gene_exp_sbicolor\cuffdiff_out_sb\gene_exp.csv' with (FORMAT csv, header true) ;
\copy dai_200_isf from 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\cuffdiff_out_sb\isoform_exp.csv' with (FORMAT csv, header true) ;
\copy dai_200_gene from 'R:\project\sugarcane_as\hpc_analysis\200d_gene_exp_sbicolor\cuffdiff_out_sb\gene_exp.csv' with (FORMAT csv, header true) ;


/*change the inf to Infinity*/
UPDATE dai_5_gene
	SET log2fc = 'Infinity' 
	where log2fc='inf';
UPDATE dai_5_isf 
	SET log2fc = 'Infinity' 
	where log2fc='inf';
UPDATE dai_200_gene 
	SET log2fc = 'Infinity' 
	where log2fc='inf';
UPDATE dai_200_isf 
	SET log2fc = 'Infinity' 
	where log2fc='inf';


/*change the -inf to -Infinity*/
/*This is becuase float values accepts Infinity in psql*/
UPDATE dai_5_gene SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE dai_5_isf SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE dai_200_gene SET log2fc = '-Infinity' where log2fc='-inf';
UPDATE dai_200_isf SET log2fc = '-Infinity' where log2fc='-inf';


/*convert log2fc from varchar to double*/
ALTER TABLE dai_5_gene ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE dai_5_isf ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE dai_200_gene ALTER COLUMN log2fc type double precision using (log2fc::double precision);
ALTER TABLE dai_200_isf ALTER COLUMN log2fc type double precision using (log2fc::double precision);

/*Add annotation column to genes*/
ALTER TABLE dai_5_gene ADD anot varchar(500);
ALTER TABLE dai_5_isf ADD anot varchar(500);
ALTER TABLE dai_200_gene ADD anot varchar(500);
ALTER TABLE dai_200_isf ADD anot varchar(500);

/*Lil projec-add ncbi ids*/
/*
ALTER TABLE cd_md ADD ncbi_gene_id int;
ALTER TABLE cud_cd ADD ncbi_gene_id int;
ALTER TABLE cud_md ADD ncbi_gene_id int;
ALTER TABLE cud_mud ADD ncbi_gene_id int;
ALTER TABLE mud_md ADD ncbi_gene_id int;
ALTER TABLE nmd_md ADD ncbi_gene_id int;
ALTER TABLE nmud_md ADD ncbi_gene_id int;
ALTER TABLE nmud_mud ADD ncbi_gene_id int;

ALTER TABLE nmd_mud ADD ncbi_gene_id int;
ALTER TABLE nmud_nmd ADD ncbi_gene_id int;
ALTER TABLE cd_mud ADD ncbi_gene_id int;
*/

/*join annotation from sorghum_anot database*/
/*first copy rice_msu_7 table to respective db from plat_db db*/

UPDATE dai_5_gene as a set anot=b.anot from sbicolor_anot as b where a.gene=b.locus;
UPDATE dai_5_isf as a set anot=b.anot from sbicolor_anot as b where a.gene=b.locus;
UPDATE dai_200_gene as a set anot=b.anot from sbicolor_anot as b where a.gene=b.locus;
UPDATE dai_200_isf as a set anot=b.anot from sbicolor_anot as b where a.gene=b.locus;

/* create a table for up and down-regulated genes */
/* criteria log2fc>=2 and q_value <0.05 (or variable) */
drop table if exists dai_5_gene_up;
create table dai_5_gene_up as
	select * from dai_5_gene
	where log2fc>=1 and p_value<=0.05;

drop table if exists dai_5_gene_down;
create table dai_5_gene_down as
	select * from dai_5_gene
	where log2fc<=-1 and p_value<=0.05;
	
drop table if exists dai_5_isf_up;
create table dai_5_isf_up as
	select * from dai_5_isf
	where log2fc>=1 and p_value<=0.05;

drop table if exists dai_5_isf_down;
create table dai_5_isf_down as
	select * from dai_5_isf
	where log2fc<=-1 and p_value<=0.05;

drop table if exists dai_200_gene_up;
create table dai_200_gene_up as
	select * from dai_200_gene
	where log2fc>=1 and p_value<=0.05;

drop table if exists dai_200_gene_down;
create table dai_200_gene_down as
	select * from dai_200_gene
	where log2fc<=-1 and p_value<=0.05;	

drop table if exists dai_200_isf_up;
create table dai_200_isf_up as
	select * from dai_200_isf
	where log2fc>=1 and p_value<=0.05;

drop table if exists dai_200_isf_down;
create table dai_200_isf_down as
	select * from dai_200_isf
	where log2fc<=-1 and p_value<=0.05;	

/*=============================================END===============================================================*/
