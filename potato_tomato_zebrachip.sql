/* Potato tomato zebrachip RNA-seq project */

/*
/* Import tomato ITAG3.0 Annotation */
drop table if exists tomato_itag3_anot;
create table tomato_itag3_anot
    (trn varchar(20),
     gene varchar(20),
     anot varchar(300)
    );
\copy tomato_itag3_anot from 'R:/project/potato_zebrachip/data_obt/genome/tomato/ITAG3.0_proteins.tab';
*/


/* CastleMart (CM) Hot Cold Control Cuffdiff analysis: Dr. Avila group */
drop table if exists cm_cold_hot_control_cdiff_gene;
create table cm_cold_hot_control_cdiff_gene
		(test_id varchar(20),
		gene_id varchar(12),
		gene varchar(200),
		locus varchar(34),
		sample_1 varchar(9),
		sample_2 varchar(9),
		status varchar(7),
		value_1 double precision,
		value_2 double precision,
		log2fc varchar(18),
		test_stat varchar(20),
		p_value double precision,
		q_value double precision,
		signi varchar(5) );
\copy cm_cold_hot_control_cdiff_gene from 'R:/project/potato_zebrachip/analysis/CM_cold_hot_control_cdiff/gene_exp.diff.csv' with csv header delimiter as ',';

drop table if exists cm_control_cold_cdiff_gene;
create table cm_control_cold_cdiff_gene
		(test_id varchar(30),
		gene_id varchar(12),
		gene varchar(200),
		locus varchar(34),
		sample_1 varchar(9),
		sample_2 varchar(9),
		status varchar(7),
		value_1 double precision,
		value_2 double precision,
		log2fc varchar(18),
		test_stat varchar(20),
		p_value double precision,
		q_value double precision,
		signi varchar(5) );
\copy cm_control_cold_cdiff_gene from 'R:/project/potato_zebrachip/analysis/cm_psyllid_cdiff_pairwise/cm_control_cold_cdiff/gene_exp.diff.csv' with csv header delimiter as ',';
/* gene_exp.diff.csv renamed to cm_control_cold_gene_exp.diff.csv */

drop table if exists cm_control_hot_cdiff_gene;
create table cm_control_hot_cdiff_gene
		(test_id varchar(20),
		gene_id varchar(12),
		gene varchar(200),
		locus varchar(34),
		sample_1 varchar(9),
		sample_2 varchar(9),
		status varchar(7),
		value_1 double precision,
		value_2 double precision,
		log2fc varchar(18),
		test_stat varchar(20),
		p_value double precision,
		q_value double precision,
		signi varchar(5) );
\copy cm_control_hot_cdiff_gene from 'R:/project/potato_zebrachip/analysis/cm_psyllid_cdiff_pairwise/cm_control_hot_cdiff/gene_exp.diff.csv' with csv header delimiter as ',';
/* gene_exp.diff.csv renamed to cm_control_hot_gene_exp.diff.csv */

drop table if exists cm_cold_hot_cdiff_gene;
create table cm_cold_hot_cdiff_gene
		(test_id varchar(20),
		gene_id varchar(12),
		gene varchar(200),
		locus varchar(34),
		sample_1 varchar(9),
		sample_2 varchar(9),
		status varchar(7),
		value_1 double precision,
		value_2 double precision,
		log2fc varchar(18),
		test_stat varchar(20),
		p_value double precision,
		q_value double precision,
		signi varchar(5) );
\copy cm_cold_hot_cdiff_gene from 'R:/project/potato_zebrachip/analysis/cm_psyllid_cdiff_pairwise/cm_cold_hot_cdiff/gene_exp.diff.csv' with csv header delimiter as ',';
/* gene_exp.diff.csv renamed to cm_cold_hot_gene_exp.diff.csv */


/* change log2fc format to double precision */
update cm_cold_hot_control_cdiff_gene set log2fc = 'Infinity' where log2fc='inf';
update cm_cold_hot_control_cdiff_gene set log2fc = '-Infinity' where log2fc='-inf';
update cm_control_cold_cdiff_gene set log2fc = 'Infinity' where log2fc='inf';
update cm_control_cold_cdiff_gene set log2fc = '-Infinity' where log2fc='-inf';
update cm_control_hot_cdiff_gene set log2fc = 'Infinity' where log2fc='inf';
update cm_control_hot_cdiff_gene set log2fc = '-Infinity' where log2fc='-inf';
update cm_cold_hot_cdiff_gene set log2fc = 'Infinity' where log2fc='inf';
update cm_cold_hot_cdiff_gene set log2fc = '-Infinity' where log2fc='-inf';
alter table cm_cold_hot_control_cdiff_gene alter column log2fc type double precision using (log2fc::double precision);
alter table cm_control_cold_cdiff_gene alter column log2fc type double precision using (log2fc::double precision);
alter table cm_control_hot_cdiff_gene alter column log2fc type double precision using (log2fc::double precision);
alter table cm_cold_hot_cdiff_gene alter column log2fc type double precision using (log2fc::double precision);



/* Add annotation to cm_cold_hot_control_cdiff_gene diff table */
alter table cm_cold_hot_control_cdiff_gene add anot varchar(300);
alter table cm_control_cold_cdiff_gene add anot varchar(300);
alter table cm_control_hot_cdiff_gene add anot varchar(300);
alter table cm_cold_hot_cdiff_gene add anot varchar(300);
update cm_cold_hot_control_cdiff_gene as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update cm_control_cold_cdiff_gene as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update cm_control_hot_cdiff_gene as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;
update cm_cold_hot_cdiff_gene as a set anot=b.anot from tomato_itag3_anot as b where a.gene=b.gene;


/* export table */
/* Cold vs hot */
\copy (select * from cm_cold_hot_control_cdiff_gene where sample_1='CMC' and sample_2='CMH' and log2fc>=1 and p_value<0.05 and gene not like '%-%' and gene not like '%,%') to 'R:/project/potato_zebrachip/analysis/CM_cold_hot_control_cdiff/cm_cold_hot_cdiff_gene_sign.csv' DELIMITER ',' CSV HEADER;

/* Control vs cold */
\copy (select * from cm_cold_hot_control_cdiff_gene where sample_1='CMC' and sample_2='CMN' and log2fc>=1 and p_value<0.05 and gene not like '%-%' and gene not like '%,%') to 'R:/project/potato_zebrachip/analysis/CM_cold_hot_control_cdiff/cm_control_cold_cdiff_gene_sign.csv' DELIMITER ',' CSV HEADER;

\copy (select * from cm_control_cold_cdiff_gene where log2fc>=1 and p_value<0.05 and gene not like '%-%' and gene not like '%,%') to 'R:/project/potato_zebrachip/analysis/cm_psyllid_cdiff_pairwise/cm_control_cold_cdiff_gene_sign_up.csv' DELIMITER ',' CSV HEADER;
\copy (select * from cm_control_hot_cdiff_gene where log2fc>=1 and p_value<0.05 and gene not like '%-%' and gene not like '%,%') to 'R:/project/potato_zebrachip/analysis/cm_psyllid_cdiff_pairwise/cm_control_hot_cdiff_gene_sign_up.csv' DELIMITER ',' CSV HEADER;
\copy (select * from cm_cold_hot_cdiff_gene where log2fc>=1 and p_value<0.05 and gene not like '%-%' and gene not like '%,%') to 'R:/project/potato_zebrachip/analysis/cm_psyllid_cdiff_pairwise/cm_cold_hot_cdiff_gene_sign_up.csv' DELIMITER ',' CSV HEADER;
\copy (select * from cm_control_cold_cdiff_gene where log2fc<=-1 and p_value<0.05 and gene not like '%-%' and gene not like '%,%') to 'R:/project/potato_zebrachip/analysis/cm_psyllid_cdiff_pairwise/cm_control_cold_cdiff_gene_sign_down.csv' DELIMITER ',' CSV HEADER;
\copy (select * from cm_control_hot_cdiff_gene where log2fc<=-1 and p_value<0.05 and gene not like '%-%' and gene not like '%,%') to 'R:/project/potato_zebrachip/analysis/cm_psyllid_cdiff_pairwise/cm_control_hot_cdiff_gene_sign_down.csv' DELIMITER ',' CSV HEADER;
\copy (select * from cm_cold_hot_cdiff_gene where log2fc<=-1 and p_value<0.05 and gene not like '%-%' and gene not like '%,%') to 'R:/project/potato_zebrachip/analysis/cm_psyllid_cdiff_pairwise/cm_cold_hot_cdiff_gene_sign_down.csv' DELIMITER ',' CSV HEADER;

/* END */
