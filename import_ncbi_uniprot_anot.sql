/* import db annotation */

/*import ncbi annotation  */

drop table if exists ncbi_nr_sept_2016;
create table ncbi_nr_sept_2016 (gi varchar(20),
			source  varchar(40),
			ac  varchar(40),	/* accession number */
			species varchar(180),
			anot varchar(300),
			short  varchar(460),
			swiss_id  varchar(15)
			);

\copy ncbi_nr_sept_2016 from '/Users/renesh/Renesh_Docs/Research/tamu/db/ncbi_nr/ncbi_nr_sept_2016.tab'  ;


/* import uniprot annotation */
drop table if exists uniprot_sept_2016;
create table uniprot_sept_2016 (source  varchar(10),
			id1 varchar(10),
			id2 varchar(20),
			anot varchar(300),
			species varchar(150),
			short  varchar(10)
			);

\copy uniprot_sept_2016 from '/Users/renesh/Renesh_Docs/Research/tamu/db/uniprot/uniprot-taxonomy3A33090_sept_2016.tab' ;

/*end*/
			

